//
//  CreateMessageCustomViewController.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 07/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import SlackTextViewController

class CreateMessageCustomViewController: SLKTextViewController
{
    var view_width = CGFloat()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view_width = self.view.frame.size.width
        self.textInputbar.textView.placeholder = "Your Message"
        self.textInputbar.autoHideRightButton = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - @override existing methods in SLKViewController.
    // It has in built tableView
    
    // MARK: - TableView Datasource and Delegates
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()//.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        headerView.backgroundColor = UIColor.clear
        
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        
        var imgDp = UIImageView()
        var lblName = UILabel()
        var lblCommentStr = UILabel()
        var lblDate = UILabel()
        
        if (cell == nil)
        {
            cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
            cell?.selectionStyle = UITableViewCellSelectionStyle.gray
            cell?.transform = tableView.transform
            
            imgDp = UIImageView.init(frame: CGRect(x: view_width - 60, y: 0, width: 40, height: 40))
            imgDp.backgroundColor = UIColor.white
            imgDp.layer.cornerRadius = imgDp.frame.size.width / 2
            imgDp.layer.borderColor = UIColor.white.cgColor
            imgDp.image = #imageLiteral(resourceName: "avatar")
            imgDp.tag = 1
            
            lblName = UILabel(frame: CGRect(x: imgDp.frame.origin.x + imgDp.frame.size.width + 10 , y: 5, width: self.view.frame.size.width / 2 - 50, height: 30))
            lblName.textAlignment = .left
            lblName.font = UIFont.init(name: Font.BEBAS, size: 14)
            lblName.tag = 2
            
            lblDate = UILabel(frame: CGRect(x: self.view.frame.size.width / 2 , y: 5, width: self.view.frame.size.width / 2 - 10, height: 30))
            lblDate.textAlignment = .right
            lblDate.font = UIFont.init(name: Font.BEBAS, size: 14)
            lblDate.tag = 3
            
            lblCommentStr = UILabel(frame: CGRect(x: lblName.frame.origin.x, y: lblName.frame.origin.y + lblName.frame.size.height, width: self.view.frame.size.width - 50, height: 35))
            lblCommentStr.textAlignment = .left
            lblCommentStr.font = UIFont.init(name: Font.BEBAS, size: 18)
            lblCommentStr.numberOfLines = 0
            lblCommentStr.lineBreakMode = .byWordWrapping
            lblCommentStr.adjustsFontSizeToFitWidth = true
            lblCommentStr.tag = 4
            
            cell?.contentView.addSubview(imgDp)
            cell?.contentView.addSubview(lblName)
            cell?.contentView.addSubview(lblCommentStr)
            cell?.contentView.addSubview(lblDate)
        }
        else
        {
            imgDp = cell?.contentView.viewWithTag(1) as! UIImageView
            lblName = cell?.contentView.viewWithTag(2) as! UILabel
            lblDate = cell?.contentView.viewWithTag(3) as! UILabel
            lblCommentStr = cell?.contentView.viewWithTag(4) as! UILabel
        }
        
//        let commentJSON = arrComments[indexPath.row] as! [String : Any]
        
        // TODO: - REmove after getting thumb in API
        /*
         let pictureURL : String = commentJSON["thumb"] as! String
         if pictureURL != ""
         {
         let url = URL(string: APP_URL.BASE_URL + pictureURL )!
         let placeholderImage = #imageLiteral(resourceName: "avatar")
         let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
         
         //,filter: filter
         imgDp.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
         { response in
         }
         )
         }
         */
//        let firstName : String = commentJSON["fname"] as! String
//        let lastName : String = commentJSON["lname"] as! String
       
        lblCommentStr.text = "Test Name"
        
        lblDate.text = "Temp Message"
        
        return cell!
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : CreateMessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CreateMessageTableViewCell") as! CreateMessageTableViewCell
        
        cell.lblUserName.text = "Test Name"
        cell.lblMessage.text = "Temp Message"
        
        return cell
    }
*/
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
