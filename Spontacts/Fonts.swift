//
//  Fonts.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 03/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

struct Font
{
    static let BEBAS = "AppleSDGothicNeo-Regular"
    
    static let BEBAS_Bold = "AppleSDGothicNeo-Bold"
    
    static let BEBAS_SemiBold = "AppleSDGothicNeo-Semibold"
    
    static let BebasNeue = "BebasNeue"
    
    static let bigNoodleOblique = "big_noodle_titling_oblique"
    
    static let bigNoodle = "big_noodle_titling"
}

class Fonts: NSObject {

}
