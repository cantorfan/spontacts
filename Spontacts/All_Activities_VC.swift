//
//  All_Activities_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 13/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import GoogleMobileAds

class All_Activities_VC: UIViewController, UITableViewDataSource, UITableViewDelegate, SWRevealViewControllerDelegate
{

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var butnAdd: UIButton!
    @IBOutlet weak var butnHeart: UIButton!
    
    
    let sharedObj = Singleton.shared
    var selectedSeg = Int()
    
    var defaultDateFormatter = DateFormatter()
    
    var nearbyActivityArray = NSMutableArray()
    var interestActivityArray = NSMutableArray()

    var activityParaToSend = Para_Activity()
    var userParaToSend = Para_UserProfile()
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var lblSortedBy: UILabel!

    var timer: Timer?
    var isFromHomePage = false
    var refreshControl = UIRefreshControl()

    // Filter parameters
    //var latitude = Double()
    //var longitude = Double()
    //var sort_by = 0
    /* "DATE", 0
     "REVALENCE", 1
     "chronologically", 2
     */
    //var from_date = 0.0
    //var to_date = 0.0
    //var public_or_friends_activities = 1
    //var for_categories = [Int]()
    //var distance = 0
    //var search_keyword = ""
    //var sortedByStr = ""
    
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = AdMob_BannerID
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        
        return adBannerView
    }()

    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "Activities"
        self.navigationController?.setNavigationBarHidden(false, animated: true)

        defaultDateFormatter.dateFormat = "dd/MM/YYYY"
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "")
        self.refreshControl.tintColor = UIColor.lightGray
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.tableView?.addSubview(refreshControl)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationInterestsChosen(noti:)), name: NSNotification.Name(rawValue: "Interest_Chosen"), object: nil)
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.animate), userInfo: nil, repeats: true)
        
        adBannerView.load(GADRequest())
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            self.callReadStatusAPI()
        }
    }

    func setFilterLbl()
    {
        var arrTextToAppend = [String]()
        
        if sharedObj.para_filter.sortbyStr != ""
        {
            arrTextToAppend.append(sharedObj.para_filter.sortbyStr)
        }
        
        if sharedObj.para_filter.categories_array.count != 0
        {
            for catid in sharedObj.para_filter.categories_array
            {
                arrTextToAppend.append(sharedObj.arrCat[catid])
            }
        }
        
        if sharedObj.para_filter.timeStr != ""
        {
            arrTextToAppend.append(sharedObj.para_filter.timeStr)
        }
        
        if sharedObj.userPara.userLocationStr != sharedObj.para_filter.locationStr
        {
            arrTextToAppend.append(sharedObj.para_filter.locationStr)
        }
        
        if sharedObj.para_filter.search_keyword != ""
        {
            let str = "Contains : " + sharedObj.para_filter.search_keyword
            arrTextToAppend.append(str)
        }
        
        if sharedObj.para_filter.visibleTo == 0
        {
            let str = "Visible to : My Friends"
            arrTextToAppend.append(str)
        }

        if arrTextToAppend.count == 1
        {
            lblSortedBy.text = "Sorted by : All"
        }
        else
        {
            lblSortedBy.text = "Sorted by : " + arrTextToAppend.joined(separator: ", ")
        }
    }
    
    func refresh(sender:AnyObject)
    {
        view.endEditing(true)
        
        self.callFetchAllActivitiesAPI()
    }
    
    func notificationInterestsChosen(noti : NSNotification)
    {
        let receivedObj : [String : Any] = noti.object as! [String : Any]
        let array = receivedObj["array"] as! NSArray
        let intArray = array as! [Int]
        
        self.callFetchMyInterestActivityAPI(catArray: intArray)
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        addLeftBarButton()
        addRightBarButton()

        setFilterLbl()
        
        // Apply filter only on All activities
        // Dont call this on my interest
        if selectedSeg == 0
        {
            callFetchAllActivitiesAPI()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftBarButton()
    {
        if isFromHomePage == true
        {
            let btnBack = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.didTapBackButn))
            navigationItem.leftBarButtonItem = btnBack
        }
        else
        {
            let customView = UIView.init(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            button.setImage(#imageLiteral(resourceName: "menu_white"), for: .normal)
            button.tintColor = UIColor.white
            button.addTarget(self, action: #selector(self.didTapMenuButn), for: .touchUpInside)
            
            let label = UILabel.init(frame: CGRect(x: 30, y: 5, width: 15, height: 15))
            label.backgroundColor = UIColor.white
            label.layer.cornerRadius = label.frame.size.width / 2
            label.clipsToBounds =  true
            
            customView.addSubview(button)
            
            if sharedObj.unread_All_Activities_Count != 0
            {
                customView.addSubview(label)
            }
            navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: customView) // customView
            
            if revealViewController() != nil
            {
                revealViewController().rightViewRevealWidth = 200
                
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            }
        }
    }
    
    func addRightBarButton()
    {
        let sortButn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "sort"), style: .plain, target: self, action: #selector(self.didTapSortButn))
        let filterButn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "filter"), style: .plain, target: self, action: #selector(self.didTapFilterButn))
        
        navigationItem.rightBarButtonItems = [filterButn, sortButn]
    }

    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in

        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func animate()
    {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations:
            {
                self.butnHeart.alpha = 0.2
                self.butnAdd.alpha = 0.2
        },
                       completion:
            {
                (finished: Bool) -> Void in
                
                // Fade in
                UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations:
                    {
                        self.butnHeart.alpha = 1.0
                        self.butnAdd.alpha = 1.0
                        
                }, completion: nil)
        })
    }

    // MARK: - Action Methods
    @IBAction func didTapBackButn()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapSortButn()
    {
        let alert = UIAlertController.init(title: "Sort by", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
            alert -> Void in
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "Date", style: .default, handler: {
            alert -> Void in
            
            self.sharedObj.para_filter.sortbyStr = "Date"
            self.sharedObj.para_filter.sort_by = 0
            self.setFilterLbl()
            self.callFetchAllActivitiesAPI()
            self.tableView.reloadData()
        }))
        alert.addAction(UIAlertAction.init(title: "Relevance", style: .default, handler: {
            alert -> Void in
            
            self.sharedObj.para_filter.sortbyStr = "Relevance"
            self.sharedObj.para_filter.sort_by = 1
            self.setFilterLbl()
            self.callFetchAllActivitiesAPI()
            self.tableView.reloadData()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didTapFilterButn()
    {
        performSegue(withIdentifier: "segueAllActivityToFilterView", sender: nil)
    }
    
    @IBAction func didTapMenuButn()
    {
        revealViewController().revealToggle(animated: true)
    }

    @IBAction func didChangeSegmentedControl(sender : UISegmentedControl)
    {
        if sender.selectedSegmentIndex == 1
        {
            // Here add condition to check if user has set interests
            if UserDefaults.standard.value(forKey: "interest") != nil
            {
                let array : [Int] = UserDefaults.standard.value(forKey: "interest") as! [Int]
                if array.count == 0
                {
                    showChooseInterestPopup()
                }
                else
                {
                    callFetchMyInterestActivityAPI(catArray: array)
                }
            }
            else
            {
                showChooseInterestPopup()
            }
        }
        else
        {
            selectedSeg = 0
            tableView.reloadData()
        }
    }

    @IBAction func btnActionAdd(_ sender: Any)
    {
        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAct_MandatoryInfo") as! CreateAct_MandatoryInfo
        self.navigationController?.pushViewController(presentVC, animated: true)
    }
    
    @IBAction func btnActionHeart(_ sender: Any)
    {
        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "DonationViewController") as! DonationViewController
        self.navigationController?.pushViewController(presentVC, animated: true)
    }
    
    // MARK: - Logical Methods
    func callFetchAllActivitiesAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Loading Activities...")
        loader.startAnimating()

        let headerObject = ["authtoken" : sharedObj.userPara.userToken]

        let requestObject : [String : Any] = ["lat": sharedObj.para_filter.latitude,
                             "lng": sharedObj.para_filter.longitude,
                             "sort_by": sharedObj.para_filter.sort_by,
                             "from_date": sharedObj.para_filter.from_date,
                             "to_date": sharedObj.para_filter.to_date,
                             "public_or_friends_activities": sharedObj.para_filter.visibleTo,
                             "for_subcategories": sharedObj.para_filter.categories_array,
                             "distance": sharedObj.para_filter.distance,
                             "search_keyword": sharedObj.para_filter.search_keyword] as [String : Any]

        print(requestObject)
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.GET_ALL_ACT_LIST, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if self.refreshControl.isRefreshing
                {
                    self.refreshControl.endRefreshing()
                }
                
                if isSuccess
                {
                    if responseData["nearby_activities"] != nil
                    {
                        let array = responseData["nearby_activities"] as! NSArray
                        print("activities fetched ", array.count)
                        self.nearbyActivityArray = array.mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func callFetchMyInterestActivityAPI(catArray : [Int])
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Loading Activities")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject : [String : Any] = ["lat": sharedObj.para_filter.latitude,
                                              "lng": sharedObj.para_filter.longitude,
                                              "sort_by": sharedObj.para_filter.sort_by,
                                              "from_date": sharedObj.para_filter.from_date,
                                              "to_date": sharedObj.para_filter.to_date,
                                              "public_or_friends_activities": sharedObj.para_filter.visibleTo,
                                              "for_subcategories": catArray,
                                              "distance": sharedObj.para_filter.distance,
                                              "search_keyword": sharedObj.para_filter.search_keyword] as [String : Any]
        print(requestObject)
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.GET_ALL_ACT_LIST, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["nearby_activities"] != nil
                    {
                        let array = responseData["nearby_activities"] as! NSArray
                        print("activities fetched ", array.count)
                        
                        self.selectedSeg = 1
                        self.interestActivityArray = array.mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func callFetchAttendees_InterestedAPI(actId : Int)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Loading Activities")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject : [String : Any] = ["activity_id": actId] as [String : Any]
        
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.GET_ACT_GOING_INTERESTED_COMMENTS_LIST, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    if responseData["going"] != nil
                    {
                        let array = responseData["going"] as! [[String:Any]]
                        self.activityParaToSend.actGoingArray = array
                    }
                    if responseData["interested"] != nil
                    {
                        let array = responseData["interested"] as! [[String:Any]]
                        self.activityParaToSend.actInterstedArray = array
                    }
                    if responseData["comments"] != nil
                    {
                        let array = responseData["comments"] as! NSArray
                        self.activityParaToSend.actCommentsArray = array.mutableCopy() as! NSMutableArray
                    }
                    
                    print(self.activityParaToSend.actGoingArray.count, self.activityParaToSend.actInterstedArray.count)
                }
                else
                {
                    //self.showAlert(title: "Fail!", msg: responseMessage)
                }
                
                self.performSegue(withIdentifier: "segueAllActivityToShowActivity", sender: nil)
        })
    }
    
    func showChooseInterestPopup()
    {
        let alert = UIAlertController.init(title: "Tailored to you", message: "Choose your interests and you'll only see your favourite activities.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Choose your interests", style: .default, handler:
            { alert -> Void in
            
                self.selectedSeg = 1
                
                let catVC : InterestsCategoriesViewController = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "InterestsCategoriesViewController") as! InterestsCategoriesViewController
                
                if UserDefaults.standard.value(forKey: "interest") != nil
                {
                    let array : [Int] = UserDefaults.standard.value(forKey: "interest") as! [Int]
                    catVC.selectedCatArray = array //as! NSMutableArray
                }
                
                self.navigationController?.pushViewController(catVC, animated: true)
            }))
        alert.addAction(UIAlertAction.init(title: "Later", style: .cancel, handler:
            { alert -> Void in
                self.selectedSeg = 0
                self.segmentControl.selectedSegmentIndex = 0
                self.tableView.reloadData()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func callReadStatusAPI()
    {
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["my_activities":0,
                             "joined_activities":1,
                             "recomendations":0,
                             "profile_visitors":0,
                             "friends":0]
        
        GlobalConstants().postData_Header_Parameter(subUrl: REFRESH_STATUS.READ_NOTIFICATION, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                self.sharedObj.unread_All_Activities_Count = 0
                print(responseData)
                
                if isSuccess
                {
                    print("Read notifications")
                }
                else
                {
                    print("Error in reading status")
                }
        })
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if selectedSeg == 0
        {
            if self.nearbyActivityArray.count == 0
            {
                let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                emptyLabel.text = "No Activities Found With Selected Filter"
                emptyLabel.textColor = UIColor.lightGray
                emptyLabel.textAlignment = NSTextAlignment.center
                self.tableView.backgroundView = emptyLabel
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                return 0
            }
            else
            {
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                self.tableView.backgroundView?.isHidden = true
                return self.nearbyActivityArray.count
            }
        }
        else //if selectedSeg == 1
        {
            if self.interestActivityArray.count == 0
            {
                let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                emptyLabel.text = "No Activities Found With Your Interests"
                emptyLabel.textColor = UIColor.lightGray
                emptyLabel.textAlignment = NSTextAlignment.center
                self.tableView.backgroundView = emptyLabel
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                return 0
            }
            else
            {
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                self.tableView.backgroundView?.isHidden = true
                return self.interestActivityArray.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : ActivityListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ActivityListTableViewCell", for: indexPath) as! ActivityListTableViewCell
        
        var activityJSON = [String:Any]()
        
        if selectedSeg == 0
        {
            activityJSON = nearbyActivityArray[indexPath.row] as! [String : Any]
        }
        else
        {
            activityJSON = interestActivityArray[indexPath.row] as! [String : Any]
        }
        
        //print(activityJSON)
        let catId = activityJSON["category_id"] as! Int
        if sharedObj.arrCatImageCropped.count > catId
        {
            let actCatUrl = activityJSON["category_picture_thumb"] as! String
            if catId == 8 && actCatUrl != ""
            {
                cell.imgActivity.af_setImage(withURL: URL.init(string: APP_URL.BASE_URL + actCatUrl)!, placeholderImage: #imageLiteral(resourceName: "image_placeolder"), completion: nil)
                cell.imgActivity.clipsToBounds = true
            }
            else
            {
                cell.imgActivity.image = sharedObj.arrCatImageCropped[catId]
                cell.imgActivity.clipsToBounds = true
            }
        }
        
        cell.lblActName.text = activityJSON["name"] as? String
        let timeInterval = activityJSON["time"] as? Int
        let date = Date.init(timeIntervalSince1970: TimeInterval(timeInterval!))
        cell.lblDate.text = defaultDateFormatter.string(from: date)
        
        cell.lblAttendees.text = "\(activityJSON["going_count"] as! Int)" //"1"
        cell.lblComments.text = "\(activityJSON["comments_count"] as! Int)" //"0"
        
        let distanceInt = Int(activityJSON["distance"] as! NSNumber)
        cell.lblLocation.text = (activityJSON["location_string"] as? String)! + " (\(distanceInt) km)"
        
        let organiserJSON = activityJSON["organiser"] as! [String : Any]
        let fname = organiserJSON["fname"] as! String
        
        cell.lblOrganising.text = fname
        
        if activityJSON["updated"] as! Int == 1
        {
            cell.lblUpdated.isHidden = false
        }
        else
        {
            cell.lblUpdated.isHidden = true
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var JSON = [String:Any]()
        
        if selectedSeg == 0
        {
            JSON = nearbyActivityArray[indexPath.row] as! [String : Any]
        }
        else
        {
            JSON = interestActivityArray[indexPath.row] as! [String : Any]
        }
        
        activityParaToSend = Para_Activity()
        
        activityParaToSend.actLocatnLongitude = JSON["lng"] as! Double
        activityParaToSend.actIsFlexible = (JSON["time_flexible"] != nil)
        let timeInterval = JSON["time"] as? Int
        activityParaToSend.actDate = Date.init(timeIntervalSince1970: Double(timeInterval!))
        activityParaToSend.actId = JSON["id"] as! Int
        activityParaToSend.actVisibleTo = JSON["visibility"] as! Int
        activityParaToSend.actDesc = JSON["description"] as! String
        activityParaToSend.actLocatnLatitude = JSON["lat"] as! Double
        activityParaToSend.actName = JSON["name"] as! String
        activityParaToSend.actCreatedEpoch = JSON["created_time"] as! Double
        activityParaToSend.actCategoryId = JSON["category_id"] as! Int
        activityParaToSend.actLocatnStr = JSON["location_string"] as! String
        activityParaToSend.actImgJSONArr = JSON["images"] as! NSArray
        activityParaToSend.isInterested = JSON["interested"] as! Int == 1 ? true : false //(JSON["interested"] != nil)
        activityParaToSend.isGoing = JSON["going"] as! Int == 1 ? true : false //(JSON["going"] != nil)
        activityParaToSend.actCommentsCount = JSON["comments_count"] as! Int
        activityParaToSend.actLocatnDistance = Int(JSON["distance"] as! NSNumber)
        activityParaToSend.actCategoryImgURL = JSON["category_picture"] as! String
        
        userParaToSend = Para_UserProfile()
        let organiserJSON = JSON["organiser"] as! [String : Any]
        userParaToSend.userFirstName = organiserJSON["fname"] as! String
        userParaToSend.userLastName = organiserJSON["lname"] as! String
        userParaToSend.userEmail = organiserJSON["email"] as! String
        userParaToSend.userLocationLat = organiserJSON["lat"] as! Double
        userParaToSend.userLocationLong = organiserJSON["lng"] as! Double
        userParaToSend.userId = organiserJSON["id"] as! Int
        userParaToSend.userPictureThumbUrl = organiserJSON["thumb"] as! String
        userParaToSend.isPlusMember = organiserJSON["suscribed"] as! Int
        
        callFetchAttendees_InterestedAPI(actId: activityParaToSend.actId)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueAllActivityToShowActivity"
        {
            let showActivity : ShowActivity_VC = segue.destination as! ShowActivity_VC
            showActivity.userPara = userParaToSend
            showActivity.activityPara = activityParaToSend
        }
    }
    
    
    
    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }
}

extension All_Activities_VC : GADBannerViewDelegate
{
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        if sharedObj.userPara.isPlusMember == 1
        {
            print("No ads to plus user") 
        }
        else
        {
            print("Banner loaded successfully")
            tableView.tableHeaderView?.frame = bannerView.frame
            tableView.tableHeaderView = bannerView
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }
}
