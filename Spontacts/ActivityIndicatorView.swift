//
//  ActivityIndicatorView.swift
//  Concur App
//
//  Created by NanoStuffs on 01/10/15.
//  Copyright (c) 2015 Nanostuffs. All rights reserved.
//

import UIKit
import Foundation

func startActivityIndicator(view:UIView, waitingMessage:String)->ActivityIndicatorView
{
    var activityIndicatorView: ActivityIndicatorView!
    
    activityIndicatorView = ActivityIndicatorView(title: waitingMessage, center: view.center)
    
    view.addSubview(activityIndicatorView.getViewActivityIndicator())
    
    return activityIndicatorView
}

func stopActivityIndicator(view:UIView)
{
    var activityIndicatorView: ActivityIndicatorView!
    
    activityIndicatorView = ActivityIndicatorView(title: "", center: view.center)
    
    activityIndicatorView.stopAnimating()
    
   // view.willRemoveSubview(activityIndicatorView.getViewActivityIndicator())
}

class ActivityIndicatorView
{
    var view: UIView!
    
    var activityIndicator: UIActivityIndicatorView!
    
    var title: String!
    
    init(title: String, center: CGPoint, width: CGFloat = 250.0, height: CGFloat = 50.0)
    {
        self.title = title
        
        let x = center.x - width/2.0
        let y = center.y - height/2.0
        
        self.view = UIView(frame: CGRect(x: x, y: y, width: width, height: height))
        self.view.backgroundColor = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1.0)
        self.view.layer.cornerRadius = 10
        
        self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = #colorLiteral(red: 0.8951961398, green: 0.3051324189, blue: 0.1987941265, alpha: 1)//UIColor.green
        self.activityIndicator.hidesWhenStopped = false
        
        let titleLabel = UILabel(frame: CGRect(x: 30, y: 10, width: 200, height: 30))
        titleLabel.font = UIFont.init(name: Font.BEBAS_SemiBold, size: 18)
        titleLabel.textColor = UIColor.black
        titleLabel.textAlignment = .center
        titleLabel.text = title
        
        self.view.addSubview(self.activityIndicator)
        self.view.addSubview(titleLabel)
    }
    
    func getViewActivityIndicator() -> UIView
    {
        return self.view
    }
    
    func startAnimating()
    {
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopAnimating()
    {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        self.view.removeFromSuperview()
    }
}
