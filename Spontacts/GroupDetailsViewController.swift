//
//  GroupDetailsViewController.swift
//  Spontacts
//
//  Created by maximess142 on 22/10/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class GroupDetailsViewController: UIViewController
{
    
    @IBOutlet weak var imgViewGroupPic: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var imgViewAdmin: UIImageView!
    @IBOutlet weak var lblAdminName: UILabel!
    
    var groupDetails = [String:Any]()
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Group Details"
        self.addLeftBarbutn()
        self.assignValueToLabels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftBarbutn()
    {
        let leftBarbUtn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .done, target: self, action: #selector(self.didTapLeftBarButn))
        self.navigationItem.leftBarButtonItem = leftBarbUtn
    }
    
    func didTapLeftBarButn()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func assignValueToLabels()
    {
        /*
         "group_id": 7,
         "admin_id": 3,
         "description": "cgfgf",
         "name": "vhvhvh",
         "admin_qbid": 676767,
         "picture": "uploads/721465d981b58f68c2140542bef319f0.jpg",
         "thumb": "uploads/thumb/721465d981b58f68c2140542bef319f0.jpg",
         "group_qbid": "gjgfjhjj",
         "created_date": 6767676767,
         "admin": {
         "fname": "dhiraj",
         "picture": "",
         "thumb": ""
         }
         */
        self.lblName.text = groupDetails["name"] as? String
        self.lblDescription.text = groupDetails["description"] as? String
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd MMM yyyy"
        let timeInterval = groupDetails["created_date"] as? Double
        self.lblCreatedDate.text = dateformatter.string(from: Date.init(timeIntervalSince1970: TimeInterval(timeInterval!)))
        
        let pictureUrl = groupDetails["picture"] as! String
        if pictureUrl != ""
        {
            self.imgViewGroupPic.af_setImage(for: .normal, url: URL.init(string: APP_URL.BASE_URL + pictureUrl)!)
        }
        
        let adminJSOn = groupDetails["admin"] as! [String : Any]
        self.lblAdminName.text = adminJSOn["fname"] as? String
        let adminPicUrl = adminJSOn["thumb"] as! String
        if adminPicUrl != ""
        {
            self.imgViewAdmin.af_setImage(withURL: URL.init(string: APP_URL.BASE_URL + adminPicUrl)!)
        }
    }
    
    //MARK: - IBOutlets
    
    @IBAction func didTapAdminName(_ sender: Any)
    {
        let presentVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "UserProfile_VC") as! UserProfile_VC
        
        let adminId = groupDetails["admin_id"] as! Int
        presentVC.user_Id = adminId
        // Will allow to go back
        self.navigationController?.pushViewController(presentVC, animated: true)
    }
    
}
