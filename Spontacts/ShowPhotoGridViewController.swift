//
//  ShowPhotoGridViewController.swift
//  Spontacts
//
//  Created by maximess142 on 12/10/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class ShowPhotoGridViewController: UIViewController {

    @IBOutlet weak var butnPicture1: UIButton!
    @IBOutlet weak var butnPicture2: UIButton!
    @IBOutlet weak var butnPicture3: UIButton!
    @IBOutlet weak var butnPicture4: UIButton!
   
    
    @IBOutlet weak var butnAddPlus: UIButton!
    @IBOutlet weak var butnEdit1: UIButton!
    @IBOutlet weak var butnEdit2: UIButton!
    @IBOutlet weak var butnEdit3: UIButton!
    @IBOutlet weak var butnDone: UIButton!
    @IBOutlet weak var butnEdit4: UIButton!
    
    
    var dataArray = [[String:Any]]()
    var indexToEdit = Int()
    var isFriendProfile = Bool()
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if isFriendProfile == true
        {
            self.butnAddPlus.isHidden = true
            self.butnEdit1.isHidden = true
            self.butnEdit2.isHidden = true
            self.butnEdit3.isHidden = true
            self.butnEdit4.isHidden = true
            self.butnDone.isHidden = true
        }
        assignImagesToButton()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func didTapPhotoButn(_ sender: UIButton)
    {
        indexToEdit = (sender.tag / 100) - 1

        let dictToSend = ["index" : indexToEdit] as [String : Any]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "viewImageAtIndex"), object: nil, userInfo: dictToSend)
        
        if isFriendProfile == true
        {
            let viewPicVC = storyboards.profilePicStoryboard.instantiateViewController(withIdentifier: "ViewProfilePhotoViewController") as! ViewProfilePhotoViewController
            //viewPicVC.dataArray = self.dataArray
            viewPicVC.isFriendProfile = true
            self.navigationController?.pushViewController(viewPicVC, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func didTapBackButn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapAddButn(_ sender: UIButton)
    {
        indexToEdit = (sender.tag / 1000) - 1
        
        let alert = UIAlertController.init(title: "Choose from", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: {
            alert -> Void in
            
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: {
            alert -> Void in
            
            
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didTapBigAddButn(_ sender: Any)
    {
        let upgradeVC : UpgradeViewController = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "UpgradeViewController") as! UpgradeViewController
        
        self.navigationController?.pushViewController(upgradeVC, animated: true)
    }
    
    @IBAction func didTapDoneButn(_ sender: Any)
    {
        if dataArray.count > 0
        {
            self.callUploadPhotosAPI()
        }
        else
        {
            showLandingPage()
        }
    }
    
    
    // MARK: - Custom Methods
    func assignImagesToButton()
    {
        if dataArray.count > 0
        {
            if isContentOfTypeImage(index: 0) == true
            {
                let image = dataArray[0]["image"] as! UIImage
                self.butnPicture1.setImage(image, for: .normal)
            }
            else
            {
                let url = dataArray[0]["picture"] as! String
                if url != ""
                {
                  
                   self.butnPicture1.setImage(nil, for: .normal)
                    self.butnPicture1.af_setImage(for: .normal, url: URL.init(string: APP_URL.BASE_URL + url)!, placeholderImage: #imageLiteral(resourceName: "image_placeolder"), filter: nil, progress: nil, completion: nil)
                }
            }
        }
        if dataArray.count > 1
        {
            if isContentOfTypeImage(index: 1) == true
            {
                let image = dataArray[1]["image"] as! UIImage
                self.butnPicture2.setImage(image, for: .normal)
            }
            else
            {
                let url = dataArray[1]["picture"] as! String
                if url != ""
                {
                    self.butnPicture2.setImage(nil, for: .normal)
                    self.butnPicture2.af_setImage(for: .normal, url: URL.init(string: APP_URL.BASE_URL + url)!, placeholderImage: #imageLiteral(resourceName: "image_placeolder"), filter: nil, progress: nil, completion: nil)
                }
            }
        }
        if dataArray.count > 2
        {
            if isContentOfTypeImage(index: 2) == true
            {
                let image = dataArray[2]["image"] as! UIImage
                self.butnPicture3.setImage(image, for: .normal)
            }
            else
            {
                let url = dataArray[2]["picture"] as! String
                if url != ""
                {
                    self.butnPicture3.setImage(nil, for: .normal)
                    self.butnPicture3.af_setImage(for: .normal, url: URL.init(string: APP_URL.BASE_URL + url)!, placeholderImage: #imageLiteral(resourceName: "image_placeolder"), filter: nil, progress: nil, completion: nil)
                }
            }
        }
        if dataArray.count > 3
        {
            if isContentOfTypeImage(index: 3) == true
            {
                let image = dataArray[3]["image"] as! UIImage
                self.butnPicture4.setImage(image, for: .normal)
            }
            else
            {
                let url = dataArray[3]["picture"] as! String
                if url != ""
                {
                    self.butnPicture4.setImage(nil, for: .normal)
                    self.butnPicture4.af_setImage(for: .normal, url: URL.init(string: APP_URL.BASE_URL + url)!, placeholderImage: #imageLiteral(resourceName: "image_placeolder"), filter: nil, progress: nil, completion: nil)
                }
            }
        }
        
        self.view.layoutIfNeeded()
    }
    
    func isContentOfTypeImage(index : Int) -> Bool
    {
        let data = dataArray[index]
        
        if (data["image"] as? UIImage) != nil
        {
            return true
        }
        else if (data["picture"] as? String) != nil
        {
            return false
        }
        
        return false
    }
    
    func showLandingPage()
    {
        let popoverContent = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController

        let rootVC = storyboards.mainStoryboard.instantiateViewController( withIdentifier: "LandingPage") as! LandingPage
        
        let menuvc = storyboards.mainStoryboard.instantiateViewController( withIdentifier: "Menu_VC") as! Menu_VC
        
        popoverContent.frontViewController = rootVC //mainvc
        popoverContent.rearViewController = menuvc
        
        self.navigationController?.pushViewController(popoverContent, animated: true)
    }
    
    // MARK: - WEB API
    func callUploadPhotosAPI()
    {
        var token = ""
        if UserDefaults.standard.value(forKey: "userJSON") != nil
        {
            let userJSON = UserDefaults.standard.value(forKey: "userJSON") as! [String : Any]
            
            token = userJSON["token"] as! String
        }
        let headerObject = ["authtoken" : token]
        
        for index in 0 ... dataArray.count - 1
        {
            let data = dataArray[index]
            
            if let image = (data["image"] as? UIImage)// != nil
            {
                //Now use image to create into NSData format
                let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
                let base64String = imageData.base64EncodedString(options: .lineLength64Characters)
                
                var text = ""
                if let caption = data["text"] as? String
                {
                    text = caption
                }
                let requestObject : [String : Any] = ["text" : text,
                    "image_base_64" : base64String]
                
                let loader = startActivityIndicator(view: self.view, waitingMessage: "Uploading Image " + "\(index + 1)/\(dataArray.count)")
                loader.startAnimating()
                
                GlobalConstants().postData_Header_Parameter(subUrl: PROFILE_PIC_API.ADD_IMAGE, header: headerObject, parameter: requestObject, completionHandler:
                    {
                        (isSuccess,responseMessage,responseData) -> Void in
                        
                        print(responseData)
                        loader.stopAnimating()
                        
                        if isSuccess
                        {
                            self.showAlert(title: "Success!", msg: responseMessage)
                            
                        }
                        else
                        {
                            self.showAlert(title: "Fail!", msg: responseMessage)
                        }
                        
                       
                })
            }
            
        }
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
            self.showLandingPage()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ShowPhotoGridViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        print(" ~~~~~~~~~~~~~~~~~~~~~~~~~ Image selected ~~~~~~~~~~~~~~~~~~~~~~~~")
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return // No image selected.
        }
        
        var data = dataArray[indexToEdit]
        
        data["image"] = image
        
        self.assignImagesToButton()
        
        let dictToSend = ["index" : indexToEdit, "image" : image] as [String : Any]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "profilePicturesChanged"), object: nil, userInfo: dictToSend)

        self.dismiss(animated: true, completion:
            { () -> Void in
                
                self.navigationController?.popViewController(animated: true)
        })
    }
}
