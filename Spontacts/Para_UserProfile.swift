//
//  Para_UserProfile.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 5/26/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class Para_UserProfile: NSObject
{
    var userId = Int()
    var userToken = String()

    var userFirstName = String()
    var userLastName = String()
    var userEmail = String()
    var userMobileNo = String()
    var userPassword = String()
    
    var userLocationStr = String()
    var userLocationLat = Double()
    var userLocationLong = Double()
    
    var userPictureBase64 = String()
    var userPictureImage = UIImage()
    var userPictureUrl = String()
    var userPictureThumbUrl = String()
    var userPictureThumb = UIImage()
    
    var userAge = Int()
    var userGender = Int()
    var userRelationStatus = Int()
    var userDescription = String()
    
    var user_qb_id = Int()
    var isPlusMember = Int()
    var userName = String()
}
