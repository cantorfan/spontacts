//
//  MessageViewController.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 09/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import JSQMessagesViewController


class MessageViewController: JSQMessagesViewController
{
    var messages = [JSQMessage]()
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
//    var friendId = Int()
//    var friendName = String()
    var chatPara = Para_Conversation()
    
    let sharedObj = Singleton.shared
    var savedMessages = [Para_Message]()
    var dateFormatterMsg = DateFormatter()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = chatPara.Friend_name
        
        addLeftBarButn()
        dateFormatterMsg.dateFormat = "hh:mm a"
        senderDisplayName = "My name"
        senderId = "\(sharedObj.userPara.userId)"
        
        // This tells the layout to size each avatar at CGSize.zero, which is “no size”.
        // No avatars
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        savedMessages = DBManager.shared.getMessageTableData_ConversationId(coversation_Id: chatPara.Id)
    }

    func addLeftBarButn()
    {
        let backButn = UIBarButtonItem(image: #imageLiteral(resourceName: "back_pink"),  style: .plain, target: self, action: #selector(didTapBackButn))
        navigationItem.leftBarButtonItem = backButn
    }
    
    func didTapBackButn()
    {
        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "Message_VC") as! Message_VC
        
        // Will allow to go back
        self.navigationController?.pushViewController(presentVC, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
        
        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        // messages from someone else
//        addMessage(withId: "\(friendName)", name: friendName, text: "I am so fast!")
//        // messages sent from local sender
//        addMessage(withId: senderId, name: "Me", text: "I bet I can run faster than you!")
//        addMessage(withId: senderId, name: "Me", text: "I like to run!")
        // animates the receiving of a new message on the view
        for messagePara in savedMessages
        {
            if messagePara.Sender_Id == Int(senderId)
            {
                addMessage(withId: senderId, name: "Me", text: messagePara.Message, dateDouble: messagePara.Time)
            }
            else
            {
                addMessage(withId: "\(chatPara.Friend_Id)", name: chatPara.Friend_name, text: messagePara.Message, dateDouble: messagePara.Time)
            }
        }
        
        finishReceivingMessage()
        
        // Add this code in realtime,
//        observeMessages()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: ------------ JSQMessage Datasource -------------
    // MARK: Actual Datasource
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    // MARK: Setting up Bubble Images
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
        let message = messages[indexPath.item] // 1 Here you retrieve the message.
        if message.senderId == senderId { // 2 If the message was sent by the local user, return the outgoing image view.
            return outgoingBubbleImageView
        } else { // 3  Otherwise, return the incoming image view.
            return incomingBubbleImageView
        }
    }
    
    // MARK: Removing the Avatars
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    // MARK: Change Message Bubble Text Color
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        
        cell.cellBottomLabel.text = dateFormatterMsg.string(from: message.date)
        if message.senderId == senderId
        {
            cell.textView?.textColor = UIColor.white
        }
        else
        {
            cell.textView?.textColor = UIColor.black
        }
        return cell
    }
    
    // MARK: # Added newly
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString?
    {
//        if (indexPath.item % 10 == 0) {
            return NSMutableAttributedString(string: JSQMessagesTimestampFormatter.shared().relativeDate(for: messages[indexPath.row].date))
//        }
//        
//        return nil
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat
    {
//        if indexPath.item % 10 == 0
//        {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
//        }
//        return 0.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellBottomLabelAt indexPath: IndexPath) -> NSAttributedString
    {
        return NSMutableAttributedString(string: dateFormatterMsg.string(from: messages[indexPath.row].date))
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellBottomLabelAt indexPath: IndexPath) -> CGFloat
    {
//        if indexPath.item % 1 == 0
//        {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
//        }
//        return 0.0
    }
    
    
    // MARK: - Supporting JSQMessages
    // MARK: Set up message bubles UI
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    //This helper method creates a new JSQMessage and adds it to the data source.
    private func addMessage(withId id: String, name: String, text: String, dateDouble: Double)
    {
        if let message = JSQMessage.init(senderId: id, senderDisplayName: name, date: Date.init(timeIntervalSince1970: dateDouble), text: text)
        {
            messages.append(message)
        }
    }
    
    private func observeMessages()
    {
//        self.addMessage(withId: senderId, name: senderDisplayName, text: "Hello")
//        
//        // 5
//        self.finishReceivingMessage()
    }
    
    /*{
        messageRef = channelRef!.child("messages")
        // 1.
        let messageQuery = messageRef.queryLimited(toLast:25)
        
        // 2. We can use the observe method to listen for new
        // messages being written to the Firebase DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            // 3
            let messageData = snapshot.value as! Dictionary<String, String>
            
            if let id = messageData["senderId"] as String!, let name = messageData["senderName"] as String!, let text = messageData["text"] as String!, text.characters.count > 0 {
                // 4
                self.addMessage(withId: id, name: name, text: text)
                
                // 5
                self.finishReceivingMessage()
            } else {
                print("Error! Could not decode message data")
            }
        })
    }*/
    
    // MARK: - Action Methods
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
//        let itemRef = messageRef.childByAutoId() // 1
//        let messageItem = [ // 2
//            "senderId": senderId!,
//            "senderName": senderDisplayName!,
//            "text": text!,
//            ]
//        
//        itemRef.setValue(messageItem) // 3
        
        self.inputToolbar.contentView.rightBarButtonItem.isEnabled = false

        JSQSystemSoundPlayer.jsq_playMessageSentSound() // 4
        
        callSendMessageAPI(message: text, date: date) // Send to server API
    }
    
    // MARK:- Database Methods
    func saveMessageInDatabase(message : String, messageid : Int)
    {
        let msgPara = Para_Message()
        msgPara.Id = messageid
        msgPara.Chat_Id = chatPara.Id
        msgPara.Message = message
        msgPara.Time = Date().timeIntervalSince1970
        msgPara.Sender_Id = sharedObj.userPara.userId
        msgPara.Receiver_Id = chatPara.Friend_Id
        
        DBManager.shared.insertMessageTable(para: msgPara)
    }
    
    func saveConversationIdInDatabase(message : String)
    {
        //chatPara.Friend_Id = chatPara.Friend_Id
        chatPara.Last_Synced_Time = Date().timeIntervalSince1970
        //chatPara.Friend_name = chatPara.Friend_name
        chatPara.thumb = ""
        chatPara.Last_Message = message

        DBManager.shared.insertConversationTable(para: chatPara)
    }
    
    func updateConversationIdInDatabase(message : String)
    {
        chatPara.Last_Synced_Time = Date().timeIntervalSince1970
        chatPara.Last_Message = message
        
        DBManager.shared.updateConversationTable(para: chatPara)
    }
    
    // MARK: - API Methods
    func callSendMessageAPI(message : String, date: Date)
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject = ["message" : message,
                             "reciver" : chatPara.Friend_Id] as [String : Any]
        
        GlobalConstants().postData_Header_Parameter(subUrl: MESSAGE_URL.SEND_MSG, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                print( "Success!", responseData)
                //self.showAlert(title: "Success!", msg: responseMessage)
                self.addMessage(withId: self.senderId, name: "Me", text: message, dateDouble: date.timeIntervalSince1970)

                if self.chatPara.Id == 0
                {
                    // This means this is first time these two are chatting,
                    // So save the chat_id from reponse in conversation_table

                    self.chatPara.Id = responseData["chat_id"] as! Int
                    self.saveConversationIdInDatabase(message: message)
                }
                else
                {
                    self.chatPara.Id = responseData["chat_id"] as! Int
                    self.updateConversationIdInDatabase(message: message)
                }
                
                // TODO: - Temporarily removing saving in Message table
                // As I am syncing all the messages in the Chat list, this particular msg is saved here and when I go back sync API is called so this one is retrieved there also as this msg was not available in last sync
                // SO find out a better solution than this if any
                //self.saveMessageInDatabase(message: message, messageid: responseData["message_id"] as! Int) // Save in database
            }
            else
            {
                print("Fail!", responseMessage)
                //self.showAlert(title: "Fail!", msg: responseMessage)
            }
            
            self.inputToolbar.contentView.rightBarButtonItem.isEnabled = true
            self.finishSendingMessage() // 5
        })
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
