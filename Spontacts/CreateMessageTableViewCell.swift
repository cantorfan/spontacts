//
//  CreateMessageTableViewCell.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 07/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class CreateMessageTableViewCell: UITableViewCell
{
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
