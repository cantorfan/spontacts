//
//  InviteFrndsToAct.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 17/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import ContactsUI

class InviteFrndsToAct: UIViewController, UITableViewDataSource, UITableViewDelegate
{

    @IBOutlet weak var tableView: UITableView!
   
    let sharedObj = Singleton.shared
    var arrFriends = NSMutableArray()
    var arrSelectedFrnds = NSMutableArray()
    var arrSelectedContacts = NSMutableArray()
    var selectedSeg = Int()
    
    var view_width = CGFloat()
    var view_height = CGFloat()
    
    var invitedFrndsIds = NSMutableArray()
    var activityId = Int()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "Invite"
        view_width = view.frame.size.width
        view_height = view.frame.size.height
        
        tableView.dataSource = self
        tableView.delegate = self
        
        addRightBarButton()
        retrieveMyFriendList()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Logical Methods
    func addRightBarButton()
    {
        let butnSend = UIBarButtonItem.init(title: "Send", style: .plain, target: self, action: #selector(self.didTapSendButn))
        
        navigationItem.rightBarButtonItem = butnSend
    }
    
    func retrieveMyFriendList()
    {
        //        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Getting requests")
        //        loader.startAnimating()
        //
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl:FRIENDS_URL.GET_MY_FRND_LIST , header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                //                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["friend_list"] != nil
                    {
                        print(responseData["friend_list"] as Any)
                        let array = responseData["friend_list"] as! NSArray
                        print("friends array ", array.count)
                        self.arrFriends = array.mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in

        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Action Methods
    func didTapSendButn()
    {
        if arrSelectedFrnds.count == 0
        {
            self.showAlert(title: "Error!", msg: "Please Select at least 1 friend to invite.")
        }
        else
        {
            let loader =  startActivityIndicator(view: self.view, waitingMessage: "Getting requests")
            loader.startAnimating()
            
            let headerObject = ["authtoken" : sharedObj.userPara.userToken]
            
            for index in 0 ... arrSelectedFrnds.count - 1
            {
                let json = arrFriends[index] as! [String : Any]
                
                let id = json["id"] as! Int
                invitedFrndsIds.add(id)
            }
            
            let requestObject : [String : Any] = ["invited_friends" : invitedFrndsIds,
                                                  "activity_id" : activityId  ] as [String : Any]
            
            GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.INVITE_FRNDS_PRIVATE_ACT, header: headerObject, parameter: requestObject, completionHandler:
                {
                    (isSuccess,responseMessage,responseData) -> Void in
                    
                    loader.stopAnimating()
                    
                    if isSuccess
                    {
                        self.showAlert(title: "Success!", msg: responseMessage)
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
            })
        }
    }
    
    func didChangeSegmentedControl(sender : UISegmentedControl)
    {
        selectedSeg = sender.selectedSegmentIndex
        
        if selectedSeg == 1
        {
            let cnPicker = CNContactPickerViewController()
            cnPicker.delegate = self
            self.present(cnPicker, animated: true, completion: nil)
        }
        
        tableView.reloadData()
    }
    
    func didChangeSelectAllSwitch(sender : UISwitch)
    {
        
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if  section == 1
        {
            if selectedSeg == 0
            {
                return arrFriends.count
            }
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if  section == 0
        {
            return 0 //60
        }
        else if section == 1
        {
            if selectedSeg == 0
            {
                return 50
            }
            else
            {
                return  100
            }
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()//.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "ffffff")
        
        if section == 0
        {
            let segmentControl = UISegmentedControl.init(items: ["Friends", "Contacts"])
            segmentControl.frame = CGRect(x: 0, y: 0, width: view_width, height: 50)
            segmentControl.addTarget(self, action: #selector(self.didChangeSegmentedControl(sender:)), for: .valueChanged)
            segmentControl.selectedSegmentIndex = selectedSeg
            
            headerView.addSubview(segmentControl)
        }
        else if section == 1
        {
            if selectedSeg == 0
            {
                let lblSelectAll = UILabel(frame: CGRect(x: 10, y: 0, width: view_width / 2, height: 50))
                lblSelectAll.textAlignment = .left
                lblSelectAll.font = UIFont.init(name: Font.BEBAS, size: 18)
                lblSelectAll.numberOfLines = 0
                lblSelectAll.lineBreakMode = .byWordWrapping
                lblSelectAll.text = "Select all friends"
                headerView.addSubview(lblSelectAll)
                
                let fbSwitch = UISwitch.init(frame: CGRect(x: view.frame.size.width - 60, y: 5, width: 50, height: 40))
                fbSwitch.addTarget(self, action: #selector(didChangeSelectAllSwitch(sender:)), for: .valueChanged)
//                fbSwitch.setOn(activityPara.actIsPostToFacebook, animated: false)
                headerView.addSubview(fbSwitch)
                
                let sep = UIView.init(frame: CGRect(x: 0, y: 50, width: view_width, height: 1))
                sep.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
                headerView.addSubview(sep)
            }
            else if selectedSeg == 1
            {
                let lblHeader = UILabel(frame: CGRect(x: 10, y: 0, width: view_width - 20, height: 50))
                lblHeader.textAlignment = .center
                lblHeader.font = UIFont.init(name: Font.BEBAS, size: 18)
                lblHeader.numberOfLines = 0
                lblHeader.lineBreakMode = .byWordWrapping
                lblHeader.text = "Choose friends and invite them via SMS"
                headerView.addSubview(lblHeader)

                let lblSelectAll = UILabel(frame: CGRect(x: 10, y: 50, width: view_width / 2, height: 50))
                lblSelectAll.textAlignment = .left
                lblSelectAll.font = UIFont.init(name: Font.BEBAS, size: 18)
                lblSelectAll.numberOfLines = 0
                lblSelectAll.lineBreakMode = .byWordWrapping
                lblSelectAll.text = "Select all friends"
                headerView.addSubview(lblSelectAll)
                
                let fbSwitch = UISwitch.init(frame: CGRect(x: view.frame.size.width - 60, y: lblSelectAll.frame.origin.y + 5, width: 50, height: 40))
                fbSwitch.addTarget(self, action: #selector(didChangeSelectAllSwitch(sender:)), for: .valueChanged)
                //fbSwitch.setOn(activityPara.actIsPostToFacebook, animated: false)
                headerView.addSubview(fbSwitch)
                
                let sep = UIView.init(frame: CGRect(x: 0, y: 50, width: view_width, height: 1))
                sep.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
                headerView.addSubview(sep)
            }
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        
        var imgDp = UIImageView()
        var lblName = UILabel()

        cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
        cell?.selectionStyle = UITableViewCellSelectionStyle.gray
        
        imgDp = UIImageView.init(frame: CGRect(x: 10, y: 5, width: 40, height: 40))
        imgDp.backgroundColor = UIColor.white
        imgDp.layer.cornerRadius = 50
        imgDp.layer.borderColor = UIColor.white.cgColor
        imgDp.image = #imageLiteral(resourceName: "avatar")
        imgDp.tag = 1
        
        lblName = UILabel(frame: CGRect(x: imgDp.frame.origin.x + imgDp.frame.size.width + 10 , y: 10, width: view_width - 50, height: 30))
        lblName.textAlignment = .left
        lblName.font = UIFont.init(name: Font.BEBAS, size: 18)
        lblName.tag = 2
        
        cell?.contentView.addSubview(lblName)
        cell?.contentView.addSubview(imgDp)
        
        let userJSON = arrFriends[indexPath.row] as! [String : Any]
        
        let firstName : String = userJSON["fname"] as! String
        let lastName : String = userJSON["lname"] as! String
        
        lblName.text = firstName + " "  + lastName
        
        if selectedSeg == 0
        {
            if arrSelectedFrnds.contains(indexPath.row)
            {
                cell?.accessoryType = .checkmark
            }
            else
            {
                cell?.accessoryType = .none
            }
        }
        else if selectedSeg == 1
        {
            if arrSelectedContacts.contains(indexPath.row)
            {
                cell?.accessoryType = .checkmark
            }
            else
            {
                cell?.accessoryType = .none
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if selectedSeg == 0
        {
            if arrSelectedFrnds.contains(indexPath.row)
            {
                arrSelectedFrnds.remove(indexPath.row)
            }
            else
            {
                arrSelectedFrnds.add(indexPath.row)
            }
        }
//        else
//        {
//            if arrSelectedContacts.contains(indexPath.row)
//            {
//                arrSelectedContacts.remove(indexPath.row)
//            }
//            else
//            {
//                arrSelectedContacts.add(indexPath.row)
//            }
//        }
        tableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InviteFrndsToAct : CNContactPickerDelegate
{
    //MARK:- CNContactPickerDelegate Method
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact])
    {
        contacts.forEach
            { contact in
            for number in contact.phoneNumbers
            {
                let phoneNumber = number.value
                print("number is = \(phoneNumber)")
            }
        }
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
}
