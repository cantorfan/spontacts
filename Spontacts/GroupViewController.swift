//
//  GroupViewController.swift
//  Spontacts
//
//  Created by maximess142 on 08/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import QMServices
import AlamofireImage

class GroupViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    @IBOutlet weak var collectionView: UICollectionView!
    
    var dialogsArray = [QBChatDialog]()
    var timer: Timer?
    @IBOutlet weak var butnAdd: UIButton!
    @IBOutlet weak var butnHeart: UIButton!

    @IBOutlet weak var txtSearchBar: UISearchBar!
    //@IBOutlet weak var constraintSearchBarLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintSearchBarHeight: NSLayoutConstraint!
  
    let sharedObj = Singleton.shared
    var groupsArray = [[String:Any]]()
    var isFromHomePage = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Groups"
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.animate), userInfo: nil, repeats: true)

        addLeftBarButton()
        addRightBarButton()
        fetchChatRoomsFromQB(searchStr: "")
        
        ServicesManager.instance().chatService.addDelegate(self)
        
        ServicesManager.instance().authService.add(self)
        
        /*
        if (QBChat.instance().isConnected) {
            self.getDialogs()
        }
        */
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        //fetchChatRoomsFromQB(searchStr: "")
        fetchGroupFromLocalServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftBarButton()
    {
        if isFromHomePage == false
        {
            let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu_new"), style: .plain,  target: self, action: #selector(didTapMenuButn))
            navigationItem.leftBarButtonItem = btnMenu
            
            if revealViewController() != nil
            {
                revealViewController().rightViewRevealWidth = 200
                
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            }
        }
        else
        {
            let btnBack = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.didTapBackButn))
            navigationItem.leftBarButtonItem = btnBack
        }
    }
    
    func addRightBarButton()
    {
        let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "search"), style: .plain,  target: self, action: #selector(didTapSearchButn))
        navigationItem.rightBarButtonItem = btnMenu
    }
    
    func animate()
    {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations:
            {
                self.butnHeart.alpha = 0.2
                self.butnAdd.alpha = 0.2
        },
                       completion:
            {
                (finished: Bool) -> Void in
                
                // Fade in
                UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations:
                    {
                        self.butnHeart.alpha = 1.0
                        self.butnAdd.alpha = 1.0
                        
                }, completion: nil)
        })
    }
    
    // MARK: - Custom Methods
    func fetchGroupFromLocalServer()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: GROUPS_API.GET_ALL_GROUPS , header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    if responseData["groups"] != nil
                    {
                        self.groupsArray = responseData["groups"] as! [[String : Any]]
                        self.collectionView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func callSearchGroupFromLocalServer(search : String)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject = ["keyword" : self.txtSearchBar.text!]
       
        GlobalConstants().postData_Header_Parameter(subUrl: GROUPS_API.SEARCH_GROUP, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["groups"] != nil
                    {
                        self.groupsArray = responseData["groups"] as! [[String : Any]]
                        self.collectionView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func fetchChatRoomsFromQB(searchStr : String)
    {
        var extendedRequest = [String:String]()
        if searchStr != ""
        {
            extendedRequest = ["type" : "1", "name[ctn]" : searchStr]
        }
        else
        {
            extendedRequest = ["type" : "1"]
        }
        
        let page = QBResponsePage(limit: 100, skip: 0)
        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Loading Groups...")
        loader.startAnimating()
        
        QBRequest.dialogs(for: page, extendedRequest: extendedRequest, successBlock: { (response: QBResponse, dialogs: [QBChatDialog]?, dialogsUsersIDs: Set<NSNumber>?, page: QBResponsePage?) -> Void in
            
            loader.stopAnimating()

            print(dialogs as Any)
            
            self.dialogsArray = dialogs!
            
            //     *  @param join YES to join in dialog immediately
            
            ServicesManager.instance().chatService.dialogsMemoryStorage.add(dialogs!, andJoin: false)
            
            self.collectionView.reloadData()
        }) { (response: QBResponse) -> Void in
            loader.stopAnimating()
            print(response.error?.description)
        }
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Action Methods
    @IBAction func didTapMenuButn()
    {
        revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func didTapBackButn()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapSearchButn()
    {
        self.txtSearchBar.isHidden = false
        
        UIView.animate(withDuration: 0.6, animations: {
            self.constraintSearchBarHeight.constant = 45
            self.view.layoutIfNeeded()
        })
        
        self.txtSearchBar.becomeFirstResponder()
    }

    @IBAction func didTapCreateButn(_ sender: Any)
    {
        let createVC = storyboards.groupStoryboard.instantiateViewController(withIdentifier: "CreateGroupViewController") as! CreateGroupViewController
        self.navigationController?.pushViewController(createVC, animated: true)
    }

    @IBAction func btnActionHeart(_ sender: Any)
    {
        let presentVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "DonationViewController") as! DonationViewController
        self.navigationController?.pushViewController(presentVC, animated: true)
    }

    // MARK: - DataSource Action
    
    func getDialogs() {
        
        if let lastActivityDate = ServicesManager.instance().lastActivityDate {
            
            ServicesManager.instance().chatService.fetchDialogsUpdated(from: lastActivityDate as Date, andPageLimit: kDialogsPageLimit, iterationBlock: { (response, dialogObjects, dialogsUsersIDs, stop) -> Void in
                
            }, completionBlock: { (response) -> Void in
                
                if (response.isSuccess) {
                    
                    ServicesManager.instance().lastActivityDate = NSDate()
                }
            })
        }
        else {
            
            SVProgressHUD.show(withStatus: "SA_STR_LOADING_DIALOGS".localized, maskType: SVProgressHUDMaskType.clear)
            
            ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                
            }, completion: { (response: QBResponse?) -> Void in
                
                guard response != nil && response!.isSuccess else {
                    SVProgressHUD.showInfo(withStatus: "SA_STR_FAILED_LOAD_DIALOGS".localized)
                    //SVProgressHUD.showError(withStatus: "SA_STR_FAILED_LOAD_DIALOGS".localized)
                    return
                }
                
                SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
                ServicesManager.instance().lastActivityDate = NSDate()
            })
        }
    }
    
    // MARK: - DataSource
    
    func dialogs() -> [QBChatDialog]? {
        
        // Returns dialogs sorted by updatedAt date.
        return ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
    }

    // MARK: -
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //return dialogsArray.count
        return groupsArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupCollectionViewCell", for: indexPath as IndexPath) as! GroupCollectionViewCell
        
        /*
        let dialog = dialogsArray[indexPath.row]
        
        cell.lblName.text = dialog.name
        cell.lblCount.text = "\(dialog.unreadMessagesCount)"
        
        if dialog.photo != nil
        {
            let id = UInt(dialog.photo!)
            if let _ = id
            {
                QBRequest.blobObjectAccess(withBlobID: id!, successBlock: {
                    (_, qbaccess) in
                    
                    print("the link is - ", qbaccess?.url ?? "")
                    
                    let placeholderImage = #imageLiteral(resourceName: "avatar")
                    let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
                    
                    //,filter: filter
                    cell.imgViewGroup.af_setImage(withURL: (qbaccess?.url)!, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                        { response in
                            
                            print("Hiii")    
                    })
                }, errorBlock: { ( error) in
                    print(error.error?.description as Any ?? "error occured")
                })
            }
        }*/
        
        let groupData = self.groupsArray[indexPath.row]
        cell.lblName.text = groupData["name"] as? String
        
        let stringUrl = groupData["picture"] as! String
        
        if stringUrl != ""
        {
            let placeholderImage = #imageLiteral(resourceName: "image_placeolder")
            let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
            
            cell.imgViewGroup.af_setImage(withURL: URL.init(string: APP_URL.BASE_URL + stringUrl)!, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion: nil)
        }
        else
        {
            cell.imgViewGroup.image = #imageLiteral(resourceName: "image_placeolder")
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing)
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(3))
        

            return CGSize(width: size-0, height: 150-0)
        
        //        return CGSize(width: view.frame.size.width / 3, height: view.frame.size.width / 3)
    }
    
    func collectionView(collectinView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        /*
        if (ServicesManager.instance().isProcessingLogOut!) {
            return
        }
        
        let dialogToSend = self.dialogsArray[indexPath.row]
            let chatVC = storyboards.qbStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            
            chatVC.dialog = self.dialogsArray[indexPath.row]
            //        chatVC.dialog = dialog
            self.navigationController?.pushViewController(chatVC, animated: true)*/
        
        let groupData = self.groupsArray[indexPath.row]
        let qbId = groupData["group_qbid"] as! String
        
        ServicesManager.instance().chatService.fetchDialog(withID: qbId, completion: { (chatDialog) in
            
            let chatVC = storyboards.qbStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            
            chatVC.dialog = chatDialog
            chatVC.groupDetails = groupData
            self.navigationController?.pushViewController(chatVC, animated: true)
        })
    }

    func downloadTeamMemberAvatar(avatarFileId:String,completion:      @escaping (UIImage?)->Void){
        
        let id = UInt(avatarFileId)
        
        if let _ = id
        {
            QBRequest.downloadFile(withID: id!, successBlock: { (_, imageData) in
                
                let image = UIImage(data:imageData)
                
                completion(image)
                
            }, statusBlock: { (_, _) in
                
            }, errorBlock: { (_) in
                
                completion(nil)
            })
            
        }else{completion(nil)}
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GroupViewController : QMChatServiceDelegate, QMChatConnectionDelegate, QMAuthServiceDelegate
{
    // MARK: - QMChatServiceDelegate
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService,didUpdateChatDialogsInMemoryStorage dialogs: [QBChatDialog]){
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogsToMemoryStorage chatDialogs: [QBChatDialog]) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String){
        
        self.reloadTableViewIfNeeded()
    }
    
    // MARK: QMChatConnectionDelegate
    
    func chatServiceChatDidFail(withStreamError error: Error) {
        SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    func chatServiceChatDidAccidentallyDisconnect(_ chatService: QMChatService) {
        SVProgressHUD.showError(withStatus: "SA_STR_DISCONNECTED".localized)
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
        SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized, maskType:.clear)
        if !ServicesManager.instance().isProcessingLogOut! {
            self.getDialogs()
        }
    }
    
    func chatService(_ chatService: QMChatService,chatDidNotConnectWithError error: Error){
        SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
        SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized, maskType: .clear)
        if !ServicesManager.instance().isProcessingLogOut! {
            self.getDialogs()
        }
    }
    
    // MARK: - Helpers
    func reloadTableViewIfNeeded() {
        if !ServicesManager.instance().isProcessingLogOut! {
            self.collectionView.reloadData()
        }
    }

}

extension GroupViewController : UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        UIView.animate(withDuration: 0.6, animations:{
            self.constraintSearchBarHeight.constant = 0
            self.view.layoutIfNeeded()
            self.txtSearchBar.isHidden = true
        })
        
        self.fetchChatRoomsFromQB(searchStr: "")
        self.txtSearchBar.text = ""
        self.txtSearchBar.resignFirstResponder()
        self.fetchGroupFromLocalServer()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        if self.txtSearchBar.text != ""
        {
            if self.txtSearchBar.text! != ""
            {
                self.fetchChatRoomsFromQB(searchStr: self.txtSearchBar.text!)
                self.callSearchGroupFromLocalServer(search: self.txtSearchBar.text!)
            }
        }
        
        searchBar.resignFirstResponder()
    }
    
    func dismissKeyboard()
    {
        if let recognizers = self.view.gestureRecognizers
        {
            for recognizer in recognizers
            {
                self.view.removeGestureRecognizer(recognizer)
            }
        }
        
        view.endEditing(true)
    }
}

extension GroupViewController : SWRevealViewControllerDelegate
{
    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }
}
