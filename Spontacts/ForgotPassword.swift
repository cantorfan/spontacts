//
//  ForgotPassword.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 5/24/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class ForgotPassword: UIViewController {

    @IBOutlet weak var txtEmailId: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Hide the navigation bar on this view only.
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // As soon as view is dismissed, bring back the navigation bar
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    @IBAction func didTapBackButn(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapResetPasswordButn(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if txtEmailId.text! == ""
        {
            self.showAlert(title: "", msg: "Please Enter Email")
        }
        else if Singleton.shared.isValidEmail(testStr: txtEmailId.text!) == false
        {
            self.showAlert(title: "", msg: "Please Enter Valid Email")
        }
        else
        {
            callResetPasswordAPI()
        }
    }

    func callResetPasswordAPI()
    {        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please wait..")
        loader.startAnimating()
        
        let str = txtEmailId.text!
        
        let headerObject = ["email" : str]
        
        GlobalConstants().postData_OnlyParameter(subUrl: APP_URL.RESET_PASSWORD, parameter: headerObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            print(responseData)
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }

    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
