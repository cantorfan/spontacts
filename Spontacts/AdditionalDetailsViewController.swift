//
//  AdditionalDetailsViewController.swift
//  Spontacts
//
//  Created by maximess142 on 14/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class AdditionalDetailsViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtCategory: UITextField!
    
    var ideaStr = ""
    var idesDescription = ""
    var categoryId = 0
    
    let sharedObj = Singleton.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.txtEmail.text = sharedObj.userPara.userEmail
        self.txtName.text = sharedObj.userPara.userFirstName
        
        addLeftBarButn()
        
        let notificationName = Notification.Name("feedbackCategorySelected")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationCategoryChosedn(noti:)), name: notificationName, object: nil)
        
        let postBtn = UIBarButtonItem.init(title: "Post", style: .done, target: self, action: #selector(self.postBtnTapped))
        self.navigationItem.rightBarButtonItem = postBtn
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    func addLeftBarButn()
    {
        let backButn = UIBarButtonItem(image: #imageLiteral(resourceName: "back_pink"),  style: .plain, target: self, action: #selector(didTapBackButn))
        navigationItem.leftBarButtonItem = backButn
    }

    @IBAction func didTapBackButn()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    func notificationCategoryChosedn(noti : Notification)
    {
        let receivedObj : [String : Any] = noti.object as! [String : Any]
        
        categoryId = receivedObj["id"] as! Int
        txtCategory.text = receivedObj["name"] as? String
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapCategoryButn(_ sender: Any)
    {
        let catVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackCategoryViewController") as! FeedbackCategoryViewController
        
        self.navigationController?.pushViewController(catVC, animated: true)
    }

    func postBtnTapped()
    {
        dismissKeyboard()
        
        print(self.navigationController?.viewControllers ?? "")
            

        if self.txtEmail.text == ""
        {
            self.showAlert(title: "Error!", msg: "Please enter your email address before submitting your ticket.")
            return
        }
        
        if !sharedObj.isValidEmail(testStr: self.txtEmail.text!)
        {
            self.showAlert(title: "Error!", msg: "Please enter valid email address before submitting your ticket.")
            return
        }
        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let parameter = ["idea" : ideaStr,
                         "idea_description" : idesDescription,
                         "name" : txtName.text ?? "",
                         "email" : txtEmail.text ?? "",
                         "category" : categoryId] as [String : Any]
        
//        // to fetch users
        GlobalConstants().postData_Header_Parameter(subUrl: SETTINGS_URL.POST_FEEDBACK, header: headerObject, parameter: parameter, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    // Go to feedback list without adding viewcontroller in navigation stack
                    var navArray = self.navigationController?.viewControllers
                    navArray?.removeLast(2)
                    self.navigationController?.viewControllers = navArray!

                    self.showAlert(title: "Success!", msg: responseMessage)
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
