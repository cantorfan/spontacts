//
//  ShowFeedbakTableViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 16/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class ShowFeedbakTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCategory: UILabel!
    
    @IBOutlet weak var lblTitle: MarqueeLabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var imgUserPhoto: UIImageView!
    
    @IBOutlet weak var lblCommentText: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
