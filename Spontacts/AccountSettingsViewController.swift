//
//  AccountSettingsViewController.swift
//  Spontacts
//
//  Created by maximess142 on 12/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class AccountSettingsViewController: UIViewController
{
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var txtOldPassword: UITextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "Account Settings"
        
        txtEmail.text = Singleton.shared.userPara.userEmail
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapChangePassword(_ sender: Any)
    {
        self.view.endEditing(true)
        if txtNewPassword.text == ""
        {
            self.showAlert(title: "", msg: "Password cannot be empty")
        }
        else if txtConfirmPassword.text == ""
        {
            self.showAlert(title: "", msg: "Please Re-enter new password")
        }
        else if txtNewPassword.text != txtConfirmPassword.text
        {
            self.showAlert(title: "", msg: "Passwords do not match")
        }
        else if txtOldPassword.text == ""
        {
            self.showAlert(title: "", msg: "Please Enter Old Password")
        }
        else
        {
            callResetPasswordAPI()
        }
    }

    // MARK: - Custom Methods
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func callResetPasswordAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject : [String : Any] = ["old_password" : txtOldPassword.text!,
                                              "new_password" : txtNewPassword.text!]
                as [String : Any]
        
        
        GlobalConstants().postData_Header_Parameter(subUrl: APP_URL.CHANGE_PASSWORD, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    let alert = UIAlertController.init(title: "Success!", message: responseMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
                        alert -> Void in
                        
                        self.navigationController?.popViewController(animated: true)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
            })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
