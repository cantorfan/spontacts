//
//  FeedbackCategoryViewController.swift
//  Spontacts
//
//  Created by maximess142 on 14/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class FeedbackCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    let ARR_FEEDBACK_CAT = ["(None)", "Activity Create", "Notifications", "Representation in the app", "Recommendations/Invitations", "Filter function and Sorting", "Friends", "Criticism of GAY AND YOU?", "Confirmer Manage", "News", "Profile", "Others", "Past Activities"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ARR_FEEDBACK_CAT.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = ARR_FEEDBACK_CAT[indexPath.row]
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = ["id" : indexPath.row, "name" : ARR_FEEDBACK_CAT[indexPath.row]] as [String : Any]
        
        let notificationName = Notification.Name("feedbackCategorySelected")
        NotificationCenter.default.post(name: notificationName, object: dict)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
