//
//  LandingPage.swift
//  Spontacts
//
//  Created by Mac on 30/05/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//


import UIKit
import GooglePlaces
import Alamofire

class LandingPage: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, SWRevealViewControllerDelegate, GMSAutocompleteViewControllerDelegate
{

    lazy var searchBars: UISearchBar = UISearchBar(frame: CGRect(x:0, y: 0, width: 100, height:20))

    //@IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let arrNames = ["FESTIVALS", "FOOD & DRINKS", "BAR & CLUB", "HEALTH & WELLNESS", "TRAVEL & OUTDOOR", "SPORT", "EDUCATION", "CULTURE & ART", "SEE MORE.." ]
    
    @IBOutlet weak var butnAdd: UIButton!
    @IBOutlet weak var butnHeart: UIButton!
    @IBOutlet weak var butnQuotes: UIButton!
    
    @IBOutlet weak var butnMenu: UIButton!
    @IBOutlet weak var butnLocation: UIButton!
    @IBOutlet weak var txtSearchBar: UISearchBar!
    @IBOutlet weak var constraintSearchBarLeading: NSLayoutConstraint!
    
    @IBOutlet weak var lblUnreadCount: UILabel!
    let sharedObj = Singleton.shared
    var timer: Timer?

    @IBOutlet weak var constraintHeartButnWIdth: NSLayoutConstraint!
    //var from_date = 0.0
    //var to_date = 0.0
    //var for_categories = [Int]()
    //var sortedByStr = ""
    //var searchText = ""

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.titleView = searchBars


        butnLocation.setTitle(sharedObj.userPara.userLocationStr, for: .normal)
        addRightBarButton()
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(LandingPage.animate), userInfo: nil, repeats: true)
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        if revealViewController() != nil {
            //            revealViewController().rearViewRevealWidth = 62
            butnMenu.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            
            revealViewController().rightViewRevealWidth = 200
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }

        self.txtSearchBar.isHidden = true
        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        DispatchQueue.global(qos: .background).async
        {
            print("This is run on the background queue")
            self.getUserDataInServer()

            DispatchQueue.main.async
            {
                self.butnLocation.setTitle(self.sharedObj.userPara.userLocationStr, for: .normal)
                
                self.sharedObj.resetFilterPara()
                
                loader.stopAnimating()
                
                print("This is run on the main queue, after the previous code in outer block")
                
               // self.initialiseQBUser()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        //self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK :- Custom Methods
    func addRightBarButton()
    {
        if sharedObj.userPara.isPlusMember == 1
        {
            constraintHeartButnWIdth.constant = 40
        }
        else
        {
            constraintHeartButnWIdth.constant = 0
        }
    }
    
    func getUserDataInServer()
    {
        if UserDefaults.standard.value(forKey: "userJSON") != nil
        {
            if sharedObj.userPara.userToken == ""
            {
                
                let userJSON = UserDefaults.standard.value(forKey: "userJSON") as! [String : Any]
                print("filling user para from userDefaults", userJSON)

                sharedObj.userPara = Para_UserProfile()
                
                sharedObj.userPara.userId = userJSON["id"] as! Int
                sharedObj.userPara.userEmail = userJSON["email"] as! String
                sharedObj.userPara.userLocationLat = userJSON["lat"] as! Double
                sharedObj.userPara.userFirstName = userJSON["fname"] as! String
                sharedObj.userPara.userLastName = userJSON["lname"] as! String
                sharedObj.userPara.userToken = userJSON["token"] as! String
                sharedObj.userPara.userLocationLong = userJSON["lng"] as! Double
                //sharedObj.userPara.userPassword = userJSON["password"] as! String
                sharedObj.userPara.userPictureThumbUrl = userJSON["thumb"] as! String
                sharedObj.userPara.userPictureUrl = userJSON["picture"] as! String
                sharedObj.userPara.userLocationStr = userJSON["location_string"] as! String
                sharedObj.userPara.isPlusMember = userJSON["suscribed"] as! Int
                
                self.callRefreshNotificationCount()
                
                if ServicesManager.instance().currentUser() == nil
                {
                    QBRequest.logIn(withUserLogin: sharedObj.userPara.userToken, password: kQuickbloxCommonPassword, successBlock:{
                        (success,  user) -> Void in
                        
                        user?.password = kQuickbloxCommonPassword
                        QBChat.instance().connect(with: user!, completion: nil)
                        print("Logged in with QuickBlox")
                        
                        IS_QB_LOGGEDIN = true
                    },
                                    errorBlock:{
                                        (erro) -> Void in
                                        
                                        print("\(erro)")
                                        
                    })
                }
            }
        }
    }
    
    func animate()
    {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations:
            {
                self.butnHeart.alpha = 0.2
                self.butnAdd.alpha = 0.2
                self.butnQuotes.alpha = 0.2
        },
                       completion:
            {
                (finished: Bool) -> Void in

                // Fade in
                UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations:
                    {
                        self.butnHeart.alpha = 1.0
                        self.butnAdd.alpha = 1.0
                        self.butnQuotes.alpha = 1.0
                        
                }, completion: nil)
        })
    }
    
    // MARK: ------- Refresh Notifications -------
    func callRefreshNotificationCount()
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let urlString = APP_URL.BASE_URL + REFRESH_STATUS.SYNC_NOTIFICATION
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method:.post, parameters: nil, encoding: JSONEncoding.default, headers: headerObject).responseJSON
            { response in
                
                print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! [String : Any]
                        
                        if (dict["my_activities"] as? Int) != nil
                        {
                            self.sharedObj.unread_MyActivities_Count = dict["my_activities"] as! Int
                            self.sharedObj.unread_All_Activities_Count = dict["joined_activities"] as! Int
                            self.sharedObj.unread_Friends_Count = dict["friends"] as! Int
                            self.sharedObj.unread_Profile_Visitors_Count = dict["profile_visitors"] as! Int
                            self.sharedObj.unread_Recommendations = dict["recomendations"] as! Int
                            
                            self.sharedObj.unread_Total_Count = self.sharedObj.getTotalCount()
                            if self.sharedObj.unread_Total_Count != 0
                            {
                                self.lblUnreadCount.isHidden = false
                                self.lblUnreadCount.text = "\(self.sharedObj.unread_Total_Count)"
                            }
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    print("Failure in sync notification - ", error.localizedDescription)
                    break
                }
                
           }
    }
    
    // MARK: - Action Methods
    
    @IBAction func didTapAllGroups(_ sender: Any)
    {
        let groupVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "GroupViewController") as! GroupViewController
        groupVC.isFromHomePage = true
        self.navigationController?.pushViewController(groupVC, animated: true)
    }
    
    @IBAction func didTapQuotesButn(_ sender: Any)
    {
//        let quotesVC = storyboards.profilePicStoryboard.instantiateViewController(withIdentifier: "AddProfilePicturesViewController") as! AddProfilePicturesViewController
//
//        self.navigationController?.pushViewController(quotesVC, animated: true)
//
        let quotesVC = storyboards.quotesStoryboard.instantiateViewController(withIdentifier: "QuotesViewController") as! QuotesViewController

        self.navigationController?.pushViewController(quotesVC, animated: true)
    }
    
    @IBAction func didTapLocationButn(_ sender: Any)
    {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func didTapSearchButn()
    {
        self.txtSearchBar.isHidden = false
        ;
        UIView.animate(withDuration: 0.6, animations: {
            self.constraintSearchBarLeading.constant = 0
            self.view.layoutIfNeeded()
        })
        
        self.txtSearchBar.becomeFirstResponder()
    }
    
    @IBAction func didTapHomeButn()
    {
        
    }
    
    @IBAction func didTapWingsButton(_ sender: Any)
    {
        performSegue(withIdentifier: "segueLandingPageToDonation", sender: nil)
    }
    
    @IBAction func didTapAddButn(_ sender: Any)
    {
        self.performSegue(withIdentifier: "segueLandingPageToCreateActivity", sender: nil)
    }
    
    @IBAction func didTapHeartButn(_ sender: Any)
    {
        performSegue(withIdentifier: "segueLandingPageToDonation", sender: nil)        
    }
    
    @IBAction func didTapTodayButn(_ sender: Any)
    {
        sharedObj.resetFilterPara()
        
        sharedObj.para_filter.from_date = Date().startOfDayEpoch
        sharedObj.para_filter.to_date = Date().endOfDayEpoch
        sharedObj.para_filter.timeStr = "Today"
        
        self.performSegue(withIdentifier: "segueLandingPageToAllActivities", sender: nil)
    }
    
    @IBAction func didTapTomorrowButn(_ sender: Any)
    {
        let tomorrowDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        sharedObj.para_filter.from_date = tomorrowDate.startOfDayEpoch
        sharedObj.para_filter.to_date = tomorrowDate.endOfDayEpoch
        sharedObj.para_filter.timeStr = "Tomorrow"
        
        self.performSegue(withIdentifier: "segueLandingPageToAllActivities", sender: nil)
    }
    
    @IBAction func didTapWeekendButn(_ sender: Any)
    {
        let today = Date()
        let calendar = Calendar.current
        
        let todayWeekday = calendar.component(.weekday, from: today)
        
        let addWeekdays = 7 - todayWeekday  // 7: Saturday number
        var components = DateComponents()
        components.weekday = addWeekdays
        
        let nextSaturday = calendar.date(byAdding: components, to: today)
        sharedObj.para_filter.from_date = (nextSaturday?.startOfDayEpoch)!

        let nextSunday = Calendar.current.date(byAdding: .day, value: 1, to: nextSaturday!)!
        sharedObj.para_filter.to_date = nextSunday.endOfDayEpoch
        
        sharedObj.para_filter.timeStr = "Weekend"
        self.performSegue(withIdentifier: "segueLandingPageToAllActivities", sender: nil)
    }
    
    @IBAction func didTapAllActivitiesButn(_ sender: Any)
    {
        sharedObj.para_filter.categories_array.removeAll()
        
        self.performSegue(withIdentifier: "segueLandingPageToAllActivities", sender: nil)
    }
    
    @IBAction func didTapDIscoverButn(_ sender: Any)
    {
        
    }
    
    //MARK: - Collection view Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 9
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LandingPageCollectionViewCell", for: indexPath as IndexPath) as! LandingPageCollectionViewCell
        
        cell.imgCell.image = sharedObj.arrCatImageCropped[indexPath.item]
        cell.lblTitle.text = arrNames[indexPath.item]
        
        if indexPath.item == 0 || indexPath.item == 1 || indexPath.item == 2
        {
            cell.lblTitle.backgroundColor = sharedObj.hexStringToUIColor(hex: "FF1694").withAlphaComponent(0.5)
        }
        else if indexPath.row == 3 || indexPath.item == 4 || indexPath.item == 5
        {
            cell.lblTitle.backgroundColor = sharedObj.hexStringToUIColor(hex: "3CB043").withAlphaComponent(0.5)
        }
        else if indexPath.row == 6 || indexPath.item == 7 || indexPath.item == 8
        {
            cell.lblTitle.backgroundColor = sharedObj.hexStringToUIColor(hex: "3F51B5").withAlphaComponent(0.5)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing)
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(3))
        
        if collectionView == self.collectionView
        {
            return CGSize(width: size-0, height: size-0)
        }
        else
        {
            return CGSize(width: size-0, height: 60)
        }
        
//        return CGSize(width: view.frame.size.width / 3, height: view.frame.size.width / 3)
    }
    
    func collectionView(collectinView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        sharedObj.para_filter.categories_array = [indexPath.row]
        
        self.performSegue(withIdentifier: "segueLandingPageToAllActivities", sender: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "goToChat".localized
        {
            if let chatVC = segue.destination as? ChatViewController
            {
                chatVC.dialog = sender as? QBChatDialog
            }
        }
        else if segue.identifier == "segueLandingPageToAllActivities"
        {
            let allVC : All_Activities_VC = segue.destination as! All_Activities_VC
            
            allVC.isFromHomePage = true
        }
    }

    
    // MARK: - GMSAutocompleteViewController Delegate
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        sharedObj.userPara.userLocationStr = place.name
        sharedObj.userPara.userLocationLat = place.coordinate.latitude
        sharedObj.userPara.userLocationLong = place.coordinate.longitude
        
        butnLocation.setTitle(place.name, for: .normal)
        
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print(place.coordinate)
        
        callUpdateUserLocationAPI()
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
    {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    // MARK: - Web API
    func callUpdateUserLocationAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]

        let requestObject : [String : Any] = ["lat": "\(sharedObj.userPara.userLocationLat)",
            "lng": "\(sharedObj.userPara.userLocationLong)",
            "location_string": sharedObj.userPara.userLocationStr] as [String : Any]
        
        print(requestObject)
        
        GlobalConstants().postData_Header_Parameter(subUrl: APP_URL.UPDATE_USER_LOCATION, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    if responseData["user"] != nil
                    {
                        let userJSON = responseData["user"] as! [String : Any]
                        UserDefaults.standard.set(userJSON, forKey: "userJSON")
                        UserDefaults.standard.synchronize()

                        self.sharedObj.userPara.userId = userJSON["id"] as! Int
                        self.sharedObj.userPara.userEmail = userJSON["email"] as! String
                        self.sharedObj.userPara.userLocationLat = userJSON["lat"] as! Double
                        self.sharedObj.userPara.userFirstName = userJSON["fname"] as! String
                        self.sharedObj.userPara.userLastName = userJSON["lname"] as! String
                        self.sharedObj.userPara.userToken = userJSON["token"] as! String
                        self.sharedObj.userPara.userLocationLong = userJSON["lng"] as! Double
                        //self.sharedObj.userPara.userPassword = userJSON["password"] as! String
                        self.sharedObj.userPara.userPictureThumbUrl = userJSON["thumb"] as! String
                        self.sharedObj.userPara.userPictureUrl = userJSON["picture"] as! String
                        self.sharedObj.userPara.userLocationStr = userJSON["location_string"] as! String
                        self.sharedObj.userPara.userAge = userJSON["age"] as! Int
                        self.sharedObj.userPara.userGender = userJSON["gender"] as! Int
                        self.sharedObj.userPara.userRelationStatus = userJSON["relationship_status"] as! Int
                        self.sharedObj.userPara.userDescription = userJSON["about"] as! String
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }

}

extension LandingPage : UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        UIView.animate(withDuration: 0.6, animations:{
            self.constraintSearchBarLeading.constant = self.view.frame.size.width
            self.view.layoutIfNeeded()
            self.txtSearchBar.isHidden = true
        })

        self.txtSearchBar.text = ""        
        self.txtSearchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        if self.txtSearchBar.text != ""
        {
            if self.txtSearchBar.text! != ""
            {
                sharedObj.para_filter.search_keyword = self.txtSearchBar.text!
                self.performSegue(withIdentifier: "segueLandingPageToAllActivities", sender: nil)
            }
        }
        
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if self.txtSearchBar.text != ""
        {
            if self.txtSearchBar.text! != ""
            {
                
            }
        }
    }
    
    
    func dismissKeyboard()
    {
        if let recognizers = self.view.gestureRecognizers
        {
            for recognizer in recognizers
            {
                self.view.removeGestureRecognizer(recognizer)
            }
        }
        
        view.endEditing(true)
    }
}
