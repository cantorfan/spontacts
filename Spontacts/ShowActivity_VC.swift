//
//  ShowActivity_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 07/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import YangMingShan
import GoogleMaps
import AlamofireImage
import EventKit
import EventKitUI
import MapKit
import Alamofire
import GoogleMobileAds

class ShowActivity_VC: UIViewController, EKEventEditViewDelegate
{
    let sharedObj = Singleton.shared
    

    @IBOutlet weak var tableView: UITableView!
    
    var userPara = Para_UserProfile()
    var activityPara = Para_Activity()
    var isFromCreateActivity = Bool()
    
    var actDateFormatter = DateFormatter()
    var timeDateformatter = DateFormatter()
   
    var imagesArray = [UIImage]()
    var imgUrlArray = [String]()
    
    var pictureJSONToSend = [String : Any]()
    var isGoingInEditMode = Bool()

    var arrCommentsArr = NSMutableArray()
    var commentDateFormatter = DateFormatter()
    
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = AdMob_BannerID
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        
        return adBannerView
    }()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        
        title = activityPara.actName
        addLeftBarButn()
        actDateFormatter.dateFormat = "EEEE, dd/MM/yyyy"
        timeDateformatter.dateFormat = "hh:mm a"
        commentDateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
        addLocalNotiObserver()
        
        //activityPara.actUserCombineArray = activityPara.actGoingArray.addingObjects(from: (activityPara.actInterstedArray ) as! [Any]) as! NSMutableArray
        
        adBannerView.load(GADRequest())
        
        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        loadPhotosUrl()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftBarButn()
    {
        let backButn = UIBarButtonItem(image: #imageLiteral(resourceName: "back_pink"),  style: .plain, target: self, action: #selector(didTapBackButn))
        navigationItem.leftBarButtonItem = backButn
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in

        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func loadPhotosUrl()
    {
        if activityPara.actImgJSONArr.count != 0
        {
            imgUrlArray.removeAll()
            
            for index in 0 ... activityPara.actImgJSONArr.count - 1
            {
                let picJSON : [String : Any] = activityPara.actImgJSONArr[index] as! [String : Any]
                
                let thumbUrl : String = picJSON["thumb"] as! String
                if thumbUrl != ""
                {
                    imgUrlArray.append(thumbUrl)
                    
                }
            }
        }
        
        tableView.reloadData()
    }
    
    func addLocalNotiObserver()
    {
        let notificationName = Notification.Name("activityUpdated")
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationActivityUpdated(noti:)), name: notificationName, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationImageDeleted(noti:)), name: NSNotification.Name(rawValue: "imageDeleted"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationCommentAdded), name: NSNotification.Name(rawValue: "commentAdded"), object: nil)
    }
    
    func notificationActivityUpdated(noti : Notification)
    {
        let receivedObj : [String : Any] = noti.object as! [String : Any]
        
        activityPara = receivedObj["activity"] as! Para_Activity
        
        tableView.reloadData()
    }
    
    func notificationImageDeleted(noti : Notification)
    {
        let receivedObj : [String : Any] = noti.object as! [String : Any]
        
        activityPara.actImgJSONArr = receivedObj["data"] as! NSArray
        
        loadPhotosUrl()
    }
    
    func notificationCommentAdded()
    {
        let authToken : String = sharedObj.userPara.userToken
        print(authToken as Any)
        
        let headerObject = ["authtoken" : authToken]
        let requestObject : [String : Any] = [ "activity_id": activityPara.actId ] as [String : Any]
        
        let loader = startActivityIndicator(view: self.view, waitingMessage: "")
        loader.startAnimating()
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.GET_ACTIVITY_COMMENTS, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            print(responseData)
            if isSuccess
            {
                //self.showAlert(title: "Success!", msg: responseMessage)
                if responseData["comments"] != nil
                {
                    let array = responseData["comments"] as! NSArray
                    print("comments count ", array.count)
                    self.activityPara.actCommentsArray = array.mutableCopy() as! NSMutableArray
                    self.tableView.reloadData()
                }
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    // MARK: - Action Methods
    @IBAction func didTapBackButn()
    {        
        if isFromCreateActivity == true
        {
            self.performSegue(withIdentifier: "segueShowActivityToLandingPage", sender: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func didTapEditActivityButn()
    {
        isGoingInEditMode = true
        self.performSegue(withIdentifier: "segueShowActivityToCreateActivity", sender: nil)
    }
    
    @IBAction func didTapCancelButn()
    {
        let alert = UIAlertController.init(title: "Do you really want to cancel this activity?", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
            alert -> Void in
            
        }))
        alert.addAction(UIAlertAction.init(title: "Yes, Cancel activity!", style: .default, handler: {
            alert -> Void in
            
            self.callCancelActivityAPI()
        }))
       
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didTapInviteFrnds()
    {
        performSegue(withIdentifier: "segueShowActivityToInviteFrnds", sender: nil)
    }
    
    @IBAction func didTapGoingButn()
    {
        callGoingActivityAPI()
    }
    
    @IBAction func didTapInterestedButn()
    {
        callInterestedInActivityAPI()
    }
    
    @IBAction func didTapDoNotObserve()
    {
        // Not inetersted
        callNot_InterestedInActivityAPI()
    }
    
    @IBAction func didTapUnsubscribe()
    {
        // Call not interested and not going api
        callNot_GoingActivityAPI()
        //callNot_InterestedInActivityAPI()
    }
    
    @IBAction func didTapShareButn()
    {
        // text to share
        let text = "Looks like fun! Who's going to join us? https://www.gayandyou.de"
        
        // set up activity view controller
        let textToShare = [ text , URL.init(string: "www.maximess.com")!] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
       // activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func didTapUploadPhotos()
    {
        
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 10
        let customColor = UIColor.init(red: 64.0/255.0, green: 0.0, blue: 144.0/255.0, alpha: 1.0)
        let customCameraColor = UIColor.init(red: 86.0/255.0, green: 1.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customCameraColor
        pickerViewController.theme.cameraVeilColor = customCameraColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .lightContent
        
        
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    @IBAction func didTapReportActivity()
    {
        self.performSegue(withIdentifier: "segueShowActivityToReport", sender: nil)
    }
    
    @IBAction func didTapPrivaryPolicy()
    {
        showAlert(title: "Privacy Policy", msg: "If you upload photos make sure the people on the photos agree to the publication on GAY AND YOU?")
    }
    
    @IBAction func didTapCreateNewActivity()
    {
        isGoingInEditMode = false
        self.performSegue(withIdentifier: "segueShowActivityToCreateActivity", sender: nil)
    }
    
    @IBAction func didTapAddToCalendar()
    {
        let store = EKEventStore()
        
        // Request access to calendar first
        store.requestAccess(to: .event, completion: { (granted, error) in
            if granted {
                print("calendar allowed")
                
                // create the event object
                let event = EKEvent(eventStore: store)
                event.title = self.activityPara.actName
                event.startDate = self.activityPara.actDate
                event.endDate = self.activityPara.actDate.addingTimeInterval(3600) // 1 hr time
                event.location = self.activityPara.actLocatnStr
                event.calendar = store.defaultCalendarForNewEvents
                
                let controller = EKEventEditViewController()
                controller.event = event
                controller.eventStore = store
                controller.editViewDelegate = self
                self.present(controller, animated: true)
            }
            else
            {
                print("calendar not allowed")
            }           
        })
    }
    
    @IBAction func didTapShowLocation()
    {
        openMapForPlace()
    }
    
    @IBAction func didTapWriteCommentButn()
    {
        print("Added comment")
        self.performSegue(withIdentifier: "segueShowActivityToWriteComment", sender: nil)
    }
    
    @IBAction func didTapShowOrganiserProfile()
    {
        if self.userPara.userId == sharedObj.userPara.userId
        {
            let presentVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
            presentVC.isShowBackBtn = true
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
        else
        {
            let presentVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "UserProfile_VC") as! UserProfile_VC
            presentVC.user_Id = userPara.userId
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
    
    // MARK: - Calendar Delegates
    func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
        controller.dismiss(animated: true)
    }

    func openMapForPlace() {
        
        let latitude: CLLocationDegrees = activityPara.actLocatnLatitude// 37.2
        let longitude: CLLocationDegrees = activityPara.actLocatnLongitude// 22.9
        
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMake(coordinates, MKCoordinateSpanMake(0.01, 0.02))
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = activityPara.actName //"Place Name"
        mapItem.openInMaps(launchOptions: options)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueShowActivityToReport"
        {
            let reportView : ReportActivity_VC = segue.destination as! ReportActivity_VC
            reportView.activityPara = activityPara
        }
        else if segue.identifier == "segueShowActivityToInviteFrnds"
        {
            let inviteView : InviteFrndsToAct = segue.destination as! InviteFrndsToAct
            inviteView.activityId = activityPara.actId
        }
        else if segue.identifier == "segueShowActivityToImageView"
        {
            let imgView : ViewImage_VC = segue.destination as! ViewImage_VC
            imgView.pictureJSON = pictureJSONToSend
        }
        else if segue.identifier == "segueShowActivityToWriteComment"
        {
            let commentVC : WriteComment_VC = segue.destination as! WriteComment_VC
            commentVC.activityId = activityPara.actId
        }
        else if segue.identifier == "segueShowActivityToCreateActivity"
        {
            let createAct : CreateAct_MandatoryInfo = segue.destination as! CreateAct_MandatoryInfo
            
            createAct.isEditMode = isGoingInEditMode
            
            if createAct.isEditMode == false
            {
                createAct.activityPara = Para_Activity()
            }
            else
            {
                createAct.activityPara = activityPara
                createAct.activityPara.actCategoryStr = sharedObj.arrCat[activityPara.actCategoryId]
                
                let dateFormatter = DateFormatter()
                if activityPara.actIsFlexible == true
                {
                    dateFormatter.dateFormat = "dd MMM YYYY"
                }
                else
                {
                    dateFormatter.dateFormat = "dd MMM YYYY hh:mm a"
                }
                
                createAct.strDate = dateFormatter.string(from: activityPara.actDate)
            }
        }
    }
}

// MARK: - API Methods
extension ShowActivity_VC
{
    func callCancelActivityAPI()
    {
        let authToken : String = sharedObj.userPara.userToken
        
        let headerObject = ["authtoken" : authToken]
        let requestObject = ["activity_id" : activityPara.actId]
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.CANCEL_ACTIVITY, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.navigationController?.popViewController(animated: true)
                self.showAlert(title: "Activity Cancelled Successfully!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func callUploadPhotosAPI()
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
 
        /*
        let mainParamter : [String:Any] = ["activity_id" : self.activityPara.actId, "image_element_id" :"imageupload"]
        
        var jsonData: Data!
        do
        {
            jsonData = try JSONSerialization.data(withJSONObject: mainParamter, options: [])
        }
        catch _ {
        }
        
        var jsonString: String
        
        if jsonData != nil
        {
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
        }
        else
        {
            NSLog("Got an error: ")
            jsonString = ""
        }
        
        let url = try! URLRequest(url: APP_URL.BASE_URL + ACTIVITY_URL.ADD_ACT_IMG_MULTIPART, method: .post, headers: headerObject)
//        let loader = startActivityIndicator(view: self.view, waitingMessage: "Uploading Images " + "\(i + 1)/\(self.imagesArray.count)")

        let loader = startActivityIndicator(view: self.view, waitingMessage: "Uploading Images")
        loader.startAnimating()

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                    for i in 0...self.imagesArray.count-1
                    {
                    print("added\(i)")
                        
                    let imageData = UIImagePNGRepresentation(self.imagesArray[i])!
                    multipartFormData.append( imageData, withName: "imageupload", fileName: "uploadImage\(i).png", mimeType: "image/png")
                }
                
                multipartFormData.append(jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: "request")
                
        },
            with: url,
            encodingCompletion: { encodingResult in
                
                switch encodingResult
                {
                case .success(let upload, _, _):
                    upload.responseJSON
                        { response in
                            print(response)
                        if((response.result.value) != nil)
                        {
                            let dict = response.result.value! as! NSDictionary
                            let responseMessage = dict.value(forKey: "message") as! String
                            let status = dict.value(forKey: "status") as! Int
                            if status == 200
                            {
                                let activityJSON : [String : Any] = dict.value(forKey: "activity") as! [String : Any]
                                self.activityPara.actImgJSONArr = NSArray()
                                self.imgUrlArray.removeAll()
                                self.activityPara.actImgJSONArr = activityJSON["images"] as! NSArray
                                self.loadPhotosUrl()
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Fail!", message: responseMessage, preferredStyle: UIAlertControllerStyle.alert)
                                loader.stopAnimating()
                                self.present(alert, animated: true, completion: nil)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                    
                                    ACTION in
                                    
                                }))
                            }
                            
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Fail!", message: "Connection timed out.", preferredStyle: UIAlertControllerStyle.alert)
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                
                                ACTION in
                                
                            }))
                            loader.stopAnimating()
                        }
                    }
                case .failure( _):
                    loader.stopAnimating()
                    //                    print(Error.self)
                    break
                }
        })
*/
       
        for index in 0 ... imagesArray.count - 1
        {
            let image = imagesArray[index] 

            //Now use image to create into NSData format
            let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
            let base64String = imageData.base64EncodedString(options: .lineLength64Characters)
            
            let requestObject : [String : Any] = ["activity_id" : self.activityPara.actId,
                                                  "image_base_64" : base64String]
            
            let loader = startActivityIndicator(view: self.view, waitingMessage: "Uploading Image " + "\(index + 1)/\(imagesArray.count)")
            loader.startAnimating()
            
            GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.ADD_ACTIVITY_IMG, header: headerObject, parameter: requestObject, completionHandler:
                {
                    (isSuccess,responseMessage,responseData) -> Void in
                    
                    print(responseData)
                    loader.stopAnimating()
                    
                    if isSuccess
                    {
                        //self.showAlert(title: "Success!", msg: responseMessage)
                        let activityJSON : [String : Any] = responseData["activity"] as! [String : Any]
                        self.activityPara.actImgJSONArr = NSArray()
                        self.imgUrlArray.removeAll()
                        self.activityPara.actImgJSONArr = activityJSON["images"] as! NSArray
                        self.loadPhotosUrl()
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
            })
        }
    }
    
    func callInterestedInActivityAPI()
    {
        let authToken : String = sharedObj.userPara.userToken
        
        let headerObject = ["authtoken" : authToken]
        let requestObject = ["activity_id" : activityPara.actId]
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.ACT_INTERESTED, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            print(responseData)
            if isSuccess
            {
                //self.showAlert(title: "Interested In!", msg: responseMessage)
                self.activityPara.isInterested = true
                
                let data : [String:Any] = responseData["data"] as! [String:Any]
                if data["interested"] != nil
                {
                    let array = data["interested"] as! [[String:Any]]
                    self.activityPara.actInterstedArray = array
                    self.activityPara.actUserCombineArray = self.activityPara.actGoingArray + self.activityPara.actInterstedArray
                }

                self.tableView.reloadData()
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func callNot_InterestedInActivityAPI()
    {
        let authToken : String = sharedObj.userPara.userToken
        
        let headerObject = ["authtoken" : authToken]
        let requestObject = ["activity_id" : activityPara.actId]
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.ACT_NOT_INTERESTED, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            print(responseData)
            if isSuccess
            {
                self.activityPara.isInterested = false
                
                if responseData["interested"] != nil
                {
                    let array = responseData["interested"] as! [[String:Any]]
                    self.activityPara.actInterstedArray = array
                    self.activityPara.actUserCombineArray = self.activityPara.actGoingArray + self.activityPara.actInterstedArray
                }
                self.tableView.reloadData()
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func callGoingActivityAPI()
    {
        let authToken : String = sharedObj.userPara.userToken
        
        let headerObject = ["authtoken" : authToken]
        let requestObject = ["activity_id" : activityPara.actId]
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.ACT_GOING, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.activityPara.isGoing = true
                
                let alert = UIAlertController.init(title: "YEAH! You are in!", message: "Share your activity on Facebook and invite some friends.", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "No, Thanks", style: .default, handler: {
                    alert -> Void in
                    
                }))
                
                alert.addAction(UIAlertAction.init(title: "Post", style: .default, handler: {
                    alert -> Void in
                    
                }))
                
                print(responseData)
                self.present(alert, animated: true, completion:
                    {
                        let data : [String:Any] = responseData["data"] as! [String:Any]
                        if data["going"] != nil
                        {
                            let array = data["going"] as! [[String:Any]]
                            self.activityPara.actGoingArray = array
                            self.activityPara.actUserCombineArray = array + self.activityPara.actInterstedArray
                        }

                        self.tableView.reloadData()
                })
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }

    func callNot_GoingActivityAPI()
    {
        let authToken : String = sharedObj.userPara.userToken
        
        let headerObject = ["authtoken" : authToken]
        let requestObject = ["activity_id" : activityPara.actId]
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.ACT_NOT_GOING, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            print(responseData)
            if isSuccess
            {
                self.activityPara.isGoing = false
                
                if responseData["going"] != nil
                {
                    let array = responseData["going"] as! [[String:Any]]
                    self.activityPara.actGoingArray = array
                    self.activityPara.actUserCombineArray = self.activityPara.actGoingArray + self.activityPara.actInterstedArray
                }
                self.tableView.reloadData()
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
}

// MARK: - UI Methods
extension ShowActivity_VC
{
    func firstHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 180))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let containerView = UIImageView.init(frame: CGRect(x: 0, y: 2, width: view.frame.size.width, height: headerView.frame.size.height - 2))
        containerView.backgroundColor = UIColor.gray
        containerView.contentMode = .scaleAspectFill
        containerView.clipsToBounds = true
       // let catId = activityJSON["category_id"] as! Int
        if sharedObj.arrCatImages.count > activityPara.actCategoryId
        {
            containerView.image = sharedObj.arrCatImages[activityPara.actCategoryId]
        }
        
        let btnUserImage = UIImageView.init(frame: CGRect(x: view.frame.size.width / 2 - 50, y: 15, width: 100, height: 100))
        btnUserImage.backgroundColor = UIColor.white
        btnUserImage.layer.cornerRadius = 50
        btnUserImage.layer.borderColor = UIColor.white.cgColor
        btnUserImage.contentMode = .scaleToFill
        btnUserImage.clipsToBounds = true

        let url = URL(string: APP_URL.BASE_URL + userPara.userPictureThumbUrl )!
        let placeholderImage = #imageLiteral(resourceName: "avatar_big")
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: 60.0, height: 60.0), radius: 50)
        btnUserImage.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
            { response in
                
        })
        

        let nameContainer = UILabel.init(frame: CGRect(x: view.frame.size.width / 4, y: btnUserImage.frame.origin.y + btnUserImage.frame.size.height + 10, width: view.frame.size.width / 2, height: 40))
        nameContainer.backgroundColor = sharedObj.hexStringToUIColor(hex: "efefef")
        nameContainer.layer.cornerRadius = 5
        nameContainer.font = UIFont.init(name: Font.BEBAS, size: 12)
        nameContainer.clipsToBounds = true
        
        if userPara.isPlusMember == 1
        {
            let lblLeft = UILabel(frame: CGRect(x: 5, y: 0, width: nameContainer.frame.size.width - 50, height: nameContainer.frame.size.height))
            lblLeft.textAlignment = .center
            lblLeft.font = UIFont.init(name: Font.BEBAS, size: 13)
            lblLeft.text = userPara.userFirstName + " " + userPara.userLastName
            nameContainer.addSubview(lblLeft)
            
            let iconStar = UIImageView.init(frame: CGRect(x:nameContainer.frame.size.width - 35, y: 5, width: 30, height: 30))
            //iconStar.backgroundColor = sharedObj.hexStringToUIColor(hex: "2196f3")
            iconStar.image = #imageLiteral(resourceName: "heart_wings_40")
            nameContainer.addSubview(iconStar)
            
            let verticalSep = UIView.init(frame: CGRect(x:iconStar.frame.origin.x - 5, y: 0, width: 1, height: nameContainer.frame.size.height))
            verticalSep.backgroundColor = UIColor.gray
            nameContainer.addSubview(verticalSep)
        }
        else
        {
            let lblLeft = UILabel(frame: CGRect(x: 5, y: 0, width: nameContainer.frame.size.width - 10, height: nameContainer.frame.size.height))
            lblLeft.textAlignment = .center
            lblLeft.font = UIFont.init(name: Font.BEBAS, size: 13)
            lblLeft.text = userPara.userFirstName + " " + userPara.userLastName
            nameContainer.addSubview(lblLeft)
        }
        
        // If activity has expired, don't show the
        if activityPara.actDate < Date()
        {
            let grayBgView = UIView.init(frame: CGRect.init(x: 0, y: 30, width: self.view.frame.size.width, height: 90))
            grayBgView.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
            
            let lblExpired = UILabel(frame: CGRect(x: 5, y: 5, width: grayBgView.frame.size.width - 10, height: 20))
            lblExpired.textAlignment = .center
            lblExpired.textColor = UIColor.white
            lblExpired.font = UIFont.init(name: Font.BEBAS_Bold, size: 18)
            lblExpired.text = "This activity has expired."
            grayBgView.addSubview(lblExpired)
            
            let btnCreate = UIButton.init(frame: CGRect(x: 5, y: 30, width: view.frame.size.width - 10, height: 50))
            btnCreate.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
            btnCreate.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
            btnCreate.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
            btnCreate.addTarget(self, action: #selector(self.didTapCreateNewActivity), for: .touchUpInside)
            btnCreate.setTitleColor(UIColor.white, for: .normal)
            btnCreate.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 17)
            btnCreate.setTitle("CREATE ACTIVITY", for: .normal)
            btnCreate.layer.borderColor = UIColor.white.cgColor
            grayBgView.addSubview(btnCreate)
        
            containerView.addSubview(grayBgView)

            let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.didTapCreateNewActivity))
            headerView.addGestureRecognizer(tap)
            
        }
        else
        {
            containerView.addSubview(btnUserImage)
            containerView.addSubview(nameContainer)

            let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.didTapShowOrganiserProfile))
            headerView.addGestureRecognizer(tap)
        }

        //headerView.addSubview(containerView)
        //return headerView
        
        let customCell : ShowActivityHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FirstHeaderTableViewCell") as! ShowActivityHeaderTableViewCell
        
        return customCell
    }
    
    func secondHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 110))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "ffffff")
        
        if userPara.userId == sharedObj.userPara.userId
        {
            let btnEdit = UIButton.init(frame: CGRect(x: 5, y: 10, width: view.frame.size.width - 10, height: 50))
            btnEdit.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
            btnEdit.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
            btnEdit.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
            btnEdit.addTarget(self, action: #selector(self.didTapEditActivityButn), for: .touchUpInside)
            btnEdit.setTitleColor(UIColor.white, for: .normal)
            btnEdit.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 17)
            btnEdit.setTitle("EDIT", for: .normal)
            //btnEdit.layer.cornerRadius = 5
            btnEdit.layer.borderColor = UIColor.white.cgColor
            headerView.addSubview(btnEdit)
            
            let btnCancel   = UIButton(type: .custom)
            btnCancel.frame =  CGRect(x: 5, y: btnEdit.frame.origin.y + btnEdit.frame.size.height + 5, width: view.frame.size.width / 2 - 7.5, height: 40)
            btnCancel.backgroundColor = UIColor.white
            btnCancel.setTitleColor(sharedObj.hexStringToUIColor(hex: "9E9E9E"), for: .normal)
            btnCancel.layer.borderColor = sharedObj.hexStringToUIColor(hex: "9e9e9e").cgColor
            btnCancel.imageEdgeInsets = UIEdgeInsets(top: 4, left: -10, bottom: 4, right: 0)
            btnCancel.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0)
            btnCancel.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
            btnCancel.setImage(#imageLiteral(resourceName: "decline_request"), for: .normal)
            btnCancel.setTitle("Cancel activity", for: .normal)
            
            btnCancel.layer.borderWidth = 1.0
            //btnCancel.layer.cornerRadius = 5.0
            btnCancel.clipsToBounds = true
            btnCancel.addTarget(self, action: #selector(self.didTapCancelButn), for: UIControlEvents.touchUpInside)
            headerView.addSubview(btnCancel)
            
            
            let btnInvite   = UIButton(type: .custom)
            btnInvite.frame =  CGRect(x: btnCancel.frame.origin.x + btnCancel.frame.size.width + 5, y: btnCancel.frame.origin.y , width: view.frame.size.width / 2 - 7.5, height: 40)
            //btnCancel.tintColor = UIColor.white
            btnInvite.backgroundColor = UIColor.white
            btnInvite.setTitleColor(sharedObj.hexStringToUIColor(hex: "9E9E9E"), for: .normal)
            btnInvite.layer.borderColor = sharedObj.hexStringToUIColor(hex: "9e9e9e").cgColor
            
            btnInvite.imageEdgeInsets = UIEdgeInsets(top: 4, left: -10, bottom: 4, right: 0)
            btnInvite.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0)
            btnInvite.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
            btnInvite.setImage(#imageLiteral(resourceName: "invite"), for: .normal)
            btnInvite.setTitle("Invite", for: .normal)
            
            btnInvite.layer.borderWidth = 1.0
            //btnInvite.layer.cornerRadius = 5.0
            btnInvite.clipsToBounds = true
            btnInvite.addTarget(self, action: #selector(didTapInviteFrnds), for: .touchUpInside)
            headerView.addSubview(btnInvite)
            
            headerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 110)
        }
        else
        {
            // Here also check going to and interested variables
            
            // Normal condition
            if activityPara.isGoing == false && activityPara.isInterested == false
            {
                let btnGoing = UIButton.init(frame: CGRect(x: 5, y: 10, width: view.frame.size.width - 10, height: 50))
                btnGoing.backgroundColor = UIColor.green
                btnGoing.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
                btnGoing.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
                btnGoing.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
                btnGoing.addTarget(self, action: #selector(self.didTapGoingButn), for: .touchUpInside)
                btnGoing.setTitleColor(UIColor.white, for: .normal)
                btnGoing.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 17)
                btnGoing.setTitle("ARE YOU JOINING?", for: .normal)
                headerView.addSubview(btnGoing)
                
                let btnInterested   = UIButton(type: .custom)
                btnInterested.frame =  CGRect(x: 5, y: btnGoing.frame.origin.y + btnGoing.frame.size.height + 5, width: view.frame.size.width / 2 - 7.5, height: 40)
                btnInterested.backgroundColor = UIColor.white
                btnInterested.setTitleColor(sharedObj.hexStringToUIColor(hex: "9E9E9E"), for: .normal)
                btnInterested.layer.borderColor = sharedObj.hexStringToUIColor(hex: "9e9e9e").cgColor
                btnInterested.imageEdgeInsets = UIEdgeInsets(top: 4, left: -10, bottom: 4, right: 0)
                btnInterested.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0)
                btnInterested.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
                btnInterested.setImage(#imageLiteral(resourceName: "interested"), for: .normal)
                btnInterested.setTitle("Interested", for: .normal)
                btnInterested.layer.borderWidth = 1.0
                //btnInterested.layer.cornerRadius = 5.0
                btnInterested.clipsToBounds = true
                btnInterested.addTarget(self, action: #selector(self.didTapInterestedButn), for: UIControlEvents.touchUpInside)
                headerView.addSubview(btnInterested)
                
                
                let btnShare   = UIButton(type: .custom)
                btnShare.frame =  CGRect(x: btnInterested.frame.origin.x + btnInterested.frame.size.width + 5, y: btnInterested.frame.origin.y , width: view.frame.size.width / 2 - 7.5, height: 40)
                btnShare.backgroundColor = UIColor.white
                btnShare.setTitleColor(sharedObj.hexStringToUIColor(hex: "9E9E9E"), for: .normal)
                btnShare.layer.borderColor = sharedObj.hexStringToUIColor(hex: "9e9e9e").cgColor
                
                btnShare.imageEdgeInsets = UIEdgeInsets(top: 4, left: -10, bottom: 4, right: 0)
                btnShare.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0)
                btnShare.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
                btnShare.setImage(#imageLiteral(resourceName: "share"), for: .normal)
                btnShare.setTitle("Share", for: .normal)
                btnShare.layer.borderWidth = 1.0
                //btnShare.layer.cornerRadius = 5.0
                btnShare.clipsToBounds = true
                btnShare.addTarget(self, action: #selector(didTapShareButn), for: .touchUpInside)
                headerView.addSubview(btnShare)
                
                headerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 110)
            }
            else if activityPara.isGoing == false && activityPara.isInterested == true
            {
                let btnGoing = UIButton.init(frame: CGRect(x: 5, y: 10, width: view.frame.size.width - 10, height: 50))
                btnGoing.backgroundColor = UIColor.green
                btnGoing.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
                btnGoing.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
                btnGoing.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
                btnGoing.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 17)
                btnGoing.addTarget(self, action: #selector(self.didTapGoingButn), for: .touchUpInside)
                btnGoing.setTitleColor(UIColor.white, for: .normal)
                btnGoing.setTitle("ARE YOU JOINING?", for: .normal)
                headerView.addSubview(btnGoing)
                
                let btnDoNotObserve   = UIButton(type: .custom)
                btnDoNotObserve.frame =  CGRect(x: 5, y: btnGoing.frame.origin.y + btnGoing.frame.size.height + 5, width: view.frame.size.width / 2 - 7.5, height: 40)
                //btnCancel.tintColor = UIColor.white
                btnDoNotObserve.backgroundColor = UIColor.white
                btnDoNotObserve.setTitleColor(sharedObj.hexStringToUIColor(hex: "9E9E9E"), for: .normal)
                btnDoNotObserve.layer.borderColor = sharedObj.hexStringToUIColor(hex: "9e9e9e").cgColor
                btnDoNotObserve.imageEdgeInsets = UIEdgeInsets(top: 4, left: -10, bottom: 4, right: 0)
                btnDoNotObserve.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0)
                btnDoNotObserve.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
                btnDoNotObserve.setImage(#imageLiteral(resourceName: "decline_request"), for: .normal)
                btnDoNotObserve.setTitle("Do not observe", for: .normal)
                btnDoNotObserve.layer.borderWidth = 1.0
                //btnDoNotObserve.layer.cornerRadius = 5.0
                btnDoNotObserve.clipsToBounds = true
                btnDoNotObserve.addTarget(self, action: #selector(self.didTapDoNotObserve), for: UIControlEvents.touchUpInside)
                headerView.addSubview(btnDoNotObserve)
                
                
                let btnShare   = UIButton(type: .custom)
                btnShare.frame =  CGRect(x: btnDoNotObserve.frame.origin.x + btnDoNotObserve.frame.size.width + 5, y: btnDoNotObserve.frame.origin.y , width: view.frame.size.width / 2 - 7.5, height: 40)
                //btnCancel.tintColor = UIColor.white
                btnShare.backgroundColor = UIColor.white
                btnShare.setTitleColor(sharedObj.hexStringToUIColor(hex: "9E9E9E"), for: .normal)
                btnShare.layer.borderColor = sharedObj.hexStringToUIColor(hex: "9e9e9e").cgColor
                
                btnShare.imageEdgeInsets = UIEdgeInsets(top: 4, left: -10, bottom: 4, right: 0)
                btnShare.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0)
                btnShare.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
                btnShare.setImage(#imageLiteral(resourceName: "share"), for: .normal)
                btnShare.setTitle("Share", for: .normal)
                
                btnShare.layer.borderWidth = 1.0
                //btnShare.layer.cornerRadius = 5.0
                btnShare.clipsToBounds = true
                btnShare.addTarget(self, action: #selector(didTapShareButn), for: .touchUpInside)
                headerView.addSubview(btnShare)
                
                headerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 110)
            }
            else if activityPara.isGoing == true
            {
                let btnUnsubscribe   = UIButton(type: .custom)
                btnUnsubscribe.frame =  CGRect(x: 5, y:  5, width: view.frame.size.width / 2 - 7.5, height: 40)
                //btnCancel.tintColor = UIColor.white
                btnUnsubscribe.backgroundColor = UIColor.white
                btnUnsubscribe.setTitleColor(sharedObj.hexStringToUIColor(hex: "9E9E9E"), for: .normal)
                btnUnsubscribe.layer.borderColor = sharedObj.hexStringToUIColor(hex: "9e9e9e").cgColor
                
                btnUnsubscribe.imageEdgeInsets = UIEdgeInsets(top: 4, left: -10, bottom: 4, right: 0)
                btnUnsubscribe.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0)
                btnUnsubscribe.setImage(#imageLiteral(resourceName: "decline_request"), for: .normal)
                btnUnsubscribe.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
                btnUnsubscribe.setTitle("Unsubscribe", for: .normal)
                
                btnUnsubscribe.layer.borderWidth = 1.0
                //btnUnsubscribe.layer.cornerRadius = 5.0
                btnUnsubscribe.clipsToBounds = true
                btnUnsubscribe.addTarget(self, action: #selector(self.didTapUnsubscribe), for: UIControlEvents.touchUpInside)
                headerView.addSubview(btnUnsubscribe)
                
                
                let btnShare   = UIButton(type: .custom)
                btnShare.frame =  CGRect(x: btnUnsubscribe.frame.origin.x + btnUnsubscribe.frame.size.width + 5, y: btnUnsubscribe.frame.origin.y , width: view.frame.size.width / 2 - 7.5, height: 40)
                //btnCancel.tintColor = UIColor.white
                btnShare.backgroundColor = UIColor.white
                btnShare.setTitleColor(sharedObj.hexStringToUIColor(hex: "9E9E9E"), for: .normal)
                btnShare.layer.borderColor = sharedObj.hexStringToUIColor(hex: "9e9e9e").cgColor
                
                btnShare.imageEdgeInsets = UIEdgeInsets(top: 4, left: -10, bottom: 4, right: 0)
                btnShare.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0)
                btnShare.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
                btnShare.setImage(#imageLiteral(resourceName: "share"), for: .normal)
                btnShare.setTitle("Share", for: .normal)
                
                btnShare.layer.borderWidth = 1.0
                //btnShare.layer.cornerRadius = 5.0
                btnShare.clipsToBounds = true
                btnShare.addTarget(self, action: #selector(didTapShareButn), for: .touchUpInside)
                headerView.addSubview(btnShare)
                
                headerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50)
            }
        }
        
        return headerView
    }
    
    func thirdHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 130))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let adsView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        adsView.backgroundColor = UIColor.white
        adsView.addSubview(adBannerView)
        headerView.addSubview(adsView)
        
        let commentView = UIView.init(frame: CGRect(x: 5, y: adsView.frame.origin.y + adsView.frame.size.height, width: view.frame.size.width - 10, height: 50))
        commentView.backgroundColor = UIColor.white
        
        let iconComment = UIImageView.init(frame: CGRect(x: 20, y: 10, width: 30, height: 30))
        iconComment.image = #imageLiteral(resourceName: "comment")
        commentView.addSubview(iconComment)
        
        let lblCount = UILabel(frame: CGRect(x: iconComment.frame.origin.x + iconComment.frame.size.width + 5, y: iconComment.frame.origin.y, width: view.frame.size.width / 2, height: 30))
        lblCount.textAlignment = .left
        lblCount.font = UIFont.init(name: Font.BEBAS, size: 18)
        lblCount.text = "\(activityPara.actCommentsArray.count)" + " comments"
        commentView.addSubview(lblCount)
        
        let btnShowAll = UIButton.init(frame: CGRect(x: view.frame.size.width / 2 - 20, y: iconComment.frame.origin.y, width: view.frame.size.width / 2 - 10, height: 30))
        //btnShowAll.backgroundColor = sharedObj.hexStringToUIColor(hex: "ffffff")
        btnShowAll.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnShowAll.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnShowAll.addTarget(self, action: #selector(self.didTapWriteCommentButn), for: .touchUpInside)
        btnShowAll.setTitleColor(sharedObj.hexStringToUIColor(hex: "2196f3"), for: .normal)
        btnShowAll.contentHorizontalAlignment = .right
        btnShowAll.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
        btnShowAll.setTitle("Show all", for: .normal)
        commentView.addSubview(btnShowAll)
        
        headerView.addSubview(commentView)
        
        //print(commentView.frame.origin.y + commentView.frame.size.height, "comments")
        return headerView
    }
    
    func cellSubView1() -> UIView
    {
        let containerAddAct = UIButton.init(frame: CGRect(x: 10, y: 10, width: view.frame.size.width - 20, height: 60))
        containerAddAct.backgroundColor = UIColor.init(red: 98/255, green: 251/255, blue: 217/255, alpha: 1.0)
        //sharedObj.hexStringToUIColor(hex: "00edbf")//UIColor.init(red: 19/255, green: 42/255, blue: 55/255, alpha: 1.0)
        containerAddAct.addTarget(self, action: #selector(self.didTapCreateNewActivity), for: .touchUpInside)
        
        let iconAdd = UIImageView.init(frame: CGRect(x: 10, y: 10, width: 40, height: 40))
        iconAdd.image = #imageLiteral(resourceName: "add_your_own")
        iconAdd.layer.cornerRadius = iconAdd.frame.size.width / 2
        iconAdd.clipsToBounds = true
        
        let lblNotThing = UILabel(frame: CGRect(x: iconAdd.frame.origin.x + iconAdd.frame.size.width + 10, y: 5, width: view.frame.size.width - 100, height: 20))
        lblNotThing.textAlignment = .left
        lblNotThing.textColor = UIColor.white
        lblNotThing.font = UIFont.init(name: "AppleSDGothicNeo-Bold", size: 18)
        lblNotThing.text = "Not your thing?"
        
        let lblDoOwn = UILabel(frame: CGRect(x: lblNotThing.frame.origin.x, y: 30, width: view.frame.size.width - 100, height: 20))
        lblDoOwn.textAlignment = .left
        lblDoOwn.textColor = UIColor.white
        lblDoOwn.font = UIFont.init(name: "AppleSDGothicNeo-Bold", size: 15)
        lblDoOwn.text = "Do your own!"
        
        let iconArrow1 = UIImageView.init(frame: CGRect(x: containerAddAct.frame.size.width - 50, y: 15, width: 30, height: 30))
        iconArrow1.image = #imageLiteral(resourceName: "arrow_right_white")
        
        containerAddAct.addSubview(iconAdd)
        containerAddAct.addSubview(lblNotThing)
        containerAddAct.addSubview(lblDoOwn)
        containerAddAct.addSubview(iconArrow1)
        
        return containerAddAct
    }
    
    func cellSubView2() -> UIView
    {
        let containerAddAct = UIButton.init(frame: CGRect(x: 10, y: 10 + 60 + 5, width: view.frame.size.width - 20, height: 60))
        containerAddAct.backgroundColor = UIColor.white
        containerAddAct.addTarget(self, action: #selector(self.didTapAddToCalendar), for: .touchUpInside)
        
        let iconAdd = UIImageView.init(frame: CGRect(x: 10, y: 10, width: 40, height: 40))
        iconAdd.image = #imageLiteral(resourceName: "calendar_activity")
        
        let lblNotThing = UILabel(frame: CGRect(x: iconAdd.frame.origin.x + iconAdd.frame.size.width + 10, y: 5, width: view.frame.size.width - 100, height: 20))
        lblNotThing.textAlignment = .left
        lblNotThing.font = UIFont.init(name: Font.BEBAS, size: 18)
        lblNotThing.text = "Add to calendar"
        
        let lblDoOwn = UILabel(frame: CGRect(x: lblNotThing.frame.origin.x, y: 30, width: view.frame.size.width - 100, height: 30))
        lblDoOwn.textAlignment = .left
        lblDoOwn.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblDoOwn.text = actDateFormatter.string(from: activityPara.actDate) // "Tuesday, 06/06/2017"
        
        let iconArrow1 = UIImageView.init(frame: CGRect(x: containerAddAct.frame.size.width - 50, y: 15, width: 30, height: 30))
        iconArrow1.image = #imageLiteral(resourceName: "arrow_right")
        
        containerAddAct.addSubview(iconAdd)
        containerAddAct.addSubview(lblNotThing)
        containerAddAct.addSubview(lblDoOwn)
        containerAddAct.addSubview(iconArrow1)
        
        return containerAddAct
    }
    
    func cellSubView3() -> UIView
    {
        let containerAddAct = UIButton.init(frame: CGRect(x: 10, y: 75 + 60 + 5, width: view.frame.size.width - 20, height: 60))
        containerAddAct.backgroundColor = UIColor.white
        containerAddAct.addTarget(self, action: #selector(self.didTapShowLocation), for: .touchUpInside)
        
        let iconAdd = UIImageView.init(frame: CGRect(x: 10, y: 10, width: 40, height: 40))
        iconAdd.image = #imageLiteral(resourceName: "location_show")
        
        let lblNotThing = UILabel(frame: CGRect(x: iconAdd.frame.origin.x + iconAdd.frame.size.width + 10, y: 5, width: view.frame.size.width - 100, height: 20))
        lblNotThing.textAlignment = .left
        lblNotThing.font = UIFont.init(name: Font.BEBAS, size: 18)
        lblNotThing.text = "See meeting point"
        
        let lblDoOwn = MarqueeLabel(frame: CGRect(x: lblNotThing.frame.origin.x, y: 30, width: containerAddAct.frame.size.width - lblNotThing.frame.origin.x - 50 - 5, height: 20))
        lblDoOwn?.textAlignment = .left
        lblDoOwn?.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblDoOwn?.text = activityPara.actLocatnStr
        
        let iconArrow1 = UIImageView.init(frame: CGRect(x: containerAddAct.frame.size.width - 50, y: 15, width: 30, height: 30))
        iconArrow1.image = #imageLiteral(resourceName: "arrow_right")
        
        containerAddAct.addSubview(iconAdd)
        containerAddAct.addSubview(lblNotThing)
        containerAddAct.addSubview(lblDoOwn!)
        containerAddAct.addSubview(iconArrow1)
        
        //print(containerAddAct.frame.size.height + containerAddAct.frame.origin.y, "meeting point")
        
        return containerAddAct
    }
}

// MARK: - TableView Datasource and Delegates

extension ShowActivity_VC : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            return 5
        }
        else if section == 2
        {
            if activityPara.actCommentsArray.count >= 3
            {
                return 3
            }
            else
            {
                return activityPara.actCommentsArray.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if  section == 0
        {
            return 250
        }
        else if section == 1
        {
            // If activity has expired
            if activityPara.actDate < Date()
            {
                return 0
            }
            else
            {
                if userPara.userId == sharedObj.userPara.userId
                {
                    return 110
                }
                else
                {
                    if activityPara.isGoing == false && activityPara.isInterested == false
                    {
                        return 110
                    }
                    else if activityPara.isGoing == false && activityPara.isInterested == true
                    {
                        return 110
                    }
                    else if activityPara.isGoing == true
                    {
                        return 50
                    }
                }
            }
            return 0
        }
        else if section == 2
        {
            return 130
        }
        else if section == 3
        {
            return 45
        }
        else if section == 4
        {
            return 136
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 1
        {
            // Time, location etc
            if indexPath.row == 0
            {
                return 82
            }
                // Activity name, desc, report
            else if indexPath.row == 1
            {
                return UITableViewAutomaticDimension //185
            }
                // Photo Upload, privacy policy
            else if indexPath.row == 2
            {
                if imgUrlArray.count == 0
                {
                    return 216
                }
                else
                {
                    return 250
                }
            }
                // Attendees
            else if indexPath.row == 3
            {
                return 120
            }
                // Add act, calendar, meeting point
            else if indexPath.row == 4
            {
                return 215
            }
        }
        else //if indexPath.section == 2
        {
            return 80 //UITableViewAutomaticDimension
        }
        
        return 120
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()//.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        headerView.backgroundColor = UIColor.clear
        
        if section == 0
        {
            let customCell : ShowActivityHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FirstHeaderTableViewCell") as! ShowActivityHeaderTableViewCell
            
            // Set category image
            if sharedObj.arrCatImages.count > activityPara.actCategoryId && activityPara.actCategoryId != 8
            {
                customCell.imgViewCatImg.image = sharedObj.arrCatImages [activityPara.actCategoryId]
                customCell.imgViewCatImg.clipsToBounds = true

                //customCell.butnCategoryImg.setBackgroundImage( sharedObj.arrCatImages [activityPara.actCategoryId] , for: .normal)
            }
            else if activityPara.actCategoryId == 8
            {
                if activityPara.actCategoryImgURL != ""
                {
                    customCell.imgViewCatImg.af_setImage(withURL: URL.init(string: APP_URL.BASE_URL + activityPara.actCategoryImgURL)!)
                    customCell.imgViewCatImg.clipsToBounds = true
                }
                else
                {
                    customCell.imgViewCatImg.image = sharedObj.arrCatImages [activityPara.actCategoryId]
                    customCell.imgViewCatImg.clipsToBounds = true
                }
            }
            
            // Set user image
            let url = URL(string: APP_URL.BASE_URL + userPara.userPictureThumbUrl )!
            let placeholderImage = #imageLiteral(resourceName: "avatar_big")
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: 60.0, height: 60.0), radius: 30)
            
            
            
            
            if userPara.userPictureThumbUrl != ""
            {
                customCell.imgViewUserImage.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                    { response in
                        
                })
            }
            //customCell.butnUserImage.clipsToBounds = true

            // Set user name
            customCell.lblUserName.text = userPara.userFirstName + " " + userPara.userLastName

            // Set wings icon
            if userPara.isPlusMember == 1
            {
                customCell.butnHeartWings.isHidden = false
            }
            else
            {
                customCell.butnHeartWings.isHidden = true
            }
            
            // If activity is expired, show below things
            if activityPara.actDate < Date()
            {
                customCell.viewExpired.isHidden = false
            }
            else
            {
                customCell.viewExpired.isHidden = true
            }
            
            return customCell
//            headerView.addSubview(firstHeaderView(headerView: headerView))
        }
        else if section == 1
        {
            headerView.addSubview(secondHeaderView(headerView: headerView))
        }
        else if section == 2
        {
            headerView.addSubview(thirdHeaderView(headerView: headerView))
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if  indexPath.section == 1
        {
            if indexPath.row == 1
            {
                let customCell : ShowActivityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ShowActivityTableViewCell", for: indexPath) as! ShowActivityTableViewCell
                customCell.selectionStyle = .none
                customCell.lblActName.text = activityPara.actName
                customCell.lblActName.sizeToFit()

                if isFromCreateActivity == true
                {
                    customCell.lblCreatedTime.text = "Created: just now"
                }
                else
                {
                    customCell.lblCreatedTime.text = "Created: " + actDateFormatter.string(from: Date.init(timeIntervalSince1970: Double(activityPara.actCreatedEpoch)))
                }
                
                customCell.lblDesc.text = activityPara.actDesc
                
                return customCell
            }
            else
            {
                var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
                
                cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                if indexPath.row == 0
                {
                    let iconTime = UIImageView.init(frame: CGRect(x: 20, y: 5, width: 15, height: 15))
                    //iconTime.backgroundColor = UIColor.black
                    iconTime.image = #imageLiteral(resourceName: "clock")
                    
                    let lblTime = UILabel(frame: CGRect(x: 40, y: 5, width: view.frame.size.width - 100, height: 15))
                    lblTime.textAlignment = .left
                    lblTime.font = UIFont.init(name: Font.BEBAS, size: 14)
                    lblTime.text = actDateFormatter.string(from: activityPara.actDate) + " at " + timeDateformatter.string(from: activityPara.actDate) //"Tue, 06/06/17 10:12 Am"
                    
                    let iconLocation = UIImageView.init(frame: CGRect(x: 20, y: 30, width: 15, height: 15))
                    //iconLocation.backgroundColor = UIColor.black
                    iconLocation.image = #imageLiteral(resourceName: "location")
                    
                    let lblLocation = MarqueeLabel(frame: CGRect(x: 40, y: iconLocation.frame.origin.y, width: view.frame.size.width - 50, height: 15))
                    lblLocation?.textAlignment = .left
                    lblLocation?.font = UIFont.init(name: Font.BEBAS, size: 14)
                    let distancestr = activityPara.actLocatnDistance
                    lblLocation?.text = activityPara.actLocatnStr + " (\(distancestr) km)"
                    
                    let iconAttendees = UIImageView.init(frame: CGRect(x: 20, y: 30 + 20 + 5, width: 15, height: 15))
                    //iconAttendees.backgroundColor = UIColor.black
                    iconAttendees.image = #imageLiteral(resourceName: "friends_blue")
                    
                    let lblAttendees = UILabel(frame: CGRect(x: 40, y: iconAttendees.frame.origin.y, width: 30, height: 15))
                    lblAttendees.textAlignment = .left
                    lblAttendees.font = UIFont.init(name: Font.BEBAS, size: 12)
                    lblAttendees.text = "\(activityPara.actUserCombineArray.count)"
                    
                    let iconComments = UIImageView.init(frame: CGRect(x: lblAttendees.frame.origin.x + lblAttendees.frame.size.width + 5, y: iconAttendees.frame.origin.y, width: 15, height: 15))
                    //iconComments.backgroundColor = UIColor.black
                    iconComments.image = #imageLiteral(resourceName: "comment")
                    
                    let lblComments = UILabel(frame: CGRect(x: iconComments.frame.origin.x + iconComments.frame.size.width + 5, y: iconAttendees.frame.origin.y, width: 30, height: 15))
                    lblComments.textAlignment = .left
                    lblComments.font = UIFont.init(name: Font.BEBAS, size: 12)
                    lblComments.text = "\(activityPara.actCommentsArray.count)"
                    
                    let horisep = UIView.init(frame: CGRect(x: 0, y: iconAttendees.frame.origin.y + iconAttendees.frame.size.height + 5, width: view.frame.size.width, height: 1))
                    horisep.backgroundColor = #colorLiteral(red: 0.5062589049, green: 0.5177934766, blue: 0.5180256963, alpha: 1)
                    
                    //print(horisep.frame.origin.y, "created time etc")
                    cell?.contentView.addSubview(iconTime)
                    cell?.contentView.addSubview(lblTime)
                    cell?.contentView.addSubview(iconLocation)
                    cell?.contentView.addSubview(lblLocation!)
                    cell?.contentView.addSubview(iconAttendees)
                    cell?.contentView.addSubview(lblAttendees)
                    cell?.contentView.addSubview(iconComments)
                    cell?.contentView.addSubview(lblComments)
                    cell?.contentView.addSubview(horisep)
                }
                else if  indexPath.row == 2
                {
                    var yForUploadButn = CGFloat()
                    
                    let lblPhotos = UILabel(frame: CGRect(x: 20, y: 15, width: view.frame.size.width - 100, height: 30))
                    lblPhotos.textAlignment = .left
                    lblPhotos.font = UIFont.init(name: Font.BEBAS, size: 20)
                    lblPhotos .text = "Photos"
                    cell?.contentView.addSubview(lblPhotos)
                    
                    if imgUrlArray.count == 0
                    {
                        let lblUploadLbl = UILabel(frame: CGRect(x: 20, y: lblPhotos.frame.origin.y + lblPhotos.frame.size.height, width: view.frame.size.width - 20, height: 60))
                        lblUploadLbl.textAlignment = .left
                        lblUploadLbl.font = UIFont.init(name: Font.BEBAS, size: 18)
                        lblUploadLbl.numberOfLines = 0
                        lblUploadLbl.lineBreakMode = .byWordWrapping
                        lblUploadLbl.text = "Be the first and upload photos from your activity"
                        cell?.contentView.addSubview(lblUploadLbl)
                        
                        yForUploadButn = lblUploadLbl.frame.origin.y + lblUploadLbl.frame.size.height + 5
                    }
                    else
                    {
                        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                        layout.itemSize = CGSize(width: 80, height: 80)
                        layout.scrollDirection = .horizontal
                        
                        let myCollectionView:UICollectionView = UICollectionView(frame: CGRect(x: 20, y: lblPhotos.frame.origin.y + lblPhotos.frame.size.height, width: view.frame.size.width - 40, height: 90), collectionViewLayout: layout)
                        myCollectionView.dataSource = self
                        myCollectionView.delegate = self
                        myCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
                        myCollectionView.backgroundColor = UIColor.white
                        myCollectionView.isScrollEnabled = true
                        myCollectionView.alwaysBounceHorizontal = true
                        myCollectionView.bounces = true
                        myCollectionView.tag = 1
                        cell?.contentView.addSubview(myCollectionView)
                        
                        yForUploadButn = myCollectionView.frame.origin.y + myCollectionView.frame.size.height + 5
                    }
                    
                    
                    let btnUpload   = UIButton(type: .custom)
                    btnUpload.frame =  CGRect(x: 20, y: yForUploadButn, width: view.frame.size.width - 40, height: 40)
                    btnUpload.setTitleColor(#colorLiteral(red: 0.5062589049, green: 0.5177934766, blue: 0.5180256963, alpha: 1), for: .normal)
                    btnUpload.setImage(#imageLiteral(resourceName: "photos"), for: .normal)
                    btnUpload.imageEdgeInsets = UIEdgeInsets(top: 4, left: -10, bottom: 4, right: 0)
                    btnUpload.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0)
                    btnUpload.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
                    btnUpload.setTitle("Photo Upload", for: .normal)
                    btnUpload.layer.borderWidth = 1.0
                    btnUpload.backgroundColor = UIColor.white
                    btnUpload.layer.borderColor = UIColor.gray.cgColor
                    btnUpload.addTarget(self, action: #selector(self.didTapUploadPhotos), for: UIControlEvents.touchUpInside)
                    
                    let btnPrivacyPolicy = UIButton.init(frame: CGRect(x: 20, y: btnUpload.frame.origin.y + btnUpload.frame.size.height + 5, width: view.frame.size.width - 40, height: 40))
                    btnPrivacyPolicy.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)
                    btnPrivacyPolicy.setTitle("Privacy Policy", for: .normal)
                    btnPrivacyPolicy.setTitleColor(#colorLiteral(red: 0.5062589049, green: 0.5177934766, blue: 0.5180256963, alpha: 1), for: .normal)
                    btnPrivacyPolicy.addTarget(self, action:#selector(self.didTapPrivaryPolicy), for: .touchUpInside)
                    
                    let horisep = UIView.init(frame: CGRect(x: 0, y: btnPrivacyPolicy.frame.origin.y + btnPrivacyPolicy.frame.size.height + 10, width: view.frame.size.width, height: 1))
                    horisep.backgroundColor = #colorLiteral(red: 0.5062589049, green: 0.5177934766, blue: 0.5180256963, alpha: 1)
                    
                    //print(horisep.frame.origin.y, "photo upload")
                    cell?.contentView.addSubview(btnUpload)
                    cell?.contentView.addSubview(btnPrivacyPolicy)
                    cell?.contentView.addSubview(horisep)
                }
                else if  indexPath.row == 3
                {
                    let lblAttendees = UILabel(frame: CGRect(x: 20, y: 0, width: view.frame.size.width - 100, height: 30))
                    lblAttendees.textAlignment = .left
                    lblAttendees.font = UIFont.init(name: Font.BEBAS, size: 16)
                    lblAttendees.text = "\(activityPara.actGoingArray.count) Attendee, \(activityPara.actInterstedArray.count) interested"
                    
                    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                    layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                    layout.itemSize = CGSize(width: 60, height: 60)
                    layout.scrollDirection = .horizontal
                    
                    let myCollectionView:UICollectionView = UICollectionView(frame: CGRect(x: 10, y: lblAttendees.frame.origin.y + lblAttendees.frame.size.height, width: view.frame.size.width - 20, height: 90), collectionViewLayout: layout)
                    myCollectionView.dataSource = self
                    myCollectionView.delegate = self
                    myCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
                    myCollectionView.backgroundColor = UIColor.white
                    myCollectionView.isScrollEnabled = true
                    myCollectionView.alwaysBounceHorizontal = true
                    myCollectionView.tag = 2
                    myCollectionView.bounces = true
                    
                    cell?.contentView.addSubview(lblAttendees)
                    cell?.contentView.addSubview(myCollectionView)
                }
                else if indexPath.row == 4
                {
                    cell?.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
                    
                    cell?.contentView.addSubview(cellSubView1())
                    cell?.contentView.addSubview(cellSubView2())
                    cell?.contentView.addSubview(cellSubView3())
                }
                
                return cell!
            }
        }
        else // indexPath.section == 2
        {
            print("Comments section is :", indexPath.section)
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as! CommentTableViewCell!

            let commentObj : [String:Any] = activityPara.actCommentsArray[indexPath.row] as! [String : Any]
            cell?.lblUserName.text = commentObj["fname"] as? String
            cell?.lblDateTime.text = commentDateFormatter.string(from: Date.init(timeIntervalSince1970: commentObj["time"] as! Double))
            cell?.lblComment.text = commentObj["comment"] as? String
            let strUrl = commentObj["thumb"] as? String
            let url = URL(string: APP_URL.BASE_URL + strUrl! )!
            let placeholderImage = #imageLiteral(resourceName: "avatar")
            let filter = AspectScaledToFillSizeFilter(size: CGSize(width: 60.0, height: 60.0))
            
            cell?.userImgView.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                { response in
                    
            }
            )
            
            return cell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 3
        {
            activityPara.actVisibleTo = indexPath.row
            tableView.reloadData()
        }
        else if indexPath.section == 2 // Comments
        {
            let commentObj : [String:Any] = activityPara.actCommentsArray[indexPath.row] as! [String : Any]
            
            let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfile_VC") as! UserProfile_VC
            presentVC.user_Id = commentObj["commented_by"] as! Int
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if section == 2
        {
            return 50
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let footerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50))
        footerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let btnWrite = UIButton.init(frame: CGRect(x: 5, y: 5, width: view.frame.size.width - 10, height: 40))
        btnWrite.backgroundColor =  sharedObj.hexStringToUIColor(hex: "ffffff")
        btnWrite.addTarget(self, action: #selector(self.didTapWriteCommentButn), for: .touchUpInside)
        
        let iconadd = UIImageView.init(frame: CGRect(x: 10, y: 5, width: 30, height: 30))
        //iconadd.backgroundColor = sharedObj.hexStringToUIColor(hex: "2196f3")
        iconadd.image = #imageLiteral(resourceName: "add_plus")
        
        let lblWrite = UILabel(frame: CGRect(x: iconadd.frame.origin.x + iconadd.frame.size.width + 5, y: iconadd.frame.origin.y, width: view.frame.size.width / 2, height: 30))
        lblWrite.textAlignment = .left
        lblWrite.font = UIFont.init(name: Font.BEBAS, size: 18)
        lblWrite.text = "Write a comment"
        
        
        //
        btnWrite.addSubview(iconadd)
        btnWrite.addSubview(lblWrite)
        footerView.addSubview(btnWrite)

        return footerView
    }
}

// MARK: - Collection View Datasource and Delegates
extension ShowActivity_VC : UICollectionViewDelegate , UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView.tag == 1
        {
            return imgUrlArray.count
        }
        else
        {
            return activityPara.actUserCombineArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let mycell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath)
        //mycell.backgroundColor = sharedObj.hexStringToUIColor(hex: "333333")
        if collectionView.tag == 1
        {
            let imgViewPhoto = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 80, height: 80))
            imgViewPhoto.contentMode = .scaleToFill
            imgViewPhoto.clipsToBounds = true

            let strUrl = imgUrlArray[indexPath.row]
            
            let url = URL(string: APP_URL.BASE_URL + strUrl )!
            let placeholderImage = #imageLiteral(resourceName: "avatar")
            let filter = AspectScaledToFillSizeFilter(size: CGSize(width: 80.0, height: 80.0))
            
            imgViewPhoto.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                { response in
                    
            }
            )
            
            mycell.addSubview(imgViewPhoto)
        }
        else
        {
            let imgViewPhoto = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 60, height: 60))
            imgViewPhoto.contentMode = .scaleToFill
            imgViewPhoto.clipsToBounds = true
            imgViewPhoto.layer.cornerRadius = 30

            let userJSON : [String : Any] = activityPara.actUserCombineArray[indexPath.row]

            let strUrl = userJSON["thumb"] as! String
            let url = URL(string: APP_URL.BASE_URL + strUrl )!
            let placeholderImage = #imageLiteral(resourceName: "avatar")
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: 60.0, height: 60.0), radius: 30.0)
            
            imgViewPhoto.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion: nil)
            
            mycell.addSubview(imgViewPhoto)
        }
        
        return mycell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView.tag == 1
        {
            if activityPara.actImgJSONArr.count != 0
            {
                pictureJSONToSend = activityPara.actImgJSONArr[indexPath.row] as! [String : Any]
            }
            
            let storyboard = UIStoryboard(name: "CommonStoryboard", bundle: nil)
            let presentVC = storyboard.instantiateViewController(withIdentifier: "ImageSliderViewController") as! ImageSliderViewController
           
            presentVC.imageArray = activityPara.actImgJSONArr as! [[String : Any]]
           // presentVC.getCurrentPage = indexPath.row            
            
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
        else
        {
            let presentVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "UserProfile_VC") as! UserProfile_VC
            
            let userJSON : [String : Any] = activityPara.actUserCombineArray[indexPath.row]
            presentVC.user_Id = userJSON["user_id"] as! Int
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
}

// MARK: - Photo Picker Delegates
extension ShowActivity_VC : YMSPhotoPickerViewControllerDelegate
{
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
            }
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
            }
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            
            let mutableImages: NSMutableArray! = []
            
            for asset: PHAsset in photoAssets
            {
                // let scale = UIScreen.main.scale
                
                // This will be the final size that I am going to show in bigger view.
                // In the collection view of this cell, the images shown are after downloading the respective thumb nails.
                let targetSize = CGSize(width: 400, height: 400)
                
                imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: options, resultHandler: { (image, info) in
                    mutableImages.add(image!)
                })
            }
            // Assign to Array with images
            self.imagesArray = mutableImages.copy() as! [UIImage]
            
            //self.tableView.reloadData()
            
            self.callUploadPhotosAPI()
        }
    }
}

extension ShowActivity_VC : GADBannerViewDelegate
{
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        if sharedObj.userPara.isPlusMember == 1
        {
            print("No ads to plus user")
        }
        else
        {
            print("Banner loaded successfully")
            adBannerView = bannerView
            tableView.reloadData()
        }
    }
    
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }
}
