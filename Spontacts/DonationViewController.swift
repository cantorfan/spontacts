//
//  DonationViewController.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 17/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class DonationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource
{
    
    @IBOutlet weak var tableView: UITableView!
    
    var isShowFullText = Bool()
    let imgArray = [#imageLiteral(resourceName: "donation_charity_image_1.png"), #imageLiteral(resourceName: "donation_charity_image_2.png"), #imageLiteral(resourceName: "donation_charity_image_3.png")]
    var subscribersArray = [[String : Any]]()
    
    // MARK:- View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Donation"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        callGetAllSubscribersList()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4 + subscribersArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 120
        }
        else if indexPath.row == 1
        {
            return 220
        }
        else if indexPath.row == 2
        {
            if isShowFullText == false
            {
                return 183
            }
            else
            {
                return 250
            }
        }
        else if indexPath.row == 3
        {
            return 30
        }
        else
        {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell : DonationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HeartButnCell", for: indexPath) as! DonationTableViewCell
            
            return cell
        }
        else if indexPath.row == 1
        {
            let cell : DonationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ImgCollectionTableCell", for: indexPath) as! DonationTableViewCell
            
            return cell
        }
        else if indexPath.row == 2
        {
            let cell : DonationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LogoTableViewCell", for: indexPath) as! DonationTableViewCell
            
            if isShowFullText == false
            {
                cell.constraintLblMissionHeight.constant = 20
                cell.btnUpDownToggle.setTitle("READ FULL DESCRIPTION", for: .normal)
                cell.btnUpDownToggle.setImage(#imageLiteral(resourceName: "down"), for: .normal)
            }
            else
            {
                cell.constraintLblMissionHeight.constant = 100
                cell.btnUpDownToggle.setTitle("SHOW LESS INFORMATION", for: .normal)
                cell.btnUpDownToggle.setImage(#imageLiteral(resourceName: "up"), for: .normal)
            }
            
            return cell
        }
        else if indexPath.row == 3
        {
            let cell : DonationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WorldChangerCell", for: indexPath) as! DonationTableViewCell
            
            return cell
        }
        else
        {
            let cell : DonationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DonatorPersonCell", for: indexPath) as! DonationTableViewCell
            
            let userJSON = subscribersArray[indexPath.row - 4]
            let strUrl = userJSON["thumb"] as! String
            let url = URL(string: APP_URL.BASE_URL + strUrl )!
            let placeholderImage = #imageLiteral(resourceName: "avatar")
            let filter = AspectScaledToFillSizeFilter(size: CGSize(width: 80.0, height: 80.0))
            
            cell.imgViewUserThumb.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion: nil)
            cell.lblUserName.text = (userJSON["fname"] as? String)! + " donated and wrote"
            cell.lblUserMsg.text = userJSON["text"] as? String
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row > 3
        {
            let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfile_VC") as! UserProfile_VC
            let commentJSON = subscribersArray[indexPath.row - 4]
            presentVC.user_Id = commentJSON["id"] as! Int
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }


    // MARK: - IBActions
    @IBAction func btnActionUpDown(_ sender: Any)
    {
        isShowFullText = !isShowFullText
        tableView.reloadData()
    }
    
    @IBAction func btnHeartClicked(_ sender : Any)
    {
        let upgradeVC : UpgradeViewController = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "UpgradeViewController") as! UpgradeViewController
        
        self.navigationController?.pushViewController(upgradeVC, animated: true)
    }
    
    //MARK: - Collection view Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let mycell = collectionView.dequeueReusableCell(withReuseIdentifier: "DonationImageCollectionViewCell", for: indexPath) as! DonationImageCollectionViewCell
        
        mycell.imgView.image = imgArray[indexPath.row]
        
        return mycell
    }
    
    // MARK: - WEB API
    func callGetAllSubscribersList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: UPGRADE_URL.GET_SUBSCRIBERS_LIST, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                
                if isSuccess
                {
                    self.subscribersArray = responseData["suscribers"] as! [[String : Any]]
                    self.tableView.reloadData()
                }
                else
                {
                    print("Error in getting susbcribers")
                }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
