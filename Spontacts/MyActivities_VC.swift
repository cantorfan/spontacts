//
//  MyActivities_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 20/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MyActivities_VC: UIViewController, UITableViewDataSource, UITableViewDelegate, SWRevealViewControllerDelegate
{
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedSeg = Int()
    let sharedObj = Singleton.shared
    var defaultDateFormatter = DateFormatter()
    
    var upcomingActArray = NSMutableArray()
    var pastActArray = NSMutableArray()
    var activityParaToSend = Para_Activity()
    
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = AdMob_BannerID
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        
        return adBannerView
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "My Activities"
        addLeftBarButton()
        
        defaultDateFormatter.dateFormat = "dd/MM/YYYY"
        
        fetchUpcomingActivitiesList()
        self.fetchPastActivitiesList()
        
        adBannerView.load(GADRequest())
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            self.callReadStatusAPI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Methods
    @IBAction func didTapMenuButn()
    {
        revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func didChangeSegmentedControl(sender : UISegmentedControl)
    {
        selectedSeg = sender.selectedSegmentIndex
        tableView.reloadData()
    }

    // MARK: - API Methods
    func fetchUpcomingActivitiesList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Getting Activities")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl:ACTIVITY_URL.GET_MYACT_UPCOMING , header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    if responseData["activities"] != nil
                    {
                        let array = responseData["activities"] as! NSArray
                        print("Upcoming ", array.count)
                        self.upcomingActArray = array.mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func fetchPastActivitiesList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Getting Activities")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl:ACTIVITY_URL.GET_MYACT_PAST , header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["activities"] != nil
                    {
                        let array = responseData["activities"] as! NSArray
                        print("Past ", array.count)
                        self.pastActArray = array.mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func callFetchAttendees_InterestedAPI(actId : Int)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Loading Activities")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject : [String : Any] = ["activity_id": actId] as [String : Any]
        
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.GET_ACT_GOING_INTERESTED_COMMENTS_LIST, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    if responseData["going"] != nil
                    {
                        let array = responseData["going"] as! [[String:Any]]
                        self.activityParaToSend.actGoingArray = array
                    }
                    if responseData["interested"] != nil
                    {
                        let array = responseData["interested"] as! [[String:Any]]
                        self.activityParaToSend.actInterstedArray = array
                    }
                    if responseData["comments"] != nil
                    {
                        let array = responseData["comments"] as! NSArray
                        self.activityParaToSend.actCommentsArray = array.mutableCopy() as! NSMutableArray
                    }
                    
                    print(self.activityParaToSend.actGoingArray.count, self.activityParaToSend.actInterstedArray.count)
                }
                else
                {
                    //self.showAlert(title: "Fail!", msg: responseMessage)
                }
                
                self.performSegue(withIdentifier: "segueMyActivityToShowActivity", sender: nil)
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPlusPopup()
    {
        let alert = UIAlertController.init(title: "GAY AND YOU?", message: "Copy your activity with one simple click. Discover GAY AND YOU? Plus.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "More", style: .default, handler:
            { alert -> Void in
                
                let upgradeVC : UpgradeViewController = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "UpgradeViewController") as! UpgradeViewController
                
                self.navigationController?.pushViewController(upgradeVC, animated: true)
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
            alert -> Void in
            //self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - UI Methods
    func addLeftBarButton()
    {
        let customView = UIView.init(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setImage(#imageLiteral(resourceName: "menu_white"), for: .normal)
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(self.didTapMenuButn), for: .touchUpInside)
        
        let label = UILabel.init(frame: CGRect(x: 30, y: 5, width: 15, height: 15))
        label.backgroundColor = UIColor.white
        label.layer.cornerRadius = label.frame.size.width / 2
        label.clipsToBounds =  true

        customView.addSubview(button)
        
        if sharedObj.unread_MyActivities_Count != 0
        {
            customView.addSubview(label)
        }
        
//        let customView = UINib(nibName: "MenuWithNotificationView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MenuButtonView
//        customView.frame = CGRect(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
//        customView.sizeToFit()
        
       // btnMenu.butnMenu.addTarget(self, action: #selector(self.didTapMenuButn), for: .touchUpInside)
        //let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu_new"), style: .plain,  target: self, action: #selector(didTapMenuButn))
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: customView) // customView
        
        if revealViewController() != nil
        {
            revealViewController().rightViewRevealWidth = 200
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    func firstHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        var addView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height : 80))
        addView.backgroundColor = UIColor.white
        addView.layer.borderWidth = 1.0
        addView.layer.borderColor = sharedObj.hexStringToUIColor(hex: "d1d1d1").cgColor
        adBannerView.frame = addView.frame
        addView.addSubview(adBannerView) //adBannerView
        headerView.addSubview(addView)
        
        return headerView
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if selectedSeg == 0
        {
            if self.upcomingActArray.count == 0
            {
                let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                emptyLabel.text = "No Upcoming Activities Found"
                emptyLabel.textColor = UIColor.lightGray
                emptyLabel.textAlignment = NSTextAlignment.center
                self.tableView.backgroundView = emptyLabel
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                return 0
            }
            else
            {
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                self.tableView.backgroundView?.isHidden = true
                return self.upcomingActArray.count
            }
        }
        else
        {
            if self.pastActArray.count == 0
            {
                let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                emptyLabel.text = "No Past Activities Found"
                emptyLabel.textColor = UIColor.lightGray
                emptyLabel.textAlignment = NSTextAlignment.center
                self.tableView.backgroundView = emptyLabel
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                return 0
            }
            else
            {
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                self.tableView.backgroundView?.isHidden = true
                return self.pastActArray.count
            }
        }
    }
//    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return adBannerView
//    }
//    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        
//        return adBannerView.frame.height
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
//    {
//        return 80
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
//    {
//        let headerView = UIView()//.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
//        headerView.backgroundColor = UIColor.clear
//        
//        if section == 0
//        {
//            headerView.addSubview(firstHeaderView(headerView: headerView))
//        }
//       
//        return headerView
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : ActivityListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ActivityListTableViewCell", for: indexPath) as! ActivityListTableViewCell
        
        if selectedSeg == 0
        {
            let activityJSON = upcomingActArray[indexPath.row] as! [String : Any]
            
            print(activityJSON)
            let catId = activityJSON["category_id"] as! Int
            if sharedObj.arrCatImageCropped.count > catId
            {
                let actCatUrl = activityJSON["category_picture_thumb"] as! String
                if catId == 8 && actCatUrl != ""
                {
                    cell.imgActivity.af_setImage(withURL: URL.init(string: APP_URL.BASE_URL + actCatUrl)!, placeholderImage: #imageLiteral(resourceName: "image_placeolder"), completion: nil)
                    cell.imgActivity.clipsToBounds = true
                }
                else
                {
                    cell.imgActivity.image = sharedObj.arrCatImageCropped[catId]
                    cell.imgActivity.clipsToBounds = true
                }
            }
            
            cell.lblActName.text = activityJSON["name"] as? String
            let timeInterval = activityJSON["time"] as? Int
            let date = Date.init(timeIntervalSince1970: TimeInterval(timeInterval!))
            cell.lblDate.text = defaultDateFormatter.string(from: date)
            
            cell.lblAttendees.text = "\(activityJSON["going_count"] as! Int)" //"1"
            cell.lblComments.text = "\(activityJSON["comments_count"] as! Int)" //"0"
            
            let distancestr = Int(activityJSON["distance"] as! NSNumber)
            cell.lblLocation.text = (activityJSON["location_string"] as? String)! + " (\(distancestr) km)"
            
            cell.lblOrganising.text = "I'm organising"
            
            if activityJSON["updated"] as! Int == 1
            {
                cell.lblUpdated.isHidden = false
            }
            else
            {
                cell.lblUpdated.isHidden = true
            }
        }
        else
        {
            let activityJSON = pastActArray[indexPath.row] as! [String : Any]
            print(activityJSON)
            let catId = activityJSON["category_id"] as! Int
            if sharedObj.arrCatImageCropped.count > catId
            {
                cell.imgActivity.image = sharedObj.arrCatImageCropped[catId]
                cell.imgActivity.clipsToBounds = true
            }
            
            cell.lblActName.text = activityJSON["name"] as? String
            let timeInterval = activityJSON["time"] as? Int
            let date = Date.init(timeIntervalSince1970: TimeInterval(timeInterval!))
            cell.lblDate.text = defaultDateFormatter.string(from: date)
            
            cell.lblAttendees.text = "\(activityJSON["going_count"] as! Int)" //"1"
            cell.lblComments.text = "\(activityJSON["comments_count"] as! Int)" //"0"
            
            let distancestr = Int(activityJSON["distance"] as! NSNumber)
            cell.lblLocation.text = (activityJSON["location_string"] as? String)! + " (\(distancestr) km)"
            
            cell.lblOrganising.text = "I'm organising"
            
            if activityJSON["updated"] as! Int == 1
            {
                cell.lblUpdated.isHidden = false
            }
            else
            {
                cell.lblUpdated.isHidden = true
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedSeg == 0
        {
            let JSON = upcomingActArray[indexPath.row] as! [String : Any]
            
            activityParaToSend = Para_Activity()
            
            activityParaToSend.actLocatnLongitude = JSON["lng"] as! Double
            activityParaToSend.actIsFlexible = (JSON["time_flexible"] != nil)
            let timeInterval = JSON["time"] as? Int
            activityParaToSend.actDate = Date.init(timeIntervalSince1970: Double(timeInterval!))
            activityParaToSend.actId = JSON["id"] as! Int
            activityParaToSend.actVisibleTo = JSON["visibility"] as! Int
            activityParaToSend.actDesc = JSON["description"] as! String
            activityParaToSend.actLocatnLatitude = JSON["lat"] as! Double
            activityParaToSend.actName = JSON["name"] as! String
            activityParaToSend.actCreatedEpoch = JSON["created_time"] as! Double
            activityParaToSend.actCategoryId = JSON["category_id"] as! Int
            activityParaToSend.actLocatnStr = JSON["location_string"] as! String
            activityParaToSend.actImgJSONArr = JSON["images"] as! NSArray
            activityParaToSend.actCommentsCount = JSON["comments_count"] as! Int
            activityParaToSend.actLocatnDistance = Int(JSON["distance"] as! NSNumber)
            activityParaToSend.actCategoryImgURL = JSON["category_picture"] as! String

//            userParaToSend = sharedObj.userPara
        }
        else if selectedSeg == 1
        {
            let JSON = pastActArray[indexPath.row] as! [String : Any]
            
            activityParaToSend = Para_Activity()
            
            activityParaToSend.actLocatnLongitude = JSON["lng"] as! Double
            activityParaToSend.actIsFlexible = (JSON["time_flexible"] != nil)
            let timeInterval = JSON["time"] as? Int
            activityParaToSend.actDate = Date.init(timeIntervalSince1970: Double(timeInterval!))
            activityParaToSend.actId = JSON["id"] as! Int
            activityParaToSend.actVisibleTo = JSON["visibility"] as! Int
            activityParaToSend.actDesc = JSON["description"] as! String
            activityParaToSend.actLocatnLatitude = JSON["lat"] as! Double
            activityParaToSend.actName = JSON["name"] as! String
            activityParaToSend.actCreatedEpoch = JSON["created_time"] as! Double
            activityParaToSend.actCategoryId = JSON["category_id"] as! Int
            activityParaToSend.actLocatnStr = JSON["location_string"] as! String
            activityParaToSend.actImgJSONArr = JSON["images"] as! NSArray
            activityParaToSend.actCommentsCount = JSON["comments_count"] as! Int
            activityParaToSend.actLocatnDistance = Int(JSON["distance"] as! NSNumber)
            activityParaToSend.actCategoryImgURL = JSON["category_picture"] as! String

//            userParaToSend = Para_UserProfile()
        }
        
        callFetchAttendees_InterestedAPI(actId: activityParaToSend.actId)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
   
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let hideAction = UITableViewRowAction(style: .default, title: "Delete")
        {action in
            //handle hide
            
            let activityJSON = self.pastActArray[indexPath.row] as! [String : Any]
            let actId = activityJSON["id"] as! Int
            self.callCancelActivityAPI(actId: actId, indexOfObject: indexPath.row)
        }
        
        let copyAction = UITableViewRowAction(style: .normal, title: "Copy")
        {action in
            //handle copy
            
            if self.sharedObj.userPara.isPlusMember == 1
            {
                var JSON = [String : Any]()
                let actPara = Para_Activity()
                if self.selectedSeg == 0
                {
                    JSON = self.upcomingActArray[indexPath.row] as! [String : Any]
                    
                    actPara.actLocatnLongitude = JSON["lng"] as! Double
                    actPara.actVisibleTo = JSON["visibility"] as! Int
                    actPara.actDesc = JSON["description"] as! String
                    actPara.actLocatnLatitude = JSON["lat"] as! Double
                    actPara.actName = JSON["name"] as! String
                    actPara.actCategoryId = JSON["category_id"] as! Int
                    actPara.actLocatnStr = JSON["location_string"] as! String
                }
                else
                {
                    JSON = self.pastActArray[indexPath.row] as! [String : Any]
                    
                    actPara.actLocatnLongitude = JSON["lng"] as! Double
                    actPara.actVisibleTo = JSON["visibility"] as! Int
                    actPara.actDesc = JSON["description"] as! String
                    actPara.actLocatnLatitude = JSON["lat"] as! Double
                    actPara.actName = JSON["name"] as! String
                    actPara.actCategoryId = JSON["category_id"] as! Int
                    actPara.actLocatnStr = JSON["location_string"] as! String
                }
                
                // Pass this activity data for creating new
                let createVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAct_MandatoryInfo") as! CreateAct_MandatoryInfo
                createVC.activityPara = actPara
                createVC.activityPara.actCategoryStr = self.sharedObj.arrCat[actPara.actCategoryId]
                self.navigationController?.pushViewController(createVC, animated: true)
            }
            else
            {
                /*
                let upgradePopupVC = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "PlusPopUpViewController") as! PlusPopUpViewController
                self.navigationController?.pushViewController(upgradePopupVC, animated: true)*/
            
                self.showPlusPopup()
            }
        }
        
        copyAction.backgroundColor = UIColor.init(red: 98/255, green: 251/255, blue: 217/255, alpha: 1.0)
        hideAction.backgroundColor = UIColor.init(red: 188/255, green: 189/255, blue: 191/255, alpha: 1.0)
        
        if selectedSeg == 0
        {
            return [copyAction]
        }
        else
        {
            return [copyAction, hideAction]
        }
    }
    
    // MARK: - WEB API
    func callCancelActivityAPI(actId : Int, indexOfObject : Int)
    {
        let authToken : String = sharedObj.userPara.userToken
        
        let headerObject = ["authtoken" : authToken]
        let requestObject = ["activity_id" : actId]
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.CANCEL_ACTIVITY, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.showAlert(title: "Activity Cancelled Successfully!", msg: responseMessage)
                //self.pastActArray.removeObject(at: indexOfObject)
                self.fetchPastActivitiesList()
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func callReadStatusAPI()
    {
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["my_activities":1,
                             "joined_activities":0,
                             "recomendations":0,
                             "profile_visitors":0,
                             "friends":0]
        
        GlobalConstants().postData_Header_Parameter(subUrl: REFRESH_STATUS.READ_NOTIFICATION, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                self.sharedObj.unread_All_Activities_Count = 0
                print(responseData)
                
                if isSuccess
                {
                    print("Read notifications")
                }
                else
                {
                    print("Error in reading status")
                }
        })
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueMyActivityToShowActivity"
        {
            let showAct : ShowActivity_VC = segue.destination as! ShowActivity_VC
            showAct.activityPara = activityParaToSend
            showAct.userPara = sharedObj.userPara
            
        }
    }

    
    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }
}

extension MyActivities_VC : GADBannerViewDelegate
{
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        if sharedObj.userPara.isPlusMember == 1
        {
            print("No ads to plus user")
        }
        else
        {
            print("Banner loaded successfully")
            tableView.tableHeaderView?.frame = bannerView.frame
            tableView.tableHeaderView = bannerView
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }
}
