//
//  ActivityListTableViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 20/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class ActivityListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgActivity : UIImageView!
    @IBOutlet weak var lblOrganising : UILabel!
    @IBOutlet weak var lblActName : UILabel!
    
    @IBOutlet weak var imgDate : UIImageView!
    @IBOutlet weak var lblDate : UILabel!
    
    @IBOutlet weak var imgAttendees : UIImageView!
    @IBOutlet weak var lblAttendees : UILabel!
    @IBOutlet weak var imgComments : UIImageView!
    @IBOutlet weak var lblComments : UILabel!
    
    @IBOutlet weak var imgLocation : UIImageView!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var lblUpdated: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
