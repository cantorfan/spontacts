//
//  AppDelegate.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 5/24/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import GooglePlaces
import FBSDKCoreKit
import GoogleMaps

import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

// For Admob
import GoogleMobileAds

import Fabric
import Crashlytics

let appDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate, NotificationServiceDelegate
{

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        setNavigationBarAppearance()
        
        configureGoogle()
        configureTwitter()
        
        // Initialise Facebook Client
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        configureFirebase()
        configureAdMob()
        configureQuickblox()

//        // iOS 8 Notifications
//        application.registerUserNotificationSettings(UIUserNotificationSettings(types: ([.sound, .alert, .badge]), categories: nil))
//        application.registerForRemoteNotifications()
        
        //Utils.copyFile("GayAndYou.sqlite")

        //DBManager.shared.createDatabase()
        print(DBManager.shared.pathToDatabase)
        
        // Configure Firebase Cloud Messaging Platform
        if #available(iOS 10.0, *)
        {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            FIRMessaging.messaging().remoteMessageDelegate = self
        }
        else
        {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        application.applicationIconBadgeNumber = 0
        
        
        // app was launched from push notification, handling it
        let remoteNotification: NSDictionary! = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
        if (remoteNotification != nil) {
            ServicesManager.instance().notificationService.pushDialogID = remoteNotification["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String
        }
        
        
        loginToQB()
        
        checkIfAlreadyLOggedIn()
        configureCrashlytics()

        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
        let subscription: QBMSubscription! = QBMSubscription()
        
        subscription.notificationChannel = QBMNotificationChannel.APNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
            //
        }) { (response: QBResponse!) -> Void in
            //
        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print("Failed to get token, error: \(error)")
    }
    
    /*
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        print(userInfo)
        print("Received remote notification")
        
        Message_VC().callSyncingAPI()
    }*/
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("my push is: %@", userInfo)
        guard application.applicationState == UIApplicationState.inactive else {
            return
        }
        
        guard let dialogID = userInfo["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String else {
            return
        }
        
        guard !dialogID.isEmpty else {
            return
        }
        
        
        let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
        if dialogWithIDWasEntered == dialogID {
            return
        }
        
        ServicesManager.instance().notificationService.pushDialogID = dialogID
        
        // calling dispatch async for push notification handling to have priority in main queue
        DispatchQueue.main.async {
            
            ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(delegate: self)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        application.applicationIconBadgeNumber = 0
        // Logging out from chat.
        ServicesManager.instance().chatService.disconnect(completionBlock: nil)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Logging in to chat.
        ServicesManager.instance().chatService.connect(completionBlock: nil)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Logging out from chat.
        ServicesManager.instance().chatService.disconnect(completionBlock: nil)
    }
    
    // MARK: NotificationServiceDelegate protocol
    
    func notificationServiceDidStartLoadingDialogFromServer() {
    }
    
    func notificationServiceDidFinishLoadingDialogFromServer() {
    }
    
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!) {
        let navigatonController: UINavigationController! = self.window?.rootViewController as! UINavigationController
        
        let chatController: ChatViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatController.dialog = chatDialog
        
        let dialogWithIDWasEntered = ServicesManager.instance().currentDialogID
        if !dialogWithIDWasEntered.isEmpty {
            // some chat already opened, return to dialogs view controller first
            navigatonController.popViewController(animated: false);
        }
        
        navigatonController.pushViewController(chatController, animated: true)
    }
    
    func notificationServiceDidFailFetchingDialog() {
    }

    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        var returnVal = Bool()
        
//        return Twitter.sharedInstance().application(app, open: url, options: options)

        if FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
        {
            print("Came back to app from Facebook")
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
        }
        else if Twitter.sharedInstance().application(application, open: url, options: annotation as! [AnyHashable : Any])
        {
            return Twitter.sharedInstance().application(application, open: url, options: annotation as! [AnyHashable : Any])
        }
        else
        {
            //////google sign in
            
            var options: [String: AnyObject] = [UIApplicationOpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject,
                                                UIApplicationOpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
            
            
            return GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: sourceApplication, annotation: annotation)
            
        }
    }
    

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    {
        if Twitter.sharedInstance().application(app, open: url, options: options)
        {
            return Twitter.sharedInstance().application(app, open: url, options: options)
        }
        else if GIDSignIn.sharedInstance().handle(url,
                                                    sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                    annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        {
            return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        else
        {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options : options)
        }
        
        return false
    }
    
    // MARK: - QuickBlox 
    func loginToQB()
    {
        if UserDefaults.standard.value(forKey: "userJSON") != nil
        {
            let userJSON = UserDefaults.standard.value(forKey: "userJSON") as! [String : Any]

            let tokenStr = userJSON["token"] as! String
            
            QBRequest.logIn(withUserLogin: tokenStr, password: kQuickbloxCommonPassword, successBlock:{
                (success,  user) -> Void in
                
                user?.password = kQuickbloxCommonPassword
                QBChat.instance().connect(with: user!, completion: {
                    (erro) -> Void in
                    
                    if erro == nil
                    {
                        print("Logged in with QuickBlox")
                        self.getCountOfUnreadMessages()
                    }
                })
                
                print("Error in Logging in with QuickBlox")
            },
                            errorBlock:{
                                (erro) -> Void in
                                
                                print("\(erro)")
                                
            })
        }
    }
 
    func getCountOfUnreadMessages()
    {
        QBRequest.totalUnreadMessageCountForDialogs(withIDs: Set<String>(), successBlock: {
            (response, count, dictionary) -> Void in
            
            print("MMMMMM -> ", count)
            Singleton.shared.unread_Chats_Count = Int(count)
        }, errorBlock:
            {(error) -> Void in
        
        })
    }
    
    // MARK: - Firebase Delegates
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage)
    {
        print("Received firebase notification")
        print(remoteMessage.appData)
    }
    
    
    // MARK: - Custom
    func checkIfAlreadyLOggedIn()
    {
        var loginState = 0
        
        if UserDefaults.standard.value(forKey: "userToken") != nil
        {
            let authToken : String = UserDefaults.standard.value(forKey: "userToken") as! String
            if authToken != ""
            {
                loginState = 1
            }
            else
            {
                loginState = 0
            }
        }
        
        if loginState == 1
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let popoverContent = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            
            let menuvc = mainStoryboard.instantiateViewController(withIdentifier: "Menu_VC") as! Menu_VC
            let mainvc = mainStoryboard.instantiateViewController(withIdentifier: "LandingPage") as! LandingPage
            
            popoverContent.frontViewController = mainvc
            popoverContent.rearViewController = menuvc
            
            let nav = UINavigationController(rootViewController: popoverContent)
            window?.rootViewController = nav
            window?.makeKeyAndVisible()
            //performSegue(withIdentifier: "segueLoginToLandingPage", sender: self)
        }
    }
    
    func setNavigationBarAppearance()
    {
        let sharedObj = Singleton.shared
        
        // Changed background color of nav bar
        UINavigationBar.appearance().barTintColor = sharedObj.hexStringToUIColor(hex: "4BB4E7")
        //UINavigationBar.appearance().barTintColor = sharedObj.hexStringToUIColor(hex: "ffffff")
       
        // Changes text color of bar buttons
        UINavigationBar.appearance().tintColor = sharedObj.hexStringToUIColor(hex: "FFFFFF")
        //UINavigationBar.appearance().tintColor = sharedObj.hexStringToUIColor(hex: "4BB4E7")//("FFFFFF")
        UINavigationBar.appearance().barStyle = UIBarStyle.black
        
       // UINavigationBar.appearance().isTranslucent = false
        
         //Font size for view title
//        let attributes = [NSFontAttributeName: UIFont.init(name: Font.BEBAS, size: 18)] //change size as per your need here.
//        UINavigationBar.appearance().titleTextAttributes = attributes as Any as! [String : Any]
//        
//        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: sharedObj.hexStringToUIColor(hex: "4BB4E7"), NSFontAttributeName: UIFont.init(name: Font.BEBAS, size: 18)!]
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.init(name: "AppleSDGothicNeo-Bold", size: 18)!]

        //UINavigationBar.appearance().setBackgroundImage(#imageLiteral(resourceName: "grad_Navbar"), for: .default)
        
        let gradient = CAGradientLayer()
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
        
        gradient.frame = defaultNavigationBarFrame
        
        gradient.colors = [sharedObj.hexStringToUIColor(hex: "ea5b0c").cgColor, sharedObj.hexStringToUIColor(hex: "ed6188").cgColor]
        
        UINavigationBar.appearance().setBackgroundImage(self.image(fromLayer: gradient), for: .default)
    }
    
    func image(fromLayer layer: CALayer) -> UIImage
    {
        UIGraphicsBeginImageContext(layer.frame.size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
    }

    // MARK: - SDK Configuration
    func configureGoogle()
    {
        // Set up google places picker
        GMSPlacesClient.provideAPIKey("AIzaSyDadrLXDKO42NBFggNVkIhwvX4eWCSJhTI")
        
        // Place marker
        GMSServices.provideAPIKey("AIzaSyDadrLXDKO42NBFggNVkIhwvX4eWCSJhTI")

        // Initialize Google sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
    }
    
    func configureTwitter()
    {
        Twitter.sharedInstance().start(withConsumerKey:"YjomN1VMwlxsc6ODVr5uAF9ys", consumerSecret:"vJcsUvDltLOf9UIEnuxoCpNMH6NQTKpA6YkSB2t9mm2D7DJKRS")
    }
    
    func configureAdMob()
    {
        // Set AdMob
        GADMobileAds.configure(withApplicationID: "ca-app-pub-1230813742493123~3982406147")
        // My Id - ca-app-pub-6631853374417128~8652483787
    }
    
    func configureCrashlytics()
    {
        Fabric.with([Crashlytics.self, Twitter.self])
    }
    
    func configureFirebase()
    {
        FIRApp.configure()
        
        // Generate token for device
        
        let fcmDeviceToken = FIRInstanceID.instanceID().token()
        print("FCM token: \(fcmDeviceToken ?? "")")
        UserDefaults.standard.setValue(fcmDeviceToken, forKey: "fcmDeviceToken")
        UserDefaults.standard.synchronize()
    }
    
    func configureQuickblox()
    {
        /*
        // Chat settings from Pratiksha Account
        QBSettings.setApplicationID(61518)
        QBSettings.setAuthKey("Et6ejSJzmuyH2e5")
        QBSettings.setAuthSecret("4LVvHrBzsUG7XJv")
        QBSettings.setAccountKey("zjWTnYdVs1_vXtVhPHLW")*/
        
        // Chat settings from Sien Account
        QBSettings.setApplicationID(63149)
        QBSettings.setAuthKey("JKvYUqccHrMkCmS")
        QBSettings.setAuthSecret("waeaXxm4uPQuCee")
        QBSettings.setAccountKey("qQ9noRjX7mKaj4gUSG6J")
        
        QBSettings.setLogLevel(.debug)
        QBSettings.enableXMPPLogging()
        
        // Send messages to all logged in devices of user
        QBSettings.setCarbonsEnabled(true)
    }
}
