//
//  InterestsCategoriesViewController.swift
//  Spontacts
//
//  Created by maximess142 on 24/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class InterestsCategoriesViewController: UIViewController
{
    let sharedObj = Singleton.shared
    var selectedCatArray = [Int]()
    
    @IBOutlet weak var tableView: UITableView!

    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Choose Interests"
        
        // Do any additional setup after loading the view.
        let backBtn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back_pink"), style: .plain, target: self, action: #selector(self.didTapBackButn))
        navigationItem.leftBarButtonItem = backBtn
    }
    
    func didTapBackButn()
    {
        callAddInterestsAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callAddInterestsAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject = [ "category_ids" : selectedCatArray]
        
        GlobalConstants().postData_Header_Parameter(subUrl: INTEREST_URL.ADD_INTERESTS, header: headerObject, parameter: requestObject, completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            if isSuccess
            {
                if responseData["interest"] != nil
                {
                    let array = responseData["interest"] as! [Int]
                    UserDefaults.standard.setValue(array, forKey: "interest")
                    UserDefaults.standard.synchronize()
                    
                    let dict : [String : Any] = ["array" : array]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Interest_Chosen"), object: dict)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        } )
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

}

extension InterestsCategoriesViewController : UITableViewDataSource, UITableViewDelegate
{
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sharedObj.arrCat.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        
        var titleLabel = UILabel()
        
        if (cell == nil)
        {
            cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
            cell?.selectionStyle = UITableViewCellSelectionStyle.gray
            
            titleLabel = UILabel(frame: CGRect(x: 30, y: 5, width: view.frame.size.width - 80, height: 40))
            titleLabel.textAlignment = .left
            titleLabel.font = UIFont.init(name: Font.BEBAS, size: 16)
            titleLabel.tag = 1
            
            cell?.contentView.addSubview(titleLabel)
        }
        else
        {
            titleLabel = cell?.contentView.viewWithTag(1) as! UILabel
        }
        
        titleLabel.text = sharedObj.arrCat[indexPath.row]
        
        if selectedCatArray.contains(indexPath.row)
        {
            cell?.accessoryType = .checkmark
        }
        else
        {
            cell?.accessoryType = .none
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if selectedCatArray.contains(indexPath.row)
        {
            let index = selectedCatArray.index(of: indexPath.row)
            selectedCatArray.remove(at: index!)
        }
        else
        {
            selectedCatArray.append(indexPath.row)
        }
        
        tableView.reloadData()
    }
    
}
