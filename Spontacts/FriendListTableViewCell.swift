//
//  FriendListTableViewCell.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 10/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class FriendListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgViewUserTHumb: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
