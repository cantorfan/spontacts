//
//  QuotesViewController.swift
//  Spontacts
//
//  Created by maximess142 on 08/10/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class QuotesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    let sharedObj = Singleton.shared
    var quotesArray = [[String : Any]]()
    
    @IBOutlet weak var quotesCollectionView: UICollectionView!
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.navigationController?.setNavigationBarHidden(false, animated: true)
        fetchAllQuotes()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    @IBAction func didTapBackButn()
    {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapForwardButton(_ sender: Any)
    {
//        print(selectedIndex, quotesArray.count)
//        if selectedIndex < quotesArray.count
//        {
            selectedIndex = selectedIndex + 1
            self.quotesCollectionView.reloadData()
//        }
    }
    
    @IBAction func didTapBackwardButton(_ sender: Any)
    {
//        if selectedIndex > quotesArray.count
//        {
            selectedIndex = selectedIndex - 1
            self.quotesCollectionView.reloadData()
//        }
    }
    
    //MARK: // ----------------------------- UIScrollView Delegate ---------------------
    
    var  lastContentOffset = CGFloat()
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        //        if progressPhotoFlag  {
        self.view.endEditing(true)
        
        var visibleRect = CGRect()
        
        visibleRect.origin = quotesCollectionView.contentOffset
        visibleRect.size = quotesCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        let visibleIndexPath: IndexPath = quotesCollectionView.indexPathForItem(at: visiblePoint)!
        
        selectedIndex = visibleIndexPath.row
        self.quotesCollectionView.reloadData()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        self.lastContentOffset = scrollView.contentOffset.x
    }
    
    // MARK: - Collection View Datasource and Delegates
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: self.view.frame.size.width, height: self.quotesCollectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quotesArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : QuotesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuotesCollectionViewCell", for: indexPath) as! QuotesCollectionViewCell
        
//        if selectedIndex > quotesArray.count
//        {
            let quoteData = quotesArray[selectedIndex] //[indexPath.row]
            
            cell.lblQuote.text = quoteData["quote_text"] as? String
            cell.lblAuthor.text = quoteData["author_name"] as? String
//        }
        
        cell.butnForward.isHidden = false
        cell.butnBackward.isHidden = false
        
        if selectedIndex == 0
        {
            cell.butnBackward.isHidden = true
        }
        else if selectedIndex == quotesArray.count - 1
        {
            cell.butnForward.isHidden = true
        }
        
        if quotesArray.count <= 1
        {
            cell.butnForward.isHidden = true
        }
        
        return cell
    }

    // MARK: - WEB API
    func fetchAllQuotes()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait...")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl:QUOTES_API.GET_QUOTES , header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    if responseData["quotes"] != nil
                    {
                        self.quotesArray = responseData["quotes"] as! [[String:Any]]
                        print("Number of quotes ", self.quotesArray.count)
                        self.quotesCollectionView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
                
        })
    }

    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
