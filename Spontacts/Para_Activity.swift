//
//  Para_Activity.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 03/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class Para_Activity: NSObject
{
    var actId = Int()
    
    var actName = String()
    var actDesc = String()
    var actCategoryStr = String()
   
    var actCategoryId = Int()
    
    var actVisibleTo = Int()
    
    var actMaxAttendees = Int()
    
    var actLocatnStr = String()
    var actLocatnLatitude = Double()
    var actLocatnLongitude = Double()
    var actLocatnDistance = Int()
    
    var actDate = Date()
    var actIsFlexible = Bool()
    
    var actIsPostToFacebook = Bool()
    
    var actCreatedEpoch = Double()
    
    var actImgJSONArr = NSArray()
    
    var isGoing = Bool()
    var isInterested =  Bool()
    
    var actCommentsCount = Int()
    var actGoingArray = [[String:Any]]()
    var actInterstedArray = [[String:Any]]()
    var actUserCombineArray = [[String:Any]]()
    var actCommentsArray = NSMutableArray()
    
    var actCategoryImgURL = String()
}
