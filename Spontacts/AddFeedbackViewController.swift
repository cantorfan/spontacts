//
//  AddFeedbackViewController.swift
//  Spontacts
//
//  Created by maximess142 on 02/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class AddFeedbackViewController: UIViewController
{
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "Post an idea"
        
        addLeftBarButn()
        
        let nextbtn = UIBarButtonItem.init(title: "Next", style: .done, target: self, action: #selector(self.nextBtnTapped))
        self.navigationItem.rightBarButtonItem = nextbtn
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftBarButn()
    {
        let backButn = UIBarButtonItem(image: #imageLiteral(resourceName: "back_pink"),  style: .plain, target: self, action: #selector(didTapBackButn))
        navigationItem.leftBarButtonItem = backButn
    }
    
    @IBAction func didTapBackButn()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - IBActions
    func nextBtnTapped()
    {
        dismissKeyboard()
        let additionalVC : AdditionalDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AdditionalDetailsViewController") as! AdditionalDetailsViewController
        
        additionalVC.ideaStr = txtTitle.text!
        additionalVC.idesDescription = txtDescription.text
        
        self.navigationController?.pushViewController(additionalVC, animated: true)
    }

    func dismissKeyboard()
    {
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
