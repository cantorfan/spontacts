//
//  FriendModuleTableViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 17/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class FriendModuleTableViewCell: UITableViewCell
{    
    // Discover View
    @IBOutlet weak var imgViewDiscover: UIImageView!
    @IBOutlet weak var lblUserNameDiscover: UILabel!
    @IBOutlet weak var lblAddressDiscover: UILabel!
    @IBOutlet weak var btnAddDiscover: UIButton!

    @IBOutlet weak var btnCancelRequestDiscover: UIButton!
    // Request View
    @IBOutlet weak var imgViewRequest: UIImageView!
    @IBOutlet weak var lblUserNameRequests: UILabel!
    @IBOutlet weak var lblAddressRequest: UILabel!
    @IBOutlet weak var btnAcceptRequest: UIButton!
    @IBOutlet weak var btnRejectRequest: UIButton!
    
    
    // Facebook Friends
    @IBOutlet weak var imgViewFacebook: UIImageView!
    @IBOutlet weak var lblUserNameFacebook: UILabel!
    
    @IBOutlet weak var lblAddressFacebook: UILabel!
    @IBOutlet weak var imgViewFacebookFriends: UIImageView!
    
    @IBOutlet weak var btnAddFacebookFriend: UIButton!
    
    // My Friend
    @IBOutlet weak var imgViewMyFriends: UIImageView!
    @IBOutlet weak var lblUserNameMyFriends: UILabel!
    @IBOutlet weak var lblAddressMyFriends: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
