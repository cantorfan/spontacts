//
//  UserFriendsCollectionViewCell.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 01/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class UserFriendsCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgViewUserFriendThumb: UIImageView!
    
}
