

//
//  ImageSliderViewController.swift
//  Projec10
//
//  Created by maximess114 on 25/11/16.
//  Copyright © 2016 maximess114. All rights reserved.
//

import UIKit

class ImageSliderViewController: UIViewController,UIScrollViewDelegate, UICollectionViewDataSource,UICollectionViewDelegate
{
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var sliderView: UIScrollView!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var imageArray = [[String:Any]]() //:[String] = [String]()
    
    var getCurrentPage : Int = Int()
    
    var photoImageView = UIImageView()
    
    @IBOutlet weak var btnStartSlideShow: UIButton!
    
    // MARK: - View Delegates
    override func viewDidLoad() 
    {
        super.viewDidLoad()
        
        setNavigationBar()
        addRightBarButton()
        
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        
        print("ImageSliderViewController")
        
        print(self.imageArray)
        if let currentPage = self.imageArray[getCurrentPage] as? [String: Any] {
            
            if let caption = self.imageArray[getCurrentPage]["file_caption"] as? String
            {
                lblTitle.text=caption
            }
        }
        
//        self.pageController.currentPage = getCurrentPage
//        self.pageController.numberOfPages = imageArray.count
//                
//        let indexToScrollTo = IndexPath(item: getCurrentPage, section: 0)
//        self.imageCollectionView.scrollToItem(at: indexToScrollTo, at: .centeredHorizontally, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.pageController.numberOfPages = imageArray.count
        self.pageController.currentPage = getCurrentPage
        
        let indexToScrollTo = IndexPath(item: getCurrentPage, section: 0)
        self.imageCollectionView.scrollToItem(at: indexToScrollTo, at: .centeredHorizontally, animated: false)
    }
    
    func setNavigationBar()
    {
//        self.navigationController?.isNavigationBarHidden = true
//
//        let storyboard = UIStoryboard(name: "NavigationBarStoryboard", bundle: nil)
//        let topVC: BackNavBarMainViewController = storyboard.instantiateViewController(withIdentifier: "BackNavBarMainViewController") as! BackNavBarMainViewController
//        self.view.addSubview(topVC.view)
//        self.addChildViewController(topVC)
//        topVC.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 65)
//        topVC.btnScreenName.setTitle("Progress Photos", for: .normal)
    }
    
    func addRightBarButton()
    {
        self.navigationItem.rightBarButtonItem = nil
        
         if let userId = self.imageArray[getCurrentPage]["user_id"] as? Int
        {
            
            if userId == Singleton.shared.userPara.userId
            {
                let rightButn = UIButton.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                rightButn.backgroundColor = UIColor.clear
                rightButn.tintColor = UIColor.white
                

                rightButn.setImage(#imageLiteral(resourceName: "edit_white"), for: .normal)
                rightButn.addTarget(self, action: #selector(self.didTapSettingsButn), for: .touchUpInside)
                rightButn.tintColor = UIColor.white

                let btnMenu = UIBarButtonItem.init(customView: rightButn)
//                let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "edit"), style: .plain,  target: self, action: #selector(didTapSettingsButn))
                navigationItem.rightBarButtonItem = btnMenu
            }
            else
            {
                let rightButn = UIButton.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                rightButn.backgroundColor = UIColor.clear
                rightButn.tintColor = UIColor.white
                

                rightButn.setImage(#imageLiteral(resourceName: "report_white"), for: .normal)
                rightButn.addTarget(self, action: #selector(self.didTapSettingsButn), for: .touchUpInside)
                rightButn.tintColor = UIColor.white

                let btnMenu = UIBarButtonItem.init(customView: rightButn)
//                let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "report"), style: .plain,  target: self, action: #selector(didTapSettingsButn))
                navigationItem.rightBarButtonItem = btnMenu
            }
        }
    }
    
    var xValue : CGFloat = 0.0
    
    func ChangeImage()
    {
        UIView.animate(withDuration: 0.5) { () -> Void in
            
            self.xValue = self.xValue + self.sliderView.frame.width //* CGFloat(self.getCurrentPage)
            if  self.xValue >= self.sliderView.contentSize.width
            {
                self.xValue = 0
            }
            self.sliderView.contentOffset = CGPoint(x: self.xValue, y: 0)
        }
    }
    
    //MARK: // ----------------------------- UIScrollView Delegate ---------------------
    
    var  lastContentOffset = CGFloat()
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        //        if progressPhotoFlag  {
        self.view.endEditing(true)
        
        var visibleRect = CGRect()
        
        visibleRect.origin = imageCollectionView.contentOffset
        visibleRect.size = imageCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        let visibleIndexPath: IndexPath = imageCollectionView.indexPathForItem(at: visiblePoint)!
        
//        currentPageIndex = visibleIndexPath.row
        
        getCurrentPage = visibleIndexPath.row
        if let caption = self.imageArray[visibleIndexPath.row]["file_caption"] as? String
        {
            lblTitle.text=caption
        }
        
        self.pageController?.currentPage = getCurrentPage
        self.addRightBarButton()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        self.lastContentOffset = scrollView.contentOffset.x
    }
    
    //MARK: // ----------------------------- @IBAction ---------------------------------------
    
    func didTapSettingsButn()
    {
        let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
            alert -> Void in
            
        }))
        
        if let userId = self.imageArray[getCurrentPage]["user_id"] as? Int
        {
            print(userId, Singleton.shared.userPara.userId)
            if userId == Singleton.shared.userPara.userId
            {
                alert.addAction(UIAlertAction.init(title: "Delete Photo", style: .default, handler: {
                    alert -> Void in
                    
                    self.callDeletePhotoAPI()
                }))
            }
            else
            {
                alert.addAction(UIAlertAction.init(title: "Report Photo", style: .default, handler: {
                    alert -> Void in
                    
                }))
            }
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    var slideShowTimer = Timer()
    
    //    @IBAction func slideShowBtnClicked(_ sender: AnyObject)
    //    {
    //        self.btnStartSlideShow.isSelected = !self.btnStartSlideShow.isSelected
    //
    //        if self.btnStartSlideShow.isSelected
    //        {
    //            self.slideShowTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ImageSliderViewController.ChangeImage), userInfo: nil, repeats: true)
    //        }
    //        else
    //        {
    //            self.slideShowTimer.invalidate()
    //        }
    //
    //    }
    
    
    @IBAction func closeBtnClicked(_ sender: AnyObject)
    {
        self.dismissView()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(imageArray.count)
        return imageArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : ImageSliderCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageSlider", for: indexPath) as! ImageSliderCollectionViewCell
        
        if let urlStr = imageArray[indexPath.row]["image"]  as? String
        {

                //Load online url images
                let url = URL(string:APP_URL.BASE_URL + urlStr )!
                let placeholderImage = #imageLiteral(resourceName: "image_placeolder")
                
                cell.imageToZoom.af_setImage(
                    withURL: url,
                    placeholderImage: placeholderImage,
                    imageTransition: .crossDissolve(0.2)
                )
        }
        
        return cell
    }
    
    //MARK: // ----------------------------- Func ---------------------------------------

    func dismissView()
    {
        self.slideShowTimer.invalidate()
        self.dismiss(animated: true, completion: nil)
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Web APIs
    func callDeletePhotoAPI()
    {
        let pictureId : Int = (self.imageArray[getCurrentPage]["id"] as? Int)!
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        let requestObject = ["image_id" : pictureId]
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.DELETE_ACTIVITY_IMG, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            print(responseData)
            
            if isSuccess
            {
                let imgArray : NSArray = responseData["images"] as! NSArray

                var userDict = [String:Any]()
                userDict  = [ "data": imgArray]
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "imageDeleted"), object: userDict, userInfo: nil)
                
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }

}
