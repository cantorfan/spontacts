//
//  QuotesCollectionViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 10/10/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class QuotesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblQuote: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!

    @IBOutlet weak var butnForward: UIButton!
    
    @IBOutlet weak var butnBackward: UIButton!
}
