//
//  UserProfileTableViewCell.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 01/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class UserProfileTableViewCell: UITableViewCell
{

    @IBOutlet weak var imgViewUserPic: UIImageView!
  
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var btnMaleFemale: UIButton!
    
    @IBOutlet weak var lblAge: UILabel!
    
    @IBOutlet weak var btnHeart: UIButton!
    
    @IBOutlet weak var lblRelationStatus: UILabel!
    
    @IBOutlet weak var lblDistance: UILabel!
   
    @IBOutlet weak var btnMessage: UIButton!
    
    @IBOutlet weak var btnAddFriend: UIButton!
    
    
    @IBOutlet weak var btnUpcomingAct: UIButton!
    
    @IBOutlet weak var btnPastAct: UIButton!
    
    @IBOutlet weak var lblUserDesciption: UILabel!
    
    @IBOutlet weak var collectionViewFriends: UICollectionView!
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension UserProfileTableViewCell :  UICollectionViewDelegate , UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let mycell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserFriendsCollectionViewCell", for: indexPath) as! UserFriendsCollectionViewCell

        return mycell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//        if activityPara.actImgJSONArr.count != 0
//        {
//            pictureJSONToSend = activityPara.actImgJSONArr[indexPath.row] as! [String : Any]
//        }
//        
//        performSegue(withIdentifier: "segueShowActivityToImageView", sender: nil)
    }
}
