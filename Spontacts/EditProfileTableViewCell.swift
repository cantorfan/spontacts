//
//  EditProfileTableViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 12/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class EditProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var lblRelationshipOption: UILabel!
    @IBOutlet weak var lblRelationshipSelected: UILabel!

    @IBOutlet weak var lblLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
