//
//  Settings_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 12/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import FacebookLogin

class Settings_VC: UIViewController, SWRevealViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIDocumentInteractionControllerDelegate
{
    let arrHeaderTitles = ["Notifications", "Help", "Send Us Feedback", "Rate This App", "Terms & Policy", "Restore Subscription"]
    
    let sharedObj = Singleton.shared
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addLeftBarButton()
        title = "Settings"
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftBarButton()
    {
        let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu_new"), style: .plain,  target: self, action: #selector(didTapMenuButn))
        navigationItem.leftBarButtonItem = btnMenu
        
        if revealViewController() != nil
        {
            revealViewController().rightViewRevealWidth = 200
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }

    // MARK: - Action Methods
    @IBAction func didTapMenuButn()
    {
        revealViewController().revealToggle(animated: true)
    }

    @IBAction func didTapLogoutButn()
    {
        // Clear reference to Facebook account if any
        LoginManager().logOut()
        
        // Clear reference to Google account if any
        if GIDSignIn.sharedInstance() != nil
        {
            GIDSignIn.sharedInstance().signOut()
        }
        
        // Clear reference to Twitter account if any
        let store = Twitter.sharedInstance().sessionStore
        if let userID = store.session()?.userID {
            store.logOutUserID(userID)
            Twitter.sharedInstance().sessionStore.logOutUserID(userID)
        }
        
        // Clear current app userdata
        UserDefaults.standard.removeObject(forKey: "userToken")
        UserDefaults.standard.removeObject(forKey: "userJSON")
        sharedObj.userPara = Para_UserProfile()

        // Clear firebase notifications data
        UserDefaults.standard.removeObject(forKey: "fcmDeviceToken")
        
        // Clear previous messages
        DBManager().deleteDatabase()
        UserDefaults.standard.setValue(0, forKey: "groupCount")
        UserDefaults.standard.setValue(0, forKey: "messageCount")
        UserDefaults.standard.synchronize()
        
//        logoutQuickblox()
        
        QBRequest.logOut(successBlock: { (response) in
            
        }, errorBlock: { (error) in
            
        })
        
        //Login_VC
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let presentVC = mainStoryboard.instantiateViewController(withIdentifier: "Login_VC") as! Login_VC
        let NavVC : UINavigationController = UINavigationController(rootViewController: presentVC)
        self.present(NavVC, animated: false, completion: nil)
        //performSegue(withIdentifier: "segueSettingsToLoginScreen", sender: nil)
    }
    
    func logoutQuickblox() {
        
        if !QBChat.instance().isConnected {
            
            SVProgressHUD.showError(withStatus: "Error")
            return
        }
        
        SVProgressHUD.show(withStatus: "SA_STR_LOGOUTING".localized, maskType: SVProgressHUDMaskType.clear)
        
        ServicesManager.instance().logoutUserWithCompletion { [weak self] (boolValue) -> () in
            
            guard let strongSelf = self else { return }
            if boolValue {
                NotificationCenter.default.removeObserver(strongSelf)
                
                 //ServicesManager.instance().chatService.removeDelegate(strongSelf)
                //ServicesManager.instance().authService.remove(strongSelf)
                
                ServicesManager.instance().lastActivityDate = nil;
                
                let _ = strongSelf.navigationController?.popToRootViewController(animated: true)
                
                SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
            }
        }
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return arrHeaderTitles.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
        
        let cellLbl = UILabel(frame: CGRect(x: 10, y: 5, width: view.frame.size.width - 100, height: 40))
        cellLbl.textAlignment = .left
        
        if indexPath.section == 0
        {
            cellLbl.font = UIFont.init(name: Font.BEBAS, size: 17)
            cellLbl.textColor = UIColor.black.withAlphaComponent(0.8)
            cellLbl.text = arrHeaderTitles[indexPath.row]
        }
        else if indexPath.section == 1
        {
            cellLbl.frame = CGRect(x: 25, y: 5, width: view.frame.size.width - 100, height: 40)
            cellLbl.font = UIFont.init(name: Font.BEBAS, size: 16)
            cellLbl.textColor = UIColor.blue.withAlphaComponent(0.8)
            cellLbl.text = "Log out"
        }
        else
        {
            cellLbl.frame = CGRect(x: 25, y: 5, width: view.frame.size.width - 100, height: 40)
            cellLbl.font = UIFont.init(name: Font.BEBAS, size: 14)
            cellLbl.textColor = UIColor.black.withAlphaComponent(0.5)
            cellLbl.text = "GAY AND YOU? version: 1.0.0"
        }
        
        cell?.contentView.addSubview(cellLbl)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
 
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                let notificationVC = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "NotificationsSettingViewController") as! NotificationsSettingViewController
                self.navigationController?.pushViewController(notificationVC, animated: true)
            }
            else if indexPath.row == 1
            {
                let url = URL(string: "http://acencore.com/spontact/help.php")!
                if UIApplication.shared.canOpenURL(url)
                {
                    if #available(iOS 10.0, *)
                    {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else
                    {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(url)
                    }
                }
            }
            else if indexPath.row == 2
            {
                let feedbackVC = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                
                self.navigationController?.pushViewController(feedbackVC, animated: true)
            }
            else if indexPath.row == 3
            {
                let url = URL(string: "https://itunes.apple.com/us/app/gay-and-you/id1278713370?ls=1&mt=8")!
                if UIApplication.shared.canOpenURL(url)
                {
                    if #available(iOS 10.0, *)
                    {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else
                    {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(url)
                    }
                }
            }
            else if indexPath.row == 4
            {
                if let url = Bundle.main.url(forResource: "GAYANDYOU_terms_of_use_August_2017", withExtension: "pdf", subdirectory: nil, localization: nil)
                {
                    let documentInteractionController = UIDocumentInteractionController(url: url)
                    documentInteractionController.delegate = self
                    documentInteractionController.presentPreview(animated: false)
                }
            }
            else if indexPath.row == 5
            {
                callGetUserInformationAPI()
            }
        }
        else if indexPath.section == 1
        {
            didTapLogoutButn()
        }
    }

    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    func callGetUserInformationAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: APP_URL.GET_SELF_PROFILE, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    let userJSON = responseData["user"] as! [String : Any]
                    UserDefaults.standard.set(userJSON, forKey: "userJSON")
                    UserDefaults.standard.synchronize()
                    
                    self.sharedObj.userPara.userId = userJSON["id"] as! Int
                    self.sharedObj.userPara.userEmail = userJSON["email"] as! String
                    self.sharedObj.userPara.userLocationLat = userJSON["lat"] as! Double
                    self.sharedObj.userPara.userFirstName = userJSON["fname"] as! String
                    self.sharedObj.userPara.userLastName = userJSON["lname"] as! String
                    self.sharedObj.userPara.userToken = userJSON["token"] as! String
                    self.sharedObj.userPara.userLocationLong = userJSON["lng"] as! Double
                    //self.sharedObj.userPara.userPassword = userJSON["password"] as! String
                    self.sharedObj.userPara.userPictureThumbUrl = userJSON["thumb"] as! String
                    self.sharedObj.userPara.userPictureUrl = userJSON["picture"] as! String
                    self.sharedObj.userPara.userLocationStr = userJSON["location_string"] as! String
                    self.sharedObj.userPara.userAge = userJSON["age"] as! Int
                    self.sharedObj.userPara.userGender = userJSON["gender"] as! Int
                    self.sharedObj.userPara.userRelationStatus = userJSON["relationship_status"] as! Int
                    self.sharedObj.userPara.userDescription = userJSON["about"] as! String
                    self.sharedObj.userPara.isPlusMember = userJSON["suscribed"] as! Int
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }

    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }
}
