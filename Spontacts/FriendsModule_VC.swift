//
//  FriendsModule_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 20/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage
import FacebookCore
import FacebookLogin

class FriendsModule_VC: UIViewController
{
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    
    // Header View
    @IBOutlet weak var headerImgDiscover: UIImageView!
    @IBOutlet weak var headerLblDiscover: UILabel!
    @IBOutlet weak var headerLblDiscoverCount: UILabel!
    
    @IBOutlet weak var headerImgRequests: UIImageView!
    @IBOutlet weak var headerLblRequests: UILabel!
    @IBOutlet weak var headerLblRequestCount: UILabel!
    
    @IBOutlet weak var headerImgFacebook: UIImageView!
    @IBOutlet weak var headerLblFacebook: UILabel!
    @IBOutlet weak var headerLblFacebookCount: UILabel!
    
    @IBOutlet weak var headerImgMyFriends: UIImageView!
    @IBOutlet weak var headerLblMyFriends: UILabel!
    @IBOutlet weak var headerLblMyFriendsCount: UILabel!

    var view_width = CGFloat()
    var view_height = CGFloat()
    
    let sharedObj = Singleton.shared
    var selectedSeg = Int()
    
    var searchArray = NSMutableArray()
    let charSetEmail = CharacterSet(charactersIn: "@")
    let charSetName = CharacterSet(charactersIn: " ")
    var strSearchBar = String()
    
    var requestArray = NSMutableArray()
    //var sentRequestArray = NSMutableArray()
    
    var DISCOVER_ARRAY = NSMutableArray()
    var REQUESTS_ARRAY = NSMutableArray()
    var SENT_REQUESTS_ARRAY = NSMutableArray()
    var FRIENDS_ARRAY = NSMutableArray()
    var BLOCKED_USER_ARRAY = NSMutableArray()
    
    var peopleArray = NSMutableArray()
    
    var friendsArray = NSMutableArray()
    
    var fbFrndsArray = NSMutableArray()
    
    //var friendsSearchArray = NSMutableArray()
    
    // MARK: - View delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        view_width = view.frame.size.width
        view_height = view.frame.size.height
        title = "Friends"
        
        addLeftBarButton()
        //setUpSegmentedControl()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        // Get them while entering the view
        // retrieveFriendRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        retrieveFriendRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        retrievePeopleYouMayKnowList()
        
    }
    
    // MARK: - Custom Methods
    func addLeftBarButton()
    {
        let customView = UIView.init(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setImage(#imageLiteral(resourceName: "menu_white"), for: .normal)
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(self.didTapMenuButn), for: .touchUpInside)
        
        let label = UILabel.init(frame: CGRect(x: 30, y: 5, width: 15, height: 15))
        label.backgroundColor = UIColor.white
        label.layer.cornerRadius = label.frame.size.width / 2
        label.clipsToBounds =  true
        
        customView.addSubview(button)
        
        if sharedObj.unread_Friends_Count != 0
        {
            customView.addSubview(label)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: customView) // customView
        
        if revealViewController() != nil
        {
            revealViewController().rightViewRevealWidth = 200
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func filterProjectsForBy(search: String)
    {
        var searchPredicate = NSPredicate()
        
        if search != ""
        {
            searchPredicate = NSPredicate(format: "((fname CONTAINS[C] %@) OR (lname CONTAINS[C] %@))", search, search)
        }
        
        // Apply filter to online data
        friendsArray = (FRIENDS_ARRAY as NSArray).filtered(using: searchPredicate) as! NSMutableArray
        
//        if self.refreshControl.isRefreshing
//        {
//            self.refreshControl.endRefreshing()
//        }
        
        self.tableView.reloadData()
    }
    
    
    // MARK: - UI Methods
    func uiDiscoverView() -> UIView
    {
        let subContainerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view_width, height: self.tableView.frame.size.height - 50))
        subContainerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")

        let btnInvite = UIButton.init(frame: CGRect(x: 20, y: subContainerView.frame.midY - 20, width: view.frame.size.width - 40, height: 40))
        btnInvite.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
        btnInvite.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnInvite.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnInvite.addTarget(self, action: #selector(self.didTapInviteNow), for: .touchUpInside)
        btnInvite.setTitleColor(UIColor.white, for: .normal)
        btnInvite.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 18)
        btnInvite.setTitle("INVITE FRIENDS NOW", for: .normal)
        subContainerView.addSubview(btnInvite)

        let lblDidntMatch = UILabel(frame: CGRect(x: 30, y: btnInvite.frame.origin.y - 80, width: view_width - 60, height: 60))
        lblDidntMatch.textAlignment = .center
        lblDidntMatch.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblDidntMatch.numberOfLines = 0
        lblDidntMatch.lineBreakMode = .byWordWrapping
        lblDidntMatch.text = "Unfortunately your search didn't match any result."
        subContainerView.addSubview(lblDidntMatch)
        
        
        let lblInvite = UILabel(frame: CGRect(x: 30, y: btnInvite.frame.origin.y + btnInvite.frame.size.height + 20, width: view_width - 60, height: 80))
        lblInvite.textAlignment = .center
        lblInvite.font = UIFont.init(name: Font.BEBAS, size: 13)
        lblInvite.numberOfLines = 0
        lblInvite.lineBreakMode = .byWordWrapping
        lblInvite.text = "Invite your friends to GAY AND YOU?"
        subContainerView.addSubview(lblInvite)
        
        return subContainerView
    }
    
    func uiFrndRequest() -> UIView
    {
        let mainContainerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view_width, height: tableView.frame.size.height))
        mainContainerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let btnFindFriends = UIButton.init(frame: CGRect(x: 20, y: mainContainerView.frame.midY - 20, width: view.frame.size.width - 40, height: 40))
        btnFindFriends.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnFindFriends.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnFindFriends.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
        btnFindFriends.setTitleColor(UIColor.white, for: .normal)
        btnFindFriends.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 18)
        btnFindFriends.addTarget(self, action: #selector(self.didTapFindFriends), for: .touchUpInside)
        btnFindFriends.setTitle("FIND FRIENDS", for: .normal)
        mainContainerView.addSubview(btnFindFriends)

        
        let lblNoFrndReq = UILabel(frame: CGRect(x: 30, y: btnFindFriends.frame.origin.y - 80, width: view_width - 60, height: 60))
        lblNoFrndReq.textAlignment = .center
        lblNoFrndReq.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblNoFrndReq.numberOfLines = 0
        lblNoFrndReq.lineBreakMode = .byWordWrapping
        lblNoFrndReq.text = "You have no friend requests"
        mainContainerView.addSubview(lblNoFrndReq)
        
        
        let lblInvite = UILabel(frame: CGRect(x: 30, y: btnFindFriends.frame.origin.y + btnFindFriends.frame.size.height + 20, width: view_width - 60, height: 80))
        lblInvite.textAlignment = .center
        lblInvite.font = UIFont.init(name: Font.BEBAS, size: 13)
        lblInvite.numberOfLines = 0
        lblInvite.lineBreakMode = .byWordWrapping
        lblInvite.text = "You currently have no friend requests.\nFind new friends now."
        mainContainerView.addSubview(lblInvite)
        
        return mainContainerView
    }
    
    func uiFacebook() -> UIView
    {
        let mainContainerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view_width, height: tableView.frame.size.height))
        mainContainerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let btnFindFriends = UIButton.init(frame: CGRect(x: 20, y: mainContainerView.frame.midY  - 20, width: view.frame.size.width - 40, height: 40))
        btnFindFriends.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnFindFriends.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnFindFriends.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
        btnFindFriends.setTitleColor(UIColor.white, for: .normal)
        btnFindFriends.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 18)
        btnFindFriends.addTarget(self, action: #selector(self.didTapConnectWithFacebook), for: .touchUpInside)
        btnFindFriends.setTitle("CONNECT WITH FACEBOOK NOW", for: .normal)
        mainContainerView.addSubview(btnFindFriends)

        
        let lblNoFrndReq = UILabel(frame: CGRect(x: 30, y: btnFindFriends.frame.origin.y - 80, width: view_width - 60, height: 60))
        lblNoFrndReq.textAlignment = .center
        lblNoFrndReq.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblNoFrndReq.numberOfLines = 0
        lblNoFrndReq.lineBreakMode = .byWordWrapping
        lblNoFrndReq.text = "Not connected with Facebook"
        mainContainerView.addSubview(lblNoFrndReq)
        
        
        let lblInvite = UILabel(frame: CGRect(x: 30, y: btnFindFriends.frame.origin.y + btnFindFriends.frame.size.height + 10, width: view_width - 60, height: 80))
        lblInvite.textAlignment = .center
        lblInvite.font = UIFont.init(name: Font.BEBAS, size: 13)
        lblInvite.numberOfLines = 0
        lblInvite.lineBreakMode = .byWordWrapping
        lblInvite.text = "Connect your Facebook account with GAY AND YOU? to check out which of your Facebook friends are already using GAY AND YOU?"
        mainContainerView.addSubview(lblInvite)
        
        return mainContainerView
    }
    
    func uiMyFriendsList() -> UIView
    {
        let mainContainerView = UIView.init(frame: CGRect(x: 0, y: 5, width: view_width, height: tableView.frame.size.height))
        mainContainerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let btnFindFriends = UIButton.init(frame: CGRect(x: 20, y: mainContainerView.frame.midY  - 20, width: view.frame.size.width - 40, height: 40))
        btnFindFriends.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnFindFriends.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnFindFriends.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
        btnFindFriends.setTitleColor(UIColor.white, for: .normal)
        btnFindFriends.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 18)
        btnFindFriends.setTitle("FIND FRIENDS", for: .normal)
        btnFindFriends.addTarget(self, action: #selector(self.didTapFindFriends), for: .touchUpInside)
        mainContainerView.addSubview(btnFindFriends)
        
        let lblNoFrndReq = UILabel(frame: CGRect(x: 30, y: btnFindFriends.frame.origin.y - 80, width: view_width - 60, height: 60))
        lblNoFrndReq.textAlignment = .center
        lblNoFrndReq.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblNoFrndReq.numberOfLines = 0
        lblNoFrndReq.lineBreakMode = .byWordWrapping
        lblNoFrndReq.text = "No friends yet"
        mainContainerView.addSubview(lblNoFrndReq)
        
        
        let lblInvite = UILabel(frame: CGRect(x: 30, y: btnFindFriends.frame.origin.y + btnFindFriends.frame.size.height + 10, width: view_width - 60, height: 80))
        lblInvite.textAlignment = .center
        lblInvite.font = UIFont.init(name: Font.BEBAS, size: 13)
        lblInvite.numberOfLines = 0
        lblInvite.lineBreakMode = .byWordWrapping
        lblInvite.text = "You currently have no friends.\nFind new friends now."
        mainContainerView.addSubview(lblInvite)
        
        return mainContainerView
    }
    
    // MARK: - Action Methods
    @IBAction func didTapMenuButn()
    {
        revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func didTapInviteNow()
    {
        // text to share
        let text = "Hi, come and join this awesome app. Download the app from here. https://itunes.apple.com/us/app/gay-and-you/id1278713370?ls=1&mt=8"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        //        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    func didTapFindFriends()
    {
        selectedSeg = 0
        tableView.reloadData()
    }

    @IBAction func didTapHeaderButns(_ sender: Any)
    {
        self.view.endEditing(true)
        
        let btn = sender as! UIButton
        self.selectedSeg = btn.tag / 100 - 1
      
        self.tableView.reloadData()
        headerImgDiscover.image = #imageLiteral(resourceName: "discover_gray")
        headerImgRequests.image = #imageLiteral(resourceName: "requests_gray")
        headerImgFacebook.image = #imageLiteral(resourceName: "facebook_gray")
        headerImgMyFriends.image = #imageLiteral(resourceName: "myFriends_gray")
        
        headerLblDiscover.textColor = UIColor.black.withAlphaComponent(0.6)
        headerLblRequests.textColor = UIColor.black.withAlphaComponent(0.6)
        headerLblFacebook.textColor = UIColor.black.withAlphaComponent(0.6)
        headerLblMyFriends.textColor = UIColor.black.withAlphaComponent(0.6)

        let yellowColor = UIColor.init(red: 210/255, green: 153/255, blue: 58/255, alpha: 1.0)
        
        if selectedSeg == 0
        {
            headerImgDiscover.image = #imageLiteral(resourceName: "discover")
            headerLblDiscover.textColor = yellowColor
        }
        if selectedSeg == 1
        {
            headerImgRequests.image = #imageLiteral(resourceName: "requests")
            headerLblRequests.textColor = yellowColor

            retrieveFriendRequest()
        }
        else if selectedSeg == 2
        {
            headerImgFacebook.image = #imageLiteral(resourceName: "facebook_friends")
            headerLblFacebook.textColor = yellowColor

            retrieveFacebookFriendsList()
        }
        else if selectedSeg == 3
        {
            headerImgMyFriends.image = #imageLiteral(resourceName: "myFriends")
            headerLblMyFriends.textColor = yellowColor

            retrieveMyFriendList()
        }
    }
    
    
    @IBAction func didTapButnAddDiscover(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        if selectedSeg == 0
        {
            var userJSON = peopleArray[(indexPath?.row)!] as! [String : Any]
            let userId : Int = userJSON["id"] as! Int
            callSendFriendRequestAPI(userID: userId)
            
            // Add to local array to change it's image to cancel
            userJSON["is_requested"] = 1
            DISCOVER_ARRAY[(indexPath?.row)!] = userJSON
            
            //DISCOVER_ARRAY.removeObject(at: (indexPath?.row)!)
            peopleArray  = DISCOVER_ARRAY
            //selectedSeg = 1
            tableView.reloadData()
        }
    }
    
    @IBAction func didTapCancelSentRequest (_ sender : Any)
    {
        if selectedSeg == 0
        {
            let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tableView)
            let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
            
            var userJSON = DISCOVER_ARRAY[(indexPath?.row)!] as! [String : Any]
            let userId : Int = userJSON["id"] as! Int
            callCancel_SentRequestAPI(userID: userId)
            
            userJSON["is_requested"] = 0
            DISCOVER_ARRAY[(indexPath?.row)!] = userJSON
            tableView.reloadData()
        }
        else
        {
            // Not in use
            let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tableView)
            let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
            
            let userJSON = SENT_REQUESTS_ARRAY[(indexPath?.row)!] as! [String : Any]
            let userId : Int = userJSON["id"] as! Int
            callCancel_SentRequestAPI(userID: userId)
            
            SENT_REQUESTS_ARRAY.removeObject(at: (indexPath?.row)!)
            tableView.reloadData()

        }
    }
    
    @IBAction func didTapButnAcceptRequest(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        if indexPath?.section == 0
        {
            let userJSON = requestArray[(indexPath?.row)!] as! [String : Any]
            let requestId : Int = userJSON["friend_request_id"] as! Int
            
            // Remove from local array as action is taken against this request.
            requestArray.removeObject(at: (indexPath?.row)!)
            if requestArray.count != 0
            {
                self.headerLblRequestCount.text = "\(requestArray.count)"
                self.headerLblRequestCount.isHidden = false
            }
            else
            {
                self.headerLblRequestCount.isHidden = true
            }
            tableView.reloadData()
            
            callAcceptFriendRequestAPI(requestId: requestId)
        }
        else if indexPath?.section == 1
        {
            let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tableView)
            let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
            
            let userJSON = SENT_REQUESTS_ARRAY[(indexPath?.row)!] as! [String : Any]
            let userId : Int = userJSON["id"] as! Int
            callCancel_SentRequestAPI(userID: userId)
            
            SENT_REQUESTS_ARRAY.removeObject(at: (indexPath?.row)!)
            tableView.reloadData()
        }
        else if indexPath?.section == 2
        {
            let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tableView)
            let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
            
            let userJSON = BLOCKED_USER_ARRAY[(indexPath?.row)!] as! [String : Any]
            let userId : Int = userJSON["id"] as! Int
            callUnblock_UserAPI(userID: userId)
            
            BLOCKED_USER_ARRAY.removeObject(at: (indexPath?.row)!)
            tableView.reloadData()
        }
    }
    
    @IBAction func didTapRejectRequests(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let userJSON = requestArray[(indexPath?.row)!] as! [String : Any]
        let requestId : Int = userJSON["friend_request_id"] as! Int
        
        // Remove from local array as action is taken against this request.
        requestArray.removeObject(at: (indexPath?.row)!)
        tableView.reloadData()
        callDeclineFriendRequestAPI(requestId: requestId)
    }
    
    @IBAction func didTapConnectWithFacebook()
    {
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile, .email , .userFriends], viewController: self)
        { loginResult in
            switch loginResult
            {
            case .failed(let error):
                print(error.localizedDescription)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                // print("Granted permissions -", grantedPermissions)
                
                // print("Declined permissions -", declinedPermissions)
                print("Access token - ", accessToken)
                
                if AccessToken.current != nil
                {
                    print("Got access token")
                    self.callFacebookLoginAPI(fbToken: "\(accessToken.authenticationToken)")
                }
            }
        }
    }
    
    @IBAction func didTapAddFacebookFriend(_ sender: Any)
    {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let userJSON = fbFrndsArray[(indexPath?.row)!] as! [String : Any]
        let userId : Int = userJSON["id"] as! Int
        callSendFriendRequestAPI(userID: userId)
        
        // Add to local array to change it's image to cancel
        tableView.reloadData()
    }
}

// MARK: - Navigation
extension FriendsModule_VC : SWRevealViewControllerDelegate
{
    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }
}

// MARK: - TableView Datasource and Delegates
extension FriendsModule_VC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if selectedSeg == 0
        {
            return 2 //3
        }
        else if selectedSeg == 1
        {
            return 3
        }
        else if selectedSeg == 2
        {
            return 1
        }
        else if selectedSeg == 3
        {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if selectedSeg == 0
        {
            if section == 1
            {
                if peopleArray.count == 0
                {
                    let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
                    emptyLabel.text = "No People Found"
                    emptyLabel.textColor = UIColor.lightGray
                    emptyLabel.textAlignment = NSTextAlignment.center
                    self.tableView.separatorStyle = .none
                    self.tableView.backgroundView = uiDiscoverView()
                    return 0
                }
                else
                {
                    self.tableView.separatorStyle = .singleLine
                    self.tableView.backgroundView?.isHidden = true
                    return peopleArray.count
                }
            }
            
            return 0
        }
        else if selectedSeg == 1
        {
            if requestArray.count == 0 && SENT_REQUESTS_ARRAY.count == 0 && BLOCKED_USER_ARRAY.count == 0
            {
                self.tableView.separatorStyle = .none
                self.tableView.backgroundView = uiFrndRequest()
                return 0
            }
            else
            {
                self.tableView.separatorStyle = .singleLine
                self.tableView.backgroundView?.isHidden = true
                
                if section == 0
                {
                    return requestArray.count
                }
                else if section == 1
                {
                    return SENT_REQUESTS_ARRAY.count
                }
                else if section == 2
                {
                    return BLOCKED_USER_ARRAY.count
                }
            }
        }
        else if selectedSeg == 2
        {
            if fbFrndsArray.count == 0
            {
                self.tableView.separatorStyle = .none
                self.tableView.backgroundView = uiFacebook()
                return 0
            }
            else
            {
                self.tableView.separatorStyle = .singleLine
                self.tableView.backgroundView?.isHidden = true
                return fbFrndsArray.count
            }
        }
        else if selectedSeg == 3
        {
            if section == 1
            {
                // Have no friends
                if FRIENDS_ARRAY.count == 0
                {
                    self.tableView.separatorStyle = .none
                    self.tableView.backgroundView = uiMyFriendsList()
                    return 0
                }
                    // Can be searched friends
                else if friendsArray.count == 0
                {
                    let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
                    emptyLabel.text = "No Friend matches Search Results"
                    emptyLabel.textColor = UIColor.lightGray
                    emptyLabel.textAlignment = NSTextAlignment.center
                    emptyLabel.backgroundColor = UIColor.clear
                    self.tableView.separatorStyle = .none
                    self.tableView.backgroundView = emptyLabel
                    return 0
                }
                else
                {
                    self.tableView.separatorStyle = .singleLine
                    self.tableView.backgroundView?.isHidden = true
                    return friendsArray.count
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if selectedSeg == 0
        {
            if section == 0
            {
                return 50
            }
            else if section == 1
            {
                if peopleArray.count != 0
                {
                    return 20
                }
                else
                {
                    return 0
                }
            }
        }
        else if selectedSeg == 1
        {
            if requestArray.count != 0
            {
                if section == 0
                {
                    return 25
                }
            }
            if SENT_REQUESTS_ARRAY.count != 0
            {
                if section == 1
                {
                    return 25
                }
            }
            
                if section == 2 && BLOCKED_USER_ARRAY.count != 0
                {
                    return 25
                }
            
        }
        else if selectedSeg == 2
        {
            return 0
        }
        else if selectedSeg == 3
        {
            if section == 0
            {
                return 50
            }
            else
            {
                return 30
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
//        if indexPath.section == 0
//        {
//            if selectedSeg == 1
//            {
//                return 80
//            }
//            else if selectedSeg == 2
//            {
//                return 80
//            }
//            else
//            {
//                return 80
//            }
//        }
//        
//        if selectedSeg == 0
//        {
//            if indexPath.section == 1
//            {
//                return 80
//            }
//        }
//        else if selectedSeg == 1
//        {
//            if indexPath.section == 0
//            {
//                return 80
//            }
//        }
//        else if selectedSeg == 2
//        {
//            if indexPath.section == 0
//            {
//                return 80
//            }
//        }
//        else if selectedSeg == 3
//        {
//            if indexPath.section == 1
//            {
//                return 80
//            }
//        }
        return 80
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()//.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        if selectedSeg == 0
        {
            // Add search bar
            if section == 0
            {
                let searchBar = UISearchBar.init(frame: CGRect(x: 10, y: 5, width: view_width - 20, height: 40))
                searchBar.placeholder = "First and last name or email"
                searchBar.delegate = self
                searchBar.returnKeyType = .search
                searchBar.text = strSearchBar
                headerView.addSubview(searchBar)
            }
            else //if section == 1
            {
                if peopleArray.count != 0
                {
                    let lblPeople = UILabel.init(frame: CGRect(x: 10, y: 0, width: view_width - 20, height: 20))
                    lblPeople.textAlignment = .left
                    lblPeople.font = UIFont.init(name: Font.BEBAS, size: 15)
                    lblPeople.numberOfLines = 0
                    lblPeople.lineBreakMode = .byWordWrapping
                    lblPeople.text = "People you may know :"
                    headerView.addSubview(lblPeople)
                }
            }
        }
        else if selectedSeg == 1
        {
            if section == 0
            {
                if requestArray.count != 0
                {
                    let requstLbl = UILabel.init(frame: CGRect(x: 10, y: 5, width: view_width - 20, height: 20))
                    requstLbl.textAlignment = .left
                    requstLbl.font = UIFont.init(name: Font.BEBAS, size: 15)
                    requstLbl.numberOfLines = 0
                    requstLbl.lineBreakMode = .byWordWrapping
                    requstLbl.text = "Friend Requests:"
                    headerView.addSubview(requstLbl)
                }
            }
            else if section == 1
            {
                if SENT_REQUESTS_ARRAY.count != 0
                {
                    let requstLbl = UILabel.init(frame: CGRect(x: 10, y: 5, width: view_width - 20, height: 20))
                    requstLbl.textAlignment = .left
                    requstLbl.font = UIFont.init(name: Font.BEBAS, size: 15)
                    requstLbl.numberOfLines = 0
                    requstLbl.lineBreakMode = .byWordWrapping
                    requstLbl.text = "Sent Requests:"
                    headerView.addSubview(requstLbl)
                }
            }
            else if section == 2
            {
                if BLOCKED_USER_ARRAY.count != 0
                {
                    let requstLbl = UILabel.init(frame: CGRect(x: 10, y: 5, width: view_width - 20, height: 20))
                    requstLbl.textAlignment = .left
                    requstLbl.font = UIFont.init(name: Font.BEBAS, size: 15)
                    requstLbl.numberOfLines = 0
                    requstLbl.lineBreakMode = .byWordWrapping
                    requstLbl.text = "Ignored Users:"
                    headerView.addSubview(requstLbl)
                }
            }
        }
        else if selectedSeg == 2
        {
            //            if section == 0
            //            {
            //                if fbFrndsArray.count != 0
            //                {
            //                    // Add invite and all other things
            //                }
            //                else
            //                {
            //                    headerView.addSubview(uiFacebook())
            //                }
            //            }
        }
        else if selectedSeg == 3
        {
            // Add search bar
            if section == 0
            {
                let searchBar = UISearchBar.init(frame: CGRect(x: 10, y: 5, width: view_width - 20, height: 40))
                searchBar.placeholder = "First and last name or email"
                searchBar.delegate = self
                searchBar.returnKeyType = .search
                searchBar.text = strSearchBar
                headerView.addSubview(searchBar)
            }
            else if section == 1
            {
                if friendsArray.count != 0
                {
                    let lblPeople = UILabel.init(frame: CGRect(x: 10, y: 0, width: view_width - 20, height: 30))
                    lblPeople.textAlignment = .left
                    lblPeople.font = UIFont.init(name: Font.BEBAS, size: 20)
                    lblPeople.numberOfLines = 0
                    lblPeople.lineBreakMode = .byWordWrapping
                    lblPeople.text = "Friends :"
                    headerView.addSubview(lblPeople)
                }
                else
                {
                    let lblInvite = UILabel(frame: CGRect(x: 10, y: 0, width: view_width / 2, height: 35))
                    lblInvite.textAlignment = .left
                    lblInvite.font = UIFont.init(name: Font.BEBAS, size: 15)
                    lblInvite.numberOfLines = 0
                    lblInvite.lineBreakMode = .byWordWrapping
                    lblInvite.text = "Invite more friends?"
                    headerView.addSubview(lblInvite)
                    
                    let btnInvite = UIButton.init(frame: CGRect(x: view_width - 120, y: lblInvite.frame.origin.y + 5, width: 100 , height: 30))
                    btnInvite.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
                    btnInvite.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
                    btnInvite.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
                    btnInvite.addTarget(self, action: #selector(self.didTapInviteNow), for: .touchUpInside)
                    btnInvite.setTitleColor(UIColor.white, for: .normal)
                    btnInvite.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 15)
                    btnInvite.setTitle("INVITE NOW", for: .normal)
                    headerView.addSubview(btnInvite)
                }
            }
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if selectedSeg == 0
        {
            let cell : FriendModuleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellDiscover", for: indexPath) as! FriendModuleTableViewCell

            let userJSON = peopleArray[indexPath.row] as! [String : Any]

            let pictureURL : String = userJSON["thumb"] as! String
            if pictureURL != ""
            {
                let url = URL(string: APP_URL.BASE_URL + pictureURL )!
                let placeholderImage = #imageLiteral(resourceName: "avatar")
                let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
                
                //,filter: filter
                cell.imgViewDiscover.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                    { response in
                }
                )
            }

            let firstName : String = userJSON["fname"] as! String
            let lastName : String = userJSON["lname"] as! String
            
            cell.lblUserNameDiscover.text = firstName + " "  + lastName
            
            let distance : Int = userJSON["distance"] as! Int
            let locationStr : String = userJSON["location_string"] as! String
            
            cell.lblAddressDiscover.text = locationStr + "(< \(distance) km)"
            
            cell.btnAddDiscover.isHidden = false
            cell.btnCancelRequestDiscover.isHidden = true

            if userJSON["is_requested"] == nil {
                return cell
            }
            
            if userJSON["is_requested"] as! Int == 1
            {
                cell.btnCancelRequestDiscover.isHidden = false
                cell.btnAddDiscover.isHidden = true
            }
            else
            {
                cell.btnCancelRequestDiscover.isHidden = true
                cell.btnAddDiscover.isHidden = false
            }
            return cell
        }
        else if selectedSeg == 1
        {
            let cell : FriendModuleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellRequests", for: indexPath) as! FriendModuleTableViewCell
            
            var userJSON =  [String : Any]()
            
            if indexPath.section == 0
            {
                userJSON = requestArray[indexPath.row] as! [String : Any]
                cell.btnAcceptRequest.addTarget(self, action: #selector(self.didTapButnAcceptRequest(_:)), for: .touchUpInside)
            }
            else if indexPath.section == 1
            {
                userJSON = SENT_REQUESTS_ARRAY[indexPath.row] as! [String : Any]
                
                cell.btnRejectRequest.isHidden = true
                cell.btnAcceptRequest.setImage(#imageLiteral(resourceName: "decline_request"), for: .normal)
            }
            else if indexPath.section == 2
            {
                userJSON = BLOCKED_USER_ARRAY[indexPath.row] as! [String : Any]
                
                cell.btnRejectRequest.isHidden = true
                cell.btnAcceptRequest.setImage(#imageLiteral(resourceName: "decline_request"), for: .normal)
            }
            
            let pictureURL : String = userJSON["thumb"] as! String
            if pictureURL != ""
            {
                let url = URL(string: APP_URL.BASE_URL + pictureURL )!
                let placeholderImage = #imageLiteral(resourceName: "avatar")
                let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
                
                //,filter: filter
                cell.imgViewRequest.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                    { response in
                }
                )
            }
            
            let firstName : String = userJSON["fname"] as! String
            let lastName : String = userJSON["lname"] as! String
            
            cell.lblUserNameRequests.text = firstName + " "  + lastName
            
            let distance : Int = userJSON["distance"] as! Int
            let locationStr : String = userJSON["location_string"] as! String
            
            cell.lblAddressRequest.text = locationStr + "(< \(distance) km)"
            
            return cell
        }
        else if selectedSeg == 2
        {
            let cell : FriendModuleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellFacebookFriends", for: indexPath) as! FriendModuleTableViewCell
            
            let userJSON = fbFrndsArray[indexPath.row] as! [String : Any]
            
            let pictureURL : String = userJSON["thumb"] as! String
            if pictureURL != ""
            {
                let url = URL(string: APP_URL.BASE_URL + pictureURL )!
                let placeholderImage = #imageLiteral(resourceName: "avatar")
                let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
                
                cell.imgViewFacebook.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                    { response in
                }
                )
            }
            
            let isFriend = userJSON["is_friend"] as! Int
            if isFriend == 1
            {
                cell.btnAddFacebookFriend.isHidden = true
                cell.imgViewFacebookFriends.isHidden = false
            }
            else
            {
                cell.imgViewFacebookFriends.isHidden = true
                cell.btnAddFacebookFriend.isHidden = false
            }
            
            let firstName : String = userJSON["fname"] as! String
            let lastName : String = userJSON["lname"] as! String
            
            cell.lblUserNameFacebook.text = firstName + " "  + lastName
            
            let distance : Int = userJSON["distance"] as! Int
            let locationStr : String = userJSON["location_string"] as! String
            
            cell.lblAddressFacebook.text = locationStr + "(< \(distance) km)"
            
            return cell
        }
        else
        {
            let cell : FriendModuleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMyFriends", for: indexPath) as! FriendModuleTableViewCell

            let userJSON = friendsArray[indexPath.row] as! [String : Any]
            
            let pictureURL : String = userJSON["thumb"] as! String
            if pictureURL != ""
            {
                let url = URL(string: APP_URL.BASE_URL + pictureURL )!
                let placeholderImage = #imageLiteral(resourceName: "avatar")
                let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
                
                //,filter: filter
                cell.imgViewMyFriends.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                    { response in
                }
                )
            }
            
            let firstName : String = userJSON["fname"] as! String
            let lastName : String = userJSON["lname"] as! String
            
            cell.lblUserNameMyFriends.text = firstName + " "  + lastName
            
            let distance : Int = userJSON["distance"] as! Int
            let locationStr : String = userJSON["location_string"] as! String
            
            cell.lblAddressMyFriends.text = locationStr + "(< \(distance) km)"
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfile_VC") as! UserProfile_VC

        if selectedSeg == 0
        {
            let userJSON = peopleArray[indexPath.row] as! [String : Any]
            presentVC.user_Id = userJSON["id"] as! Int
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
        else if selectedSeg == 1
        {
            var userJSON =  [String : Any]()
            
            if indexPath.section == 0
            {
                userJSON = requestArray[indexPath.row] as! [String : Any]
            }
            else if indexPath.section == 1
            {
                userJSON = SENT_REQUESTS_ARRAY[indexPath.row] as! [String : Any]
            }
            else if indexPath.section == 2
            {
                userJSON = BLOCKED_USER_ARRAY[indexPath.row] as! [String : Any]
            }
            
            presentVC.user_Id = userJSON["id"] as! Int
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
        else if selectedSeg == 2
        {
            let userJSON = fbFrndsArray[indexPath.row] as! [String : Any]
            presentVC.user_Id = userJSON["id"] as! Int
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
        // See friends list friend
        else if selectedSeg == 3
        {
            let userJSON = friendsArray[indexPath.row] as! [String : Any]
            presentVC.user_Id = userJSON["id"] as! Int
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
}

//MARK: - Search Delegate

extension FriendsModule_VC : UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        view.endEditing(true)
        
        let strSearch = searchBar.text!
        strSearchBar = strSearch
        
        // Perform user search
        if selectedSeg == 0
        {
            var strSearchQuery = ""
            
            // If its email
            if strSearch.rangeOfCharacter(from: charSetEmail) != nil
            {
                print("has email")
                strSearchQuery = "email=\(strSearch)"
            }
            else if strSearch.rangeOfCharacter(from: charSetName) != nil
            {
                print("first name and last name")
                
                var myStringArr = strSearch.components(separatedBy: " ")
                
                let fname: String = myStringArr [0]
                let lname: String = myStringArr [1]
                
                strSearchQuery = "fname=\(fname)&lname=\(lname)"
            }
            else
            {
                print("first name")
                strSearchQuery = "fname=\(strSearch)&lname="
            }
            
            print(strSearchQuery)
            callGeneralSearchAPI(strSearchQuery: strSearchQuery)
        }
            // Perform friends search
        else if selectedSeg == 3
        {
            filterProjectsForBy(search: strSearch)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        // Logic to implement when clear button is pressedf
        if searchText.characters.count == 0
        {
            if selectedSeg == 0
            {
                searchBar.resignFirstResponder()
                self.view.endEditing(true)
                
                strSearchBar = ""
                searchArray = NSMutableArray()
                
                peopleArray = DISCOVER_ARRAY
                tableView.reloadData()
            }
            else if selectedSeg == 3
            {
                strSearchBar = ""
                friendsArray = FRIENDS_ARRAY
                tableView.reloadData()
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        print(searchBar.text!)
    }
}

// MARK: - Web API
extension FriendsModule_VC
{
    // MARK: - GET API
    func retrieveFriendRequest()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Getting requests")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject = [ "lat" : sharedObj.userPara.userLocationLat,
                              "lng" : sharedObj.userPara.userLocationLong]
        
        GlobalConstants().postData_Header_Parameter(subUrl: "v1/get_friend_request_list", header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                //print(responseData)
                
                if isSuccess
                {
                    print(responseData)
                    
                    if responseData["friend_requests"] != nil
                    {
                        let array = responseData["friend_requests"] as! NSArray
                        self.requestArray = array.mutableCopy() as! NSMutableArray
                        self.REQUESTS_ARRAY = self.requestArray
                        
                        if self.requestArray.count != 0
                        {
                            self.headerLblRequestCount.isHidden = false
                            self.headerLblRequestCount.text = "\(self.requestArray.count)"
                        }
                        self.tableView.reloadData()
                    }
                    
                    if responseData["sent_requests"] != nil
                    {
                        let array = responseData["sent_requests"] as! NSArray
                        self.SENT_REQUESTS_ARRAY = array.mutableCopy() as! NSMutableArray
                        
                        self.tableView.reloadData()
                    }
                    
                    if responseData["blocked_users"] != nil
                    {
                        let array = responseData["blocked_users"] as! NSArray
                        self.BLOCKED_USER_ARRAY = array.mutableCopy() as! NSMutableArray
                        
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func retrievePeopleYouMayKnowList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let strUrl = "v1/search_friend?lat=\(sharedObj.userPara.userLocationLat)&lng=\(sharedObj.userPara.userLocationLong)"
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl:strUrl , header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["user"] != nil
                    {
                        let array = responseData["user"] as! NSArray
                        print("people you may know ", array.count)
                        self.peopleArray = array.mutableCopy() as! NSMutableArray
                        self.DISCOVER_ARRAY = self.peopleArray
                        
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func retrieveFacebookFriendsList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Fetching Friends")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject = [ "lat" : sharedObj.userPara.userLocationLat,
                              "lng" : sharedObj.userPara.userLocationLong]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.GET_FB_FRNDS, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["user"] != nil
                    {
                        print(responseData["user"] as Any)
                        let array = responseData["user"] as! NSArray
                        print("friends array ", array.count)
                        self.fbFrndsArray = array.mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func retrieveMyFriendList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject = [ "lat" : sharedObj.userPara.userLocationLat,
                              "lng" : sharedObj.userPara.userLocationLong]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.GET_MY_FRND_LIST, header: headerObject, parameter: requestObject, completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            if isSuccess
            {
                if responseData["friend_list"] != nil
                {
                    print(responseData["friend_list"] as Any)
                    let array = responseData["friend_list"] as! NSArray
                    print("friends array ", array.count)
                    self.friendsArray = array.mutableCopy() as! NSMutableArray
                    self.FRIENDS_ARRAY = self.friendsArray
                    self.tableView.reloadData()
                }
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        } )
    }
    
    func callGeneralSearchAPI(strSearchQuery : String)
    {
        let subURL = "v1/search_user?" + strSearchQuery
        let url = subURL.appending("&lat=\(sharedObj.userPara.userLocationLat)&lng=\(sharedObj.userPara.userLocationLong)")
        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Searching Users..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = [ "lat" : sharedObj.userPara.userLocationLat,
                              "lng" : sharedObj.userPara.userLocationLong]
        
        GlobalConstants().postData_Header_Parameter(subUrl: url, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                //print(responseData)
                
                if isSuccess
                {
                    print(responseData)
                    
                    if responseData["user"] != nil
                    {
                        let array = responseData["user"] as! NSArray
                        self.peopleArray = array.mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    // MARK: - Action API
    func callSendFriendRequestAPI(userID : Int)
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["request_to_id" : userID]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.SEND_REQ, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Ooops!", msg: responseMessage)
            }
        })
    }
    
    func callCancel_SentRequestAPI(userID : Int)
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["user_id" : userID]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.CANCEL_SENT_REQ, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Ooops!", msg: responseMessage)
            }
        })
    }
    
    func callDeclineFriendRequestAPI(requestId : Int)
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["friend_request_id" : requestId]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.DECLINE_REQ, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Ooops!", msg: responseMessage)
            }
        })
    }
    
    func callAcceptFriendRequestAPI(requestId : Int)
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["friend_request_id" : requestId]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.ACCEPT_REQ, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Ooops!", msg: responseMessage)
            }
        })
    }
    
    func callUnblock_UserAPI(userID : Int)
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["user_id" : userID]
        
        GlobalConstants().postData_Header_Parameter(subUrl: USER_URL.UNBLOCK_USER, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Ooops!", msg: responseMessage)
            }
        })
    }
    
    // MARK: - Facebook API
    func callFacebookLoginAPI(fbToken : String)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["social_token":fbToken]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.CONNECT_WITH_FB, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    print(responseData)
                    self.retrieveFacebookFriendsList()
                }
                else
                {
                    self.showAlert(title: "Ooops!", msg: responseMessage)
                }
        })
    }

}
