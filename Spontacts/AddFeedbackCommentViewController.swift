//
//  AddFeedbackCommentViewController.swift
//  Spontacts
//
//  Created by maximess142 on 16/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class AddFeedbackCommentViewController: UIViewController {

    @IBOutlet weak var txtComment: UITextView!
    var feedbackId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let commentbtn = UIBarButtonItem.init(title: "Comment", style: .done, target: self, action: #selector(self.didTapCommentBtn))
        self.navigationItem.rightBarButtonItem = commentbtn
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.txtComment.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTapCommentBtn()
    {
        self.view.endEditing(true)
        
        self.addFeedbackComment()
    }

    func addFeedbackComment()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait...")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["feedback_id" : feedbackId,
                             "comment" : txtComment.text] as [String : Any]
        
        GlobalConstants().postData_Header_Parameter(subUrl: SETTINGS_URL.ADD_FEEDBACK_COMMENT, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    self.showAlert(title: "Success!", msg: responseMessage)
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }

    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
