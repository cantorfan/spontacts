//
//  UserActivitiesViewController.swift
//  Spontacts
//
//  Created by maximess142 on 23/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import GoogleMobileAds

class UserActivitiesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    
    @IBOutlet weak var topView: UIView!

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var selectedSeg = Int()
    let sharedObj = Singleton.shared
    var defaultDateFormatter = DateFormatter()
    
    var organisedArray = [[String : Any]]()
    var attendArray = [[String : Any]]()
    var activityParaToSend = Para_Activity()
    
    var friend_Id = Int()
    var isUpcoming = false
    var userJSON = [String : Any]()
    var userParaToSend = Para_UserProfile()
    
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = AdMob_BannerID
        //My Banner Id - "ca-app-pub-6631853374417128/4154657966"
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        
        return adBannerView
    }()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        addLeftBarButton()
        
        defaultDateFormatter.dateFormat = "dd/MM/YYYY"
        
        if isUpcoming == true
        {
            title = "Upcoming Activities"
            fetchUpcomingActivitiesList()
        }
        else
        {
            title = "Past Activities"
            self.fetchPastActivitiesList()
        }
        adBannerView.load(GADRequest())
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didChangeSegmentedControl(_ sender: Any)
    {
        let seg = sender as! UISegmentedControl
        selectedSeg = seg.selectedSegmentIndex
        tableView.reloadData()
    }

    // MARK: - API Methods
    func fetchUpcomingActivitiesList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Getting Activities")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["user_id" : friend_Id]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.GET_USER_UPCOMING_ACT_LIST, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            print(responseData)
            if isSuccess
            {
                if responseData["activities"] != nil
                {
                    let actDict = responseData["activities"] as! [String : Any]
                    
                    self.organisedArray = actDict["organised"] as! [[String : Any]]
                    self.segmentControl.setTitle("Organizes(\(self.organisedArray.count))", forSegmentAt: 0)
                    
                    self.attendArray = actDict["attended"] as! [[String : Any]]
                    self.segmentControl.setTitle("Is Going(\(self.attendArray.count))", forSegmentAt: 1)

                    self.tableView.reloadData()
                }
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func fetchPastActivitiesList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Getting Activities")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["user_id" : friend_Id]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.GET_USER_PAST_ACT_LIST, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            print(responseData)
            if isSuccess
            {
                if responseData["activities"] != nil
                {
                    let actDict = responseData["activities"] as! [String : Any]
                    
                    self.organisedArray = actDict["organised"] as! [[String : Any]]
                    self.segmentControl.setTitle("Organized(\(self.organisedArray.count))", forSegmentAt: 0)

                    self.attendArray = actDict["attended"] as! [[String : Any]]
                    self.segmentControl.setTitle("Attended(\(self.attendArray.count))", forSegmentAt: 1)

                    self.tableView.reloadData()
                }
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func callFetchAttendees_InterestedAPI(actId : Int)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Loading Activities")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject : [String : Any] = ["activity_id": actId] as [String : Any]
        
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.GET_ACT_GOING_INTERESTED_COMMENTS_LIST, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    if responseData["going"] != nil
                    {
                        let array = responseData["going"] as! [[String:Any]]
                        self.activityParaToSend.actGoingArray = array
                    }
                    if responseData["interested"] != nil
                    {
                        let array = responseData["interested"] as! [[String:Any]]
                        self.activityParaToSend.actInterstedArray = array
                    }
                    if responseData["comments"] != nil
                    {
                        let array = responseData["comments"] as! NSArray
                        self.activityParaToSend.actCommentsArray = array.mutableCopy() as! NSMutableArray
                    }
                    
                    print(self.activityParaToSend.actGoingArray.count, self.activityParaToSend.actInterstedArray.count)
                }
                else
                {
                    //self.showAlert(title: "Fail!", msg: responseMessage)
                }
                
                
                let VC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "ShowActivity_VC") as! ShowActivity_VC
                VC.userPara = self.userParaToSend
                VC.activityPara = self.activityParaToSend
                self.navigationController?.pushViewController(VC, animated: true)
                
               // self.performSegue(withIdentifier: "segueMyActivityToShowActivity", sender: nil)
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - UI Methods
    func addLeftBarButton()
    {
//        let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu_new"), style: .plain,  target: self, action: #selector(didTapMenuButn))
//        navigationItem.leftBarButtonItem = btnMenu
//        
//        if revealViewController() != nil
//        {
//            revealViewController().rightViewRevealWidth = 200
//            
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
//        }
    }
    
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if selectedSeg == 0
        {
            if self.organisedArray.count == 0
            {
                let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                emptyLabel.text = "No Upcoming Activities Found"
                emptyLabel.textColor = UIColor.lightGray
                emptyLabel.textAlignment = NSTextAlignment.center
                self.tableView.backgroundView = emptyLabel
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                return 0
            }
            else
            {
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                self.tableView.backgroundView?.isHidden = true
                return self.organisedArray.count
            }
        }
        else
        {
            if self.attendArray.count == 0
            {
                let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                emptyLabel.text = "No Past Activities Found"
                emptyLabel.textColor = UIColor.lightGray
                emptyLabel.textAlignment = NSTextAlignment.center
                self.tableView.backgroundView = emptyLabel
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                return 0
            }
            else
            {
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                self.tableView.backgroundView?.isHidden = true
                return self.attendArray.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : UserActivitiesListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UserActivitiesListTableViewCell", for: indexPath) as! UserActivitiesListTableViewCell
        
        if selectedSeg == 0
        {
            let activityJSON = organisedArray[indexPath.row]
            
            print(activityJSON)
            let catId = activityJSON["category_id"] as! Int
            if sharedObj.arrCatImageCropped.count > catId
            {
                let actCatUrl = activityJSON["category_picture_thumb"] as! String
                if catId == 8 && actCatUrl != ""
                {
                    cell.imgActivity.af_setImage(withURL: URL.init(string: APP_URL.BASE_URL + actCatUrl)!, placeholderImage: #imageLiteral(resourceName: "image_placeolder"), completion: nil)
                    cell.imgActivity.clipsToBounds = true
                }
                else
                {
                    cell.imgActivity.image = sharedObj.arrCatImageCropped[catId]
                    cell.imgActivity.clipsToBounds = true
                }
            }
            
            cell.lblActName.text = activityJSON["name"] as? String
            let timeInterval = activityJSON["time"] as? Int
            let date = Date.init(timeIntervalSince1970: TimeInterval(timeInterval!))
            cell.lblDate.text = defaultDateFormatter.string(from: date)
            
            cell.lblAttendees.text = "\(activityJSON["going_count"] as! Int)" //"1"
            cell.lblComments.text = "\(activityJSON["comments_count"] as! Int)" //"0"
            
            let distancestr = Int(activityJSON["distance"] as! NSNumber)
            cell.lblLocation.text = (activityJSON["location_string"] as? String)! + " (\(distancestr) km)"
            
            let headStr = isUpcoming == true ? "I'm organising" : "Organised"
            cell.lblOrganising.text = headStr
        }
        else
        {
            let activityJSON = attendArray[indexPath.row]
            print(activityJSON)
            let catId = activityJSON["category_id"] as! Int
            if sharedObj.arrCatImageCropped.count > catId
            {
                cell.imgActivity.image = sharedObj.arrCatImageCropped[catId]
                cell.imgActivity.clipsToBounds = true
            }
            
            cell.lblActName.text = activityJSON["name"] as? String
            let timeInterval = activityJSON["time"] as? Int
            let date = Date.init(timeIntervalSince1970: TimeInterval(timeInterval!))
            cell.lblDate.text = defaultDateFormatter.string(from: date)
            
            cell.lblAttendees.text = "\(activityJSON["going_count"] as! Int)" //"1"
            cell.lblComments.text = "\(activityJSON["comments_count"] as! Int)" //"0"
            
            let distancestr = Int(activityJSON["distance"] as! NSNumber)
            cell.lblLocation.text = (activityJSON["location_string"] as? String)! + " (\(distancestr) km)"
            
            let organiserJSON = activityJSON["organiser"] as! [String : Any]
            let fname = organiserJSON["fname"] as! String
            
            cell.lblOrganising.text = fname
            
           // let headStr = isUpcoming == true ? "I'm going" : "Attended"
           // cell.lblOrganising.text = headStr
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedSeg == 0
        {
            let JSON = organisedArray[indexPath.row]
            
            activityParaToSend = Para_Activity()
            
            activityParaToSend.actLocatnLongitude = JSON["lng"] as! Double
            activityParaToSend.actIsFlexible = (JSON["time_flexible"] != nil)
            let timeInterval = JSON["time"] as? Int
            activityParaToSend.actDate = Date.init(timeIntervalSince1970: Double(timeInterval!))
            activityParaToSend.actId = JSON["id"] as! Int
            activityParaToSend.actVisibleTo = JSON["visibility"] as! Int
            activityParaToSend.actDesc = JSON["description"] as! String
            activityParaToSend.actLocatnLatitude = JSON["lat"] as! Double
            activityParaToSend.actName = JSON["name"] as! String
            activityParaToSend.actCreatedEpoch = JSON["created_time"] as! Double
            activityParaToSend.actCategoryId = JSON["category_id"] as! Int
            activityParaToSend.actLocatnStr = JSON["location_string"] as! String
            activityParaToSend.actImgJSONArr = JSON["images"] as! NSArray
            activityParaToSend.actCommentsCount = JSON["comments_count"] as! Int
            activityParaToSend.actCategoryImgURL = JSON["category_picture"] as! String

            userParaToSend = Para_UserProfile()
            userParaToSend.userFirstName = userJSON["fname"] as! String
            userParaToSend.userLastName = userJSON["lname"] as! String
            userParaToSend.userEmail = userJSON["email"] as! String
            userParaToSend.userLocationLat = userJSON["lat"] as! Double
            userParaToSend.userLocationLong = userJSON["lng"] as! Double
            userParaToSend.userId = userJSON["id"] as! Int
            userParaToSend.userPictureThumbUrl = userJSON["thumb"] as! String
            userParaToSend.isPlusMember = userJSON["suscribed"] as! Int
        }
        else if selectedSeg == 1
        {
            let JSON = attendArray[indexPath.row] 
            
            activityParaToSend = Para_Activity()
            
            activityParaToSend.actLocatnLongitude = JSON["lng"] as! Double
            activityParaToSend.actIsFlexible = (JSON["time_flexible"] != nil)
            let timeInterval = JSON["time"] as? Int
            activityParaToSend.actDate = Date.init(timeIntervalSince1970: Double(timeInterval!))
            activityParaToSend.actId = JSON["id"] as! Int
            activityParaToSend.actVisibleTo = JSON["visibility"] as! Int
            activityParaToSend.actDesc = JSON["description"] as! String
            activityParaToSend.actLocatnLatitude = JSON["lat"] as! Double
            activityParaToSend.actName = JSON["name"] as! String
            activityParaToSend.actCreatedEpoch = JSON["created_time"] as! Double
            activityParaToSend.actCategoryId = JSON["category_id"] as! Int
            activityParaToSend.actLocatnStr = JSON["location_string"] as! String
            activityParaToSend.actImgJSONArr = JSON["images"] as! NSArray
            activityParaToSend.actCommentsCount = JSON["comments_count"] as! Int
            activityParaToSend.actCategoryImgURL = JSON["category_picture"] as! String

            userParaToSend = Para_UserProfile()
            userJSON = JSON["organiser"] as! [String:Any]
            userParaToSend.userFirstName = userJSON["fname"] as! String
            userParaToSend.userLastName = userJSON["lname"] as! String
            userParaToSend.userEmail = userJSON["email"] as! String
            userParaToSend.userLocationLat = userJSON["lat"] as! Double
            userParaToSend.userLocationLong = userJSON["lng"] as! Double
            userParaToSend.userId = userJSON["id"] as! Int
            userParaToSend.userPictureThumbUrl = userJSON["thumb"] as! String
            userParaToSend.isPlusMember = userJSON["suscribed"] as! Int
        }
        
        callFetchAttendees_InterestedAPI(actId: activityParaToSend.actId)
 
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueMyActivityToShowActivity"
        {
            let showAct : ShowActivity_VC = segue.destination as! ShowActivity_VC
            showAct.activityPara = activityParaToSend
            showAct.userPara = sharedObj.userPara
            
        }
    }
}

extension UserActivitiesViewController : GADBannerViewDelegate
{
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        if sharedObj.userPara.isPlusMember == 1
        {
            print("No ads to plus user")
        }
        else
        {
            print("Banner loaded successfully")
            tableView.tableHeaderView?.frame = bannerView.frame
            tableView.tableHeaderView = bannerView
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }
}
