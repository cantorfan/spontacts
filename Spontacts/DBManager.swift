//
//  DBManager.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 15/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

let sharedInstanceGayAndYouSQLite = DBManager()

class DBManager: NSObject
{
    static let shared: DBManager = DBManager()
    
    let databaseFileName = "GayAndYou.sqlite"
    
    var pathToDatabase: String!
    
    var database: FMDatabase!
    
    // MARK: - Initialisation
    override init()
    {
        super.init()
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(databaseFileName)
        pathToDatabase = fileURL.path
    }
    
    class func getInstanceProject() -> DBManager
    {
        if(sharedInstanceGayAndYouSQLite.database == nil)
        {
            sharedInstanceGayAndYouSQLite.database = FMDatabase(path: Utils.getPath("projec10sqllite.sqlite"))
        }
        return sharedInstanceGayAndYouSQLite
    }
    
    // MARK: - Insert
    func insertConversationTable(para :Para_Conversation)
    {
        database = FMDatabase(path: pathToDatabase!)
        database.open()
        
        let success = database.executeUpdate("INSERT INTO Conversation_table (Id, Friend_Id, Last_Synced_Time, Friend_name, Thumb, Last_Message) VALUES (?, ?, ?, ?, ?, ?)", withArgumentsIn: [para.Id ,para.Friend_Id, para.Last_Synced_Time, para.Friend_name, para.thumb, para.Last_Message] )
        print("Conversation insert - ", success.description)
        database.close()
    }
    
    func insertMessageTable(para :Para_Message)
    {
        database = FMDatabase(path: pathToDatabase!)
        if database != nil
        {
            // Open the database.
            if database.open()
            {
                // executeUpdate is necessary. DO not fire executeQuery
                let success = database.executeUpdate("INSERT INTO Message_table(Id, Chat_Id, Message, Time, Sender_Id, Receiver_Id) VALUES (?, ?, ?, ?, ?, ?)", withArgumentsIn: [para.Id ,para.Chat_Id, para.Message, para.Time, para.Sender_Id, para.Receiver_Id] )
                print("Message insert - ", success as Any)
            }
        }
        database.close()
    }

    // MARK:- Update
    func updateConversationTable(para : Para_Conversation)
    {
        database = FMDatabase(path : pathToDatabase)
        if database != nil
        {
            if database.open()
            {
                let success = database.executeUpdate("UPDATE Conversation_table SET Last_Synced_Time=?, Last_Message=? WHERE Id=?", withArgumentsIn: [para.Last_Synced_Time, para.Last_Message, para.Id])
                print("Chat update - ", success as Any)
            }
        }
        database.close()
    }
    
    // MARK:- Retrieve
    func getConversationTableData() -> [Para_Conversation]
    {
        database = FMDatabase(path: pathToDatabase!)
        database.open()
        
        var returnArray = [Para_Conversation]()
        
        let resultSet : FMResultSet! = database.executeQuery("SELECT * from Conversation_table", withArgumentsIn: nil)
        
        if resultSet != nil
        {
            while resultSet.next()
            {
                let chatPara = Para_Conversation()

                //(Friend_Id, Last_Synced_Time, Friend_name, Thumb)
                chatPara.Id = Int(resultSet.int(forColumn: "Id"))
                chatPara.Friend_Id = Int(resultSet.int(forColumn: "Friend_Id"))
                chatPara.Last_Synced_Time =  resultSet.double(forColumn: "Last_Synced_Time")
                chatPara.Friend_name = resultSet.string(forColumn: "Friend_name")
                chatPara.thumb = resultSet.string(forColumn: "Thumb")
                chatPara.Last_Message = resultSet.string(forColumn: "Last_Message")
                
                returnArray.append(chatPara)
            }
        }
        
        database.close()
        return returnArray
    }
    
    func getConversationTableData_ChatId(chat_id : Int) -> Bool
    {
        database = FMDatabase(path: pathToDatabase!)
        database.open()
        
        var isExist =  false
        
        let resultSet : FMResultSet! = database.executeQuery("SELECT * from Conversation_table where Id = \(chat_id)", withArgumentsIn: nil)
        
        if resultSet != nil
        {
            while resultSet.next()
            {
                isExist = true
            }
        }
        
        database.close()
        return isExist
    }
    
    func getConversationTableData_FriendId(friend_Id : Int) -> [Para_Message]
    {
        database = FMDatabase(path: pathToDatabase!)
        database.open()
        
        var returnArray =  [Para_Message]()
        
        let resultSet : FMResultSet! = database.executeQuery("SELECT * from Conversation_table where Friend_Id = \(friend_Id)", withArgumentsIn: nil)
        
        if resultSet != nil
        {
            while resultSet.next()
            {
                
            }
        }
        
        database.close()
        return returnArray
    }
    
    func getMessageTableData_ConversationId(coversation_Id : Int) -> [Para_Message]
    {
        database = FMDatabase(path: pathToDatabase!)
        database.open()
        
        var returnArray = [Para_Message]()
        
        let resultSet : FMResultSet! = database.executeQuery("SELECT * from Message_table where Chat_Id = \(coversation_Id)", withArgumentsIn: nil)
        
        if resultSet != nil
        {
            while resultSet.next()
            {
                let messagePara = Para_Message()
                //(Chat_Id, Message, Time, Sender_Id, Receiver_Id)
                
                messagePara.Id = Int(resultSet.int(forColumn: "Id"))
                messagePara.Chat_Id = Int(resultSet.int(forColumn: "Chat_Id"))
                messagePara.Message = resultSet.string(forColumn: "Message")
                messagePara.Time =  resultSet.double(forColumn: "Time")
                messagePara.Sender_Id = Int(resultSet.int(forColumn: "Sender_Id"))
                messagePara.Receiver_Id = Int(resultSet.int(forColumn: "Receiver_Id"))
                
                returnArray.append(messagePara)
            }
        }
        
        database.close()
        return returnArray
    }
    
    func getMessageTableData_LastMessage(coversation_Id : Int) -> Para_Message
    {
        database = FMDatabase(path: pathToDatabase!)
        database.open()
        
        var para = Para_Message()
        
        //SELECT * FROM table_name where chat_id = 2 ORDER BY id DESC LIMIT 1
        let resultSet : FMResultSet! = database.executeQuery("SELECT * from Message_table where Chat_Id = \(coversation_Id) ORDER BY Id DESC LIMIT 1", withArgumentsIn: nil)
        
        if resultSet != nil
        {
            while resultSet.next()
            {
                let messagePara = Para_Message()
                //(Chat_Id, Message, Time, Sender_Id, Receiver_Id)
                
                messagePara.Id = Int(resultSet.int(forColumn: "Id"))
                messagePara.Chat_Id = Int(resultSet.int(forColumn: "Chat_Id"))
                messagePara.Message = resultSet.string(forColumn: "Message")
                messagePara.Time =  resultSet.double(forColumn: "Time")
                messagePara.Sender_Id = Int(resultSet.int(forColumn: "Sender_Id"))
                messagePara.Receiver_Id = Int(resultSet.int(forColumn: "Receiver_Id"))
                
                para = messagePara
            }
        }
        
        database.close()
        return para
    }
    
    // MARK: - DELETE
    func deleteDatabase()
    {
        database = FMDatabase(path: pathToDatabase!)
        database.open()

        database.executeUpdate("DELETE FROM Message_table", withArgumentsIn: nil)
        database.executeUpdate("DELETE FROM Conversation_table", withArgumentsIn: nil)
        
        database.close()
    }
}
