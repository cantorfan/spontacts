//
//  UIView+CustomMethods.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 11/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

extension UIView
{
    func animateViewMoving (up:Bool, moveValue :CGFloat)
    {
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.frame = self.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
}

class UIView_CustomMethods: NSObject {

}
