//
//  ImageSliderCollectionViewCell.swift
//  Projec10
//
//  Created by maximess121 on 24/04/17.
//  Copyright © 2017 maximess114. All rights reserved.
//

import UIKit

class ImageSliderCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollViewToZoom: UIScrollView!
    @IBOutlet weak var imageToZoom: UIImageView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        scrollViewToZoom.delegate = self
        scrollViewToZoom.minimumZoomScale = 1.0
        scrollViewToZoom.maximumZoomScale = 6.0
        scrollViewToZoom.isUserInteractionEnabled = true
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return imageToZoom
    }
    
    
}
