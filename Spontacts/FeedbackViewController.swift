//
//  FeedbackViewController.swift
//  Spontacts
//
//  Created by maximess142 on 02/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var feedbackArray = [[String : Any]]()
    var FEEDBACK_ARRAY = [[String : Any]]()
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Feedback Community"
        
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.retrieveFeedbacks()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - IBActions
    @IBAction func didTapPostIdea(_ sender: Any)
    {
        let notificationVC = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "AddFeedbackViewController") as! AddFeedbackViewController
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }

    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return feedbackArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 35
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "FeedbackTableViewHeader") as! FeedbackTableViewCell
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackTableViewCell", for: indexPath) as! FeedbackTableViewCell
        
        let json = feedbackArray[indexPath.row]
        cell.lblFeedbackText.text = json["idea"] as? String
        
        let likesCount = json["suscribers"] as! Int
        cell.lblLikeCount.text = "\(likesCount)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let showVC : ShowFeedbackViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShowFeedbackViewController") as! ShowFeedbackViewController
        
        showVC.feedbackDict = feedbackArray[indexPath.row]
        self.navigationController?.pushViewController(showVC, animated: true)
    }
    
    
    // MARK: - SearchBar Delegates
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
        callSearchFeedbackAPI()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == ""
        {
            self.view.endEditing(true)
            self.feedbackArray = self.FEEDBACK_ARRAY
            self.tableView.reloadData()
            self.searchBar.resignFirstResponder()
        }
    }
    
    // MARK: - Web API

    func callSearchFeedbackAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["search_term" : searchBar.text!]
        // to fetch users
        GlobalConstants().postData_Header_Parameter(subUrl: SETTINGS_URL.SEARCH_FEEDBACK, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["feedbacks"] != nil
                    {
                        print(responseData["feedbacks"] as Any)
                        self.feedbackArray = responseData["feedbacks"] as! [[String : Any]]
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func retrieveFeedbacks()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        // to fetch users
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: SETTINGS_URL.GET_ALL_FEEDBACK, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["feedbacks"] != nil
                    {
                        print(responseData["feedbacks"] as Any)
                        self.feedbackArray = responseData["feedbacks"] as! [[String : Any]]
                        self.FEEDBACK_ARRAY = self.feedbackArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
