//
//  LoginWithAppCredentials_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 5/24/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import FirebaseInstanceID


class LoginWithAppCredentials_VC: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIDocumentInteractionControllerDelegate
{
    // MARK: - IBOutlet Properties
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var butnLogin: UIButton!
    @IBOutlet weak var butnForgotPass: UIButton!
    @IBOutlet weak var viewBorderTfs: UIView!
    @IBOutlet weak var btnShowPassword: UIButton!
    
    @IBOutlet weak var txtTerms: UITextView!
    
    let sharedObj = Singleton.shared
    
    //MARK: - Global Vars
    var userPara = Para_UserProfile()
    let server = Singleton.shared

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addToolBar()
        addAdditionalProperties()
        addAttributedText()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // As soon as view is dismissed, bring back the navigation bar
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    
    func addAdditionalProperties()
    {
//        viewBorderTfs.layer.borderColor = sharedObj.hexStringToUIColor(hex: "CBCBCB").cgColor
//        viewBorderTfs.layer.borderWidth = 1.0
//        viewBorderTfs.layer.cornerRadius = 5.0
//        viewBorderTfs.clipsToBounds = true
//        
//        butnLogin.layer.cornerRadius = 3.0
//        butnLogin.clipsToBounds = true
    }
    
    func addAttributedText()
    {
        self.txtTerms.delegate = self
        
        self.txtTerms = sharedObj.addAttributedText(txtTerms: self.txtTerms)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func didTapDoneKeypad()
    {
        view.endEditing(true)
    }
    
    func didTapNextKeypad()
    {
        if tfEmail.isFirstResponder
        {
            tfPassword.becomeFirstResponder()
        }
        else if tfPassword.isFirstResponder
        {
            tfPassword.resignFirstResponder()
        }
    }
    
    // MARK: - ACtion Methods
    
    @IBAction func didTapBackButn(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapLogin(_ sender: Any)
    {
        view.endEditing(true)
        
        //        self.performSegue(withIdentifier: "segueLoginToLanding", sender: self)
        
        
        if userPara.userEmail == ""
        {
            self.showAlert(title: "", msg: "Please Enter Email")
        }
        else if server.isValidEmail(testStr: userPara.userEmail) == false
        {
            self.showAlert(title: "", msg: "Please Enter Valid Email")
        }
        else if userPara.userPassword == ""
        {
            self.showAlert(title: "", msg: "Please Enter Password")
        }
        else
        {
            callLogInAPI()
        }
    }

    @IBAction func didTapCreateAccountButn(_ sender: Any)
    {
        performSegue(withIdentifier: "segueLogInToSignUp", sender: nil)
    }
    
    
    @IBAction func didTapPasswordButn(_ sender: Any)
    {
        self.btnShowPassword.isSelected = !self.btnShowPassword.isSelected
        
        self.tfPassword.isSecureTextEntry = !self.tfPassword.isSecureTextEntry
    }

    // MARK: - Logical Methods
    func callLogInAPI()
    {
        print("Celling login API")
        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Logging In")
        loader.startAnimating()
        
        if UserDefaults.standard.value(forKey: "fcmDeviceToken") == nil
        {
            let fcmDeviceToken = FIRInstanceID.instanceID().token()
            print("FCM token: \(fcmDeviceToken ?? "")")
            UserDefaults.standard.setValue(fcmDeviceToken, forKey: "fcmDeviceToken")
            UserDefaults.standard.synchronize()
        }
        
        //header - username, password, devicetype, devicetoken
        let deviceToken = UserDefaults.standard.value(forKey: "fcmDeviceToken") as? String ?? ""

        let headerObject = ["username":userPara.userEmail, "password" : userPara.userPassword, "devicetype" : "1", "devicetoken" : deviceToken]
        
        GlobalConstants().postDataFor(subUrl: APP_URL.APP_LOGIN_SUBURL, header: headerObject, resultKeyName: "user" , completionHandler:
            {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            print(responseData)
                
            if isSuccess
            {
                print(responseData)
                
                let token = responseData["token"] as! String
                UserDefaults.standard.set(token, forKey: "userToken")
                
                UserDefaults.standard.set(responseData, forKey: "userJSON")

                UserDefaults.standard.synchronize()
                
                ////TODO: -  ADD navigation
                self.performSegue(withIdentifier: "segueLoginToLanding", sender: self)
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func addToolBar()
    {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = UIBarStyle.default
        toolBar.items = [
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(didTapDoneKeypad)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.plain, target: self, action: #selector(didTapNextKeypad))]
        toolBar.sizeToFit()
        
        tfEmail.inputAccessoryView = toolBar
        tfPassword.inputAccessoryView = toolBar
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in

        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    func showDocument()
    {
        if let url = Bundle.main.url(forResource: "GAYANDYOU_terms_of_use_August_2017", withExtension: "pdf", subdirectory: nil, localization: nil)
        {
            let documentInteractionController = UIDocumentInteractionController(url: url)
            documentInteractionController.delegate = self
            documentInteractionController.presentPreview(animated: false)
        }
    }

    // MARK: - TextView Delegates
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        if (URL.absoluteString == termsAndConditionsURL)
        {
            self.showDocument()//(title: "Go to Terms", msg: "")
        }
        else if (URL.absoluteString == paymentsURL)
        {
            self.showDocument() //(title: "Go to Payment", msg: "")
        }
        else if (URL.absoluteString == privacyURL)
        {
            self.showDocument()//(title: "Go to Privacy", msg: "")
        }
        else if (URL.absoluteString == nonDiscriminationURL)
        {
            self.showDocument()//(title: "Go to Nondiscrimination", msg: "")
        }
        
        return false
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }

    
    // MARK: - Textfield Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: 0, y: -70, width: self.view.frame.size.width, height: self.view.frame.size.height)
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == tfEmail
        {
            userPara.userEmail = textField.text!
        }
        else if textField == tfPassword
        {
            userPara.userPassword = textField.text!
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
