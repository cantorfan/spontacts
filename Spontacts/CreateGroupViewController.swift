//
//  CreateGroupViewController.swift
//  Spontacts
//
//  Created by maximess142 on 09/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import YangMingShan

class CreateGroupViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate
{

    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtViewDescription: UITextView!
    
    @IBOutlet weak var btnPhoto: UIButton!
    @IBOutlet weak var btnCreateGroup: UIButton!
    
    let sharedObj = Singleton.shared
    var imageData = Data()
    var loader : ActivityIndicatorView?
    var imageBase64 = ""
    
    var isEditMode = false
    var groupDetails = [String:Any]()
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtName.delegate = self
        txtViewDescription.delegate = self
        
        addLeftBarbutn()
        assignValueInEditMode()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func assignValueInEditMode()
    {
        if isEditMode == true
        {
            self.txtName.text = groupDetails["name"] as? String
            self.txtViewDescription.text = groupDetails["description"] as! String
            
            let pictureUrl = groupDetails["thumb"] as! String
            if pictureUrl != ""
            {
                self.btnPhoto.af_setImage(for: .normal, url: URL.init(string: APP_URL.BASE_URL + pictureUrl)!)
            }
            
            self.btnCreateGroup.setTitle("UPDATE", for: .normal)
            
            title = "Edit Group"
        }
    }
    
    func addLeftBarbutn()
    {
        let leftBarbUtn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .done, target: self, action: #selector(self.didTapLeftBarButn))
        self.navigationItem.leftBarButtonItem = leftBarbUtn
    }

    func didTapLeftBarButn()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Action Methods
    @IBAction func didTapChoosePhoto(_ sender: Any)
    {
        let pickerViewController = YMSPhotoPickerViewController.init()
        //        pickerViewController.numberOfPhotoToSelect = 2
        let customColor = UIColor.init(red: 64.0/255.0, green: 0.0, blue: 144.0/255.0, alpha: 1.0)
        let customCameraColor = UIColor.init(red: 86.0/255.0, green: 1.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customCameraColor
        pickerViewController.theme.cameraVeilColor = customCameraColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .lightContent
        pickerViewController.shouldReturnImageForSingleSelection = true
        
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }

    @IBAction func didTapCreateButn(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if self.txtName.text == ""
        {
            self.showAlert(title: "Error!", msg: "Please enter Group name!")
            return
        }
        
        loader =
            startActivityIndicator(view: self.view, waitingMessage: "Please Wait...")
        loader?.startAnimating()
        
        let chatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.publicGroup)
        chatDialog.name = txtName.text!
        
        if isEditMode == true
        {
            self.updateGroupChatDialog()
        }
        else
        {
            self.createGroupChatRoom(dialogToCreate: chatDialog)
        }
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func createGroupChatRoom(dialogToCreate : QBChatDialog)
    {
        QBRequest.createDialog(dialogToCreate, successBlock:
            { (response: QBResponse?, createdDialog : QBChatDialog?) -> Void in
                
                print("Created dialofg")
                print(createdDialog ?? "")
                
                if self.loader != nil{
                    self.loader?.stopAnimating()
                }
                
                ServicesManager.instance().chatService.dialogsMemoryStorage.add([createdDialog!], andJoin: true)
                
                self.callCreateGroupAPI(groupQBID: (createdDialog?.id)!, dialog: createdDialog!)

        }) { (responce : QBResponse!) -> Void in
            
            if self.loader != nil{
                self.loader?.stopAnimating()
            }
            
            self.showAlert(title: "Error!", msg: "Could not create group. Please try again later!")
        }
    }
    
    func updateGroupChatDialog()
    {
        let groupQbID = self.groupDetails["group_qbid"] as! String
        
        let updateDialog = QBChatDialog(dialogID: groupQbID, type: QBChatDialogType.publicGroup)
        //updateDialog.pushOccupantsIDs(["300", "301", "302"])
        updateDialog.name = self.txtName.text!
        QBRequest.update(updateDialog, successBlock:
            {(responce: QBResponse?, dialog: QBChatDialog?) in
                
                if self.loader != nil{
                    self.loader?.stopAnimating()
                }
                
                //ServicesManager.instance().chatService.dialogsMemoryStorage.add([createdDialog!], andJoin: true)
                
                self.callEditGroupAPI(groupQBID: groupQbID)
        },
         errorBlock: { (responce : QBResponse!) -> Void in
            
            if self.loader != nil{
                self.loader?.stopAnimating()
            }
            
            self.showAlert(title: "Error!", msg: "Could not create group. Please try again later!")
        })
    }
    
    // MARK: - API
    func callCreateGroupAPI(groupQBID : String, dialog : QBChatDialog)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["group_id" : 0,
                             "admin_id" : sharedObj.userPara.userId,
                             "description" : self.txtViewDescription.text,
                             "name" : self.txtName.text!,
                             "admin_qbid" : sharedObj.userPara.user_qb_id,
                             "image_base_64" : imageBase64,
                             "created_date" : Date().timeIntervalSince1970,
                             "group_qbid" : groupQBID] as [String : Any]
        
        GlobalConstants().postData_Header_Parameter(subUrl: GROUPS_API.ADD_GROUP, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    print(responseData)
                    
                    self.navigationController?.popViewController(animated: true)

                    let chatVC = storyboards.qbStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                    chatVC.dialog = dialog
                    self.navigationController?.pushViewController(chatVC, animated: true)
                }
                else
                {
                    self.showAlert(title: "Ooops!", msg: responseMessage)
                }
        })
    }
    
    func callEditGroupAPI(groupQBID : String)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["description" : txtViewDescription.text,
                             "name" : self.txtName.text!,
                             "image_base_64" : imageBase64,
                             "group_qbid" : groupQBID] as [String : Any]
        
        GlobalConstants().postData_Header_Parameter(subUrl: GROUPS_API.UPDATE_GROUP, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    print(responseData)
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    self.showAlert(title: "Ooops!", msg: responseMessage)
                }
        })
    }
    
    // MARK: - Textfield Delegates
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }

    func textViewDidBeginEditing(_ textView: UITextView)
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Photo Picker Delegates
extension CreateGroupViewController : YMSPhotoPickerViewControllerDelegate
{
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
            }
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
            }
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!)
    {
        picker.dismiss(animated: true, completion:{
            Void in
            self.btnPhoto.setImage(image, for: .normal)
            
            //Now use image to create into NSData format
            self.imageData = (image?.lowestQualityJPEGNSData)!
            
            self.imageBase64 = self.imageData.base64EncodedString(options: .lineLength64Characters)
        })
    }
}

