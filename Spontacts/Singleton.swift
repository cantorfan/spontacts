//
//  Singleton.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 5/27/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

let termsAndConditionsURL = "http://www.example.com/terms";
let paymentsURL = "http://www.example.com/payments"
let privacyURL = "http://www.example.com/privacy";
let nonDiscriminationURL = "http://www.example.com/nonDiscrimination";

class Singleton: NSObject
{

    // Can't init is singleton
    private override init() { }
    
    // MARK: Shared Instance
    
    static let shared = Singleton()
    
    // MARK: Local Variable
    
    let arrCat = ["Festivals", "Food & Drinks",  "Bar & Club", "Health & Wellness",  "Travel & Outdoor", "Sport", "Education", "Culture & Art", "Others"]
    
    let arrCatImages = [#imageLiteral(resourceName: "LA_Festivals.jpg"), #imageLiteral(resourceName: "LA_Food.jpg"), #imageLiteral(resourceName: "LA_Bar.jpg"), #imageLiteral(resourceName: "LA_Yoga.jpg"), #imageLiteral(resourceName: "LA_Travel.jpg"), #imageLiteral(resourceName: "LA_Sport.jpg"), #imageLiteral(resourceName: "LA_Education.jpg"), #imageLiteral(resourceName: "LA_Culture.jpg"), #imageLiteral(resourceName: "LA_SeeMore.jpg")]
    
    //
    let arrCatImageCropped = [UIImage.init(named: "festivals"), UIImage.init(named: "food_drinks"), UIImage.init(named: "bar_club"), UIImage.init(named: "health_wellness"), UIImage.init(named: "travel_outdoor"), UIImage.init(named: "sport"), UIImage.init(named: "education"), UIImage.init(named: "culture_art"), UIImage.init(named: "see_more")] //[#imageLiteral(resourceName: "festivals.png"), #imageLiteral(resourceName: "food_drinks.png"), #imageLiteral(resourceName: "bar_club.png"), #imageLiteral(resourceName: "health_wellness.png"), #imageLiteral(resourceName: "travel_outdoor.png"), #imageLiteral(resourceName: "sport.png"), #imageLiteral(resourceName: "education.png"), #imageLiteral(resourceName: "culture_art.png"), #imageLiteral(resourceName: "see_more.png")]
    
    var userPara : Para_UserProfile = Para_UserProfile()
    
    var unread_Chats_Count = Int()
    var unread_MyActivities_Count = Int()
    var unread_All_Activities_Count = Int()
    var unread_Profile_Visitors_Count = Int()
    var unread_Friends_Count = Int()
    var unread_Recommendations = Int()
    
    var unread_Total_Count = Int()
    
    var para_filter = Para_Filter()
    
    var catImgBase64 = ""

    func getTotalCount() -> Int
    {
        let totalCount = unread_Chats_Count + unread_MyActivities_Count + unread_All_Activities_Count + unread_Profile_Visitors_Count + unread_Friends_Count + unread_Recommendations
        return totalCount
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor
    {
        var cString = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        if ((cString.characters.count) != 6)
        {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func addAttributedText(txtTerms : UITextView) -> UITextView
    {
        let strTextView = "By tapping Login, Create Account or other options, I agree to GAY AND YOU?'s Terms of Use"
        
        let attributedString = NSMutableAttributedString(string: strTextView)
        
        // Add link to Terms of Service
        var foundRange = attributedString.mutableString.range(of: "Terms of Use")
        attributedString.addAttributes([
            NSFontAttributeName : UIFont.systemFont(ofSize: 12),
            NSLinkAttributeName : termsAndConditionsURL,
            NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue], range: foundRange)
        
        /*
         // Add link to Payments Terms of Service
         foundRange = attributedString.mutableString.range(of: "Payments Terms of Service")
         attributedString.addAttributes([
         NSFontAttributeName : UIFont.systemFont(ofSize: 12),
         NSLinkAttributeName : paymentsURL,
         NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue], range: foundRange)
         
         // Add link to Privacy Policy
         foundRange = attributedString.mutableString.range(of: "Privacy Policy")
         attributedString.addAttributes([
         NSFontAttributeName : UIFont.systemFont(ofSize: 12),
         NSLinkAttributeName : privacyURL,
         NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue], range: foundRange)
         
         // Add link to Nondiscrimation Policy
         foundRange = attributedString.mutableString.range(of: "Nondiscrimation Policy")
         attributedString.addAttributes([
         NSFontAttributeName : UIFont.systemFont(ofSize: 12),
         NSLinkAttributeName : nonDiscriminationURL,
         NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue], range: foundRange)
         */
        
        txtTerms.attributedText = attributedString
        txtTerms.textAlignment = .center
        txtTerms.textColor = hexStringToUIColor(hex: "5b5b5b")
        
        return txtTerms
    }
    
    func resetFilterPara()
    {
        /*
         var latitude = Double()
         var longitude = Double()
         var locationStr = ""
         var sort_by = Int()
         var from_date = Double()
         var to_date = Double()
         var visibleTo = Int()
         var categories_array = [Int]()
         var distance = Int()
         var search_keyword = ""
         */
        self.para_filter = Para_Filter()
        self.para_filter.latitude = self.userPara.userLocationLat
        self.para_filter.longitude = self.userPara.userLocationLong
        self.para_filter.locationStr = ""
        
        self.para_filter.sort_by = 0

        self.para_filter.from_date = 0.0
        self.para_filter.to_date = 0.0
        
        self.para_filter.visibleTo = 1
        self.para_filter.categories_array.removeAll()
        self.para_filter.distance = 0
        self.para_filter.search_keyword = ""
    }
    
    
    
    
    
    
    
    
}
