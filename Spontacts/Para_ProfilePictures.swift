//
//  Para_ProfilePictures.swift
//  Spontacts
//
//  Created by maximess142 on 06/11/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class Para_ProfilePictures: NSObject
{
    var picId = Int()
    var picCaption = String()
    var picMode = Int()
    var isProfilePicture = Int()
    
    var picImage = UIImage()
    var picUrl = String()
//    var userEmail = String()
//    var userMobileNo = String()
//    var userPassword = String()
}
