//
//  FriendListViewController.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 10/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class FriendListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    var arrFriends = NSMutableArray()
    
    var friendIdToSend = Int()
    var friendNameToSend = String()
    let sharedObj = Singleton.shared
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title="My Friends"
        retrieveMyFriendList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func retrieveMyFriendList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl:FRIENDS_URL.GET_MY_FRND_LIST , header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["friend_list"] != nil
                    {
                        print(responseData["friend_list"] as Any)
                        let array = responseData["friend_list"] as! NSArray
                        print("friends array ", array.count)
                        self.arrFriends = array.mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }

    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    //MARK: - TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFriends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : FriendListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FriendListTableViewCell", for: indexPath) as! FriendListTableViewCell
        
        let userJSON = arrFriends[indexPath.row] as! [String : Any]
        
        let pictureURL : String = userJSON["thumb"] as! String
        if pictureURL != ""
        {
            let url = URL(string: APP_URL.BASE_URL + pictureURL )!
            let placeholderImage = #imageLiteral(resourceName: "avatar")
            let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
            
            //,filter: filter
            cell.imgViewUserTHumb.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                { response in
            }
            )
        }
        
        let firstName : String = userJSON["fname"] as! String
        let lastName : String = userJSON["lname"] as! String
        
        cell.lblUserName.text = firstName + " "  + lastName

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let userJSON = arrFriends[indexPath.row] as! [String : Any]

        friendIdToSend = userJSON["id"] as! Int
        let firstName : String = userJSON["fname"] as! String
        let lastName : String = userJSON["lname"] as! String
        
        friendNameToSend = firstName + " "  + lastName
        performSegue(withIdentifier: "segueMyFriendsToMessageView", sender: nil)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueMyFriendsToMessageView"
        {
            let chatVC : MessageViewController = segue.destination as! MessageViewController
            chatVC.chatPara.Friend_Id = friendIdToSend
            chatVC.chatPara.Friend_name = friendNameToSend
        }
    }
}
