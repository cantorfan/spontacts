//
//  Registration.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 5/24/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import GooglePlaces
import Alamofire
import FirebaseInstanceID
import YangMingShan

class Registration: UIViewController, UITextFieldDelegate, UITextViewDelegate, GMSAutocompleteViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentInteractionControllerDelegate
{
    // MARK: - Outlet Properties
    @IBOutlet weak var butnPicture: UIButton!
    
    @IBOutlet weak var tfFirstName: UITextField!
    
    //@IBOutlet weak var tfLastName: UITextField!
    
    @IBOutlet weak var tfEmail: UITextField!
    
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var tfMobileNum: UITextField!
    
    @IBOutlet var tfLocation: UITextField!
    
    @IBOutlet weak var butnSignUp: UIButton!
    
    @IBOutlet weak var txtViewTerms: UITextView!
    
    @IBOutlet weak var btnShowPassword: UIButton!
    
    // MARK: - Global Variables
    let server = Singleton.shared
    var userPara = Para_UserProfile()
    var isRegisterFb = Bool()
    var isRegisterGoogle = Bool()
    var isRegisterTwitter = Bool()
    var paraSocial = Para_SocialRegistration()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        addToolBar()
        addAttributedText()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        if isRegisterFb || isRegisterGoogle || isRegisterTwitter
        {
            setValuesFromPara()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // As soon as view is dismissed, bring back the navigation bar
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Methods
    @IBAction func didTapBackButn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didTapPhotoButn(_ sender: Any)
    {
        /*
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)*/
        
        
        let pickerViewController = YMSPhotoPickerViewController.init()
//        pickerViewController.numberOfPhotoToSelect = 2
        let customColor = UIColor.init(red: 64.0/255.0, green: 0.0, blue: 144.0/255.0, alpha: 1.0)
        let customCameraColor = UIColor.init(red: 86.0/255.0, green: 1.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customCameraColor
        pickerViewController.theme.cameraVeilColor = customCameraColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .lightContent
        pickerViewController.shouldReturnImageForSingleSelection = true
        
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    
    @IBAction func didTapSignUpButn(_ sender: Any)
    {
        if userPara.userFirstName == ""
        {
            self.showAlert(title: "", msg: "Please Enter Name or Username")
        }
        else if userPara.userEmail == ""
        {
            self.showAlert(title: "", msg: "Please Enter Email")
        }
        else if server.isValidEmail(testStr: userPara.userEmail) == false
        {
            self.showAlert(title: "", msg: "Please Enter Valid Email")
        }
        else if userPara.userPassword == ""
        {
            self.showAlert(title: "", msg: "Please Enter Password")
        }
        else if userPara.userLocationStr == ""
        {
            self.showAlert(title: "", msg: "Please Enter Location")
        }
        else if userPara.userPictureBase64 == ""
        {
            showPictureNotEnteredPopUp()
        }
        else
        {
            callRegistrationAPI()
        }
    }
    
    func didTapDoneKeypad()
    {
        view.endEditing(true)
    }
    
    func didTapNextKeypad()
    {
        if tfFirstName.isFirstResponder
        {
            tfEmail.becomeFirstResponder()
        }
        else if tfEmail.isFirstResponder
        {
            tfPassword.becomeFirstResponder()
        }
        else if tfPassword.isFirstResponder
        {
            tfMobileNum.becomeFirstResponder()
        }
        else if tfMobileNum.isFirstResponder
        {
            tfLocation.becomeFirstResponder()
        }
        else if tfLocation.isFirstResponder
        {
            tfLocation.resignFirstResponder()
        }
    }

    
    @IBAction func passwordBtnClicked(_ sender: Any)
    {
        self.btnShowPassword.isSelected = !self.btnShowPassword.isSelected
        
        self.tfPassword.isSecureTextEntry = !self.tfPassword.isSecureTextEntry
    }
    
    // MARK: - API Methods
    func callRegistrationAPI()
    {
        if isRegisterFb
        {
            callFacebookRegistrationAPI()
        }
        else if isRegisterGoogle
        {
            callGoogleRegistrationAPI()
        }
        else if isRegisterTwitter
        {
            callTwitterRegistrationAPI()
        }
        else
        {
            callNormalRegistrationAPI()
        }
    }
    
    func callNormalRegistrationAPI()
    {
        print("Posting the result")
        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Registering")
        loader.startAnimating()
        
        if UserDefaults.standard.value(forKey: "fcmDeviceToken") == nil
        {
            let fcmDeviceToken = FIRInstanceID.instanceID().token()
            print("FCM token: \(fcmDeviceToken ?? "")")
            UserDefaults.standard.setValue(fcmDeviceToken, forKey: "fcmDeviceToken")
            UserDefaults.standard.synchronize()
        }
        
        let deviceToken = GlobalConstants().getFCMToken()
        
        let requestObject : [String : Any] = ["fname" : userPara.userFirstName,
                                              "lname" : "",
                                              "email" : userPara.userEmail,
                                              "password" : userPara.userPassword,
                                              "lat" : userPara.userLocationLat,
                                              "lng" : userPara.userLocationLong,
                                              "image_base_64" : userPara.userPictureBase64,
                                              "location_string" : userPara.userLocationStr,
                                              "mobile_number" : userPara.userMobileNo,
                                              "devicetype" : "1",
                                              "devicetoken" : deviceToken
                                            ] as [String : Any]
        
        //print(requestObject)
        GlobalConstants().postDataFor(subUrl:APP_URL.REGISTER_USER_SUBURL, parameter:requestObject, resultKeyName: "user", completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                print(responseData)
                if isSuccess
                {
                    //print(responseData)
                    
                    let token = responseData["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(responseData, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    self.showAddPhotoVC(imageJSON: responseData["profile_ulbum"] as! [[String : Any]])
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func callFacebookRegistrationAPI()
    {
        print("Posting the result")
        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Registering")
        loader.startAnimating()
        
        let requestObject : [String : Any] = ["social_token" : paraSocial.socialToken,
                                              "facebook_id": paraSocial.facebookId,
                                              "fname" : userPara.userFirstName,
                                              "lname" : "",
                                              "email" : userPara.userEmail,
                                              "password" : userPara.userPassword,
                                              "mobile_number" : userPara.userMobileNo,
                                              "lat" : userPara.userLocationLat,
                                              "lng" : userPara.userLocationLong,
                                              "image_base_64" : userPara.userPictureBase64,
                                              "location_string" : userPara.userLocationStr] as [String : Any]
        
        GlobalConstants().postDataFor(subUrl:APP_URL.REGISTER_WITH_FACEBOOK, parameter:requestObject, resultKeyName: "user", completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    //print(responseData)
                    
                    let token = responseData["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(responseData, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    self.showAddPhotoVC(imageJSON: responseData["profile_ulbum"] as! [[String : Any]])
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func callGoogleRegistrationAPI()
    {
        print("Posting the result")
        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Registering")
        loader.startAnimating()
        
        if UserDefaults.standard.value(forKey: "fcmDeviceToken") == nil
        {
            let fcmDeviceToken = FIRInstanceID.instanceID().token()
            print("FCM token: \(fcmDeviceToken ?? "")")
            UserDefaults.standard.setValue(fcmDeviceToken, forKey: "fcmDeviceToken")
            UserDefaults.standard.synchronize()
        }
        
        //header - username, password, devicetype, devicetoken
        let deviceToken = UserDefaults.standard.value(forKey: "fcmDeviceToken") as! String

        let requestObject : [String : Any] = ["social_token" : paraSocial.socialToken,
                                              "facebook_id": paraSocial.facebookId,
                                              "fname" : userPara.userFirstName,
                                              "lname" : "",
                                              "email" : userPara.userEmail,
                                              "password" : userPara.userPassword,
                                              "mobile_number" : userPara.userMobileNo,
                                              "lat" : userPara.userLocationLat,
                                              "lng" : userPara.userLocationLong,
                                              "image_base_64" : userPara.userPictureBase64,
                                              "location_string" : userPara.userLocationStr,
                                              "devicetype": "1",
                                              "devicetoken": deviceToken] as [String : Any]
        
        GlobalConstants().postDataFor(subUrl:APP_URL.REGISTER_WITH_FACEBOOK, parameter:requestObject, resultKeyName: "user", completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    //print(responseData)
                    
                    let token = responseData["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(responseData, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    self.showAddPhotoVC(imageJSON: responseData["profile_ulbum"] as! [[String : Any]])
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func callTwitterRegistrationAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Registering")
        loader.startAnimating()
        
        if UserDefaults.standard.value(forKey: "fcmDeviceToken") == nil
        {
            let fcmDeviceToken = FIRInstanceID.instanceID().token()
            print("FCM token: \(fcmDeviceToken ?? "")")
            UserDefaults.standard.setValue(fcmDeviceToken, forKey: "fcmDeviceToken")
            UserDefaults.standard.synchronize()
        }
        
        //header - username, password, devicetype, devicetoken
        let deviceToken = UserDefaults.standard.value(forKey: "fcmDeviceToken") as! String

        let requestObject : [String : Any] = ["twitter_id" : paraSocial.twitterId,
                                              "access_token": paraSocial.access_token,
                                              "access_token_secret": paraSocial.access_token_secret,
                                              "fname" : userPara.userFirstName,
                                              "lname": "",
                                              "mobile_number" : userPara.userMobileNo,
                                              "email" : userPara.userEmail,
                                              "lat" : userPara.userLocationLat,
                                              "lng" : userPara.userLocationLong,
                                              "image_base_64" : userPara.userPictureBase64,
                                              "location_string" : userPara.userLocationStr,
                                              "devicetype": "1",
                                              "devicetoken": deviceToken,
                                              "password" : userPara.userPassword] as [String : Any]
        
        GlobalConstants().postDataFor(subUrl:APP_URL.SOCIAL_REGISTER_TWITTER, parameter:requestObject, resultKeyName: "user", completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    //print(responseData)
                    
                    let token = responseData["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(responseData, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    self.showAddPhotoVC(imageJSON: responseData["profile_ulbum"] as! [[String : Any]])
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }

    
    // MARK: - Custom Methods
    func addAttributedText()
    {
        self.txtViewTerms.delegate = self
        
        self.txtViewTerms = server.addAttributedText(txtTerms: self.txtViewTerms)
    }

    func setValuesFromPara()
    {
        self.butnPicture.setBackgroundImage(userPara.userPictureImage, for: .normal)
        self.tfFirstName.text = userPara.userFirstName
        //self.tfLastName.text = userPara.userLastName
        self.tfEmail.text = userPara.userEmail
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in

        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPictureNotEnteredPopUp()
    {
        let alert = UIAlertController.init(title: "No photo selected", message: "People will invite you more often, if they can see a photo of you.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Select photo", style: .default, handler: {
            alert -> Void in
            self.didTapPhotoButn(UIButton())
        }))
        
        alert.addAction(UIAlertAction.init(title: "Continue without profile picture", style: .cancel, handler: {
            alert -> Void in
            self.callRegistrationAPI()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // Lifting the view up
    func animateViewMoving (up:Bool, moveValue :CGFloat)
    {
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    func addToolBar()
    {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = UIBarStyle.default
        toolBar.items = [
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(didTapDoneKeypad)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.plain, target: self, action: #selector(didTapNextKeypad))]
        toolBar.sizeToFit()
        
        tfFirstName.inputAccessoryView = toolBar
        tfMobileNum.inputAccessoryView = toolBar
        tfEmail.inputAccessoryView = toolBar
        tfPassword.inputAccessoryView = toolBar
        tfLocation.inputAccessoryView = toolBar
    }
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    func showDocument()
    {
        if let url = Bundle.main.url(forResource: "GAYANDYOU_terms_of_use_August_2017", withExtension: "pdf", subdirectory: nil, localization: nil)
        {
            let documentInteractionController = UIDocumentInteractionController(url: url)
            documentInteractionController.delegate = self
            documentInteractionController.presentPreview(animated: false)
        }
    }

    func showAddPhotoVC(imageJSON : [[String : Any]])
    {
        let addPhotoVC = storyboards.profilePicStoryboard.instantiateViewController(withIdentifier: "AddProfilePicturesViewController") as! AddProfilePicturesViewController
        
        /*
         "id": 2,
         "user_id": 5,
         "isprofilepicture": 1,
         "picture": "uploads/a80da4cb4fc28aaf4514d36823c8440c.jpg",
         "text": "",
         "thumb": "uploads/thumb/a80da4cb4fc28aaf4514d36823c8440c.jpg"
         */
        if imageJSON.count != 0
        {
            let firstImg = imageJSON[0]
            let para = Para_ProfilePictures()
            para.isProfilePicture = 1
            para.picCaption = ""
            para.picMode = 3 // Update
            para.picId = firstImg["id"] as! Int
            para.picUrl = firstImg["picture"] as! String
            addPhotoVC.imagesArray.append(para)
        }

        self.navigationController?.pushViewController(addPhotoVC, animated: true)
        
        //self.performSegue(withIdentifier: "segueRegistrationToLandingPage", sender: nil)
    }
    
    // MARK: - Image Picker Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
            print(" ~~~~~~~~~~~~~~~~~~~~~~~~~ Image selected ~~~~~~~~~~~~~~~~~~~~~~~~")
            
            guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
                return // No image selected.
            }
        
        self.butnPicture.setBackgroundImage(image, for: .normal)
        
        //Now use image to create into NSData format
        let imageData = image.lowestQualityJPEGNSData
        
        // let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
        let base64String = imageData.base64EncodedString(options: .lineLength64Characters)
        self.userPara.userPictureBase64 = base64String
        
            self.dismiss(animated: true, completion:
                { () -> Void in
                    
                })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion:
            { () -> Void in
//                self.tableView.reloadData()
        })
    }
    
    // MARK: - TextView Delegates
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        if (URL.absoluteString == termsAndConditionsURL)
        {
            self.showDocument()//(title: "Go to Terms", msg: "")
        }
        else if (URL.absoluteString == paymentsURL)
        {
            self.showDocument() //(title: "Go to Payment", msg: "")
        }
        else if (URL.absoluteString == privacyURL)
        {
            self.showDocument()//(title: "Go to Privacy", msg: "")
        }
        else if (URL.absoluteString == nonDiscriminationURL)
        {
            self.showDocument()//(title: "Go to Nondiscrimination", msg: "")
        }
        
        return false
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    // MARK: - Textfield Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if  textField == tfLocation
        {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: 0, y: -70, width: self.view.frame.size.width, height: self.view.frame.size.height)
        })
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == tfFirstName
        {
            userPara.userFirstName = textField.text!
        }
        else if textField == tfMobileNum
        {
            userPara.userMobileNo = textField.text!
        }
        else if textField == tfEmail
        {
            userPara.userEmail = textField.text!
        }
        else if textField == tfPassword
        {
            userPara.userPassword = textField.text!
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        })
    }
    
    // MARK: - GMSAutocompleteViewController Delegate
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        userPara.userLocationStr = place.name
        userPara.userLocationLat = place.coordinate.latitude
        userPara.userLocationLong = place.coordinate.longitude
        
        tfLocation.text = place.name
        
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print(place.coordinate)
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
    {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Photo Picker Delegates
extension Registration : YMSPhotoPickerViewControllerDelegate
{
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
            }
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
            }
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    /*
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!)
    {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        picker.dismiss(animated: true)
        {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            
            let mutableImages: NSMutableArray! = []
            
            for asset: PHAsset in photoAssets
            {
                // let scale = UIScreen.main.scale
                
                // This will be the final size that I am going to show in bigger view.
                // In the collection view of this cell, the images shown are after downloading the respective thumb nails.
                let targetSize = CGSize(width: 400, height: 400)
                
                imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFill, options: options, resultHandler: { (image, info) in
                    mutableImages.add(image!)
                    
                    self.butnPicture.setBackgroundImage(image, for: .normal)
                    
                    //Now use image to create into NSData format
                    let imageData = image?.mediumQualityJPEGNSData
                    
                    // let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
                    let base64String = imageData?.base64EncodedString(options: .lineLength64Characters)
                    self.userPara.userPictureBase64 = base64String!
                })
            }

            // Assign to Array with images
//            self.imagesArray = mutableImages.copy() as! [UIImage]

        }
    }*/
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!)
    {
        picker.dismiss(animated: true, completion:
        {
            Void in
            self.butnPicture.setBackgroundImage(image, for: .normal)
            
            //Now use image to create into NSData format
            let imageData = image?.mediumQualityJPEGNSData
            
            // let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
            let base64String = imageData?.base64EncodedString(options: .lineLength64Characters)
            self.userPara.userPictureBase64 = base64String!
        })
    }
}

