//
//  Para_Message.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 15/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class Para_Message: NSObject
{
    var Id = Int()
    var Chat_Id = Int()
    var Message = String()
    var Time = Double()
    var Sender_Id = Int()
    var Receiver_Id = Int()
}
