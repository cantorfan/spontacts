//
//  UpgradeViewController.swift
//  Spontacts
//
//  Created by maximess142 on 02/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import StoreKit

protocol IAPurchaceViewControllerDelegate {
    
    func didBuyColorsCollection(collectionIndex: Int)
}

class UpgradeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SKProductsRequestDelegate, UITextFieldDelegate
{
    @IBOutlet weak var tableView: UITableView!
    
    var arrRowText = ["Discover your profile visitors", "Hide your own profile visits / Incognito Mode", "Saving your profile visitors (Days)", "Limit the maximal number of your attendees", "No ads in the app", "Create activities only for your gender", "Copy your activities", "Photos in your profile Album", "Message Limit", "Keep your Messages (Days)", "Groups you can join", "Frequent Donator Award / Badge"]
    
//    var arrRowText = ["Discover your profile visitors", "Limit the maximal number of your attendees", "No ads in the app", "Create activities only for your gender", "Copy your activities", "Frequent Donator Award / Badge"]

    let colorPinkBright = UIColor.init(red: 242/255, green: 203/255, blue: 206/255, alpha: 1.0)
    let colorPinkLight = UIColor.init(red: 252/255, green: 243/255, blue: 242/255, alpha: 1.0)
    let colorYellowBright = UIColor.init(red: 247/255, green: 207/255, blue: 163/255, alpha: 1.0)
    let colorYellowLight = UIColor.init(red: 252/255, green: 232/255, blue: 205/255, alpha: 1.0)

    var productIDs: Array<String?> = []
    
    var productsArray: Array<SKProduct?> = []
    
    var selectedProductIndex: Int!
    
    var transactionInProgress = false
    var numberOfMonthsChosen = Int()
    
    var countForApiCalled = 0
    let sharedObj = Singleton.shared
    var loader : ActivityIndicatorView?
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        productIDs.append("1M_Premium")
        productIDs.append("3M_Premium")
        productIDs.append("6M_Premium")
        productIDs.append("12M_Premium")
        
        requestProductInfo()
        
        SKPaymentQueue.default().add(self)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrRowText.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : UpgradeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UpgradeTableViewCell", for: indexPath) as! UpgradeTableViewCell
        
        cell.lblText.text = arrRowText[indexPath.row]

        //cell.btnFreeVersion.setImage(nil, for: .normal)
        cell.btnPlusVersion.setImage(nil, for: .normal)
        //cell.btnFreeVersion.setTitle("", for: .normal)
        cell.btnPlusVersion.setTitle("", for: .normal)
        
        switch indexPath.row
        {
        case 0:
           // cell.btnFreeVersion.setImage(#imageLiteral(resourceName: "cross_gray"), for: .normal)
            cell.btnPlusVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            
        case 1:
//            cell.btnFreeVersion.setImage(#imageLiteral(resourceName: "cross_gray"), for: .normal)
            cell.btnPlusVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            
        case 2 :
//            cell.btnFreeVersion.setTitle("3", for: .normal)
            cell.btnPlusVersion.setTitle("30", for: .normal)
           
        case 3 :
//            cell.btnFreeVersion.setImage(#imageLiteral(resourceName: "cross_gray"), for: .normal)
            cell.btnPlusVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            
        case 4:
//            cell.btnFreeVersion.setImage(#imageLiteral(resourceName: "cross_gray"), for: .normal)
            cell.btnPlusVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            
        case 5:
//            cell.btnFreeVersion.setImage(#imageLiteral(resourceName: "cross_gray"), for: .normal)
            cell.btnPlusVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            
        case 6:
//            cell.btnFreeVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            cell.btnPlusVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            
        case 7:
//            cell.btnFreeVersion.setTitle("4", for: .normal)
            cell.btnPlusVersion.setTitle("10", for: .normal)
            
        case 8:
//            cell.btnFreeVersion.setTitle("30", for: .normal)
            cell.btnPlusVersion.setTitle("2000", for: .normal)
            
        case 9:
//            cell.btnFreeVersion.setTitle("21", for: .normal)
            cell.btnPlusVersion.setTitle("90", for: .normal)
            
        case 10:
//            cell.btnFreeVersion.setTitle("6", for: .normal)
            cell.btnPlusVersion.setTitle("50", for: .normal)
          
        case 11:
//            cell.btnFreeVersion.setImage(#imageLiteral(resourceName: "cross_gray"), for: .normal)
            cell.btnPlusVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            
        case 12:
//            cell.btnFreeVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            cell.btnPlusVersion.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            
        default:
            print("default")
        }
        
        return cell
    }
    
    // MARK: - IBActions
    
    @IBAction func didTapBuyButton(_ sender: Any)
    {
        if transactionInProgress {
            return
        }
        
        /*
        12M_Premium 3
        1M_Premium 0
        3M_Premium 1
        6M_Premium 2*/
        
        switch (sender as AnyObject).tag
        {
            case 0:
            self.selectedProductIndex = 1
            self.numberOfMonthsChosen = 1
            
            case 1:
            self.selectedProductIndex = 2
            self.numberOfMonthsChosen = 3
            
            case 2:
            self.selectedProductIndex = 3
            self.numberOfMonthsChosen = 6
            
            case 3:
            self.selectedProductIndex = 0
            self.numberOfMonthsChosen = 12
            
            default:
            self.selectedProductIndex = 1
            self.numberOfMonthsChosen = 1
        }
        
        let product = productsArray[selectedProductIndex]
        let prodIdentifier = (product?.productIdentifier)!
        print(prodIdentifier, selectedProductIndex)

        /*
        // They are received in alpha betical order so doing this alternation
        switch prodIdentifier
        {
        case "1M_Premium":
            self.selectedProductIndex = 1
            self.numberOfMonthsChosen = 1
            
        case "3M_Premium":
            self.selectedProductIndex = 1
            self.numberOfMonthsChosen = 3

        case "6M_Premium":
            self.selectedProductIndex = 2
            self.numberOfMonthsChosen = 6

        case "12M_Premium":
            self.selectedProductIndex = 3
            self.numberOfMonthsChosen = 12

        default:
            self.selectedProductIndex = 0
            self.numberOfMonthsChosen = 0
        }*/
        
        callIAPController()
    }
    
    func callIAPController()
    {
        let payment = SKPayment(product: self.productsArray[self.selectedProductIndex]!)
        SKPaymentQueue.default().add(payment)
        self.transactionInProgress = true
    }
    
    // MARK: - In-App Purchase
    func requestProductInfo()
    {
        loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader?.startAnimating()
        
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = NSSet(array: productIDs)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
            
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
    {
        if loader != nil
        {
            loader?.stopAnimating()
        }
        
        if response.products.count != 0
        {
            for product in response.products
            {
                productsArray.append(product)
                print(product.productIdentifier)
            }
            
            tableView.reloadData()
            print(productsArray)
            
            //productsArray = productsArray.reversed()
            //showAlert(title: "Found products", msg: "")
        }
        else
        {
            print("There are no products.")
            //showAlert(title: "NO product", msg: "")
        }
        
        
        if response.invalidProductIdentifiers.count != 0
        {
            print(response.invalidProductIdentifiers.description)
            
            showAlert(title: "Invalid product id", msg: response.invalidProductIdentifiers.description)
        }
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showGetMsgAlert()
    {
        let alertController = UIAlertController.init(title: "Thank you!", message: "Would you like to write something on our wall? Try to limit it to 40 characters", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: configurationTextField)

        
        let confirmAction = UIAlertAction(title: "Post", style: .default) { (_) in
            if let field = alertController.textFields?[0]
            {
                print(field.text!)
                self.callSubscriptionText(subscriptionText: field.text!)
            }
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .destructive, handler: {
            alert -> Void in
            
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func configurationTextField(textField: UITextField!)
    {
        textField.placeholder = "Write here"
        textField.delegate = self
        textField.tag = 100
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 40 // Bool
    }
    
    func callSubscriptionText(subscriptionText : String)
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["text" : subscriptionText]
        
        GlobalConstants().postData_Header_Parameter(subUrl: UPGRADE_URL.ADD_SUBSCRIPTION_TEXT, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Ooops!", msg: responseMessage)
            }
        })
    }
}

//By doing so, not only we’ll be able to trigger a transaction, but also to get notified about its progress and whether is successfully finished, so eventually we unlock the extra features of the app
extension UpgradeViewController : SKPaymentTransactionObserver
{
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
    {
        for transaction in transactions 
        {
            switch transaction.transactionState
            {
            case SKPaymentTransactionState.purchased:
                print("Transaction completed successfully.")
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                Singleton.shared.userPara.isPlusMember = 1
                
                self.showGetMsgAlert()
                DispatchQueue.global(qos: .background).async
                    {
                        self.updateSubscriptionPeriodOnServer()
                }
                //delegate.didBuyColorsCollection(selectedProductIndex)
                
            case SKPaymentTransactionState.failed:
                print("Transaction Failed");
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    func updateSubscriptionPeriodOnServer()
    {
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        let requestObject = ["months" : numberOfMonthsChosen]
        
        countForApiCalled += 1
        
        GlobalConstants().postData_Header_Parameter(subUrl: UPGRADE_URL.UPDATE_SUBSCRIPTION, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            Singleton.shared.userPara.isPlusMember = 1

            self.callGetUserInformationAPI()
            if isSuccess
            {
                // Great !!
            }
            else
            {
                if self.countForApiCalled > 5
                {
                    // Call this api again until it is saved on our server
                    self.updateSubscriptionPeriodOnServer()
                }
            }
        })
    }
    
    func callGetUserInformationAPI()
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: APP_URL.GET_SELF_PROFILE, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                print(responseData)
                if isSuccess
                {
                    let userJSON = responseData["user"] as! [String : Any]
                    UserDefaults.standard.set(userJSON, forKey: "userJSON")
                    UserDefaults.standard.synchronize()
                    
                    self.sharedObj.userPara.userId = userJSON["id"] as! Int
                    self.sharedObj.userPara.userEmail = userJSON["email"] as! String
                    self.sharedObj.userPara.userLocationLat = userJSON["lat"] as! Double
                    self.sharedObj.userPara.userFirstName = userJSON["fname"] as! String
                    self.sharedObj.userPara.userLastName = userJSON["lname"] as! String
                    self.sharedObj.userPara.userToken = userJSON["token"] as! String
                    self.sharedObj.userPara.userLocationLong = userJSON["lng"] as! Double
                    //self.sharedObj.userPara.userPassword = userJSON["password"] as! String
                    self.sharedObj.userPara.userPictureThumbUrl = userJSON["thumb"] as! String
                    self.sharedObj.userPara.userPictureUrl = userJSON["picture"] as! String
                    self.sharedObj.userPara.userLocationStr = userJSON["location_string"] as! String
                    self.sharedObj.userPara.userAge = userJSON["age"] as! Int
                    self.sharedObj.userPara.userGender = userJSON["gender"] as! Int
                    self.sharedObj.userPara.userRelationStatus = userJSON["relationship_status"] as! Int
                    self.sharedObj.userPara.userDescription = userJSON["about"] as! String
                    self.sharedObj.userPara.isPlusMember = userJSON["suscribed"] as! Int
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }

}
