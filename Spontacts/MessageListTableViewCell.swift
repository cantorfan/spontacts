//
//  MessageListTableViewCell.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 07/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class MessageListTableViewCell: UITableViewCell
{

    @IBOutlet weak var imgViewUser: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblMessagePreview: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        imgViewUser.layer.cornerRadius = imgViewUser.frame.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
