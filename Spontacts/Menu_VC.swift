//
//  Menu_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 09/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class Menu_VC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    
    let sharedObj = Singleton.shared
    
    var view_width = CGFloat()
    
    let arrRowText = ["HOME", "All activities", "My activities", "Groups", "Chats", "Friends", "Recommendations", "Share GAY AND YOU?"]
    let arrImages = [#imageLiteral(resourceName: "home_white"), #imageLiteral(resourceName: "all_activities"), #imageLiteral(resourceName: "interested"), #imageLiteral(resourceName: "groups"), #imageLiteral(resourceName: "messages_Blue"), #imageLiteral(resourceName: "friends_blue"), #imageLiteral(resourceName: "recommendations"), #imageLiteral(resourceName: "share")]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view_width = 260
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Methods
    @IBAction func didTapProfileButn()
    {
        print("profile clicked")
        self.performSegue(withIdentifier: "segueMenuToUserProfile", sender: nil)
    }
    
    @IBAction func didTapVisitorButn()
    {
        self.performSegue(withIdentifier: "segueHomeToProfileVisitors", sender: nil)
//        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "VisitorsViewController") as! VisitorsViewController
//        
//        // Will allow to go back
//        self.navigationController?.pushViewController(presentVC, animated: true)
    }
    
    @IBAction func didTapShareButn()
    {
        // text to share
        let text = "Looks like fun! Check out this awesome app and join the community. https://itunes.apple.com/us/app/gay-and-you/id1278713370?ls=1&mt=8"
        
        // set up activity view controller
        let textToShare = [ text , URL.init(string: "https://itunes.apple.com/us/app/gay-and-you/id1278713370?ls=1&mt=8")!] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        // activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func didTapGroup()
    {
        //performSegue(withIdentifier: "segueLandingPageToDonation", sender: nil)
        if ServicesManager.instance().currentUser() == nil
        {
            let user = QBUUser()
            user.login = sharedObj.userPara.userToken
            user.password = kQuickbloxCommonPassword
            
            logInChatWithUser(user: user)
        }
        else
        {
            self.performSegue(withIdentifier: "segueToMessaging", sender: nil)
        }
    }
    
    func logInChatWithUser(user: QBUUser) {
        
        //SVProgressHUD.show(withStatus: "SA_STR_LOGGING_IN_AS".localized + user.login!, maskType: SVProgressHUDMaskType.clear)
        
        // Logging to Quickblox REST API and chat.
        ServicesManager.instance().logIn(with: user, completion: {
            [weak self] (success,  errorMessage) -> Void in
            
            guard let strongSelf = self else {
                return
            }
            
            guard success else {
                SVProgressHUD.showError(withStatus: errorMessage)
                return
            }
            
            strongSelf.registerForRemoteNotification()
            //SVProgressHUD.showSuccess(withStatus: "SA_STR_LOGGED_IN".localized)
            
            self?.performSegue(withIdentifier: "segueToMessaging", sender: nil)
        })
    }

    func registerForRemoteNotification() {
        // Register for push in iOS 8
        if #available(iOS 8.0, *) {
            let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
        else {
            // Register for push in iOS 7
            UIApplication.shared.registerForRemoteNotifications(matching: [UIRemoteNotificationType.badge, UIRemoteNotificationType.sound, UIRemoteNotificationType.alert])
        }
    }
    
    @IBAction func didTapHomeButn()
    {
        self.performSegue(withIdentifier: "segueMenuToHome", sender: nil)
    }
    
    // MARK:- UI Methods
    func firstHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view_width, height: 250))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "dddddd")
        
        let cloudImgView = UIImageView.init(frame: headerView.frame)
        cloudImgView.image = #imageLiteral(resourceName: "cloud_background.jpg")
        headerView.addSubview(cloudImgView)

        let btnUserImage = UIImageView.init(frame: CGRect(x: 10, y: 50, width: 80, height: 80))
        btnUserImage.backgroundColor = UIColor.white
        btnUserImage.image = UIImage.init(named: "avatar")
        btnUserImage.layer.cornerRadius = btnUserImage.frame.size.width / 2
        btnUserImage.layer.borderColor = UIColor.white.cgColor
        btnUserImage.layer.borderWidth = 1.0
        btnUserImage.clipsToBounds = true
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: 80.0, height: 80.0), radius: 40)
        btnUserImage.af_setImage(withURL:  URL.init(string: APP_URL.BASE_URL + sharedObj.userPara.userPictureThumbUrl)!, placeholderImage: #imageLiteral(resourceName: "avatar_big"), filter: filter, imageTransition: .crossDissolve(0.2), completion:
            { response in
                
        })
        
        headerView.addSubview(btnUserImage)
        
        let lblName = UILabel.init(frame: CGRect(x: btnUserImage.frame.origin.x  + btnUserImage.frame.size.width + 10, y: btnUserImage.frame.origin.y + 20, width: view_width - ( btnUserImage.frame.origin.x  + btnUserImage.frame.size.width + 10), height: 40))
        lblName.textAlignment = .left
        lblName.textColor = sharedObj.hexStringToUIColor(hex: "336197")
        lblName.font = UIFont.init(name: Font.BEBAS_SemiBold, size: 18)
        lblName.numberOfLines = 0
        lblName.lineBreakMode = .byWordWrapping
        lblName.text = sharedObj.userPara.userFirstName + " " + sharedObj.userPara.userLastName
        headerView.addSubview(lblName)
        
        let btnProfile = UIButton(frame: CGRect(x: 0, y: 0, width: view_width, height: 180))
        btnProfile.addTarget(self, action: #selector(self.didTapProfileButn), for: .touchUpInside)
        headerView.addSubview(btnProfile)
        
        let btnVisitors = UIButton(frame: CGRect(x: 0, y: btnProfile.frame.origin.y + btnProfile.frame.size.height, width: view_width, height: 50))
        btnVisitors.backgroundColor = UIColor.white
        btnVisitors.addTarget(self, action: #selector(self.didTapVisitorButn), for: .touchUpInside)
        
        let imgIcon = UIImageView(frame: CGRect(x: 10, y: 10, width: 30, height: 30))
        imgIcon.image = #imageLiteral(resourceName: "visitors")
        btnVisitors.addSubview(imgIcon)
        
        let lblVisitors = UILabel.init(frame: CGRect(x: 50, y: 5, width: btnVisitors.frame.size.width - 40, height: 40))
        lblVisitors.textAlignment = .left
        lblVisitors.font = UIFont.init(name: Font.BEBAS_SemiBold, size: 18)
        lblVisitors.textColor = UIColor.init(red: 107/255, green: 108/255, blue: 109/255, alpha: 1.0)//#colorLiteral(red: 0.5062589049, green: 0.5177934766, blue: 0.5180256963, alpha: 1)
        lblVisitors.text = "My Profile Visitors"
        btnVisitors.addSubview(lblVisitors)
        
        if sharedObj.unread_Profile_Visitors_Count != 0
        {
            let lblNotification = UILabel(frame: CGRect(x: view_width - 35, y: 15, width: 20, height: 20))
            lblNotification.textAlignment = .center
            lblNotification.font = UIFont.init(name: Font.BEBAS, size: 14)
            lblNotification.textColor = UIColor.white
            lblNotification.tag = 3
            lblNotification.layer.cornerRadius = 10
            lblNotification.clipsToBounds = true
            lblNotification.backgroundColor = #colorLiteral(red: 0.8951961398, green: 0.3051324189, blue: 0.1987941265, alpha: 1) //#colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
            btnVisitors.addSubview(lblNotification)

            lblNotification.text = "\(sharedObj.unread_Profile_Visitors_Count)"
            
            if sharedObj.unread_Profile_Visitors_Count > 50
            {
                lblNotification.text = "50+"
            }
        }
        
        headerView.addSubview(btnVisitors)
        
        return headerView
    }
    
    func secondHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view_width, height: 50))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let btnAppPlus = UIButton(frame: CGRect(x: 0, y: 0, width: view_width, height: 50))
        btnAppPlus.backgroundColor = sharedObj.hexStringToUIColor(hex: "2196f3")
        btnAppPlus.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 18)

        btnAppPlus.setTitle("Home", for: .normal)
        btnAppPlus.setTitleColor(UIColor.white, for: .normal)
        btnAppPlus.addTarget(self, action: #selector(self.didTapHomeButn), for: .touchUpInside)
        headerView.addSubview(btnAppPlus)
        
        return headerView
    }

    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            return arrRowText.count
        }
        else if section == 2
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if  section == 0
        {
            return 250
        }
        else if section == 1
        {
            return 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()//.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        headerView.backgroundColor = UIColor.clear
        
        if section == 0
        {
            headerView.addSubview(firstHeaderView(headerView: headerView))
        }
        else if section == 1
        {
            headerView.addSubview(secondHeaderView(headerView: headerView))
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        
        var titleLabel = UILabel()
        var imgIcon = UIImageView()
        var lblNotification = UILabel()
        var gradView = UIImageView()
        
        if (cell == nil)
        {
            cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
            cell?.selectionStyle = UITableViewCellSelectionStyle.gray
            
            imgIcon = UIImageView(frame: CGRect(x: 10, y: 12.5, width: 25, height: 25))
            imgIcon.tag = 1
            
            titleLabel = UILabel(frame: CGRect(x: 50, y: 7, width: view_width - 40, height: 40))
            titleLabel.textAlignment = .left
            titleLabel.textColor = UIColor.init(red: 107/255, green: 108/255, blue: 109/255, alpha: 1.0)//#colorLiteral(red: 0.5062589049, green: 0.5177934766, blue: 0.5180256963, alpha: 1)
            titleLabel.font = UIFont.init(name: Font.BEBAS_SemiBold, size: 18)
            titleLabel.tag = 2
            
            lblNotification = UILabel(frame: CGRect(x: view_width - 35, y: 15, width: 20, height: 20))
            lblNotification.textAlignment = .center
            lblNotification.font = UIFont.init(name: Font.BEBAS, size: 14)
            lblNotification.textColor = UIColor.white
            lblNotification.tag = 3
            lblNotification.layer.cornerRadius = 10
            lblNotification.clipsToBounds = true
            lblNotification.backgroundColor = #colorLiteral(red: 0.8951961398, green: 0.3051324189, blue: 0.1987941265, alpha: 1) //bj.hexStringToUIColor(hex: "2196f3")
            
            gradView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: self.view_width, height: 50))
            gradView.image = #imageLiteral(resourceName: "gradient_butn")
            gradView.tag = 4
            gradView.isHidden = true
            
            cell?.contentView.addSubview(gradView)
            cell?.contentView.addSubview(imgIcon)
            cell?.contentView.addSubview(titleLabel)
            cell?.contentView.addSubview(lblNotification)
        }
        else
        {
            imgIcon = cell?.contentView.viewWithTag(1) as! UIImageView
            titleLabel = cell?.contentView.viewWithTag(2) as! UILabel
            lblNotification = cell?.contentView.viewWithTag(3) as! UILabel
            gradView = cell?.contentView.viewWithTag(4) as! UIImageView
        }
        
        lblNotification.isHidden = true
        
        if indexPath.section == 1
        {
            //"HOME", "All activities", "My activities", "Groups", "Chats", "Friends", "Recommendations", "Share GAY AND YOU?"
            var unreadCount = 0
            
            if indexPath.row == 0
            {
                gradView.isHidden  = false
                titleLabel.textColor = UIColor.white
                titleLabel.font = UIFont.init(name: Font.BEBAS_Bold, size: 18)
            }
            else if indexPath.row == 1
            {
                unreadCount = sharedObj.unread_All_Activities_Count
            }
            else if indexPath.row == 2
            {
                unreadCount = sharedObj.unread_MyActivities_Count
            }
            else if indexPath.row == 3
            {
                unreadCount = 0
            }
            else if indexPath.row == 4
            {
                unreadCount = sharedObj.unread_Chats_Count
            }
            else if indexPath.row == 5
            {
                unreadCount = sharedObj.unread_Friends_Count
            }
            else if indexPath.row == 6
            {
                unreadCount = sharedObj.unread_Recommendations
            }
            
            titleLabel.text = arrRowText[indexPath.row]
            imgIcon.image = arrImages[indexPath.row]
            
            if unreadCount != 0
            {
                lblNotification.isHidden = false
                lblNotification.text = "\(unreadCount)"
                
                if unreadCount > 50
                {
                    lblNotification.text = "50+"
                }
            }
        }
        else
        {
            titleLabel.textColor = sharedObj.hexStringToUIColor(hex: "d1d1d1")
            titleLabel.text = "Settings"
            
            imgIcon.image = #imageLiteral(resourceName: "settings")
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 1
        {
            if indexPath.row == 0
            {
                didTapHomeButn()
            }
            else if indexPath.row == 1
            {
                performSegue(withIdentifier: "segueMenuToAllActivities", sender: nil)
            }
            else if indexPath.row == 2
            {
                performSegue(withIdentifier: "segueMenuToMyActivities", sender: nil)
            }
            else if indexPath.row == 3
            {
                performSegue(withIdentifier: "segueToCustomGroups", sender: nil)
            }
            else if indexPath.row == 4
            {
                didTapGroup()
            }
            else if indexPath.row == 5
            {
                performSegue(withIdentifier: "segueMenuToFriendsModule", sender: nil)
            }
            else if indexPath.row == 6
            {
                performSegue(withIdentifier: "segueMenuToRecommendations", sender: nil)
            }
            else if indexPath.row == 7
            {
                didTapShareButn()
            }
        }
        else
        {
            performSegue(withIdentifier: "segueMenuToSettings", sender: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
