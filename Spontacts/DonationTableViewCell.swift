//
//  DonationTableViewCell.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 17/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class DonationTableViewCell: UITableViewCell
{
    @IBOutlet weak var lblMission: UILabel!

    @IBOutlet weak var constraintLblMissionHeight: NSLayoutConstraint!
    @IBOutlet weak var btnUpDownToggle: UIButton!
    
    @IBOutlet weak var imgViewUserThumb: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserMsg: UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
