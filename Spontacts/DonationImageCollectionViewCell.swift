//
//  DonationImageCollectionViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 30/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class DonationImageCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgView: UIImageView!
    
}
