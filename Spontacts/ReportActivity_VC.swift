//
//  ReportActivity_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 08/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class ReportActivity_VC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate
{
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblReason: UILabel!
    
    @IBOutlet weak var tfChoose: UITextField!
   
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var textViewDesc: UITextView!
    
    @IBOutlet weak var butnReport: UIButton!
    
    var arrReason = [""]
    let sharedObj = Singleton.shared
    
    var activityPara = Para_Activity()
    var userIdToBlock = Int()
    
    var isUser = false
    var selectedReason = Int()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")

        if isUser == false
        {
            title = "Report activity to GAY AND YOU?!"
            
            lblHeader.text = "Report the following activity '\(activityPara.actName)' to GAY AND YOU?"
            
            arrReason = ["Vandalism", "Violent Content", "Threats", "Commercial Offer", "Fake-Profile", "Harassment", "Spam", "Inappropriate Content", "Sexual Content", "Suicidal Content", "Other"]
        }
        else
        {
            title = "Report User"
            
            lblHeader.text = "Report this user to GAY AND YOU?"
            
            arrReason = ["Vandalism", "Violent Content", "Threats", "Commercial Offer", "Fake-Profile", "Harassment", "Spam", "Inappropriate Content", "Sexual Content", "Suicidal Content", "Other"]
        }
        
        tfChoose.layer.borderColor = UIColor.lightGray.cgColor
        tfChoose.layer.borderWidth = 1.0
        tfChoose.layer.cornerRadius = 5.0
        tfChoose.clipsToBounds = true
        
        textViewDesc.layer.borderColor = UIColor.lightGray.cgColor
        textViewDesc.layer.borderWidth = 1.0
        textViewDesc.layer.cornerRadius = 5.0
        textViewDesc.clipsToBounds = true
        textViewDesc.delegate = self
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = UIBarStyle.default
        toolBar.items = [
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(didTapDoneKeypad))]
        toolBar.sizeToFit()
        textViewDesc.inputAccessoryView = toolBar
        
        let picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self
        picker.backgroundColor = UIColor.white
        tfChoose.inputView = picker
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    func didTapDoneKeypad()
    {
        view.endEditing(true)
    }
    
    @IBAction func didTapReport(_ sender: Any)
    {
        self.view.endEditing(true)
        print(tfChoose.text as Any)
        print(textViewDesc.text)
        
        if tfChoose.text == ""
        {
            self.showAlert(title: "Warning!", msg: "Please select a reason")
        }
        else if textViewDesc.text == ""
        {
            self.showAlert(title: "Warning!", msg: "Please enter description")
        }
        else
        {
            if self.isUser == true
            {
                callReportUserAPI()
            }
            else
            {
                callReportActivityAPI()
            }
        }
    }
    
    // MARK: - Picker Datasource and delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arrReason.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return arrReason[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        tfChoose.text = arrReason[row]
        selectedReason = row
        self.view.endEditing(true)
    }
    
    
    // MARK: - API Methods
    func callReportUserAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject : [String : Any] = [ "reason" : selectedReason,
                              "description" : self.textViewDesc.text!,
                              "user_id" : self.userIdToBlock] as [String : Any]
        
        GlobalConstants().postData_Header_Parameter(subUrl: REPORT_URL.REPORT_USER, header: headerObject, parameter: requestObject, completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            if isSuccess
            {
                let alert = UIAlertController.init(title: "Success!", message: responseMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
                    alert -> Void in
                    self.navigationController?.popViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        } )
    }

    func callReportActivityAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]

        let requestObject : [String : Any] = [ "reason" : selectedReason,
                                               "description" : self.textViewDesc.text!,
                                               "activity_id" : self.activityPara.actId] as [String : Any]
        
        GlobalConstants().postData_Header_Parameter(subUrl: REPORT_URL.REPORT_ACTTIVITY, header: headerObject, parameter: requestObject, completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            print(responseData)
            
            if isSuccess
            {
                let alert = UIAlertController.init(title: "Success!", message: responseMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
                    alert -> Void in
                    self.navigationController?.popViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        } )
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Textview delegates
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
