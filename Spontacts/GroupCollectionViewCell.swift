//
//  GroupCollectionViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 09/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class GroupCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgViewGroup: UIImageView!
    
    @IBOutlet weak var lblCount: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    
}
