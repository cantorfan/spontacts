//
//  FilterActivity_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 16/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import GooglePlaces

class FilterActivity_VC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var viewBottom: UIView!
    
    let sharedObj = Singleton.shared
    var view_width = CGFloat()
    var view_height = CGFloat()
    let arrRowName = ["Today", "Tomorrow", "Weekend"]
    let arrVisibleTo = [ "My friends", "All"]
    var filterPara = Para_ActivityFilter()
    var isShowVisibleTo = Bool()
    var isShowFromTo = Bool()
    
    var tfFrom = UITextField()
    var tfTo = UITextField()
    var dateFormatter = DateFormatter()
    var dateLabelFull = UILabel()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "Search & Filter"
        view_width = view.frame.size.width
        view_height = view.frame.size.height
        
        dateFormatter.dateFormat = "dd MMM yyyy"
        addRightBarButn()
        setBottomView()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        filterPara.catStr = "All"

        if self.sharedObj.para_filter.categories_array.count != 0
        {
            filterPara.catStr = ""
            for catid in self.sharedObj.para_filter.categories_array
            {
                filterPara.catStr.append(sharedObj.arrCat[catid ])
                filterPara.catStr.append(", ")
            }
        }

        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addRightBarButn()
    {
        let viewButn = UIBarButtonItem.init(title: "View", style: .done, target: self, action: #selector(self.didTapViewButn))
        self.navigationItem.rightBarButtonItem = viewButn
    }
    
    // MARK: - Action Methods
    func didTapVisibleTo()
    {
        isShowVisibleTo = !isShowVisibleTo
        tableView.reloadData()
    }
    
    func didTapWhenButn()
    {
        isShowFromTo = !isShowFromTo
        /*
        sharedObj.para_filter.from_date = 0
        sharedObj.para_filter.to_date = 0
        sharedObj.para_filter.isFromTimeRows = 0
        sharedObj.para_filter.timeStr = ""
        */
        tableView.reloadData()
    }
    
    func didTapCategoryButn()
    {
        performSegue(withIdentifier: "segueFilterActivityToCategoriesList", sender: nil)
    }

    @IBAction func didTapResetButn()
    {        
        self.sharedObj.resetFilterPara()
        
        filterPara = Para_ActivityFilter()
        tableView.reloadData()
    }
    
    @IBAction func didTapViewButn()
    {
        self.view.endEditing(true)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didChangeDate(sender : UIDatePicker)
    {
            // From date
        if sender.tag == 1
        {
            self.sharedObj.para_filter.from_date = sender.date.timeIntervalSince1970
            tfFrom.text = dateFormatter.string(from: sender.date)
        }
            // To date
        else if sender.tag == 2
        {
            self.sharedObj.para_filter.to_date = sender.date.timeIntervalSince1970
            tfTo.text = dateFormatter.string(from: sender.date)
        }
        
        sharedObj.para_filter.timeStr = tfFrom.text! + " - " + tfTo.text!
    }
    
    // MARK: - UI Methods
    func setBottomView()
    {
        let resetButn = UIButton.init(frame: CGRect(x: 0, y: 0, width: view_width / 2, height: viewBottom.frame.size.height))
        resetButn.backgroundColor = UIColor.gray
        resetButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.yellow).withAlphaComponent(1.0)), for: .selected)
        resetButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.yellow).withAlphaComponent(1.0)), for: .highlighted)
        resetButn.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 18)
        resetButn.setTitle("Reset", for: .normal)
        resetButn.setTitleColor(UIColor.white, for: .normal)
        resetButn.addTarget(self, action: #selector(self.didTapResetButn), for: .touchUpInside)
        
        let viewButn = UIButton.init(frame: CGRect(x: view_width / 2, y: 0, width: view_width / 2, height: viewBottom.frame.size.height))
        viewButn.backgroundColor = sharedObj.hexStringToUIColor(hex: "00edbf") //UIColor.green
        viewButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        viewButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        viewButn.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 18)
        viewButn.setTitle("View", for: .normal)
        viewButn.setTitleColor(UIColor.white, for: .normal)
        viewButn.addTarget(self, action: #selector(self.didTapViewButn), for: .touchUpInside)
        
        viewBottom.addSubview(resetButn)
        viewBottom.addSubview(viewButn)
    }
    
    func firstHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view_width, height: 60))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")

        let searchBar = UISearchBar.init(frame: CGRect(x: 10, y: 10, width: view_width - 20, height: 40))
        searchBar.placeholder = "Search for Activities..."
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        searchBar.inputAccessoryView = addToolBar()
        searchBar.returnKeyType = .search
        searchBar.text = self.sharedObj.para_filter.search_keyword
        headerView.addSubview(searchBar)
        
        return headerView
    }
    
    func secondHeaderView(headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view_width, height: 55))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let catButn = UIButton.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50))
        catButn.backgroundColor = UIColor.white
        catButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        catButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        catButn.addTarget(self, action: #selector(self.didTapCategoryButn), for: .touchUpInside)
        
        let lblLeft = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width / 3, height: 50))
        lblLeft.textAlignment = .left
        lblLeft.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblLeft.text = "Categories"
        catButn.addSubview(lblLeft)
        
        let lblRight = MarqueeLabel(frame: CGRect(x: view.frame.size.width / 3 + 10, y: 0, width: view.frame.size.width / 3 * 2 - 50
            , height: 50))
        lblRight?.textAlignment = .right
        lblRight?.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblRight?.text = filterPara.catStr == "" ? "All" : filterPara.catStr
        lblRight?.textColor = UIColor.init(red: 0/255, green: 80/255, blue: 190/255, alpha: 1.0) //sharedObj.hexStringToUIColor(hex: "2196F3")
        catButn.addSubview(lblRight!)
        
        let iconArrow = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: 15, width: 20, height: 20))
        iconArrow.image = #imageLiteral(resourceName: "arrow_right")
        catButn.addSubview(iconArrow)
        
        headerView.addSubview(catButn)
        
        return headerView
    }
    
    func thirdHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 86))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let containerView = UIView.init(frame: CGRect(x: 0, y: 5, width: view.frame.size.width, height: 82))
        containerView.backgroundColor = UIColor.white
        
        let tfWhere = UITextField(frame: CGRect(x: 10, y: 0, width: view.frame.size.width - 50, height: 40))
        tfWhere.textAlignment = .left
        tfWhere.font = UIFont.init(name: Font.BEBAS, size: 15)
        tfWhere.placeholder = "Where"
        tfWhere.tag = 100
        tfWhere.delegate = self
        tfWhere.text = self.sharedObj.para_filter.locationStr
        containerView.addSubview(tfWhere)
        
        let iconLocation = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: 10, width: 20, height: 20))
        iconLocation.image = #imageLiteral(resourceName: "location_20")
        containerView.addSubview(iconLocation)
        
        let sep1 = UIView.init(frame: CGRect(x: 0, y: 40, width: view.frame.size.width, height: 1))
        sep1.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        containerView.addSubview(sep1)
        
        let butnTime = UIButton.init(frame: CGRect(x: 0, y: sep1.frame.origin.y + sep1.frame.size.height, width: view.frame.size.width, height: 40))
        butnTime.backgroundColor = UIColor.white
        butnTime.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        butnTime.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        butnTime.addTarget(self, action: #selector(self.didTapWhenButn), for: .touchUpInside)
        
        let lblLeft = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width / 3, height: 50))
        lblLeft.textAlignment = .left
        lblLeft.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblLeft.text = "When"
        butnTime.addSubview(lblLeft)
        
        dateLabelFull = UILabel(frame: CGRect(x: view.frame.size.width / 3 + 10, y: 0, width: view.frame.size.width / 3 * 2 - 50
            , height: 40))
        dateLabelFull.textAlignment = .right
        dateLabelFull.font = UIFont.init(name: Font.BEBAS, size: 13)
        dateLabelFull.text = sharedObj.para_filter.timeStr == "" ? "Anytime" : sharedObj.para_filter.timeStr
        dateLabelFull.textColor = UIColor.init(red: 0/255, green: 80/255, blue: 190/255, alpha: 1.0) //sharedObj.hexStringToUIColor(hex: "2196F3")
        butnTime.addSubview(dateLabelFull)
        
        let iconArrow = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: 10, width: 20, height: 20))
        iconArrow.image = #imageLiteral(resourceName: "clock_20")
        butnTime.addSubview(iconArrow)
        
        containerView.addSubview(butnTime)
        headerView.addSubview(containerView)
        return headerView
    }

    func fourthHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 45))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let containerView = UIView.init(frame: CGRect(x: 0, y: 5, width: view.frame.size.width, height: 40))
        containerView.backgroundColor = UIColor.white
        
        let btnVisibleTo = UIButton.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        btnVisibleTo.backgroundColor = UIColor.white
        btnVisibleTo.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnVisibleTo.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnVisibleTo.addTarget(self, action: #selector(didTapVisibleTo), for: .touchUpInside)
        
        let lblLeft = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width / 3 * 2, height: 40))
        lblLeft.textAlignment = .left
        lblLeft.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblLeft.text = "Who"
        btnVisibleTo.addSubview(lblLeft)
        
        let lblRight = UILabel(frame: CGRect(x: view.frame.size.width / 3 * 2 - 50, y: 0, width: view.frame.size.width / 3, height: 40))
        lblRight.textAlignment = .right
        lblRight.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblRight.textColor = UIColor.init(red: 0/255, green: 80/255, blue: 190/255, alpha: 1.0) //sharedObj.hexStringToUIColor(hex: "2196f3")
        lblRight.text = arrVisibleTo[sharedObj.para_filter.visibleTo] as String
        btnVisibleTo.addSubview(lblRight)
        
        let icon = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: 10, width: 20, height: 20))
        icon.image = #imageLiteral(resourceName: "friends_blue")
        btnVisibleTo.addSubview(icon)
        containerView.addSubview(btnVisibleTo)
        
        headerView.addSubview(containerView)
        return headerView
    }

    func timeHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 100))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let btnFrom = UIButton.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50))
        btnFrom.backgroundColor = UIColor.white
        btnFrom.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnFrom.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnFrom.addTarget(self, action: #selector(didTapVisibleTo), for: .touchUpInside)
        
        let lblFrom = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width / 3 * 2, height: 50))
        lblFrom.textAlignment = .left
        lblFrom.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblFrom.text = "From : "
        btnFrom.addSubview(lblFrom)
        
        tfFrom = UITextField(frame: CGRect(x: view.frame.size.width / 3 * 2 - 50, y: 0, width: view.frame.size.width / 3, height: 50))
        tfFrom.textAlignment = .right
        tfFrom.font = UIFont.init(name: Font.BEBAS, size: 15)
        tfFrom.textColor = sharedObj.hexStringToUIColor(hex: "6E7171")
        tfFrom.placeholder = "Tap to select..."
        
        let startTimeInterval = sharedObj.para_filter.from_date
        if startTimeInterval != 0
        {
            let startDate = Date.init(timeIntervalSince1970: TimeInterval(startTimeInterval))
            tfFrom.text = dateFormatter.string(from: startDate)
        }
        
        let fromPicker = UIDatePicker()
        fromPicker.tag = 1
        fromPicker.backgroundColor = UIColor.white
        fromPicker.datePickerMode = .date
        fromPicker.addTarget(self, action: #selector(didChangeDate(sender:)), for: .valueChanged)
        tfFrom.inputAccessoryView = addToolBar()
        tfFrom.inputView = fromPicker
        btnFrom.addSubview(tfFrom)
        
        headerView.addSubview(btnFrom)
        let btnTo = UIButton.init(frame: CGRect(x: 0, y: btnFrom.frame.origin.y + btnFrom.frame.size.height, width: view.frame.size.width, height: 50))
        btnTo.backgroundColor = UIColor.white
        btnTo.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnTo.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnTo.addTarget(self, action: #selector(didTapVisibleTo), for: .touchUpInside)
        
        let lblTo = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width / 3 * 2, height: 50))
        lblTo.textAlignment = .left
        lblTo.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblTo.text = "To : "
        btnTo.addSubview(lblTo)
        
        tfTo = UITextField(frame: CGRect(x: view.frame.size.width / 3 * 2 - 50, y: 0, width: view.frame.size.width / 3, height: 50))
        tfTo.textAlignment = .right
        tfTo.font = UIFont.init(name: Font.BEBAS, size: 15)
        tfTo.textColor = sharedObj.hexStringToUIColor(hex: "6E7171")
        tfTo.placeholder = "Tap to select..."
        let endTimeInterval = sharedObj.para_filter.to_date
        if endTimeInterval != 0
        {
            let endDate = Date.init(timeIntervalSince1970: TimeInterval(endTimeInterval))
            tfTo.text = dateFormatter.string(from: endDate)
        }
        
        let toPicker = UIDatePicker()
        toPicker.tag = 2
        toPicker.backgroundColor = UIColor.white
        toPicker.datePickerMode = .date
        toPicker.addTarget(self, action: #selector(didChangeDate(sender:)), for: .valueChanged)
        tfTo.inputAccessoryView = addToolBar()
        tfTo.inputView = toPicker
        btnTo.addSubview(tfTo)
        
        headerView.addSubview(btnTo)
        return headerView
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
//        if section == 2
//        {
//            if isShowFromTo == true
//            {
//                return 2
//            }
//        }
        if section == 3
        {
            return arrRowName.count
        }
        else if section == 4
        {
            if isShowVisibleTo == true
            {
                return arrVisibleTo.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if  section == 0
        {
            return 60
        }
        else if section == 1
        {
            return 55
        }
        else if section == 2
        {
            return 86
        }
        else if section == 3
        {
            if isShowFromTo == true
            {
                return 100
            }
        }
        else if section == 4
        {
            return 50
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()//.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        headerView.backgroundColor = UIColor.clear
        
        if section == 0
        {
            headerView.addSubview(firstHeaderView(headerView: headerView))
        }
        else if section == 1
        {
            headerView.addSubview(secondHeaderView(headerView: headerView))
        }
        else if section == 2
        {
            headerView.addSubview(thirdHeaderView(headerView: headerView))
        }
        else if section == 3
        {
            // Keep it blank.
            if isShowFromTo == true
            {
                headerView.addSubview(timeHeaderView(headerView: headerView))
            }
        }
        else if section == 4
        {
            headerView.addSubview(fourthHeaderView(headerView: headerView))
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        
        var titleLabel = UILabel()
        var sepView = UIView()
        
        if (cell == nil)
        {
            cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
            cell?.selectionStyle = UITableViewCellSelectionStyle.gray
            
            titleLabel = UILabel(frame: CGRect(x: 30, y: 5, width: view.frame.size.width - 80, height: 40))
            titleLabel.textAlignment = .left
            titleLabel.font = UIFont.init(name: Font.BEBAS, size: 15)
            titleLabel.tag = 1
            
            sepView = UIView.init(frame: CGRect(x: 25, y: titleLabel.frame.size.height + titleLabel.frame.origin.y, width: view.frame.size.width - 50, height: 1))
            sepView.backgroundColor = UIColor.gray
            sepView.tag = 4
            
            cell?.contentView.addSubview(titleLabel)
            cell?.contentView.addSubview(sepView)
        }
        else
        {
            titleLabel = cell?.contentView.viewWithTag(1) as! UILabel
            sepView = (cell?.contentView.viewWithTag(4))!
        }
        
        titleLabel.isHidden = true
        sepView.isHidden = true
        
        if indexPath.section == 3
        {
            titleLabel.isHidden = false
            titleLabel.text = arrRowName[indexPath.row]
            
            cell?.accessoryType = .none
            if sharedObj.para_filter.isFromTimeRows == 1
            {
                if sharedObj.para_filter.timeStr == arrRowName[indexPath.row]
                {
                    cell?.accessoryType = .checkmark
                }
            }
            else
            {
                cell?.accessoryType = .none
            }
        }
        else if indexPath.section == 4
        {
            titleLabel.isHidden = false
            titleLabel.text = arrVisibleTo[indexPath.row]
            if indexPath.row == sharedObj.para_filter.visibleTo
            {
                cell?.accessoryType = .checkmark
            }
            else
            {
                cell?.accessoryType = .none
            }
        }

        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 3
        {
            isShowFromTo = false
            sharedObj.para_filter.isFromTimeRows = 1
            sharedObj.para_filter.timeStr = arrRowName[indexPath.row]
            
            if indexPath.row == 0
            {
                sharedObj.para_filter.from_date = Date().startOfDayEpoch
                sharedObj.para_filter.to_date = Date().endOfDayEpoch
            }
            else if indexPath.row == 1
            {
                let tomorrowDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
                sharedObj.para_filter.from_date = tomorrowDate.startOfDayEpoch
                sharedObj.para_filter.to_date = tomorrowDate.endOfDayEpoch
            }
            else if indexPath.row == 2
            {
                let today = Date()
                let calendar = Calendar.current
                
                let todayWeekday = calendar.component(.weekday, from: today)
                
                let addWeekdays = 7 - todayWeekday  // 7: Saturday number
                var components = DateComponents()
                components.weekday = addWeekdays
                
                let nextSaturday = calendar.date(byAdding: components, to: today)
                sharedObj.para_filter.from_date = (nextSaturday?.startOfDayEpoch)!
                
                let nextSunday = Calendar.current.date(byAdding: .day, value: 1, to: nextSaturday!)!
                sharedObj.para_filter.to_date = nextSunday.endOfDayEpoch
            }
        }
        else if indexPath.section == 4
        {
            sharedObj.para_filter.visibleTo = indexPath.row
        }
        
        tableView.reloadData()
    }
    
    func addToolBar() -> UIToolbar
    {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = UIBarStyle.default
        toolBar.items = [
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(didTapDoneKeypad)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)]
        toolBar.sizeToFit()
        
        return toolBar
    }

    func didTapDoneKeypad()
    {
        self.view.endEditing(true)
        tableView.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let catListView : Categories_list = segue.destination as! Categories_list
        catListView.isMultiple = true
    }
}

extension FilterActivity_VC : UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if  textField.tag == 100
        {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
//        if textField.tag == 100
//        {
//            activityPara.actName = textField.text!
//        }
//        else if textField.tag == 200
//        {
//            activityPara.actDesc = textField.text!
//        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    
}

extension FilterActivity_VC : GMSAutocompleteViewControllerDelegate
{
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        sharedObj.para_filter.latitude = place.coordinate.latitude
        sharedObj.para_filter.longitude = place.coordinate.longitude
        sharedObj.para_filter.locationStr = place.name
        
        dismiss(animated: true, completion: nil)
        tableView.reloadData()
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
    {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension FilterActivity_VC : UISearchBarDelegate
{
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        print("search text entered")
        sharedObj.para_filter.search_keyword = searchBar.text!
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText == ""
        {
            sharedObj.para_filter.search_keyword = ""
        }
    }
}
