//
//  ProfilePictureCollectionViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 05/11/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class ProfilePictureCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var butnStar: UIButton!
    @IBOutlet weak var butnDelete: UIButton!
    
    
}
