//
//  UserProfile_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 20/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage
import GoogleMobileAds

class UserProfile_VC: UIViewController
{
    var newBase64Str = String()
    let sharedObject = Singleton.shared
    
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblGenderInitial: UILabel!
    @IBOutlet weak var imgGender: UIImageView!
    @IBOutlet weak var lblRelationship: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblHeartWings: UIButton!
    
    @IBOutlet weak var btnAddFriend: UIButton!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var adBannerView: GADBannerView!
    @IBOutlet weak var collectionViewFriends: UICollectionView!
    
    var arrRelationStatus = ["--", "In a Relationship" , "Single", "Dating"]
    var arrGender = ["--", "M", "F", "T"]
    var user_Id = Int()
    var userJSON = [String : Any]()
    var arrMutualFrnds = [[String : Any]]()
   // @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        title = "User Profile"
        addLeftBarButton()
        
        retrieveUserProfile()
        retrieveMutualFriendsList()
        
        //tableView.estimatedRowHeight = view.frame.size.height - 50
        
        DispatchQueue.global(qos: .background).async
            {
                print("This is run on the background queue")
                if self.sharedObject.userPara.isPlusMember == 1
                {
                    // Hide own profile visits
                }
                else
                {
                    self.addToVisitorList()
                }
            }
        
        adBannerView.adUnitID = AdMob_BannerID
        
        if sharedObject.userPara.isPlusMember == 1
        {
            print("No ads to plus user")
            adBannerView.rootViewController = nil
        }
        else
        {
            adBannerView.rootViewController = self
        }
        
        adBannerView.load(GADRequest())
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func addLeftBarButton()
    {
        let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .plain,  target: self, action: #selector(didTapBackButn))
        navigationItem.leftBarButtonItem = btnMenu
    }
    
    func assignValueToLabel()
    {
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: self.imgViewUser.frame.size.width, height: self.imgViewUser.frame.size.width), radius: self.imgViewUser.frame.size.width/2)
        self.imgViewUser.af_setImage(withURL:  URL.init(string: APP_URL.BASE_URL + (userJSON["thumb"] as! String))!, placeholderImage: #imageLiteral(resourceName: "avatar_big"), filter: filter, imageTransition: .crossDissolve(0.2), completion: nil)
        
        if userJSON["friend"] as! Int == 1
        {
            self.btnAddFriend.backgroundColor = UIColor.lightGray
            self.btnAddFriend.setTitle("UNFRIEND", for: .normal)
        }
        
        self.lblUserName.text = userJSON["fname"] as? String
        
        let age = userJSON["age"] as! Int
        self.lblAge.text = age == 0 ? "-- years" : "\(age)  years"
        self.lblAddress.text = userJSON["location_string"] as? String
        
        self.lblDescription.text = userJSON["about"] as? String
        
        self.lblRelationship.text = arrRelationStatus[(userJSON["relationship_status"] as? Int)!] 

        self.lblGenderInitial.text = arrGender[(userJSON["gender"] as? Int)!]

        if userJSON["suscribed"] as! Int == 1
        {
            self.lblHeartWings.isHidden = false
        }
        else
        {
            self.lblHeartWings.isHidden = true
        }
    }
    
    // MARK: - View Delegates
    func didTapBackButn()
    {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapMessageButn(_ sender: Any)
    {
//        let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
//        chatDialog.occupantIDs = [NSNumber(userJSON["qb_id"] as! Int)]
        
        let qbId = userJSON["qb_id"] as! String
        
        ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: UInt(qbId)!, completion: { (response, chatDialog) in
            
            let chatVC = storyboards.qbStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            
            chatVC.dialog = chatDialog
            self.navigationController?.pushViewController(chatVC, animated: true)
        })
    }
    
    @IBAction func didTapAddFriend(_ sender: Any)
    {
        if userJSON["friend"] as! Int == 1
        {
            self.callUnFriendAPI()
        }
        else
        {
            //self.btnAddFriend.isUserInteractionEnabled = false
            //self.btnAddFriend.backgroundColor = UIColor.lightGray
            self.callSendFriendRequestAPI()
        }
    }
    
    @IBAction func didTapUpcomingActivity(_ sender: Any)
    {
        let storyboard = UIStoryboard.init(name: "CommonStoryboard", bundle: nil)
        let userActVC = storyboard.instantiateViewController(withIdentifier: "UserActivitiesViewController") as! UserActivitiesViewController
        
        userActVC.friend_Id = user_Id
        userActVC.isUpcoming = true
        userActVC.userJSON = userJSON
        self.navigationController?.pushViewController(userActVC, animated: true)
    }
    
    @IBAction func didTapPastActivity(_ sender: Any)
    {
        let storyboard = UIStoryboard.init(name: "CommonStoryboard", bundle: nil)
        let userActVC = storyboard.instantiateViewController(withIdentifier: "UserActivitiesViewController") as! UserActivitiesViewController
        
        userActVC.friend_Id = user_Id
        userActVC.isUpcoming = false
        userActVC.userJSON = userJSON

        self.navigationController?.pushViewController(userActVC, animated: true)
    }
    
    @IBAction func didTapBlockUser(_ sender: Any)
    {
        callBlockUserAPI()
    }
    
    @IBAction func didTapReportUser(_ sender: Any)
    {
        let reportVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "ReportActivity_VC") as! ReportActivity_VC
        reportVC.isUser = true
        reportVC.userIdToBlock = user_Id
        
        self.navigationController?.pushViewController(reportVC, animated: true)
    }
    
    @IBAction func ddiTapUserPhoto(_ sender: Any)
    {
        if let imageArray = userJSON["profile_ulbum"] as? [[String : Any]]
        {
            if imageArray.count == 0
            {
                return
            }
            else
            {
                let addPhotoVC = storyboards.profilePicStoryboard.instantiateViewController(withIdentifier: "AddProfilePicturesViewController") as! AddProfilePicturesViewController
                addPhotoVC.isOthersProfile = true
                
                if imageArray.count != 0
                {
                    var index = 0
                    for json in imageArray
                    {
                        let para = Para_ProfilePictures()
                        para.isProfilePicture = json["isprofilepicture"] as! Int
                        para.picCaption = json["text"] as! String
                        para.picMode = 3 // Nothing
                        para.picId = json["id"] as! Int
                        para.picUrl = json["picture"] as! String
                        
                        if para.isProfilePicture != 0
                        {
                            addPhotoVC.selectedProfileImage = index
                        }
                        addPhotoVC.imagesArray.append(para)
                        index = index + 1
                    }
                }
                
                self.navigationController?.pushViewController(addPhotoVC, animated: true)
            }
        }
    }
    
    // MARK: - WEB API
    func addToVisitorList()
    {
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["visited_to" : "\(user_Id)"]
        
        GlobalConstants().postData_Header_Parameter(subUrl: VISITOR_URL.ADD_VISITOR_LIST, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                                
                if isSuccess
                {
                    
                }
                else
                {
                    
                }
        })
    }

    func callBlockUserAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["user_id" : user_Id]
        
        GlobalConstants().postData_Header_Parameter(subUrl: USER_URL.BLOCK_USER, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            if isSuccess
            {
                let alert = UIAlertController.init(title: "", message: responseMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
                    alert -> Void in
                    
                    self.navigationController?.popViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func retrieveUserProfile()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        // to fetch users
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: "v1/retrieve_user_profile?id=\(user_Id)", header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["user"] != nil
                    {
                        print(responseData["user"] as Any)
                        self.userJSON = responseData["user"] as! [String : Any]
                        self.assignValueToLabel()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }

    func retrieveMutualFriendsList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        let requestObject : [String : Any] = ["friend_user_id" : user_Id,
                                              "lat" : sharedObject.userPara.userLocationLat,
                                              "lng" : sharedObject.userPara.userLocationLong] as [String : Any]

        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.GET_MUTUAL_FRND_LIST, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            if isSuccess
            {
                if responseData["mutual_friend_list"] != nil
                {
                    print(responseData["mutual_friend_list"] as Any)
                    self.arrMutualFrnds = responseData["mutual_friend_list"] as! [[String : Any]]
                    self.collectionViewFriends.reloadData()
                }
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func callSendFriendRequestAPI()
    {
        let headerObject = ["authtoken" : sharedObject.userPara.userToken]
        let requestObject = ["request_to_id" : user_Id]
        
        GlobalConstants().postData_Header_Parameter(subUrl: FRIENDS_URL.SEND_REQ, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.btnAddFriend.backgroundColor = UIColor.lightGray
                self.btnAddFriend.setTitle("UNFRIEND", for: .normal)
                self.showAlert(title: "Success!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func callUnFriendAPI()
    {
        let headerObject = ["authtoken" : sharedObject.userPara.userToken]
        let requestObject = ["friend_id" : user_Id]
        
        GlobalConstants().postData_Header_Parameter(subUrl: "v1/remove_friend", header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: responseMessage)
                self.btnAddFriend.setTitle("ADD FRIEND", for: .normal)
                self.btnAddFriend.backgroundColor = #colorLiteral(red: 0.9701228738, green: 0.7434104085, blue: 0.332818985, alpha: 1)
                self.userJSON["friend"] = 0
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
}

extension UserProfile_VC : UICollectionViewDataSource, UICollectionViewDelegate
{
    // MARK: -  ---------------------- UICollectionView Delegate --------------- -
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if self.arrMutualFrnds.count == 0
        {
            let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.collectionViewFriends.bounds.size.width, height: self.collectionViewFriends.bounds.size.height))
            emptyLabel.font = UIFont.init(name: Font.BEBAS, size: 13)
            emptyLabel.text = "No Same Friends Found"
            emptyLabel.textColor = sharedObject.hexStringToUIColor(hex: "6E7171")
            emptyLabel.textAlignment = NSTextAlignment.center
            self.collectionViewFriends.backgroundView = emptyLabel
            return 0
        }
        else
        {
            self.collectionViewFriends.backgroundView?.isHidden = true
            return arrMutualFrnds.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let item: UserFriendsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserFriendsCollectionViewCell", for: indexPath) as! UserFriendsCollectionViewCell
        
        let userJSON : [String : Any] = arrMutualFrnds[indexPath.row]
        
        let strUrl = userJSON["thumb"] as! String
        let url = URL(string: APP_URL.BASE_URL + strUrl )!
        
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: self.imgViewUser.frame.size.width, height: self.imgViewUser.frame.size.width), radius: self.imgViewUser.frame.size.width/2)
        self.imgViewUser.af_setImage(withURL:  url, placeholderImage: #imageLiteral(resourceName: "avatar_big"), filter: filter, imageTransition: .crossDissolve(0.2), completion: nil)
        
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfile_VC") as! UserProfile_VC
        let userJSON = arrMutualFrnds[indexPath.row] 
        presentVC.user_Id = userJSON["id"] as! Int
        // Will allow to go back
        self.navigationController?.pushViewController(presentVC, animated: true)
    }
}
