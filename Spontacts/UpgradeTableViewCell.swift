//
//  UpgradeTableViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 02/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class UpgradeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnFreeVersion: UIButton!
    @IBOutlet weak var btnPlusVersion: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
