//
//  Categories_list.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 04/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class Categories_list: UIViewController
{
    
    @IBOutlet weak var tableView: UITableView!
    let sharedObj = Singleton.shared
    
    var isMultiple = Bool()
    var isChoosingInterests = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Categories"
        tableView.dataSource = self
        tableView.delegate = self
        
        let backBtn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back_pink"), style: .plain, target: self, action: #selector(self.didTapBackButn))
        navigationItem.leftBarButtonItem = backBtn
    }

    func didTapBackButn()
    {
        if isChoosingInterests == true
        {
            callAddInterestsAPI()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func callAddInterestsAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject = [ "category_ids" : self.sharedObj.para_filter.categories_array]
        
        GlobalConstants().postData_Header_Parameter(subUrl: INTEREST_URL.ADD_INTERESTS, header: headerObject, parameter: requestObject, completionHandler:{
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            if isSuccess
            {
                if responseData["interest"] != nil
                {
                    let array = responseData["interest"] as! NSArray
                    UserDefaults.standard.setValue(array, forKey: "interest")
                    UserDefaults.standard.synchronize()
                    
                    let dict : [String : Any] = ["array" : array]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Interest_Chosen"), object: dict)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        } )
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Categories_list : UITableViewDataSource, UITableViewDelegate
{
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sharedObj.arrCat.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        
        var titleLabel = UILabel()
        
        if (cell == nil)
        {
            cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
            cell?.selectionStyle = UITableViewCellSelectionStyle.gray
            
            titleLabel = UILabel(frame: CGRect(x: 30, y: 5, width: view.frame.size.width - 80, height: 40))
            titleLabel.textAlignment = .left
            titleLabel.font = UIFont.init(name: Font.BEBAS, size: 16)
            titleLabel.tag = 1
            
            cell?.contentView.addSubview(titleLabel)
        }
        else
        {
            titleLabel = cell?.contentView.viewWithTag(1) as! UILabel
        }
        
        titleLabel.text = sharedObj.arrCat[indexPath.row]
        
        if self.sharedObj.para_filter.categories_array.contains(indexPath.row)
        {
            cell?.accessoryType = .checkmark
        }
        else
        {
            cell?.accessoryType = .none
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //        sharedObj.sharedCreateActPara.actCategoryId = indexPath.row
        //        sharedObj.sharedCreateActPara.actCategoryStr = arrCat[indexPath.row]
        
        if isMultiple == false
        {
            if indexPath.row == 8
            {
                let showAlert = UIAlertController.init(title: "Does your category not match with given list?", message: "Do you want to add your own image for this activity?", preferredStyle: .alert)
                showAlert.addAction(UIAlertAction.init(title: "Yeah, let's add", style: .default, handler: { Void in
                    
                    self.showGalleryView()
                    }))
                
                showAlert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { Void in
                    
                    let notificationName = Notification.Name("categoryForActivityChosen")
                    
                    let obj = ["catId" : indexPath.row, "catStr" : self.sharedObj.arrCat[indexPath.row]] as [String : Any]
                    // Post notification
                    NotificationCenter.default.post(name: notificationName, object: obj)
                    
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(showAlert, animated: true, completion: nil)
            }
            else
            {
                let notificationName = Notification.Name("categoryForActivityChosen")
                
                let obj = ["catId" : indexPath.row, "catStr" : sharedObj.arrCat[indexPath.row]] as [String : Any]
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: obj)
                
                self.navigationController?.popViewController(animated: true)
            }
        }
        else
        {
            if self.sharedObj.para_filter.categories_array.contains(indexPath.row)
            {
                let index = self.sharedObj.para_filter.categories_array.index(of: indexPath.row)
                self.sharedObj.para_filter.categories_array.remove(at: index!)
            }
            else
            {
                self.sharedObj.para_filter.categories_array.append(indexPath.row)
            }
            
            tableView.reloadData()
        }
 
    }

    func showGalleryView()
    {
        let alert = UIAlertController.init(title: "Choose from", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: {
            alert -> Void in
            
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: {
            alert -> Void in
            
            
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension Categories_list : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        print(" ~~~~~~~~~~~~~~~~~~~~~~~~~ Image selected ~~~~~~~~~~~~~~~~~~~~~~~~")
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return // No image selected.
        }
        
        //Now use image to create into NSData format
        let imageData = image.lowestQualityJPEGNSData
        
        // let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
        let base64String = imageData.base64EncodedString(options: .lineLength64Characters)
        self.sharedObj.catImgBase64 = base64String

        self.dismiss(animated: true, completion:
            { () -> Void in
                let notificationName = Notification.Name("categoryForActivityChosen")
                
                let obj = ["catId" : 8, "catStr" : self.sharedObj.arrCat[8]] as [String : Any]
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: obj)
                
                self.navigationController?.popViewController(animated: true)
        })
        
    }
}
