//
//  EditProfileViewController.swift
//  Spontacts
//
//  Created by maximess142 on 12/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import YangMingShan
import GooglePlaces
import AlamofireImage

class EditProfileViewController: UIViewController {

    var arrRelationStatus = ["--", "In a Relationship" , "Single", "Dating"]
    var arrLocation : [[String : Any]] = [[String : Any]]()
    
    var isShowStatusRows = Bool()
    let sharedObj = Singleton.shared
    //var selectedStatusDict : [String : Any] = ["id" : 0, "value" : "--"] as [String : Any]
    //var selectedLocationIndexpath = IndexPath()
    var userImageBase64 = String()
    var isImageChanged = false
    var detailsPlaceholderText = "Write something about yourself!"
    
    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnTransgender: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var arrGenderImg = [#imageLiteral(resourceName: "male"), #imageLiteral(resourceName: "female"), #imageLiteral(resourceName: "transgender")]
    var arrGenderIntial = ["M", "F", "T"]
    var profAlbumArray = [[String : Any]]()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "Edit Profile"
        addLeftBarButton()
        //addRightBarButton()
        assignValuesToLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if UserDefaults.standard.value(forKey: "userJSON") != nil
        {
            let userJSON = UserDefaults.standard.value(forKey: "userJSON") as! [String : Any]
            profAlbumArray = userJSON["profile_ulbum"] as! [[String:Any]]
            self.collectionView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftBarButton()
    {
        let backBtn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "check_white"), style: .plain, target: self, action: #selector(self.didTapBackButn))
        navigationItem.leftBarButtonItem = backBtn
    }
    
    func addRightBarButton()
    {
        let backBtn = UIBarButtonItem.init(title: "Save", style: .plain, target: self, action: #selector(self.didTapSaveButn))
        navigationItem.rightBarButtonItem = backBtn
    }
    
    func assignValuesToLabel()
    {
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: 80.0, height: 80.0), radius: 40)
//        imgViewProfile.af_setImage(withURL:  URL.init(string: APP_URL.BASE_URL + sharedObj.userPara.userPictureThumbUrl)!, placeholderImage: #imageLiteral(resourceName: "avatar_big"), filter: filter, imageTransition: .crossDissolve(0.2), completion:
//            { response in
//
//        })
        
        self.txtName.text = sharedObj.userPara.userFirstName + " " + sharedObj.userPara.userLastName
        
        if sharedObj.userPara.userDescription != ""
        {
            self.txtDescription.text = sharedObj.userPara.userDescription
        }
        else
        {
            self.txtDescription.text = detailsPlaceholderText
        }
        
        if sharedObj.userPara.userAge != 0
        {
            self.txtAge.text = "\(sharedObj.userPara.userAge)"
        }
        
        if sharedObj.userPara.userGender != 0
        {
            self.btnMale.isSelected = false
            self.btnFemale.isSelected = false
            self.btnTransgender.isSelected = false
            
            if sharedObj.userPara.userGender == 1
            {
                self.btnMale.isSelected = true
            }
            else if sharedObj.userPara.userGender == 2
            {
                self.btnFemale.isSelected = true
            }
            else if sharedObj.userPara.userGender == 3
            {
                self.btnTransgender.isSelected = true
            }
        }
    }
    
    func dismissKeyboard()
    {
        if let recognizers = self.view.gestureRecognizers
        {
            for recognizer in recognizers
            {
                self.view.removeGestureRecognizer(recognizer)
            }
        }
        self.view.endEditing(true)
    }

    // MARK: - Action Methods
    func didTapSaveButn()
    {
        callUpdateProfileAPI()
    }
    
    @IBAction func didTapBackButn()
    {
        self.view.endEditing(true)

//            let alert = UIAlertController.init(title: "Are you sure you want to lose your changes?", message: nil, preferredStyle: .alert)
//            alert.addAction(UIAlertAction.init(title: "Discard", style: .default, handler: {
//                alert -> Void in
//                self.navigationController?.popViewController(animated: true)
//            }))
//            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
//                alert -> Void in
//            }))
//            self.present(alert, animated: true, completion: nil)

        didTapSaveButn()
    }

    @IBAction func didTapDeletePhoto(_ sender: Any)
    {
        let alert = UIAlertController.init(title: "Do you really want to delete your profile picture?", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Delete", style: .destructive, handler: {
            alert -> Void in
            
            self.isImageChanged = true
//            self.imgViewProfile.image = #imageLiteral(resourceName: "avatar")
            self.sharedObj.userPara.userPictureBase64 = ""

        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .default
            , handler: {
            alert -> Void in

        }))
        
        //alert.view.tintColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didTapGenderButns(_ sender: UIButton)
    {
        self.btnMale.isSelected = false
        self.btnFemale.isSelected = false
        self.btnTransgender.isSelected = false
        
        if sender == btnMale
        {
            self.btnMale.isSelected = true
        }
        else if sender == btnFemale
        {
            self.btnFemale.isSelected = true
        }
        else if sender == btnTransgender
        {
            self.btnTransgender.isSelected = true
        }
    }
    
    @IBAction func didTapAddPhoto(_ sender: Any)
    {
        let pickerViewController = YMSPhotoPickerViewController.init()
        //        pickerViewController.numberOfPhotoToSelect = 2
        let customColor = UIColor.init(red: 64.0/255.0, green: 0.0, blue: 144.0/255.0, alpha: 1.0)
        let customCameraColor = UIColor.init(red: 86.0/255.0, green: 1.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customCameraColor
        pickerViewController.theme.cameraVeilColor = customCameraColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .lightContent
        pickerViewController.shouldReturnImageForSingleSelection = true
        
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    func didTapStatusDownButn()
    {
        isShowStatusRows = !isShowStatusRows
        tableView.reloadData()
    }
    
    @IBAction func didTapAddLocation()
    {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func showProfilePicturesView()
    {
        let quotesVC = storyboards.profilePicStoryboard.instantiateViewController(withIdentifier: "AddProfilePicturesViewController") as! AddProfilePicturesViewController
        
        self.navigationController?.pushViewController(quotesVC, animated: true)
    }
    
    // MARK: - API Methods
    func callUpdateProfileAPI()
    {
        print("Posting the result")
        
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Updating")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        var requestObject : [String : Any] = [String : Any]()
        var subUrl = ""
        
        let strAbout = txtDescription.text == detailsPlaceholderText ? "" : txtDescription.text
        if isImageChanged == true
        {
            requestObject = ["fname" : txtName.text!,
                             "lname" : "",
                             "email" : sharedObj.userPara.userEmail,
                             "lat" : sharedObj.userPara.userLocationLat,
                             "lng" : sharedObj.userPara.userLocationLong,
                             "image_base_64" : userImageBase64,
                             "location_string" : sharedObj.userPara.userLocationStr,
                             "gender" : getSelectedGender(),
                             "age" : Int(txtAge.text!) ?? 0,
                             "relationship_status" : sharedObj.userPara.userRelationStatus,
                             "about" : strAbout!,
                             "locations": getDefaultLocation()
                ] as [String : Any]
            
            subUrl = APP_URL.UPDATE_USER_WITH_IMAGE
        }
        else
        {
            requestObject = ["fname" : txtName.text!,
                             "lname" : "",
                             "email" : sharedObj.userPara.userEmail,
                             "lat" : sharedObj.userPara.userLocationLat,
                             "lng" : sharedObj.userPara.userLocationLong,
                             "location_string" : sharedObj.userPara.userLocationStr,
                             "gender" : getSelectedGender(),
                             "age" : Int(txtAge.text!) ?? 0,
                             "relationship_status" : sharedObj.userPara.userRelationStatus,
                             "about" : strAbout!,
                             "locations": getDefaultLocation()
                ] as [String : Any]
            subUrl = APP_URL.UPDATE_USER
        }
        
        
        GlobalConstants().postData_Header_Parameter(subUrl: subUrl, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                print(responseData)
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["user"] != nil
                    {
                        let userJSON = responseData["user"] as! [String : Any]
                        
                        UserDefaults.standard.set(userJSON, forKey: "userJSON")
                        UserDefaults.standard.synchronize()
                        
                        self.sharedObj.userPara.userId = userJSON["id"] as! Int
                        self.sharedObj.userPara.userEmail = userJSON["email"] as! String
                        self.sharedObj.userPara.userLocationLat = userJSON["lat"] as! Double
                        self.sharedObj.userPara.userFirstName = userJSON["fname"] as! String
                        self.sharedObj.userPara.userLastName = userJSON["lname"] as! String
                        self.sharedObj.userPara.userToken = userJSON["token"] as! String
                        self.sharedObj.userPara.userLocationLong = userJSON["lng"] as! Double
                        //self.sharedObj.userPara.userPassword = userJSON["password"] as! String
                        self.sharedObj.userPara.userPictureThumbUrl = userJSON["thumb"] as! String
                        self.sharedObj.userPara.userPictureUrl = userJSON["picture"] as! String
                        self.sharedObj.userPara.userLocationStr = userJSON["location_string"] as! String
                        self.sharedObj.userPara.userAge = userJSON["age"] as! Int
                        self.sharedObj.userPara.userGender = userJSON["gender"] as! Int
                        self.sharedObj.userPara.userRelationStatus = userJSON["relationship_status"] as! Int
                        self.sharedObj.userPara.userDescription = userJSON["about"] as! String
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func getSelectedGender() -> Int
    {
        sharedObj.userPara.userGender = 0
        
        if btnMale.isSelected == true
        {
            sharedObj.userPara.userGender = 1
        }
        else if btnFemale.isSelected == true
        {
            sharedObj.userPara.userGender = 2
        }
        else if btnTransgender.isSelected == true
        {
            sharedObj.userPara.userGender = 3
        }
        
        return sharedObj.userPara.userGender
    }
    
    func getDefaultLocation() -> [[String:Any]]
    {
        if arrLocation.count > 0
        {
            for index in 0 ... arrLocation.count - 1
            {
                let item = arrLocation[index]
                
                if item["lat"] as? Double == sharedObj.userPara.userLocationLat && item["lng"] as? Double == sharedObj.userPara.userLocationLong
                {
                    arrLocation[index]["default"] = 1
                }
                else
                {
                    arrLocation[index]["default"] = 0
                }
            }
        }
        
        return arrLocation
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - TableView Delegates
extension EditProfileViewController : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 35
        }
        else if section == 1
        {
            return 48
        }
        else
        {
            return 35
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            if isShowStatusRows == true
            {
                return arrRelationStatus.count
            }
            else
            {
                return 0
            }
        }
        else if section == 1
        {
            return arrLocation.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 25
        }
        else if indexPath.section == 1
        {
            return 35
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0
        {
            let cell : EditProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StatusHeader") as! EditProfileTableViewCell
            
            cell.lblRelationshipSelected.text = arrRelationStatus[sharedObj.userPara.userRelationStatus]
            
            let catButn = UIButton.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: cell.frame.size.height))
            catButn.backgroundColor = UIColor.clear
            catButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
            catButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
            catButn.addTarget(self, action: #selector(self.didTapStatusDownButn), for: .touchUpInside)
            cell.addSubview(catButn)

            return cell
        }
        else if section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LocationHeader")
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddLocation")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell : EditProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StatusCell") as! EditProfileTableViewCell
            
            cell.lblRelationshipOption.text = arrRelationStatus[indexPath.row]
            if arrRelationStatus[indexPath.row] == arrRelationStatus[sharedObj.userPara.userRelationStatus]
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
            
            return cell
        }
        else //if indexPath.section == 1
        {
            let cell : EditProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LocationCell") as! EditProfileTableViewCell
            
            cell.lblLocation.text = arrLocation[indexPath.row]["location_string"] as? String
            if arrLocation[indexPath.row]["lat"] as? Double == sharedObj.userPara.userLocationLat && arrLocation[indexPath.row]["lng"] as? Double == sharedObj.userPara.userLocationLong
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0
        {
            sharedObj.userPara.userRelationStatus = indexPath.row
        }
        else if indexPath.section == 1
        {
            sharedObj.userPara.userLocationLong = arrLocation[indexPath.row]["lng"] as! Double
            sharedObj.userPara.userLocationLat = arrLocation[indexPath.row]["lat"] as! Double
            sharedObj.userPara.userLocationStr = arrLocation[indexPath.row]["location_string"] as! String

        }
        else
        {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        }
        
        tableView.reloadData()
    }
}

// MARK: - Photo Picker Delegates
extension EditProfileViewController : YMSPhotoPickerViewControllerDelegate
{
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
            }
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
            }
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!)
    {
        picker.dismiss(animated: true, completion:
            {
                Void in
                
                self.isImageChanged = true
//                self.imgViewProfile.image = image
                
                //Now use image to create into NSData format
                let imageData = image?.mediumQualityJPEGNSData
                
                let base64String = imageData?.base64EncodedString(options: .lineLength64Characters)
                self.userImageBase64 = base64String!
        })
    }
}

// MARK: - TextField Textview Delegates
extension EditProfileViewController : UITextFieldDelegate, UITextViewDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        if textView.text == detailsPlaceholderText
        {
            txtDescription.text = ""
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
}

extension EditProfileViewController : GMSAutocompleteViewControllerDelegate
{
    // MARK: - GMSAutocompleteViewController Delegate
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        let locationDict = ["lat": place.coordinate.latitude,
                            "lng": place.coordinate.longitude,
                            "location_string": place.name,
                            "default": 0] as [String : Any]
        arrLocation.append(locationDict)
        dismiss(animated: true, completion: {
            self.tableView.reloadData()
        })
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
    {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

// MARK: - Collection View Datasource and Delegates
extension EditProfileViewController : UICollectionViewDelegate , UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return profAlbumArray.count
    }
    
    /*
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        flowLayout.minimumInteritemSpacing = 20.0
        flowLayout.minimumLineSpacing = 20.0
        
        let numberOfItems = CGFloat(collectionView.numberOfItems(inSection: section))
        let combinedItemWidth = (numberOfItems * flowLayout.itemSize.width) + ((numberOfItems - 1)  * flowLayout.minimumInteritemSpacing)
        let padding = (collectionView.frame.width - combinedItemWidth) / 2
        
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }*/

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let mycell : EditProfileCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditProfileCollectionViewCell", for: indexPath) as! EditProfileCollectionViewCell
        
        let picJSON = profAlbumArray[indexPath.row]
        let picUrl = picJSON["picture"] as! String
        
        if picUrl != ""
        {
            let url = URL(string: APP_URL.BASE_URL +  picUrl)!
            let placeholderImage = #imageLiteral(resourceName: "avatar")
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: 100.0, height: 100.0), radius: 50.0)
            
            mycell.imageView.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                { response in
                    
            }
            )
        }
        
        return mycell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let addPhotoVC = storyboards.profilePicStoryboard.instantiateViewController(withIdentifier: "AddProfilePicturesViewController") as! AddProfilePicturesViewController

        if profAlbumArray.count != 0
        {
            var index = 0
            for json in profAlbumArray
            {
                let para = Para_ProfilePictures()
                para.isProfilePicture = json["isprofilepicture"] as! Int
                para.picCaption = json["text"] as! String
                para.picMode = 3 // Update
                para.picId = json["id"] as! Int
                para.picUrl = json["picture"] as! String
                
                if para.isProfilePicture != 0
                {
                    addPhotoVC.selectedProfileImage = index
                }
                addPhotoVC.imagesArray.append(para)
                index = index + 1
            }
        }
        
        addPhotoVC.isFromEditDetailsScreen = true
        self.navigationController?.pushViewController(addPhotoVC, animated: true)
    }
}
