//
//  GlobalConstants.swift
//  Spontacts
//
//  Created by MAXIMESS114 on 28/05/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import FirebaseInstanceID

var QB_ID_ARRAY = [NSNumber]()

// AdMob
let AdMob_BannerID = "ca-app-pub-1230813742493123/5726732650"
//Appcode - "ca-app-pub-8501671653071605/1974659335"
//My Banner Id - "ca-app-pub-6631853374417128/4154657966"

// Quickblox
let kQuickbloxCommonPassword = "password"
var IS_QB_LOGGEDIN = false

struct APP_URL
{
    static let BASE_URL = "http://gay-app.com/api/" // Production
    //static let BASE_URL = "http://acencore.com/spontact/api/api/" // Developement

    static let REGISTER_USER_SUBURL = "v1/register_user"
    
    static let APP_LOGIN_SUBURL = "v1/api_login"
    
    static let RESET_PASSWORD = "v1/send_forgot_password_mail"
    
    static let SOCIAL_LOGIN_FACEBOOK = "v1/social_login_fb"
    
    static let REGISTER_WITH_FACEBOOK = "v1/social_registration_fb"
    
    static let SOCIAL_LOGIN_GOOGLE = "v1/social_login_gl"
    
    static let SOCIAL_REGISTER_TWITTER = "v1/social_registration_tweeter"
    
    static let SOCIAL_LOGIN_TWITTER = "v1/social_login_tweeter"
    
    static let UPDATE_USER = "v1/update_user"
    
    static let UPDATE_USER_WITH_IMAGE = "v1/update_user_with_image"
    
    static let UPDATE_USER_LOCATION = "v1/update_user_location_and_make_default"
    
    static let CHANGE_PASSWORD = "v1/change_password"
    
    static let GET_SELF_PROFILE = "v1/retrieve_self_profile"
}

struct ACTIVITY_URL
{
    static let CREATE_ACT = "v1/add_activity"
    
    static let INVITE_FRNDS_PRIVATE_ACT = "v1/invite_friends_for_private_activities"
    
    static let ADD_ACTIVITY_IMG = "v1/add_activity_image"
    
    static let ADD_ACT_IMG_MULTIPART = "v1/add_activity_image_multipart"
    
    static let DELETE_ACTIVITY_IMG = "v1/delete_activity_image"
    
    static let GET_ALL_ACT_LIST = "v1/get_activity_list"
    
    static let GET_MYACT_UPCOMING = "v1/get_my_activity_list?type=2"
    
    static let GET_MYACT_PAST = "v1/get_my_activity_list?type=1"
    
    static let CANCEL_ACTIVITY = "v1/cancel_activity"
    
    static let UPDATE_ACTIVITY = "v1/update_activity"
    
    static let ACT_INTERESTED = "v1/interested_in_activity"
    
    static let ACT_NOT_INTERESTED = "v1/not_interested_in_activity"
    
    static let ACT_GOING = "v1/goint_to_activity"
    
    static let ACT_NOT_GOING = "v1/not_goint_to_activity"
    
    static let ADD_ACT_COMMENT = "v1/add_activity_comment"
    
    static let GET_ACTIVITY_COMMENTS = "v1/get_activity_comments"
    
    static let GET_ACT_GOING_INTERESTED_LIST = "v1/get_activity_going_and_interested_list"
    
    static let GET_ACT_GOING_INTERESTED_COMMENTS_LIST = "v1/get_activity_going_interested_comments_list"
    
    static let ADD_CATEGORY_IMG = "v1/add_activity_category_image"
}

struct VISITOR_URL
{
    static let GET_VISITORS_LIST = "v1/get_visitor_list"
    
    static let ADD_VISITOR_LIST = "v1/add_to_visitor_list"
}

struct USER_URL
{
    static let BLOCK_USER = "v1/block_user"
    
    static let UNBLOCK_USER = "v1/unblock_user"
}

struct INTEREST_URL
{
    static let ADD_INTERESTS = "v1/add_interests"
    
    static let GET_INTERESTS_LIST = "v1/get_user_interest_list"
}

struct INVITE_URL
{
    static let GET_MY_INVITES = "v1/get_my_invitation_list"
    
    static let ACCEPT_ACT_INVITE = "v1/accept_activity_invitation"
}

struct REPORT_URL
{
    static let REPORT_ACTTIVITY = "v1/report_activity"
    
    static let REPORT_USER = "v1/report_user"
}

struct FRIENDS_URL
{
    static let SEND_REQ = "v1/send_friend_request"
    
    static let CANCEL_SENT_REQ = "v1/cancel_friend_request"
    
    static let ACCEPT_REQ = "v1/accept_friend_request"
    
    static let DECLINE_REQ = "v1/decline_friend_request"
    
    static let GET_REQ_LIST = "v1/get_friend_request_list"
    
    static let GET_MY_FRND_LIST = "v1/get_my_friend_list"
    
    static let GET_FB_FRNDS = "v1/get_fb_frinds"
    
    static let GET_MUTUAL_FRND_LIST = "v1/get_mutual_friend_list"
    
    static let GET_USER_UPCOMING_ACT_LIST = "v1/get_upcomming_activity_list"
    
    static let GET_USER_PAST_ACT_LIST = "v1/get_past_activity_list"
    
    static let CONNECT_WITH_FB = "v1/connect_with_fb"
}

struct MESSAGE_URL
{
    static let SEND_MSG = "v1/send_messages"
    
    static let SYNC_ALL_MSG = "v1/sync_all_messages"
    
    static let SYNC_MSG = "v1/sync_messages"
}

struct SETTINGS_URL
{
    static let GET_NOTI_SETTINGS = "v1/get_notification_setting"
    
    static let UPDATE_NOTI_SETTINGS = "v1/update_notification_setting"
    
    static let POST_FEEDBACK = "v1/add_feedback"
    
    static let GET_ALL_FEEDBACK = "v1/get_all_feedbacks"
    
    static let GET_MY_COMMENTS_ARRAY = "v1/get_feedback_comments"
    
    static let ADD_FEEDBACK_COMMENT = "v1/add_feedback_comment"
    
    static let SUBSCRIBE_FEEDBACK = "v1/suscribe_feedback"
    
    static let UNSUBSCRIBE_FEEDBACK = "v1/unsuscribe_feedback"
    
    static let SEARCH_FEEDBACK = "v1/search_feedbacks"
}

struct UPGRADE_URL
{
    static let UPDATE_SUBSCRIPTION = "v1/update_suscription"
    
    static let GET_SUBSCRIBERS_LIST = "v1/get_all_app_suscribers"
    
    static let ADD_SUBSCRIPTION_TEXT = "v1/add_suscription_text"
}

struct REFRESH_STATUS
{
    static let SYNC_NOTIFICATION = "v1/sync_notification"
    
    static let READ_NOTIFICATION = "v1/change_read_status"
}

struct QUOTES_API
{
    static let GET_QUOTES = "v1/get_all_quotes"
}

struct GROUPS_API
{
    static let ADD_GROUP = "v1/add_group_messages_data"
    
    static let GET_GROUPS = "v1/get_group_messages_data"
    
    static let GET_ALL_GROUPS = "v1/get_all_groups"
    
    static let UPDATE_GROUP = "v1/update_group_messages_data_quickbox"
    
    static let SEARCH_GROUP = "v1/search_group"
    
    static let DELETE_GROUP = "v1/delete_group"
}

struct PROFILE_PIC_API
{
    static let ADD_IMAGE = "v1/add_profile_album_image"
    
    static let ADD_IMAGE_ARRAY = "v1/profile_pictures_curd"
}

struct storyboards
{
    static let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    static let navigationBarStoryboard = UIStoryboard.init(name: "NavigationBarStoryboard", bundle: nil)
    static let commonStoryboard = UIStoryboard.init(name: "CommonStoryboard", bundle: nil)
    static let rareStoryboard = UIStoryboard.init(name: "RareStoryboard", bundle: nil)
    static let qbStoryboard = UIStoryboard.init(name: "Main1", bundle: nil)
    static let groupStoryboard = UIStoryboard.init(name: "GroupStoryboard", bundle: nil)
    static let quotesStoryboard = UIStoryboard.init(name: "QuotesStoryboard", bundle: nil)
    static let profilePicStoryboard = UIStoryboard.init(name: "ProfilePicturesStoryboard", bundle: nil)
}

class GlobalConstants: NSObject
{
    var responseDict = [String:Any]()

    // MARK: - GET methods
    func getDataFor(subUrl:String, header: NSDictionary, completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        
        let urlString = APP_URL.BASE_URL + cleanUrl
        
        let url = URL(string: urlString)
       
        Alamofire.request(url!,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        let response = dict.value(forKey: "response_code") as! Int
                        let successMsg = dict.value(forKey: "message") as! String
                        
                        //let responseDict = [String:Any]()
                        if (response == 200)
                        {
                            self.responseDict = dict.value(forKey: "result") as! [String:Any] //NSDictionary
                            completionHandler((true,successMsg,self.responseDict))
                        }
                        else //if (response == 404 || response == 401)
                        {
                            completionHandler((false,successMsg,self.responseDict))
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    completionHandler((false, error.localizedDescription + " Please try again later.",self.responseDict))
            }
        }
    }
    
    
    // MARK: - POST methods
    // MARK: post with PARAMETER
    func postDataFor(subUrl:String, parameter:[String:Any], resultKeyName: String, completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        
        let urlString = APP_URL.BASE_URL + cleanUrl
        print(urlString)
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method:.post, parameters: parameter, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                
                //print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        
                        let response = dict.value(forKey: "status") as! Int
                        
                        if (response == 200)
                        {
                            let responseMsg = "My msg - User Registered Successfully"
                            
                            if dict.value(forKey: resultKeyName) != nil
                            {
                                self.responseDict = dict.value(forKey: resultKeyName) as! [String:Any]
                            }
                            completionHandler((true, responseMsg, self.responseDict))
                        }
                        else //if (response == 409)
                        {
                            let responseMsg = dict.value(forKey: "message") as? String

                            completionHandler((false, responseMsg!, self.responseDict))
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    completionHandler((false, error.localizedDescription + " Please try again later.",self.responseDict))
            }
        }
    }
    
    func postData_OnlyParameter(subUrl:String, parameter:[String:Any], completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        
        let urlString = APP_URL.BASE_URL + cleanUrl
        print(urlString)
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method:.post, parameters: parameter, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                
                //print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        
                        let response = dict.value(forKey: "status") as! Int
                        let responseMsg = dict.value(forKey: "message") as? String
                        
                        if (response == 200)
                        {
                            completionHandler((true, responseMsg!, dict as! [String : Any]))
                        }
                        else
                        {
                            completionHandler((false, responseMsg!, dict as! [String : Any]))
                        }
                        
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    completionHandler((false, error.localizedDescription + " Please try again later.",self.responseDict))
                }
        }
    }

    // MARK: post with HEADER
    func postDataFor(subUrl:String, header: [String:String], resultKeyName: String, completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        
        let urlString = APP_URL.BASE_URL + cleanUrl
        
        print(urlString)
        print(header)
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method:.post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON
            { response in
                
                print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        
                        let response = dict.value(forKey: "status") as! Int
                        
                        if (response == 200)
                        {
                            let responseMsg = "Temp MSG"
                            
                            if dict.value(forKey: resultKeyName) != nil
                            {
                                self.responseDict = dict.value(forKey: resultKeyName) as! [String:Any]
                            }
                            completionHandler((true, responseMsg, self.responseDict))
                        }
                        else
                        {
                            let responseMsg = dict.value(forKey: "message") as? String
                            
                            completionHandler((false, responseMsg!, dict as! [String : Any]))
                        }

                    }
                    
                    break
                    
                case .failure(let error):
                    
                    completionHandler((false, error.localizedDescription + " Please try again later.",self.responseDict))
                }
        }
    }
    
    func postDataOnlyHeader_returnWholeDict(subUrl:String, header: [String:String], completionHandler:@escaping ((Bool,String,[String : Any])) -> ())
    {
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        
        let urlString = APP_URL.BASE_URL + cleanUrl
        
        print(urlString)
        print(header)
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method:.post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON
            { response in
                
                //print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        
                        let response = dict.value(forKey: "status") as! Int
                        let responseMsg = dict.value(forKey: "message") as? String

                        if (response == 200)
                        {
//                            let responseMsg = "Temp MSG"
                            
//                            if dict.value(forKey: resultKeyName) != nil
//                            {
//                                self.responseArray = dict.value(forKey: resultKeyName) as! NSArray
//                            }
                            completionHandler((true, responseMsg!, dict as! [String : Any]))
                        }
                        else
                        {
                            
                            completionHandler((false, responseMsg!, dict as! [String : Any]))
                        }
                        
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    completionHandler((false, error.localizedDescription + " Please try again later.", self.responseDict))
                }
        }
    }
    
    // MARK: POST with HEADER, PARAMETER
    func postDataFor(subUrl:String, header: [String:String], parameter:[String:Any], resultKeyName: String, completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        
        let urlString = APP_URL.BASE_URL + cleanUrl
        
        print(urlString)
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method:.post, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON
            { response in
                
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        
                        let response = dict.value(forKey: "status") as! Int
                        
                        if (response == 200)
                        {
                            let responseMsg = "Temp MSG"

                            if dict.value(forKey: resultKeyName) != nil
                            {
                                self.responseDict = dict.value(forKey: resultKeyName) as! [String:Any]
                            }
                            completionHandler((true, responseMsg, self.responseDict))
                        }
                        else //if (response == 409)
                        {
                            let responseMsg = dict.value(forKey: "message") as? String
                            
                            completionHandler((false, responseMsg!, self.responseDict))
                        }

                    }
                    
                    break
                    
                case .failure(let error):
                    
                    completionHandler((false, error.localizedDescription + " Please try again later.",self.responseDict))
                    
                    //self.present(showAlertController(Title: "Fail!", Message: error.localizedDescription+" Please try again later."), animated: true, completion: nil)
                }
        }
    }
    
    func postData_Header_Parameter(subUrl:String, header: [String:String], parameter:[String:Any], completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        
        let urlString = APP_URL.BASE_URL + cleanUrl
        
        print(urlString)
        
        let url = URL(string: urlString)
        Alamofire.request(url!,method:.post, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON
            { response in
                
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let dict = response.result.value! as! NSDictionary
                        
                        let response = dict.value(forKey: "status") as! Int
                        let responseMsg = dict.value(forKey: "message") as? String
                        
                        if (response == 200)
                        {
                             completionHandler((true, responseMsg!, dict as! [String : Any]))
                        }
                        else
                        {
                            
                            completionHandler((false, responseMsg!, dict as! [String : Any]))
                        }
                        
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    completionHandler((false, error.localizedDescription + " Please try again later.",self.responseDict))
            }
        }
    }
    
    // MARK: -
    // MARK: - Image Manipulation
    
    func retrieveImageForUrl(subUrl : String)
    {
        let cleanUrl = subUrl.replacingOccurrences(of: " ", with: "%20")
        
        let urlString = APP_URL.BASE_URL + cleanUrl
        
        print(urlString)
        
        Alamofire.request(urlString).responseImage { response in
            debugPrint(response)
            
            print(response.request)
            print(response.response)
            debugPrint(response.result)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
            }
        }
    }
    
    func getFCMToken() -> String
    {
        if UserDefaults.standard.value(forKey: "fcmDeviceToken") == nil
        {
            let fcmDeviceToken = FIRInstanceID.instanceID().token()
            print("FCM token: \(fcmDeviceToken ?? "")")
            UserDefaults.standard.setValue(fcmDeviceToken, forKey: "fcmDeviceToken")
            UserDefaults.standard.synchronize()
            
            return fcmDeviceToken!
        }
        else
        {
            let fcmDeviceToken = FIRInstanceID.instanceID().token()
            return fcmDeviceToken!
        }
    }
}

