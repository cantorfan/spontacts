//
//  AddPictureTilesViewController.swift
//  Spontacts
//
//  Created by maximess142 on 11/10/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class AddPictureTilesViewController: UIViewController {

    @IBOutlet weak var butnPicture1: UIButton!
    @IBOutlet weak var butnPicture2: UIButton!
    @IBOutlet weak var butnPicture3: UIButton!
    @IBOutlet weak var butnPicture4: UIButton!
    
    var imagesArray = [UIImage]()
//    var imagesArray = [#imageLiteral(resourceName: "fotolia_122634194.jpg"), #imageLiteral(resourceName: "Fotolia_80778652_Subscription_Monthly_M.jpg"), #imageLiteral(resourceName: "Fotolia_159472423_Subscription_Monthly_M.jpg")]

    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        assignImagesToButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - IBAction
    @IBAction func didTapAddPhotoButn(_ sender: Any)
    {
        let alert = UIAlertController.init(title: "Choose from", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: {
            alert -> Void in
            
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: {
            alert -> Void in
            
            
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func didTapContinueButn(_ sender: Any)
    {
        if imagesArray.count == 0
        {
            self.showAlert(title: "Alert!", msg: "Please add at least 1 image to continue.")
            
            return
        }
        
        let pictureSwipeVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewProfilePhotoViewController") as! ViewProfilePhotoViewController
        
        for image in imagesArray
        {
//            var dict = ["picture" : "", "image" : image, "text" : ""] as [String : Any]
//            pictureSwipeVC.dataArray.append(dict)
        }
        //pictureSwipeVC.imageArray = imagesArray
        self.navigationController?.pushViewController(pictureSwipeVC, animated: true)
    }
    
    @IBAction func didTapSkipButn(_ sender: Any)
    {
        let landingVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "LandingPage") as! LandingPage
        self.navigationController?.pushViewController(landingVC, animated: true)
    }
    
    // MARK: - Custom Methods
    func assignImagesToButton()
    {
        if imagesArray.count > 0
        {
            let image = imagesArray[0]
            self.butnPicture1.setImage(image, for: .normal)
        }
        if imagesArray.count > 1
        {
            let image = imagesArray[1]
            self.butnPicture2.setImage(image, for: .normal)
        }
        if imagesArray.count > 2
        {
            let image = imagesArray[2]
            self.butnPicture3.setImage(image, for: .normal)
        }
        if imagesArray.count > 3
        {
            let image = imagesArray[3]
            self.butnPicture4.setImage(image, for: .normal)
        }
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddPictureTilesViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        print(" ~~~~~~~~~~~~~~~~~~~~~~~~~ Image selected ~~~~~~~~~~~~~~~~~~~~~~~~")
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return // No image selected.
        }
        
        self.imagesArray.append(image)
        self.assignImagesToButton()
//        self.butnPicture.setBackgroundImage(image, for: .normal)
//
//        //Now use image to create into NSData format
//        let imageData = image.lowestQualityJPEGNSData
//
//        // let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
//        let base64String = imageData.base64EncodedString(options: .lineLength64Characters)
//        self.userPara.userPictureBase64 = base64String
        
        self.dismiss(animated: true, completion:
            { () -> Void in
                
        })
    }
}
