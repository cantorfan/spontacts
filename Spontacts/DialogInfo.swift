//
//  DialogInfo.swift
//  Projec10
//
//  Created by MAXIMESS082 on 24/03/17.
//  Copyright © 2017 maximess114. All rights reserved.
//

import UIKit

class DialogInfo: NSObject {

    static let instance = DialogInfo()
    
    var DialogId: String = String()
    var DialogName: String = String()
    var ProjectId: String = String()
    var SenderId: String = String()    
    var ReceiverId: String = String()
    var DialogLastMsg: String = String()
    var DialogLastMsgDate: String = String()
    var DialogUnreadMsgCount: String = String()
    
    var Note_id: String = String()
    var Note_type: String = String()
    var Note_description: String = String()
    var Note_date: String = String()


}
