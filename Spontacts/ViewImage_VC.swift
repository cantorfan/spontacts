//
//  ViewImage_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 21/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class ViewImage_VC: UIViewController
{
    @IBOutlet weak var imgView: UIImageView!
    
    var pictureJSON = [String : Any]()
    
    var pictureUrl = String()
    var pictureId = Int()
    
    let sharedObj = Singleton.shared
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let optionButn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "setting"), style: .done, target: self, action: #selector(didTapOptionsButn))
        self.navigationItem.rightBarButtonItem = optionButn
    }

    override func viewWillAppear(_ animated: Bool)
    {
        pictureUrl = pictureJSON["image"] as! String
        pictureId = pictureJSON["id"] as! Int
        
        let url = URL(string: APP_URL.BASE_URL + pictureUrl )!
        let placeholderImage = #imageLiteral(resourceName: "avatar")
        let filter = AspectScaledToFitSizeFilter(size: CGSize(width: self.imgView.frame.size.width, height: self.imgView.frame.size.height))
        
        imgView.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
            { response in
                
            }
        )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    func callDeletePhotoAPI()
    {
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        let requestObject = ["image_id" : pictureId]

        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.DELETE_ACTIVITY_IMG, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: responseMessage)
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Action Methods
    func didTapOptionsButn()
    {
        let alert = UIAlertController.init(title: "", message: "Do you want to delete this photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction.init(title: "Delete Photo", style: .destructive, handler:
            { alert -> Void in
                self.callDeletePhotoAPI()
            }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: {
            alert -> Void in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
