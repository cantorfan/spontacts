//
//  NotificationsSettingViewController.swift
//  Spontacts
//
//  Created by maximess142 on 25/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class NotificationsSettingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView: UITableView!
    
    let arrHeaderTitles = ["New Activities", "Your Activities", "Personal Messages", "Friends", "News & Updates"]
    
    let arrCellNewActivities = ["When one of your friends creates an activity", "When there is a new exciting activity matching your interests."]
   
    let arrCellYourActivities = ["When an activity is updated", "When an activity is deleted", "When someone joined your activity", "When you receive a recommendation", "When you receive an invitation", "When the organizer comments its activity", "When someone comments an activity", "When we shall remind you on activity you have joined", "When it is time for awarding", "If you want to be reminded of the Photo-Upload", "When a new photo was uploaded.", "When a photo was commented."]
    
    let arrCellPersonalMsg = ["When someone sends you a message"]
    
    let arrCellFriends = ["When one of your friend joined an activity", "When you receive a friendship request", "When someone accepts your friendship request.", "When one of your Facebook friends signs up to GAY AND YOU?"]
    
    let arrCellNews = ["Keep me posted with GAY AND YOU? news and updates", "Keep me posted about your partner's offers", "Send me weekly news about interesting activities", "Send me weekly news about my profile visitors", "When you receive an invitation for an interest group"]
    
    var isSomethingEdited = false
    
    var mobileDict : [String : Any] = ["new_activites" : [1, 1],
                                        "your_activites" : [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                                        "friends" : [1, 1, 1, 1],
                                        "personal_messages" : [1],
                                        "news_updates" : [1, 1, 1, 1, 1]]
    var mailDict : [String : Any] = ["new_activites" : [1, 1],
                                      "your_activites" : [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                                      "friends" : [1, 1, 1, 1],
                                      "personal_messages" : [1],
                                      "news_updates" : [1, 1, 1, 1, 1]]
//    var dataDict = [
//                "mobile" : {
//                "new_activites" : [1, 1],
//                "your_activites" : [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
//                "friends" : [1, 1, 1, 1],
//                "personal_messages" : [1],
//                "news_updates" : [1, 1, 1, 1, 1]
//            },
//            "mail" : {
//                "new_activites" : [1, 1],
//                "your_activites" : [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
//                "friends" : [1, 1, 1, 1],
//                "personal_messages" : [1],
//                "news_updates" : [1, 1, 1, 1, 1]
//            }
//        ]
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let saveButn = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: #selector(self.didTapSaveButn))
        self.navigationItem.rightBarButtonItem = saveButn
        
        let backButn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back_pink"), style: .done, target: self, action: #selector(self.didTapBackButn))
        self.navigationItem.leftBarButtonItem = backButn
        
        self.callGetNotificationSettingAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Methods
    func didTapBackButn()
    {
        if isSomethingEdited == true
        {
            let alert = UIAlertController.init(title: "Are you sure you want to lose your changes?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
                alert -> Void in
                self.navigationController?.popViewController(animated: true)
            }))
            
            alert.addAction(UIAlertAction.init(title: "Save", style: .default, handler: {
                alert -> Void in
                self.didTapSaveButn()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didTapSaveButn()
    {
        isSomethingEdited = false
        callUpdateNotificationAPI()
    }
    
    @IBAction func didTapMobileButn(_ sender: Any)
    {
        isSomethingEdited = true
        let senderButn = sender as! UIButton
        
        let buttonPosition:CGPoint = senderButn.convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)!
        
        //senderButn.isSelected = !senderButn.isSelected
        
        var keyName = ""
        switch indexPath.section
        {
        case 0:
            keyName = "new_activites"
            
        case 1:
            keyName = "your_activites"
            
        case 2:
            keyName = "personal_messages"

        case 3:
            keyName = "friends"

        case 4:
            keyName = "news_updates"

        default:
            keyName = ""
        }
        
        var array : [Int] = mobileDict[keyName] as! [Int]
        
        // This is before click
        if array[indexPath.row] == 0
        {
            array[indexPath.row] = 1
        }
        else
        {
            array[indexPath.row] = 0
        }
        
        mobileDict[keyName] = array
        tableView.reloadData()
    }
    
    @IBAction func didTapEmailButn(_ sender: Any)
    {
        isSomethingEdited = true
        let senderButn = sender as! UIButton
        
        let buttonPosition:CGPoint = senderButn.convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)!
        
        //senderButn.isSelected = !senderButn.isSelected
        var keyName = ""
        switch indexPath.section
        {
        case 0:
            keyName = "new_activites"
            
        case 1:
            keyName = "your_activites"
            
        case 2:
            keyName = "personal_messages"
            
        case 3:
            keyName = "friends"
            
        case 4:
            keyName = "news_updates"
            
        default:
            keyName = ""
        }
        
        var array : [Int] = mailDict[keyName] as! [Int]
        
        // This is before click
        if array[indexPath.row] == 0
        {
            array[indexPath.row] = 1
        }
        else
        {
            array[indexPath.row] = 0
        }
        
        mailDict[keyName] = array
        tableView.reloadData()
    }
    
    // MARK: - Functions
    func checkButnStatus()
    {
        
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrHeaderTitles.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
        case 0:
            return arrCellNewActivities.count
        case 1:
            return arrCellYourActivities.count
        case 2:
            return arrCellPersonalMsg.count
        case 3:
            return arrCellFriends.count
        case 4:
            return arrCellNews.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell : NotiSettingsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationHeader") as! NotiSettingsTableViewCell

        cell.lblHeaderTitle.text = arrHeaderTitles[section]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : NotiSettingsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotiSettingsTableViewCell
        
        switch indexPath.section
        {
        case 0:
            cell.lblCellTitle.text = arrCellNewActivities[indexPath.row]
            let mobStatusrray : [Int] = self.mobileDict["new_activites"] as! [Int]
            cell.btnMobile.isSelected = mobStatusrray[indexPath.row] == 0 ? false : true
            
            let mailStatusrray : [Int] = self.mailDict["new_activites"] as! [Int]
            cell.btnEmail.isSelected = mailStatusrray[indexPath.row] == 0 ? false : true
            
        case 1:
            cell.lblCellTitle.text = arrCellYourActivities[indexPath.row]
            let mobStatusrray : [Int] = self.mobileDict["your_activites"] as! [Int]
            cell.btnMobile.isSelected = mobStatusrray[indexPath.row] == 0 ? false : true
            
            let mailStatusrray : [Int] = self.mailDict["your_activites"] as! [Int]
            cell.btnEmail.isSelected = mailStatusrray[indexPath.row] == 0 ? false : true
            
        case 2:
            cell.lblCellTitle.text = arrCellPersonalMsg[indexPath.row]
            let mobStatusrray : [Int] = self.mobileDict["personal_messages"] as! [Int]
            cell.btnMobile.isSelected = mobStatusrray[indexPath.row] == 0 ? false : true
            
            let mailStatusrray : [Int] = self.mailDict["personal_messages"] as! [Int]
            cell.btnEmail.isSelected = mailStatusrray[indexPath.row] == 0 ? false : true
            
        case 3:
            cell.lblCellTitle.text = arrCellFriends[indexPath.row]
            let mobStatusrray : [Int] = self.mobileDict["friends"] as! [Int]
            cell.btnMobile.isSelected = mobStatusrray[indexPath.row] == 0 ? false : true
            
            let mailStatusrray : [Int] = self.mailDict["friends"] as! [Int]
            cell.btnEmail.isSelected = mailStatusrray[indexPath.row] == 0 ? false : true
            
        case 4:
            cell.lblCellTitle.text = arrCellNews[indexPath.row]
            let mobStatusrray : [Int] = self.mobileDict["news_updates"] as! [Int]
            cell.btnMobile.isSelected = mobStatusrray[indexPath.row] == 0 ? false : true
            
            let mailStatusrray : [Int] = self.mailDict["news_updates"] as! [Int]
            cell.btnEmail.isSelected = mailStatusrray[indexPath.row] == 0 ? false : true
            
        default:
            cell.lblCellTitle.text = ""
        }
        
        cell.btnMobile.tag = 1000 * indexPath.section + 100 * indexPath.row
        cell.btnEmail.tag = 2000 * indexPath.section + 100 * indexPath.row
        
        
        return cell
    }

    // MARK: - Web API
    func callGetNotificationSettingAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: SETTINGS_URL.GET_NOTI_SETTINGS, header: headerObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            if isSuccess
            {
                if responseData["notification_setting"] != nil
                {
                    let dataDict = responseData["notification_setting"] as! [String : Any] 
                    
                    self.mobileDict = dataDict["mobile"] as! [String : Any]
                    self.mailDict = dataDict["mail"] as! [String : Any]
                    self.tableView.reloadData()
                }
            }
            else
            {
                //self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func callUpdateNotificationAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let parameter : [String : Any] = ["mobile" : mobileDict,
                                          "mail" : mailDict] as [String : Any]
        
        GlobalConstants().postData_Header_Parameter(subUrl: SETTINGS_URL.UPDATE_NOTI_SETTINGS, header: headerObject, parameter: parameter, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            
            if isSuccess
            {
                self.showAlert(title: "Success!", msg: "Settings Updated")
            }
            else
            {
                //self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
