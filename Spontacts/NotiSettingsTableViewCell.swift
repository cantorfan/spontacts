//
//  NotiSettingsTableViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 25/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class NotiSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var lblCellTitle: UILabel!
    
    @IBOutlet weak var btnMobile: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
