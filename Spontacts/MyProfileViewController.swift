//
//  MyProfileViewController.swift
//  Spontacts
//
//  Created by maximess142 on 27/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class MyProfileViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, SWRevealViewControllerDelegate, UITextFieldDelegate, UITextViewDelegate
{
    var newBase64Str = String()
    let sharedObj = Singleton.shared
    
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblGenderInitial: UILabel!
    @IBOutlet weak var imgGender: UIImageView!
    @IBOutlet weak var lblRelationship: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblHeartWings: UIButton!
    
    @IBOutlet weak var constraintDetailsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtDetails: UITextView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnVisitors: UIButton!
    @IBOutlet weak var btnChangeAccountSettings: UIButton!
    @IBOutlet weak var btnInterests: UIButton!
    
    var detailsPlaceholderText = "Write something about yourself!"
    var arrUserLocation : [[String : Any]] = [[String : Any]]()
    var arrGenderImg = [#imageLiteral(resourceName: "male"), #imageLiteral(resourceName: "female"), #imageLiteral(resourceName: "transgender")]
    var arrGenderIntial = ["M", "F", "T"]
    var arrRelationStatus = ["--", "In a Relationship" , "Single", "Dating"]
    
    // @IBOutlet weak var tableView: UITableView!
    var isShowBackBtn = Bool()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        title = "My Profile"
        addLeftBarButton()
        
        callGetUserInformationAPI()
        //tableView.estimatedRowHeight = view.frame.size.height - 50
    }
    
    override func viewWillAppear(_ animated: Bool) {
        assignValueToLabel()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func assignValueToLabel()
    {
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: self.imgViewUser.frame.size.width, height: self.imgViewUser.frame.size.width), radius: self.imgViewUser.frame.size.width/2)
        self.imgViewUser.af_setImage(withURL:  URL.init(string: APP_URL.BASE_URL + sharedObj.userPara.userPictureThumbUrl)!, placeholderImage: #imageLiteral(resourceName: "avatar_big"), filter: filter, imageTransition: .crossDissolve(0.2), completion: nil)
        
        if sharedObj.userPara.userGender != 0
        {
            self.imgGender.image = arrGenderImg[sharedObj.userPara.userGender - 1]
            self.lblGenderInitial.text = arrGenderIntial[sharedObj.userPara.userGender - 1]
        }
        
        if sharedObj.userPara.userDescription != ""
        {
            self.txtDetails.text = sharedObj.userPara.userDescription
        }
        else
        {
            self.txtDetails.text = detailsPlaceholderText
        }
        
        self.lblRelationship.text = arrRelationStatus[sharedObj.userPara.userRelationStatus]
        
        self.lblUserName.text = sharedObj.userPara.userFirstName + " " + sharedObj.userPara.userLastName
        self.lblAge.text = sharedObj.userPara.userAge == 0 ? "-- years" : "\(sharedObj.userPara.userAge)  years"
        self.lblAddress.text = sharedObj.userPara.userLocationStr
        self.lblEmail.text = sharedObj.userPara.userEmail
        
        if UserDefaults.standard.value(forKey: "interest") != nil
        {
            let array : NSArray = UserDefaults.standard.value(forKey: "interest") as! NSArray
            self.btnInterests.setTitle("My Interests(\(array.count))", for: .normal)
        }
        
        self.btnVisitors.contentHorizontalAlignment = .left
        self.btnChangeAccountSettings.contentHorizontalAlignment = .left
        self.btnInterests.contentHorizontalAlignment = .left
    }
    
    func addLeftBarButton()
    {
        if isShowBackBtn == false
        {
            let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu_new"), style: .plain,  target: self, action: #selector(didTapMenuButn))
            navigationItem.leftBarButtonItem = btnMenu
            
            if revealViewController() != nil
            {
                revealViewController().rightViewRevealWidth = 200
                
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            }
        }
        else
        {
            let btnBack = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.didTapBackButn))
            navigationItem.leftBarButtonItem = btnBack
        }
    }
    
    // MARK: - API Methods
    func callGetUserInformationAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait..")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: APP_URL.GET_SELF_PROFILE, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    let userJSON = responseData["user"] as! [String : Any]
                    UserDefaults.standard.set(userJSON, forKey: "userJSON")
                    UserDefaults.standard.synchronize()
                    
                    self.sharedObj.userPara.userId = userJSON["id"] as! Int
                    self.sharedObj.userPara.userEmail = userJSON["email"] as! String
                    self.sharedObj.userPara.userLocationLat = userJSON["lat"] as! Double
                    self.sharedObj.userPara.userFirstName = userJSON["fname"] as! String
                    self.sharedObj.userPara.userLastName = userJSON["lname"] as! String
                    self.sharedObj.userPara.userToken = userJSON["token"] as! String
                    self.sharedObj.userPara.userLocationLong = userJSON["lng"] as! Double
                    //self.sharedObj.userPara.userPassword = userJSON["password"] as! String
                    self.sharedObj.userPara.userPictureThumbUrl = userJSON["thumb"] as! String
                    self.sharedObj.userPara.userPictureUrl = userJSON["picture"] as! String
                    self.sharedObj.userPara.userLocationStr = userJSON["location_string"] as! String
                    self.sharedObj.userPara.userAge = userJSON["age"] as! Int
                    self.sharedObj.userPara.userGender = userJSON["gender"] as! Int
                    self.sharedObj.userPara.userRelationStatus = userJSON["relationship_status"] as! Int
                    self.sharedObj.userPara.userDescription = userJSON["about"] as! String
                    self.sharedObj.userPara.isPlusMember = userJSON["suscribed"] as! Int
                    
                    self.arrUserLocation = userJSON["locations"] as! [[String:Any]]
                    self.assignValueToLabel()
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Action Methods
    @IBAction func didTapMenuButn()
    {
        revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func didTapBackButn()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapEditProfile(_ sender: Any)
    {
        let controller : EditProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        
        controller.arrLocation = arrUserLocation
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func didTapProfileVisitors(_ sender: Any)
    {
        let visitors : VisitorsViewController = self.storyboard?.instantiateViewController(withIdentifier: "VisitorsViewController") as! VisitorsViewController
        self.navigationController?.pushViewController(visitors, animated: true)
    }
    
    @IBAction func didTapAccountSettings(_ sender: Any)
    {
        self.performSegue(withIdentifier: "segueProfileToAccountSetting", sender: nil)
    }

    @IBAction func didTapInterests(_ sender : Any)
    {
        let catVC : InterestsCategoriesViewController = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "InterestsCategoriesViewController") as! InterestsCategoriesViewController
        
        if UserDefaults.standard.value(forKey: "interest") != nil
        {
            let array : [Int] = UserDefaults.standard.value(forKey: "interest") as! [Int]
            catVC.selectedCatArray = array //as! NSMutableArray
        }
        
        self.navigationController?.pushViewController(catVC, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }

}

