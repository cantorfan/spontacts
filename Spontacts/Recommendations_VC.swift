//
//  Recommendations_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 22/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import GoogleMobileAds

class Recommendations_VC: UIViewController, UITableViewDataSource, UITableViewDelegate, SWRevealViewControllerDelegate
{
    @IBOutlet weak var tableView: UITableView!
    
    let sharedObj = Singleton.shared
    var selectedSeg = Int()
    var arrInviteList = NSMutableArray()
    var defaultDateFormatter = DateFormatter()
    var activityParaToSend = Para_Activity()
    var userParaToSend = Para_UserProfile()
    
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = AdMob_BannerID
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        
        return adBannerView
    }()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        addLeftBarButton()
        defaultDateFormatter.dateFormat = "dd/MM/YYYY"
        
        title = "Recommendations"
        adBannerView.load(GADRequest())
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
            self.callReadStatusAPI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        fetchInviteListAPI()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    func fetchInviteListAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Getting Invites")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl:INVITE_URL.GET_MY_INVITES , header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    if responseData["invitation_list"] != nil 
                    {
                        let array = responseData["invitation_list"] as! NSArray
                        print("You got invites ", array.count)
                        self.arrInviteList = array.mutableCopy() as! NSMutableArray
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
                
        })
    }
    
    func callFetchAttendees_InterestedAPI(actId : Int)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Loading Activities")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        let requestObject : [String : Any] = ["activity_id": actId] as [String : Any]
        
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.GET_ACT_GOING_INTERESTED_COMMENTS_LIST, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    if responseData["going"] != nil
                    {
                        let array = responseData["going"] as! [[String:Any]]
                        self.activityParaToSend.actGoingArray = array
                    }
                    if responseData["interested"] != nil
                    {
                        let array = responseData["interested"] as! [[String:Any]]
                        self.activityParaToSend.actInterstedArray = array
                    }
                    if responseData["comments"] != nil
                    {
                        let array = responseData["comments"] as! NSArray
                        self.activityParaToSend.actCommentsArray = array.mutableCopy() as! NSMutableArray
                    }
                    
                    print(self.activityParaToSend.actGoingArray.count, self.activityParaToSend.actInterstedArray.count)
                }
                else
                {
                    //self.showAlert(title: "Fail!", msg: responseMessage)
                }
                
                self.performSegue(withIdentifier: "segueRecommendToShowActivity", sender: nil)
        })
    }

    
    func addLeftBarButton()
    {
        let customView = UIView.init(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setImage(#imageLiteral(resourceName: "menu_white"), for: .normal)
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(self.didTapMenuButn), for: .touchUpInside)
        
        let label = UILabel.init(frame: CGRect(x: 30, y: 5, width: 15, height: 15))
        label.backgroundColor = UIColor.white
        label.layer.cornerRadius = label.frame.size.width / 2
        label.clipsToBounds =  true
        
        customView.addSubview(button)
        
        if sharedObj.unread_Recommendations != 0
        {
            customView.addSubview(label)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: customView) // customView
        
        if revealViewController() != nil
        {
            revealViewController().rightViewRevealWidth = 200
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func callReadStatusAPI()
    {
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["my_activities":0,
                             "joined_activities":0,
                             "recomendations":1,
                             "profile_visitors":0,
                             "friends":0]
        
        GlobalConstants().postData_Header_Parameter(subUrl: REFRESH_STATUS.READ_NOTIFICATION, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                self.sharedObj.unread_Recommendations = 0
                print(responseData)
                
                if isSuccess
                {
                    print("Read notifications")
                }
                else
                {
                    print("Error in reading status")
                }
        })
    }
    
    // MARK: - Action Methods
    @IBAction func didTapMenuButn()
    {
        revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func didChangeSegmentedControl(sender : UISegmentedControl)
    {
        selectedSeg = sender.selectedSegmentIndex
        tableView.reloadData()
    }
    
    func firstHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 60))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let segmentedControl = UISegmentedControl.init(items: ["From friends", "Explore"])
        segmentedControl.frame = CGRect.init(x: -2, y: 10, width: view.frame.size.width + 4, height: 40)
        segmentedControl.selectedSegmentIndex = selectedSeg
        segmentedControl.addTarget(self, action: #selector(self.didChangeSegmentedControl(sender:)), for: .valueChanged)
        headerView.addSubview(segmentedControl)
        
        return headerView
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.arrInviteList.count == 0
        {
            let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
            emptyLabel.text = "No Results"
            emptyLabel.textColor = UIColor.lightGray
            emptyLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = emptyLabel
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            return 0
        }
        else
        {
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
            self.tableView.backgroundView?.isHidden = true
            return self.arrInviteList.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : ActivityListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ActivityListTableViewCell", for: indexPath) as! ActivityListTableViewCell
        

        let JSON = arrInviteList[indexPath.row] as! [String : Any]
            //print(activityJSON)
        let activityJSON : [String : Any] = JSON["activity_data"] as! [String : Any]
        
        let catId = activityJSON["category_id"] as! Int
        if sharedObj.arrCatImageCropped.count > catId
        {
            let actCatUrl = activityJSON["category_picture_thumb"] as! String
            if catId == 8 && actCatUrl != ""
            {
                cell.imgActivity.af_setImage(withURL: URL.init(string: APP_URL.BASE_URL + actCatUrl)!, placeholderImage: #imageLiteral(resourceName: "image_placeolder"), completion: nil)
                cell.imgActivity.clipsToBounds = true
            }
            else
            {
                cell.imgActivity.image = sharedObj.arrCatImageCropped[catId]
                cell.imgActivity.clipsToBounds = true
            }
        }
        
        cell.lblActName.text = activityJSON["name"] as? String
        let timeInterval = activityJSON["time"] as? Int
        let date = Date.init(timeIntervalSince1970: TimeInterval(timeInterval!))
        cell.lblDate.text = defaultDateFormatter.string(from: date)
        
        cell.lblAttendees.text = "\(activityJSON["going_count"] as! Int)" //"1"
        cell.lblComments.text = "\(activityJSON["comments_count"] as! Int)" //"0"
        
        let distancestr = Int(activityJSON["distance"] as! NSNumber)
        cell.lblLocation.text = (activityJSON["location_string"] as? String)! + " (\(distancestr) km)"
        
        let organiserJSON = activityJSON["organiser"] as! [String : Any]
        let fname = organiserJSON["fname"] as! String
        
        cell.lblOrganising.text = fname + " invites you to"
        
        if activityJSON["updated"] as! Int == 1
        {
            cell.lblUpdated.isHidden = false
        }
        else
        {
            cell.lblUpdated.isHidden = true
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedSeg == 0
        {
            let JSONmain = arrInviteList[indexPath.row] as! [String : Any]
            //print(activityJSON)
            let activityJSON : [String : Any] = JSONmain["activity_data"] as! [String : Any]
            
            activityParaToSend = Para_Activity()
            
            activityParaToSend.actLocatnLongitude = activityJSON["lng"] as! Double
            activityParaToSend.actIsFlexible = (activityJSON["time_flexible"] != nil)
            let timeInterval = activityJSON["time"] as? Int
            activityParaToSend.actDate = Date.init(timeIntervalSince1970: Double(timeInterval!))
            activityParaToSend.actId = activityJSON["id"] as! Int
            activityParaToSend.actVisibleTo = activityJSON["visibility"] as! Int
            activityParaToSend.actDesc = activityJSON["description"] as! String
            activityParaToSend.actLocatnLatitude = activityJSON["lat"] as! Double
            activityParaToSend.actName = activityJSON["name"] as! String
            activityParaToSend.actCreatedEpoch = activityJSON["created_time"] as! Double
            activityParaToSend.actCategoryId = activityJSON["category_id"] as! Int
            activityParaToSend.actLocatnStr = activityJSON["location_string"] as! String
            activityParaToSend.actImgJSONArr = activityJSON["images"] as! NSArray
            activityParaToSend.isInterested = activityJSON["interested"] as! Int == 1 ? true : false //(JSON["interested"] != nil)
            activityParaToSend.isGoing = activityJSON["going"] as! Int == 1 ? true : false //(JSON["going"] != nil)
            activityParaToSend.actCommentsCount = activityJSON["comments_count"] as! Int
            activityParaToSend.actCategoryImgURL = activityJSON["category_picture"] as! String

            userParaToSend = Para_UserProfile()
            let organiserJSON = activityJSON["organiser"] as! [String : Any]
            userParaToSend.userFirstName = organiserJSON["fname"] as! String
            userParaToSend.userLastName = organiserJSON["lname"] as! String
            userParaToSend.userEmail = organiserJSON["email"] as! String
            userParaToSend.userLocationLat = organiserJSON["lat"] as! Double
            userParaToSend.userLocationLong = organiserJSON["lng"] as! Double
            userParaToSend.userId = organiserJSON["id"] as! Int
            userParaToSend.userPictureThumbUrl = organiserJSON["thumb"] as! String
            userParaToSend.isPlusMember = organiserJSON["suscribed"] as! Int
        }
        
        callFetchAttendees_InterestedAPI(actId: activityParaToSend.actId)
    }

     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?)
     {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueRecommendToShowActivity"
        {
            let showActivity : ShowActivity_VC = segue.destination as! ShowActivity_VC
            showActivity.userPara = userParaToSend
            showActivity.activityPara = activityParaToSend
        }
    }
    
    
    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }
}

extension Recommendations_VC : GADBannerViewDelegate
{
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        if sharedObj.userPara.isPlusMember == 1
        {
            print("No ads to plus user")
        }
        else
        {
            print("Banner loaded successfully")
            tableView.tableHeaderView?.frame = bannerView.frame
            tableView.tableHeaderView = bannerView
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }
}
