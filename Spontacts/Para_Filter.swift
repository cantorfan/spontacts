//
//  Para_Filter.swift
//  Spontacts
//
//  Created by maximess142 on 01/10/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class Para_Filter: NSObject
{
    var latitude = Double()
    var longitude = Double()
    var locationStr = ""
    var sort_by = Int()
    var from_date = Double()
    var to_date = Double()
    var visibleTo = Int()
    var categories_array = [Int]()
    var distance = Int()
    var search_keyword = ""
    
    var timeStr = ""
    var isFromTimeRows = Int()
    var sortbyStr = ""
}

