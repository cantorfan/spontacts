//
//  VisitorsViewController.swift
//  Spontacts
//
//  Created by maximess142 on 15/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class VisitorsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SWRevealViewControllerDelegate
{
    @IBOutlet weak var lblProfileCount: UILabel!
    
    @IBOutlet weak var viewTopUpgrade: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewTopUpgradeConstraint: NSLayoutConstraint!
    var arrVisitors : [[String : Any]] = [[String : Any]]()
    var dateFormatter = DateFormatter()
    let sharedObj = Singleton.shared
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "My Profile Visitors"
        dateFormatter.dateFormat = "dd MMM yyyy"
        retrieveMyVisitorsList()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.callReadStatusAPI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.setNavigationBarHidden(false, animated: true)
        addLeftBarButton()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftBarButton()
    {
        let customView = UIView.init(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setImage(#imageLiteral(resourceName: "menu_white"), for: .normal)
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(self.didTapMenuButn), for: .touchUpInside)
        
        let label = UILabel.init(frame: CGRect(x: 30, y: 5, width: 15, height: 15))
        label.backgroundColor = UIColor.white
        label.layer.cornerRadius = label.frame.size.width / 2
        label.clipsToBounds =  true
        
        customView.addSubview(button)
        
        if sharedObj.unread_Profile_Visitors_Count != 0
        {
            customView.addSubview(label)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: customView) // customView
        
        if revealViewController() != nil
        {
            revealViewController().rightViewRevealWidth = 200
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    // MARK: - IBAction
    @IBAction func didTapMenuButn()
    {
        revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func didTapUpgradeButn(_ sender: Any)
    {
        showUpgradeView()
    }
    
    @IBAction func didTapCreateActivity()
    {
        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAct_MandatoryInfo") as! CreateAct_MandatoryInfo
        self.navigationController?.pushViewController(presentVC, animated: true)
    }

    // MARK: - Functions
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showUpgradeView()
    {
        let upgradeVC : UpgradeViewController = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "UpgradeViewController") as! UpgradeViewController
        
        self.navigationController?.pushViewController(upgradeVC, animated: true)
    }
    
    //MARK: - TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrVisitors.count == 0
        {
            let lbl1 = UILabel.init(frame: CGRect(x: 8, y: 30, width: view.frame.size.width - 16, height: 20))
            lbl1.backgroundColor = sharedObj.hexStringToUIColor(hex: "efefef")
            lbl1.font = UIFont.init(name: Font.BEBAS, size: 18)
            lbl1.textAlignment = .center
            lbl1.text = "What a pity! No visitors yet?"
            
            let btn = UIButton(frame: CGRect(x: 20, y: 60, width: view.frame.size.width - 40, height:40))
            btn.setBackgroundImage(#imageLiteral(resourceName: "gradient_butn"), for: .normal)
            btn.titleLabel?.font = UIFont.init(name: Font.BEBAS_Bold, size: 17)
            btn.setTitleColor(UIColor.white, for: .normal)
            btn.setTitle("CREATE ACTIVITY", for: .normal)
            btn.addTarget(self, action: #selector(didTapCreateActivity), for: .touchUpInside)
            
            let lblLeft = UILabel(frame: CGRect(x: 20, y: 110, width: view.frame.size.width - 40, height: 60))
            lblLeft.textAlignment = .center
            lblLeft.font = UIFont.init(name: Font.BEBAS, size: 15)
            lblLeft.numberOfLines = 0
            lblLeft.lineBreakMode = .byWordWrapping
            lblLeft.text = "Have you already created an activity? As an organiser you get noticed by others even better."
            
            let emptyView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
            emptyView.backgroundColor = sharedObj.hexStringToUIColor(hex: "efefef")
            emptyView.addSubview(lbl1)
            emptyView.addSubview(btn)
            emptyView.addSubview(lblLeft)
            
            self.viewTopUpgrade.isHidden = true
            self.tableView.backgroundView = emptyView
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            return 0
        }
        else
        {
            if sharedObj.userPara.isPlusMember == 1
            {
                self.viewTopUpgrade.isHidden = true
                self.viewTopUpgradeConstraint.constant = 0
            }
            else
            {
                self.viewTopUpgrade.isHidden = false
            }

            self.tableView.backgroundView?.isHidden = true
            return arrVisitors.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : VisitorsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VisitorsTableViewCell", for: indexPath) as! VisitorsTableViewCell
        
        let userJSON = arrVisitors[indexPath.row] //as! [String : Any]
        
        let pictureURL : String = userJSON["thumb"] as! String
        if pictureURL != ""
        {
            let url = URL(string: APP_URL.BASE_URL + pictureURL )!
            let placeholderImage = #imageLiteral(resourceName: "avatar")
            let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
            
            //,filter: filter
            cell.imgViewPhoto.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                { response in
            }
            )
        }
        
        let timeEpochStr = userJSON["last_visit"] as! String
        cell.lblVisitCount.text = "Last Visit: " + dateFormatter.string(from: Date.init(timeIntervalSince1970: Double(timeEpochStr)!))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
     
        if sharedObj.userPara.isPlusMember == 1
        {
            let presentVC = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "UserProfile_VC") as! UserProfile_VC
            let userJSON = arrVisitors[indexPath.row]
            let userIdStr = userJSON["user_id"] as! String
            presentVC.user_Id = Int(userIdStr)!
            // Will allow to go back
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
        else
        {
            showUpgradeView()
        }
    }

    // MARK: - API Methods

    func retrieveMyVisitorsList()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: VISITOR_URL.GET_VISITORS_LIST, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["visitors"] != nil
                    {
                        print(responseData["visitors"] as Any)
                        let wholeDict : [String : Any] = responseData["visitors"] as! [String : Any]
                        
                        self.arrVisitors = wholeDict["visitors_data"] as! [[String : Any]]
                        
                        let visitorsCount = wholeDict["total_visitors"] as! Int
                        self.lblProfileCount.text = "\(visitorsCount) profile visitors were here. Curious?"
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }

    func callReadStatusAPI()
    {
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["my_activities":0,
                             "joined_activities":0,
                             "recomendations":0,
                             "profile_visitors":1,
                             "friends":0]
        
        GlobalConstants().postData_Header_Parameter(subUrl: REFRESH_STATUS.READ_NOTIFICATION, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                self.sharedObj.unread_Profile_Visitors_Count = 0
                print(responseData)
                
                if isSuccess
                {
                    print("Read notifications")
                }
                else
                {
                    print("Error in reading status")
                }
        })
    }

    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }
}
