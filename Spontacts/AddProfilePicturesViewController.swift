//
//  AddProfilePicturesViewController.swift
//  Spontacts
//
//  Created by maximess142 on 05/11/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

/*
 1. “add”
 2. “delete”
 3. “Update”
 */
class AddProfilePicturesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{

    @IBOutlet weak var butnAdd: UIButton!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var butnBack: UIButton!
    
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var butnContinue: UIButton!
    @IBOutlet weak var butnSkip: UIButton!
    
    @IBOutlet weak var butnUpdate: UIButton!
    
    @IBOutlet weak var butnAddPicture: UIButton!
    @IBOutlet weak var lblTop: UILabel!
    @IBOutlet weak var butnEdit: UIButton!
    
    @IBOutlet weak var lblMiddle: UILabel!
    
    var imagesArray = [Para_ProfilePictures]()
    var timer: Timer?
    let sharedObj = Singleton.shared
    var selectedProfileImage = 0
    var isEditMode = Bool()
    var deletedArray = [Para_ProfilePictures]()
    var isOthersProfile = false
    var isFromEditDetailsScreen = Bool()
    var dummyArray = [Para_ProfilePictures]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if isOthersProfile == false
        {
            if imagesArray.count <= 4
            {
                for var _ in 0..<(4 - imagesArray.count)
                {
                    let tempPara = Para_ProfilePictures()
                    tempPara.picImage  = #imageLiteral(resourceName: "camera_yellow")
                    dummyArray.append(tempPara)
                }
            }
            
            self.collectionView.reloadData()
        }
        
        if isFromEditDetailsScreen == true
        {
            self.butnBack.isHidden = false
            self.butnContinue.isHidden = true
            self.butnSkip.isHidden = true
            self.butnUpdate.isHidden = false
        }
        
        if isOthersProfile == true
        {
            self.butnBack.isHidden = false
            self.butnContinue.isHidden = true
            self.butnSkip.isHidden = true
            self.butnUpdate.isHidden = true
            self.lblTop.isHidden = true
            self.lblMiddle.isHidden = true
            self.butnEdit.isHidden = true
            self.lblAdd.isHidden = true
            self.butnAddPicture.isHidden = true
        }
        else
        {
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.animate), userInfo: nil, repeats: true)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.assignNewValueAtIndex(_:)), name: NSNotification.Name(rawValue: "getEditedImagesArray"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK: - IBActions
    
    @IBAction func didTapBackButn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapAddPictureButn(_ sender: Any)
    {
        var actualImgArray = [Para_ProfilePictures]()
        for image in imagesArray
        {
            if image.picMode != 0
            {
                actualImgArray.append(image)
            }
        }
        
        if actualImgArray.count >= 4 && sharedObj.userPara.isPlusMember != 1
        {
            let alert = UIAlertController.init(title: "GAY AND YOU?", message: "Add more pictures. Discover GAY AND YOU? Plus.", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "More", style: .default, handler:
                { alert -> Void in
                    
                    let upgradeVC : UpgradeViewController = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "UpgradeViewController") as! UpgradeViewController
                    
                    self.navigationController?.pushViewController(upgradeVC, animated: true)
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
                alert -> Void in
                //self.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController.init(title: "Choose from", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: {
                alert -> Void in
                
                let picker = UIImagePickerController()
                picker.allowsEditing = true
                picker.delegate = self
                picker.sourceType = .camera
                self.present(picker, animated: true, completion: nil)
                
            }))
            
            alert.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: {
                alert -> Void in
                
                
                let picker = UIImagePickerController()
                picker.allowsEditing = true
                picker.delegate = self
                
                picker.sourceType = .photoLibrary
                self.present(picker, animated: true, completion: nil)
                
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
                alert -> Void in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func didTapContinueButn(_ sender: Any)
    {
        var actualImgArray = [Para_ProfilePictures]()
        for image in imagesArray
        {
            if image.picMode != 0
            {
                actualImgArray.append(image)
            }
        }
        
        if actualImgArray.count == 0
        {
            self.showAlert(title: "Alert!", msg: "Please add at least 1 image to continue.")
            return
        }
        
        self.callUploadProfilePicturesAPI()
    }
    
    @IBAction func didTapSkipButn(_ sender: Any)
    {
        self.showLandingPage()
    }
    
    
    // MARK: - Cell IBAction
    @IBAction func didTapEditButn(_ sender: UIButton)
    {
        if sender.isSelected == false
        {
            sender.isSelected = true
            // Enable editing.
            self.isEditMode = true
            self.collectionView.reloadData()
        }
        else
        {
            sender.isSelected = false
            // Enable editing.
            self.isEditMode = false
            self.collectionView.reloadData()
        }
    }
    
    
    @IBAction func didTapStarButton(_ sender: UIButton)
    {
        let butnTag = sender.tag
        
        selectedProfileImage = butnTag
        self.collectionView.reloadData()
    }
    
    @IBAction func didTapDeleteButton(_ sender: UIButton)
    {
        let butnTag = sender.tag
        
        if imagesArray.count > butnTag
        {
            let para = self.imagesArray[butnTag]
            
            self.imagesArray.remove(at: butnTag)
            
            // Image is uploaded to server, so delete from there.
            if para.picUrl != ""
            {
                para.picMode = 2
                self.deletedArray.append(para)
            }
            
            self.collectionView.reloadData()
        }
    }
    
    
    // MARK: - Functions
    func animate()
    {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations:
            {
                self.butnAdd.alpha = 0.2
                self.lblAdd.alpha = 0.2
        },
                       completion:
            {
                (finished: Bool) -> Void in
                
                // Fade in
                UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations:
                    {
                        self.butnAdd.alpha = 1.0
                        self.lblAdd.alpha = 1.0
                        
                }, completion: nil)
        })
    }
    
    func assignNewValueAtIndex(_ notification: NSNotification)
    {
        if notification.userInfo != nil
        {
            guard  let array = notification.userInfo?["data"] as? [Para_ProfilePictures] else{
                return
            }

            imagesArray = array
            self.collectionView.reloadData()
        }
    }
    
    func showLandingPage()
    {
        if isFromEditDetailsScreen == false
        {
            let popoverContent = storyboards.mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            
            let rootVC = storyboards.mainStoryboard.instantiateViewController( withIdentifier: "LandingPage") as! LandingPage
            
            let menuvc = storyboards.mainStoryboard.instantiateViewController( withIdentifier: "Menu_VC") as! Menu_VC
            
            popoverContent.frontViewController = rootVC //mainvc
            popoverContent.rearViewController = menuvc
            
            self.navigationController?.pushViewController(popoverContent, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: - Collection view Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imagesArray.count + dummyArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePictureCollectionViewCell", for: indexPath as IndexPath) as! ProfilePictureCollectionViewCell
        let array = self.imagesArray + self.dummyArray
        
        cell.butnStar.tag = indexPath.row
        cell.butnDelete.tag = indexPath.row
        
        let para = array[indexPath.row]
        if indexPath.row == selectedProfileImage
        {
            cell.butnStar.isSelected = true
        }
        else
        {
            cell.butnStar.isSelected = false
        }
        
        if para.picUrl != ""
        {
            let url = URL(string: APP_URL.BASE_URL + para.picUrl )!
            let placeholderImage = #imageLiteral(resourceName: "avatar_big")
            let filter = AspectScaledToFillSizeFilter(size: CGSize(width: cell.bounds.size.width, height: cell.bounds.size.height))
            cell.imgView.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                { response in
                    
            })
        }
        else
        {
            cell.imgView.image = para.picImage
        }
        
        // Enable edit mode
        if isEditMode == true
        {
            cell.butnDelete.isHidden = false
            cell.butnStar.isHidden = true
        }
        else
        {
            cell.butnDelete.isHidden = true
            cell.butnStar.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing)
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(2))

        let array = self.imagesArray + self.dummyArray

        // Change the height for collection view according to number of cells
        self.heightCollectionView.constant = CGFloat(size * (array.count / 2 + array.count % 2))
        return CGSize(width: size-10, height: size-10)
    }
    
    func collectionView(collectinView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 1
    }
    
    // MARK: - UICOllection view Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if imagesArray.count == 0
        {
            return
        }
        
        let pictureSwipeVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewProfilePhotoViewController") as! ViewProfilePhotoViewController
        
        for image in imagesArray
        {
            if image.picMode != 0
            {
                pictureSwipeVC.dataArray.append(image)
            }
        }
        
        if pictureSwipeVC.dataArray.count > 0
        {
            pictureSwipeVC.isFriendProfile = isOthersProfile
            self.navigationController?.pushViewController(pictureSwipeVC, animated: true)
        }
    }
    
    // MARK: - View
    func callUploadProfilePicturesAPI()
    {
        var token = ""
        if UserDefaults.standard.value(forKey: "userJSON") != nil
        {
            let userJSON = UserDefaults.standard.value(forKey: "userJSON") as! [String : Any]
            
            token = userJSON["token"] as! String
        }
        let headerObject = ["authtoken" : token]
        var imgRequestArray = [[String : Any]]()
        let combinedArray = imagesArray + deletedArray
        
        for index in 0 ... combinedArray.count - 1
        {
            let data = combinedArray[index]
            
            // Some changes are made in image
            if data.picMode != 0
            {
                var base64String = ""
                if data.picUrl == ""
                {
                    //Now use image to create into NSData format
                    let imageData:NSData = UIImagePNGRepresentation(data.picImage)! as NSData
                    base64String = imageData.base64EncodedString(options: .lineLength64Characters)
                }
                else
                {
                    
                }
                
                let isProfPic = index == selectedProfileImage ? 1 : 0
                let jsonToInsert : [String : Any] = ["id" : data.picId,
                                                     "text" : data.picCaption,
                                                     "image_base_64" : base64String,
                                                     "operation_key" : data.picMode,
                                                     "make_prof_picture" : isProfPic]
                
                print(jsonToInsert["id"] as Any, jsonToInsert["text"] as Any, jsonToInsert["operation_key"] as Any)
                imgRequestArray.append(jsonToInsert)
            }
        }
        
        //print(imgRequestArray)
        
        if imgRequestArray.count > 0
        {
            let requestObject = ["image_data" : imgRequestArray]

            let loader = startActivityIndicator(view: self.view, waitingMessage: "Uploading Images")
            loader.startAnimating()

            GlobalConstants().postData_Header_Parameter(subUrl: PROFILE_PIC_API.ADD_IMAGE_ARRAY, header: headerObject, parameter: requestObject, completionHandler:
                {
                    (isSuccess,responseMessage,responseData) -> Void in

                    print(responseData)
                    loader.stopAnimating()

                    if isSuccess
                    {
                        let receivedArray = responseData["data"] as! [[String : Any]]
                        
                        if UserDefaults.standard.value(forKey: "userJSON") != nil
                        {
                            var userJSON = UserDefaults.standard.value(forKey: "userJSON") as! [String : Any]
                            userJSON["profile_ulbum"] = receivedArray
                            UserDefaults.standard.set(userJSON, forKey: "userJSON")
                            UserDefaults.standard.synchronize()
                        }
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }

                    self.showLandingPage()
            })
        }
        else
        {
            self.showLandingPage()
        }
    }
    
    func setProfilePictureById()
    {
        
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension AddProfilePicturesViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
            return // No image selected.
        }
        
        let para = Para_ProfilePictures()
        para.picImage = image
        para.picMode = 1
        self.imagesArray.append(para)
        
        if dummyArray.count != 0
        {
            dummyArray.remove(at: 0)
        }
        
        self.dismiss(animated: true, completion:
            { () -> Void in
                self.collectionView.reloadData()
        })
    }
}
