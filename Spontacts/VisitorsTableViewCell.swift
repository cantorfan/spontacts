//
//  VisitorsTableViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 15/08/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class VisitorsTableViewCell: UITableViewCell
{
    @IBOutlet weak var imgViewPhoto: UIImageView!

    @IBOutlet weak var lblTitleLbl: UILabel!
    @IBOutlet weak var lblVisitCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
