//
//  Message_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 06/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class Message_VC: UIViewController, UITableViewDataSource, UITableViewDelegate, SWRevealViewControllerDelegate
{
    @IBOutlet weak var btnAddMessage: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let sharedObj = Singleton.shared
    var arrConversations = [Para_Conversation]()
    let dateFormatter = DateFormatter()
    var chatParaToSend = Para_Conversation()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        addLeftBarButton()
        
        title = "Messages"
        view.backgroundColor = sharedObj.hexStringToUIColor(hex: "E8EFF1")
        tableView.backgroundColor = sharedObj.hexStringToUIColor(hex: "E8EFF1")
        
        print(DBManager.shared.pathToDatabase)
        arrConversations = DBManager.shared.getConversationTableData()
        
        dateFormatter.dateFormat = "dd-MMM hh:mm a"
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        callSyncingAPI()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLeftBarButton()
    {
        let btnMenu = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu_new"), style: .plain,  target: self, action: #selector(didTapMenuButn))
        navigationItem.leftBarButtonItem = btnMenu
        
        if revealViewController() != nil
        {
            revealViewController().rightViewRevealWidth = 200
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Action Methods
    @IBAction func didTapMenuButn()
    {
        revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func didTapAddMessageButn(_ sender: Any)
    {
        
    }
    
    // MARK: - API Methods
    func callSyncingAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Loading Messages")
        loader.startAnimating()
        
        let authToken : String = sharedObj.userPara.userToken
        print(authToken as Any)
        
        let headerObject = ["authtoken" : authToken]
        
        var last_synced_time : Double = 0
        if  UserDefaults.standard.value(forKey: "last_synced_time") != nil
        {
            last_synced_time = UserDefaults.standard.value(forKey: "last_synced_time") as! Double
        }
        
        let requestObject : [String : Any] = ["last_sync_time": last_synced_time] as [String : Any]
        
        GlobalConstants().postData_Header_Parameter(subUrl: MESSAGE_URL.SYNC_ALL_MSG, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                if isSuccess
                {
                    // This is last synced time
                    let last_synced_time = responseData["time"] as! Double
                    // Save this in userdefaults
                    UserDefaults.standard.set(last_synced_time, forKey: "last_synced_time")
                    UserDefaults.standard.synchronize()

                    if responseData["data"] != nil
                    {
                        let array = responseData["data"] as! NSArray
                        print("data fetched ", array.count)
                        self.fillDBFromSync(array: array.mutableCopy() as! NSMutableArray)
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func fillDBFromSync(array : NSMutableArray)
    {
        // CALL this api in app delegate 
        for item in array
        {
            let JSON : [String : Any] = item as! [String : Any]
            
            let chat_id = JSON["chat_id"] as! Int
            // Check if this chat id exist in db,
            let isChatExist = DBManager.shared.getConversationTableData_ChatId(chat_id: chat_id)
            
            if isChatExist == false
            {
                // NO -> 
                // 1. Insert chat data into Conversations table
                let chatPara = Para_Conversation()
                
//                let lastJSON = JSON["last_sync_message"] as! [String : Any]
//                chatPara.Last_Synced_Time = lastJSON["time"] as! Double
//                chatPara.Last_Message = lastJSON["message"] as! String
                chatPara.Last_Synced_Time = Date().timeIntervalSince1970
                //chatPara.Last_Message = "Ask this msg to send in API"
                chatPara.Id = chat_id
                
                let friendJSON = JSON["friend_data"] as! [String : Any]
                chatPara.Friend_Id = friendJSON["id"] as! Int
                let fname =  friendJSON["fname"] as! String
                let lname =  friendJSON["lname"] as! String
                chatPara.Friend_name = fname + " " + lname
                chatPara.thumb = friendJSON["thumb"] as! String
                
                DBManager.shared.insertConversationTable(para: chatPara)
                
                //2. Insert all megs in Message table
                let msgsJSONArray = JSON["messages"] as! NSArray
                
                for msgItem in msgsJSONArray
                {
                    let msgJSON : [String : Any] = msgItem as! [String : Any]
                    
                    let msgPara = Para_Message()
                    msgPara.Chat_Id = chat_id
                    msgPara.Id = msgJSON["message_id"] as! Int
                    msgPara.Message = msgJSON["message"] as! String
                    msgPara.Time = msgJSON["time"] as! Double
                    msgPara.Sender_Id = msgJSON["sender"] as! Int
                    msgPara.Receiver_Id = msgJSON["reciever"] as! Int
                    
                    DBManager.shared.insertMessageTable(para: msgPara)
                }
            }
            else
            {
                // YES -> 
                
                // 1. Update Conversation table with last synced time
                let chatPara = Para_Conversation()
                chatPara.Last_Synced_Time = Date().timeIntervalSince1970
                //chatPara.Last_Message = "Ask this msg to send in API"
                chatPara.Id = chat_id
                
                let friendJSON = JSON["friend_data"] as! [String : Any]
                chatPara.Friend_Id = friendJSON["id"] as! Int
                let fname =  friendJSON["fname"] as! String
                let lname =  friendJSON["lname"] as! String
                chatPara.Friend_name = fname + " " + lname
                chatPara.thumb = friendJSON["thumb"] as! String
                
                DBManager.shared.updateConversationTable(para: chatPara)
                
                // 2. Insert all msgs received as these are new messages
                let msgsJSONArray = JSON["messages"] as! NSArray
                
                for msgItem in msgsJSONArray
                {
                    let msgJSON : [String : Any] = msgItem as! [String : Any]
                    
                    let msgPara = Para_Message()
                    msgPara.Chat_Id = chat_id
                    msgPara.Id = msgJSON["message_id"] as! Int
                    msgPara.Message = msgJSON["message"] as! String
                    msgPara.Time = msgJSON["time"] as! Double
                    msgPara.Sender_Id = msgJSON["sender"] as! Int
                    msgPara.Receiver_Id = msgJSON["reciever"] as! Int
                    
                    DBManager.shared.insertMessageTable(para: msgPara)
                }
            }
        }
        
        arrConversations = DBManager.shared.getConversationTableData()
        tableView.reloadData()
    }
    
    // MARK: - TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrConversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : MessageListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MessageListTableViewCell", for: indexPath) as! MessageListTableViewCell
        
        var chatPara = Para_Conversation()
        chatPara = arrConversations[indexPath.row]
        
        cell.lblUserName.text = chatPara.Friend_name
        
        let strUrl = chatPara.thumb
        
        let url = URL(string: APP_URL.BASE_URL + strUrl )!
        let placeholderImage = #imageLiteral(resourceName: "avatar")
        let filter = AspectScaledToFitSizeFilter(size: CGSize(width: 60.0, height: 60.0))
        
        cell.imgViewUser.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
            { response in
        })
        
        let messagePara : Para_Message = DBManager().getMessageTableData_LastMessage(coversation_Id: chatPara.Id)
        
        let date = Date.init(timeIntervalSince1970: TimeInterval(messagePara.Time))
        cell.lblTime.text = dateFormatter.string(from: date)
        cell.lblMessagePreview.text = messagePara.Message

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        chatParaToSend = arrConversations[indexPath.row]
        self.performSegue(withIdentifier: "segueMessageListToCreateMsg", sender: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueMessageListToCreateMsg"
        {
            let msgVC : MessageViewController = segue.destination as! MessageViewController
            msgVC.chatPara = chatParaToSend
        }
    }
    
    
    //controls the frontView when the menu is opened.
    //creates a mask that dims the view and blocks any interaction except for tap or pan to close the menu.
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition){
        let tagId = 42078
        
        if revealController.frontViewPosition == FrontViewPosition.right {
            let lock = self.view.viewWithTag(tagId)
            UIView.animate(withDuration: 0.25, animations: {
                lock?.alpha = 0
            }, completion: {(finished: Bool) in
                lock?.removeFromSuperview()
            }
            )
            lock?.removeFromSuperview()
        } else if revealController.frontViewPosition == FrontViewPosition.left {
            let lock = UIView(frame: self.view.bounds)
            lock.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lock.tag = tagId
            lock.alpha = 0
            lock.backgroundColor = UIColor.black
            lock.addGestureRecognizer(UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:))))
            self.view.addSubview(lock)
            UIView.animate(withDuration: 0.5, animations: {
                lock.alpha = 0.333
            }
            )
        }
    }
}
