//
//  WriteComment_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 26/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import Foundation
import SlackTextViewController
import AlamofireImage

class WriteComment_VC: SLKTextViewController//, UITableViewDelegate, UITableViewDataSource
{
    //@IBOutlet weak var tableView: UITableView!

    var arrComments = NSMutableArray()
    let sharedObj = Singleton.shared
    let dateFormatter = DateFormatter()
    
    var activityId = Int()
    var isCommentAdded = false
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.textInputbar.textView.placeholder = "Your Comment"
        self.textInputbar.autoHideRightButton = false
        
        dateFormatter.dateFormat = "dd-MMM-YYYY hh:mm a"
//        tableView?.delegate = self
//        tableView?.dataSource = self
        
//        tableView?.estimatedRowHeight = 70
//        tableView?.rowHeight = UITableViewAutomaticDimension
        
                tableView?.rowHeight = 70
    }

    override func viewWillAppear(_ animated: Bool)
    {
        callGetAllCommentAPI()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        if isCommentAdded == true
        {
            let notificationName = Notification.Name("commentAdded")

            // Post notification
            NotificationCenter.default.post(name: notificationName, object: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didPressRightButton(_ sender: Any?) {
        
        self.view.endEditing(true)
        if let messageToSend = self.textInputbar.textView.text {
            
            // Save messageToSend to db
            self.textInputbar.textView.text = ""
            super.didPressRightButton(sender)
            print(messageToSend)
            
            self.isCommentAdded = true
            callAddCommentAPI(commentStr: messageToSend)
        }
    }
    
    // MARK: - API methods
    func callAddCommentAPI(commentStr : String)
    {
        let authToken : String = sharedObj.userPara.userToken
        print(authToken as Any)
        
        let headerObject = ["authtoken" : authToken]
        let requestObject : [String : Any] = [ "activity_id": activityId,
            "comment" : commentStr
        ] as [String : Any]
        
        let loader = startActivityIndicator(view: self.view, waitingMessage: "Please wait")
        loader.startAnimating()
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.ADD_ACT_COMMENT, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            if isSuccess
            {
                //self.showAlert(title: "Success!", msg: responseMessage)
                let commentsData : [String:Any] = responseData["data"] as! [String:Any]
                self.arrComments.insert(commentsData, at: 0)
                self.tableView?.reloadData()
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func callGetAllCommentAPI()
    {
        let authToken : String = sharedObj.userPara.userToken
        print(authToken as Any)
        
        let headerObject = ["authtoken" : authToken]
        let requestObject : [String : Any] = [ "activity_id": activityId ] as [String : Any]
        
        let loader = startActivityIndicator(view: self.view, waitingMessage: "")
        loader.startAnimating()
        
        GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.GET_ACTIVITY_COMMENTS, header: headerObject, parameter: requestObject, completionHandler: {
            (isSuccess,responseMessage,responseData) -> Void in
            
            loader.stopAnimating()
            print(responseData)
            if isSuccess
            {
                //self.showAlert(title: "Success!", msg: responseMessage)
                if responseData["comments"] != nil
                {
                    let array = responseData["comments"] as! NSArray
                    print("comments count ", array.count)
                    self.arrComments = array.mutableCopy() as! NSMutableArray
                    self.tableView?.reloadData()
                }
            }
            else
            {
                self.showAlert(title: "Fail!", msg: responseMessage)
            }
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - @override existing methods in SLKViewController.
    // It has in built tableView
    
    // MARK: - TableView Datasource and Delegates
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrComments.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()//.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        headerView.backgroundColor = UIColor.clear
        
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        
        var imgDp = UIImageView()
        var lblName = UILabel()
        var lblCommentStr = UILabel()
        var lblDate = UILabel()
        
        if (cell == nil)
        {
            cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
            cell?.selectionStyle = UITableViewCellSelectionStyle.gray
            cell?.transform = tableView.transform
            
            imgDp = UIImageView.init(frame: CGRect(x: 10, y: 5, width: 30, height: 30))
            imgDp.backgroundColor = UIColor.white
            imgDp.layer.cornerRadius = imgDp.frame.size.width / 2
            imgDp.layer.borderColor = UIColor.white.cgColor
            imgDp.image = #imageLiteral(resourceName: "avatar")
            imgDp.tag = 1
            
            lblName = UILabel(frame: CGRect(x: imgDp.frame.origin.x + imgDp.frame.size.width + 10 , y: 5, width: self.view.frame.size.width / 2 - 50, height: 30))
            lblName.textAlignment = .left
            lblName.font = UIFont.init(name: Font.BEBAS, size: 14)
            lblName.tag = 2
            
            lblDate = UILabel(frame: CGRect(x: self.view.frame.size.width / 2 , y: 5, width: self.view.frame.size.width / 2 - 10, height: 30))
            lblDate.textAlignment = .right
            lblDate.font = UIFont.init(name: Font.BEBAS, size: 14)
            lblDate.tag = 3
            
            lblCommentStr = UILabel(frame: CGRect(x: lblName.frame.origin.x, y: lblName.frame.origin.y + lblName.frame.size.height, width: self.view.frame.size.width - 50, height: 35))
            lblCommentStr.textAlignment = .left
            lblCommentStr.font = UIFont.init(name: Font.BEBAS, size: 18)
            lblCommentStr.numberOfLines = 0
            lblCommentStr.lineBreakMode = .byWordWrapping
            lblCommentStr.adjustsFontSizeToFitWidth = true
            lblCommentStr.tag = 4
            
            cell?.contentView.addSubview(imgDp)
            cell?.contentView.addSubview(lblName)
            cell?.contentView.addSubview(lblCommentStr)
            cell?.contentView.addSubview(lblDate)
        }
        else
        {
            imgDp = cell?.contentView.viewWithTag(1) as! UIImageView
            lblName = cell?.contentView.viewWithTag(2) as! UILabel
            lblDate = cell?.contentView.viewWithTag(3) as! UILabel
            lblCommentStr = cell?.contentView.viewWithTag(4) as! UILabel
        }
        
        let commentJSON = arrComments[indexPath.row] as! [String : Any]
        
        let pictureURL : String = commentJSON["thumb"] as! String
        if pictureURL != ""
        {
            let url = URL(string: APP_URL.BASE_URL + pictureURL )!
            let placeholderImage = #imageLiteral(resourceName: "avatar")
            let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
            
            //,filter: filter
            imgDp.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                { response in
            }
            )
        }
        
        let firstName : String = commentJSON["fname"] as! String
        let lastName : String = commentJSON["lname"] as! String
        lblName.text = firstName + " "  + lastName

        lblCommentStr.text = commentJSON["comment"] as? String

        let commDate = Date.init(timeIntervalSince1970: commentJSON["time"] as! Double)
        lblDate.text = dateFormatter.string(from: commDate)
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfile_VC") as! UserProfile_VC

        let commentJSON = arrComments[indexPath.row] as! [String : Any]
        presentVC.user_Id = commentJSON["commented_by"] as! Int
        // Will allow to go back
        self.navigationController?.pushViewController(presentVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
