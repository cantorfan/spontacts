//
//  CreateAccountOptionsViewController.swift
//  Spontacts
//
//  Created by maximess142 on 30/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import FirebaseInstanceID
import FacebookCore
import FacebookLogin

class CreateAccountOptionsViewController: UIViewController, UITextViewDelegate, UIDocumentInteractionControllerDelegate
{
    @IBOutlet weak var txtViewTerms: UITextView!
    
    // MARK: - View Delegates
    let sharedObj = Singleton.shared
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Hide the navigation bar on this view only.
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        let signin : GIDSignIn = GIDSignIn.sharedInstance()
        signin.shouldFetchBasicProfile = true
        
        // @pratiksha - sign out previously signed in Google User
        GIDSignIn.sharedInstance().signOut()
    }
    
    // As soon as view is dismissed, bring back the navigation bar
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    // MARK: - IBActions
    
    @IBAction func didTapBackButn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapFacebookButn(_ sender: Any)
    {
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile, .email , .userFriends], viewController: self)
        { loginResult in
            switch loginResult
            {
            case .failed(let error):
                print(error.localizedDescription)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                // print("Granted permissions -", grantedPermissions)
                
                // print("Declined permissions -", declinedPermissions)
                print("Access token - ", accessToken)
                
                if AccessToken.current != nil
                {
                    print("Got access token")
                    self.callFacebookLoginAPI(fbToken: "\(accessToken.authenticationToken)")
                }
            }
        }
    }
    
    @IBAction func didTapTwitterButn(_ sender: Any)
    {
        Twitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil)
            {
                print("signed in as ", (session?.authToken)!, (session?.authTokenSecret)!);
                
                self.callTwitterLoginAPI(authToken: (session?.authToken)!, authSecret: (session?.authTokenSecret)!)
            }
            else
            {
                print("error: ", error?.localizedDescription ?? "");
            }
        })
    }
    
    @IBAction func didTapGoogleButn(_ sender: Any)
    {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func didTapCreateAccountButn(_ sender: Any)
    {
        /*
         isGoingToRegisterFb = false
         isGoingToRegisterGoogle = false
         
         performSegue(withIdentifier: "segueLogInToRegistration", sender: nil)*/
        
        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "Registration") as! Registration
        
        presentVC.isRegisterFb = false
        presentVC.isRegisterGoogle = false
        presentVC.isRegisterTwitter = false
        // Will allow to go back
        self.navigationController?.pushViewController(presentVC, animated: true)
    }
    
    // MARK: - Custom Methods
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    func showDocument()
    {
        if let url = Bundle.main.url(forResource: "GAYANDYOU_terms_of_use_August_2017", withExtension: "pdf", subdirectory: nil, localization: nil)
        {
            let documentInteractionController = UIDocumentInteractionController(url: url)
            documentInteractionController.delegate = self
            documentInteractionController.presentPreview(animated: false)
        }
    }
    
    // MARK: - API methods
    func callGoogleLoginAPI(glToken : String)
    {
        print("Calling Google API")
        print("google -" ,glToken)
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Logging In")
        loader.startAnimating()
        
        if UserDefaults.standard.value(forKey: "fcmDeviceToken") == nil
        {
            let fcmDeviceToken = FIRInstanceID.instanceID().token()
            print("FCM token: \(fcmDeviceToken ?? "")")
            UserDefaults.standard.setValue(fcmDeviceToken, forKey: "fcmDeviceToken")
            UserDefaults.standard.synchronize()
        }
        
        //header - username, password, devicetype, devicetoken
        let deviceToken = UserDefaults.standard.value(forKey: "fcmDeviceToken") as! String
        let headerObject : [String : String] = ["devicetoken": deviceToken, "devicetype" : "1"]
        let requestObject = ["socialtoken": glToken]
        
        GlobalConstants().postData_Header_Parameter(subUrl: APP_URL.SOCIAL_LOGIN_GOOGLE, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    print(responseData)
                    
                    let token = responseData["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(responseData, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    let landingVC = self.storyboard?.instantiateViewController(withIdentifier: "LandingPage") as! LandingPage
                    self.navigationController?.pushViewController(landingVC, animated: true)
                }
                else
                {
                    let response = responseData["status"] as! Int
                    if response == 404
                    {
                        // User not registered
                        let userJSON = responseData["user"] as! [String : Any]
                        
                        let userObjToPass = Para_UserProfile()
                        
                        userObjToPass.userEmail = userJSON["email"] as! String
                        userObjToPass.userFirstName = userJSON["fname"] as! String
                        userObjToPass.userPictureBase64 = userJSON["image_base_64"] as! String
                        
                        let regiPara = Para_SocialRegistration()
                        regiPara.socialToken = userJSON["social_token"] as! String
                        
                        let registration_VC = self.storyboard?.instantiateViewController(withIdentifier: "Registration") as! Registration
                        registration_VC.isRegisterGoogle = true
                        registration_VC.userPara = userObjToPass
                        registration_VC.paraSocial = regiPara

                        if registration_VC.userPara.userPictureBase64 != ""
                        {
                            if let decodedData = Data(base64Encoded: registration_VC.userPara.userPictureBase64, options: .ignoreUnknownCharacters) {
                                registration_VC.userPara.userPictureImage = UIImage(data: decodedData)!
                            }
                        }
                        
                        self.navigationController?.pushViewController(registration_VC, animated: true)
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
                }
        })
    }
    
    func callTwitterLoginAPI(authToken : String, authSecret : String)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Logging In")
        loader.startAnimating()
        
        let deviceToekn = GlobalConstants().getFCMToken()
        let headerObject = ["accesstoken" : authToken,
                            "accesstokensecret" : authSecret,
                            "devicetoken": deviceToekn,
                            "devicetype" : "1"]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: APP_URL.SOCIAL_LOGIN_TWITTER, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)
                
                if isSuccess
                {
                    let userJSON = responseData["user"] as! [String : Any]
                    
                    let token = userJSON["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(userJSON, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    let landingVC = self.storyboard?.instantiateViewController(withIdentifier: "LandingPage") as! LandingPage
                    self.navigationController?.pushViewController(landingVC, animated: true)
                }
                else
                {
                    let response = responseData["status"] as! Int
                    if response == 404
                    {
                        // User not registered
                        let userJSON = responseData["user"] as! [String : Any]
                        
                        let userObjToPass = Para_UserProfile()
                        
                        userObjToPass.userEmail = userJSON["email"] as! String
                        userObjToPass.userFirstName = userJSON["fname"] as! String
                        userObjToPass.userPictureBase64 = userJSON["image_base_64"] as! String
                        
                        let regiPara = Para_SocialRegistration()
                        regiPara.access_token = userJSON["access_token"] as! String
                        regiPara.access_token_secret = userJSON["access_token_secret"] as! String
                        regiPara.twitterId = userJSON["twitter_id"] as! Int
                        
                        
                        let registration_VC = self.storyboard?.instantiateViewController(withIdentifier: "Registration") as! Registration
                        registration_VC.isRegisterTwitter = true
                        registration_VC.userPara = userObjToPass
                        registration_VC.paraSocial = regiPara
                        if registration_VC.userPara.userPictureBase64 != ""
                        {
                            if let decodedData = Data(base64Encoded: registration_VC.userPara.userPictureBase64, options: .ignoreUnknownCharacters) {
                                registration_VC.userPara.userPictureImage = UIImage(data: decodedData)!
                            }
                        }

                        self.navigationController?.pushViewController(registration_VC, animated: true)
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
                }
        })
    }
    
    func callFacebookLoginAPI(fbToken : String)
    {
        print("Celling login API")
        print("fb -" ,fbToken)
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Logging In")
        loader.startAnimating()
        
        let headerObject = ["socialtoken":fbToken]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: APP_URL.SOCIAL_LOGIN_FACEBOOK, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                
                if isSuccess
                {
                    print(responseData)
                    
                    let userJSON = responseData["user"] as! [String : Any]
                    
                    let token = userJSON["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(userJSON, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    let landingVC = self.storyboard?.instantiateViewController(withIdentifier: "LandingPage") as! LandingPage
                    self.navigationController?.pushViewController(landingVC, animated: true)
                }
                else
                {
                    
                    let response = responseData["status"] as! Int
                    if response == 404
                    {
                        // User not registered
                        let userJSON = responseData["user"] as! [String : Any]
                        
                        let userObjToPass = Para_UserProfile()
                        
                        userObjToPass.userEmail = userJSON["email"] as! String
                        userObjToPass.userFirstName = userJSON["fname"] as! String
                        userObjToPass.userPictureBase64 = userJSON["image_base_64"] as! String
                        
                        let regiPara = Para_SocialRegistration()
                        regiPara.facebookId = userJSON["facebook_id"] as! String
                        regiPara.socialToken = userJSON["social_token"] as! String
                        
                        let registration_VC = self.storyboard?.instantiateViewController(withIdentifier: "Registration") as! Registration
                        registration_VC.isRegisterFb = true
                        registration_VC.userPara = userObjToPass
                        registration_VC.paraSocial = regiPara
                        if registration_VC.userPara.userPictureBase64 != ""
                        {
                            if let decodedData = Data(base64Encoded: registration_VC.userPara.userPictureBase64, options: .ignoreUnknownCharacters) {
                                registration_VC.userPara.userPictureImage = UIImage(data: decodedData)!
                            }
                        }
                        
                        self.navigationController?.pushViewController(registration_VC, animated: true)
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
                }
        })
    }

    
    // MARK: - TextView Delegates
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        if (URL.absoluteString == termsAndConditionsURL)
        {
            self.showDocument()//(title: "Go to Terms", msg: "")
        }
        else if (URL.absoluteString == paymentsURL)
        {
            self.showDocument() //(title: "Go to Payment", msg: "")
        }
        else if (URL.absoluteString == privacyURL)
        {
            self.showDocument()//(title: "Go to Privacy", msg: "")
        }
        else if (URL.absoluteString == nonDiscriminationURL)
        {
            self.showDocument()//(title: "Go to Nondiscrimination", msg: "")
        }
        
        return false
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Google sign in delegates
extension CreateAccountOptionsViewController : GIDSignInDelegate, GIDSignInUIDelegate
{
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!)
    {
        // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!)
    {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if (error == nil)
        {
            // Perform any operations on signed in user here.
            let idToken = user.authentication.idToken // Safe to send to the server
            
            callGoogleLoginAPI(glToken: idToken!)
        }
        else
        {
            print("\(error.localizedDescription)")
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!)
    {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

}
