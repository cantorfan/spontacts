//
//  Para_Conversation.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 15/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class Para_Conversation: NSObject
{
    var Id = Int()
    var Friend_Id = Int()
    var Last_Synced_Time = Double()
    var Friend_name = String()
    var thumb = String()
    var Last_Message = String()
}
