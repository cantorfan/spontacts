//
//  ViewProfilePhotoViewController.swift
//  Spontacts
//
//  Created by maximess142 on 12/10/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class ViewProfilePhotoViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate
{

    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    @IBOutlet weak var butnLeftCross: UIButton!
    @IBOutlet weak var lblTopIndex: UILabel!
    
    @IBOutlet weak var butnRightCross: UIButton!
    
    @IBOutlet weak var butnRightArrow: UIButton!
    
    @IBOutlet weak var butnLeftArrow: UIButton!
    
    @IBOutlet weak var viewEditButn: UIView!
    
    @IBOutlet weak var viewPictureTitle: UIView!
    
    @IBOutlet weak var lblCaption: UILabel!
    
    @IBOutlet weak var butnDelete: UIButton!
    @IBOutlet weak var butnEditCaption: UIButton!
    
    
    //var imageArray = [#imageLiteral(resourceName: "fotolia_122634194.jpg"), #imageLiteral(resourceName: "food_drinks.png")] //:[String] = [String]()
    
    var dataArray = [Para_ProfilePictures]()
    
    var getCurrentPage = Int()
    var isFriendProfile = false

    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.assignNewValueAtIndex(_:)), name: NSNotification.Name(rawValue: "profilePicturesChanged"), object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(self.assignNewValueAtIndex(_:)), name: NSNotification.Name(rawValue: "viewImageAtIndex"), object: nil)

        assignValueToLabels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Functions
    func assignValueToLabels()
    {
        //        self.butnLeftArrow.isHidden = false
        //        self.butnLeftCross.isHidden = false
        
        if dataArray.count == 1
        {
            self.butnLeftCross.isHidden = false
            self.butnLeftArrow.isHidden = true
            self.butnRightArrow.isHidden = true
            self.butnRightCross.isHidden = true
        }
        else
        {
            // First image
            if getCurrentPage == 0
            {
                self.butnLeftCross.isHidden = false
                self.butnLeftArrow.isHidden = true
                self.butnRightArrow.isHidden = false
                self.butnRightCross.isHidden = true
            }
                // Last image
            else if getCurrentPage == dataArray.count - 1
            {
                self.butnLeftCross.isHidden = true
                self.butnLeftArrow.isHidden = false
                self.butnRightArrow.isHidden = true
                self.butnRightCross.isHidden = false
            }
            else
            {
                self.butnLeftCross.isHidden = true
                self.butnRightCross.isHidden = true
                
                self.butnRightArrow.isHidden = false
                self.butnLeftArrow.isHidden = false
            }
        }
        
        
        self.lblTopIndex.text = "\(getCurrentPage + 1)/\(dataArray.count)"
        
        let data = dataArray[getCurrentPage]
        let caption = data.picCaption
        
        if caption == ""
        {
            if isFriendProfile == true
            {
                self.viewEditButn.isHidden = true
                self.butnDelete.isHidden = true
            }
            else // self profile
            {
                self.viewEditButn.isHidden = false
                self.viewPictureTitle.isHidden = true
            }
            
            self.butnEditCaption.isHidden = true
        }
        else
        {
            self.lblCaption.text = caption
            self.viewEditButn.isHidden = true
            self.viewPictureTitle.isHidden = false
            self.butnEditCaption.isHidden = false
        }
    }
    
    func assignNewValueAtIndex(_ notification: NSNotification)
    {
        /*
        if notification.userInfo != nil
        {
            guard  let index = notification.userInfo?["index"] as? Int else{
                return
            }
            
            guard  let imageSent = notification.userInfo?["image"] as? UIImage else{
                return
            }
            
            getCurrentPage = index
            dataArray[getCurrentPage]["image"] = imageSent
            assignValueToLabels()
            self.imageCollectionView.reloadData()
        }*/
    }
    
    func viewImageAtIndex(_ notification: NSNotification)
    {
        if notification.userInfo != nil
        {
            guard  let index = notification.userInfo?["index"] as? Int else{
                return
            }

            getCurrentPage = index
            assignValueToLabels()
            self.imageCollectionView.reloadData()
        }
    }
    
    func showGetMsgAlert()
    {
        let alertController = UIAlertController.init(title: "Write something about this photo!", message: "Try to limit it to 80 characters", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: configurationTextField)
        
        
        let confirmAction = UIAlertAction(title: "Done", style: .default) { (_) in
            if let field = alertController.textFields?[0]
            {
                let data = self.dataArray[self.getCurrentPage]
                data.picCaption = field.text!
                
                if data.picMode == 0
                {
                    data.picMode = 3
                }
//                else
//                {
//                    data.picMode = 1
//                }
                
                self.dataArray[self.getCurrentPage] = data
                self.imageCollectionView.reloadData()
            }
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .destructive, handler: {
            alert -> Void in
            
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func configurationTextField(textField: UITextField!)
    {
        textField.placeholder = "Write here"
        textField.borderStyle = .none
        textField.delegate = self
        textField.tag = 100
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 80 // Bool
    }
    
    // MARK: - IBActions
    
    @IBAction func didTapCross(_ sender: Any)
    {
        if isFriendProfile == true
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
//            let viewPicVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowPhotoGridViewController") as! ShowPhotoGridViewController
//
//            viewPicVC.dataArray = dataArray
//            self.navigationController?.pushViewController(viewPicVC, animated: true)
            
            let dictToSend = ["data" : dataArray] as [String : Any]
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getEditedImagesArray"), object: nil, userInfo: dictToSend)
            
            self.navigationController?.popViewController(animated: true)

        }
    }
    
    @IBAction func didTapRightArrow(_ sender: Any)
    {
        getCurrentPage = getCurrentPage + 1
        
        assignValueToLabels()
        
        let indexToScrollTo = IndexPath(item: getCurrentPage, section: 0)
        self.imageCollectionView.scrollToItem(at: indexToScrollTo, at: .centeredHorizontally, animated: false)
    }
    
    @IBAction func didTapLeftArrow(_ sender: Any)
    {
        getCurrentPage = getCurrentPage - 1
        
        assignValueToLabels()
        
        let indexToScrollTo = IndexPath(item: getCurrentPage, section: 0)
        self.imageCollectionView.scrollToItem(at: indexToScrollTo, at: .centeredHorizontally, animated: false)
    }
    
    @IBAction func didTapDeletePicture(_ sender: Any)
    {
        let alert = UIAlertController.init(title: nil, message: "Are you sure you want to delete this picture?", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Delete", style: .destructive, handler: {
            Void in
            //self.callDeleteGroupAPI()
            
            self.dataArray.remove(at: self.getCurrentPage)
            self.assignValueToLabels()
            self.imageCollectionView.reloadData()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: {
            Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didTapEditCaption(_ sender: Any)
    {
        self.showGetMsgAlert()
    }
    
    
    //MARK: - UIScrollView Delegate
    
    var  lastContentOffset = CGFloat()
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        //        if progressPhotoFlag  {
        self.view.endEditing(true)
        
        var visibleRect = CGRect()
        
        visibleRect.origin = imageCollectionView.contentOffset
        visibleRect.size = imageCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        let visibleIndexPath: IndexPath = imageCollectionView.indexPathForItem(at: visiblePoint)!
        
        //        currentPageIndex = visibleIndexPath.row
        
        getCurrentPage = visibleIndexPath.row
        
        assignValueToLabels()

        /*if let caption = self.imageArray[visibleIndexPath.row]["file_caption"] as? String
        {
            lblTitle.text=caption
        }*/
        
        //self.pageController?.currentPage = getCurrentPage
        //self.addRightBarButton()
    }

    // MARK: - Collection view Delegates
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: self.imageCollectionView.frame.size.width, height: self.imageCollectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : ImageSliderCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageSlider", for: indexPath) as! ImageSliderCollectionViewCell
        
        let data = dataArray[indexPath.row]
        
        if data.picUrl != ""
        {
            cell.imageToZoom.af_setImage(withURL: URL.init(string: APP_URL.BASE_URL + data.picUrl)!, placeholderImage: #imageLiteral(resourceName: "image_placeolder"), filter: nil, imageTransition: .crossDissolve(0.2), completion:
                { response in
            })
        }
        else //if data.picImage != nil
        {
            cell.imageToZoom.image = data.picImage
        }

        assignValueToLabels()

        return cell
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
