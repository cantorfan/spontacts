//
//  ShowActivityHeaderTableViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 26/10/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class ShowActivityHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var butnCategoryImg: UIButton!
    @IBOutlet weak var imgViewCatImg: UIImageView!
    
    @IBOutlet weak var viewExpired: UIView!
    
    @IBOutlet weak var butnUserImage: UIButton!
    
    @IBOutlet weak var imgViewUserImage: UIImageView!
    @IBOutlet weak var butnHeartWings: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
