//
//  Login_VC.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 5/24/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import SafariServices
import FirebaseInstanceID

class Login_VC: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, UITextViewDelegate, UIDocumentInteractionControllerDelegate
{

    var window: UIWindow?
    // MARK: - IBOutlet Properties
    @IBOutlet weak var imgViewBgGradient: UIImageView!
    @IBOutlet weak var butnSignUpNow: UIButton!
    @IBOutlet weak var butnLogInFacebook: UIButton!
    @IBOutlet weak var butnLogInGoogle: UIButton!
    @IBOutlet weak var butnLogInTwitter: UIButton!
    @IBOutlet weak var butnLogInWeChat: UIButton!
    
    @IBOutlet weak var butnLogIn: UIButton!
    @IBOutlet weak var txtTerms: UITextView!
    
    let sharedObj = Singleton.shared
    var userObjToPass = Para_UserProfile()
    var regiPara = Para_SocialRegistration()
    var isGoingToRegisterFb = Bool()
    var isGoingToRegisterGoogle = Bool()
    var isGoingToRegisterTwitter = Bool()
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        addAttributedText()

        //setGradientBackground()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Hide the navigation bar on this view only.
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        let signin : GIDSignIn = GIDSignIn.sharedInstance()
        signin.shouldFetchBasicProfile = true
        
        // @pratiksha - sign out previously signed in Google User
        GIDSignIn.sharedInstance().signOut()
    }
    
    // As soon as view is dismissed, bring back the navigation bar
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    // MARK: - Custom Methods
    func addAttributedText()
    {
        self.txtTerms.delegate = self
        
        self.txtTerms = Singleton.shared.addAttributedText(txtTerms: self.txtTerms)
        self.txtTerms.textColor = UIColor.white
    }
    
    func checkIfAlreadyLOggedIn()
    {
        var loginState = 0

        if UserDefaults.standard.value(forKey: "userToken") != nil
        {
            let authToken : String = UserDefaults.standard.value(forKey: "userToken") as! String
            if authToken != ""
            {
                loginState = 1
            }
            else
            {
                loginState = 0
            }
        }
        
        if loginState == 1
        {
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let popoverContent = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            
            let menuvc = mainStoryboard.instantiateViewController(withIdentifier: "Menu_VC") as! Menu_VC
            
            let mainvc = mainStoryboard.instantiateViewController(withIdentifier: "LandingPage") as! LandingPage
            
            popoverContent.frontViewController = mainvc
            popoverContent.rearViewController = menuvc
            
            let nav = UINavigationController(rootViewController: popoverContent)
            window?.rootViewController = nav
            
            performSegue(withIdentifier: "segueLoginToLandingPage", sender: self)
            
        }
    }
    
    func callFacebookLoginAPI(fbToken : String)
    {
        print("Celling login API")
        print("fb -" ,fbToken)
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Logging In")
        loader.startAnimating()
                
        let headerObject = ["socialtoken":fbToken]

        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: APP_URL.SOCIAL_LOGIN_FACEBOOK, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                
                if isSuccess
                {
                    print(responseData)
                    
                    let userJSON = responseData["user"] as! [String : Any]
                    
                    let token = userJSON["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(userJSON, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    self.performSegue(withIdentifier: "segueLoginToLandingPage", sender: self)
                }
                else
                {
                 
                    let response = responseData["status"] as! Int
                    if response == 404
                    {
                        // User not registered
                        let userJSON = responseData["user"] as! [String : Any]
                        
                        self.userObjToPass = Para_UserProfile()
                        
                        self.userObjToPass.userEmail = userJSON["email"] as! String
                        self.userObjToPass.userFirstName = userJSON["fname"] as! String
                        self.userObjToPass.userPictureBase64 = userJSON["image_base_64"] as! String

                        self.regiPara = Para_SocialRegistration()
                        self.regiPara.facebookId = userJSON["facebook_id"] as! String
                        self.regiPara.socialToken = userJSON["social_token"] as! String
                        
                        self.isGoingToRegisterFb = true
                        self.performSegue(withIdentifier: "segueLogInToRegistration", sender: nil)
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
                }
        })
    }

    func callGoogleLoginAPI(glToken : String)
    {
        print("Calling Google API")
        print("google -" ,glToken)
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Logging In")
        loader.startAnimating()
        
        if UserDefaults.standard.value(forKey: "fcmDeviceToken") == nil
        {
            let fcmDeviceToken = FIRInstanceID.instanceID().token()
            print("FCM token: \(fcmDeviceToken ?? "")")
            UserDefaults.standard.setValue(fcmDeviceToken, forKey: "fcmDeviceToken")
            UserDefaults.standard.synchronize()
        }
        
        //header - username, password, devicetype, devicetoken
        let deviceToken = FIRInstanceID.instanceID().token()
        
        let headerObject : [String : String] = ["devicetoken": deviceToken!, "devicetype" : "1"]
        let requestObject = ["socialtoken": glToken]
        
        GlobalConstants().postData_Header_Parameter(subUrl: APP_URL.SOCIAL_LOGIN_GOOGLE, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    print(responseData)
                    
                    let token = responseData["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(responseData, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    self.performSegue(withIdentifier: "segueLoginToLandingPage", sender: self)
                }
                else
                {
                    let response = responseData["status"] as! Int
                    if response == 404
                    {
                        // User not registered
                        let userJSON = responseData["user"] as! [String : Any]
                        
                        self.userObjToPass = Para_UserProfile()
                        
                        self.userObjToPass.userEmail = userJSON["email"] as! String
                        self.userObjToPass.userFirstName = userJSON["fname"] as! String
                        self.userObjToPass.userPictureBase64 = userJSON["image_base_64"] as! String
                        
                        self.regiPara = Para_SocialRegistration()
                        self.regiPara.socialToken = userJSON["social_token"] as! String
                        
                        self.isGoingToRegisterGoogle = true
                        self.performSegue(withIdentifier: "segueLogInToRegistration", sender: nil)
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
                }
        })
    }
    
    
    func callTwitterLoginAPI(authToken : String, authSecret : String)
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Logging In")
        loader.startAnimating()
        
        let deviceToekn = GlobalConstants().getFCMToken()
        let headerObject = ["accesstoken" : authToken,
                            "accesstokensecret" : authSecret,
                            "devicetoken": deviceToekn,
                            "devicetype" : "1"]
        
        GlobalConstants().postDataOnlyHeader_returnWholeDict(subUrl: APP_URL.SOCIAL_LOGIN_TWITTER, header: headerObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                print(responseData)

                if isSuccess
                {
                    let userJSON = responseData["user"] as! [String : Any]
                    
                    let token = userJSON["token"] as! String
                    UserDefaults.standard.set(token, forKey: "userToken")
                    
                    UserDefaults.standard.set(userJSON, forKey: "userJSON")
                    
                    UserDefaults.standard.synchronize()
                    
                    self.performSegue(withIdentifier: "segueLoginToLandingPage", sender: self)
                }
                else
                {
                    let response = responseData["status"] as! Int
                    if response == 404
                    {
                        // User not registered
                        let userJSON = responseData["user"] as! [String : Any]
                        
                        self.userObjToPass = Para_UserProfile()
                        
                        self.userObjToPass.userEmail = userJSON["email"] as! String
                        self.userObjToPass.userFirstName = userJSON["fname"] as! String
                        self.userObjToPass.userPictureBase64 = userJSON["image_base_64"] as! String
                        
                        self.regiPara = Para_SocialRegistration()
                        self.regiPara.access_token = userJSON["access_token"] as! String
                        self.regiPara.access_token_secret = userJSON["access_token_secret"] as! String
                        self.regiPara.twitterId = userJSON["twitter_id"] as! Int
                        
                        self.isGoingToRegisterTwitter = true
                        self.performSegue(withIdentifier: "segueLogInToRegistration", sender: nil)
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
                }
        })
    }
    
    func setGradientBackground()
    {
        let colorTop = sharedObj.hexStringToUIColor(hex: "64B2D8").cgColor //UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        let colorBottom = sharedObj.hexStringToUIColor(hex: "6988A6").cgColor //UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        
//        self.view.layer.addSublayer(gradientLayer)
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    // MARK: - Action Methods
    
    @IBAction func didTapSignUp(_ sender: Any)
    {
        /*
        isGoingToRegisterFb = false
        isGoingToRegisterGoogle = false
        
        performSegue(withIdentifier: "segueLogInToRegistration", sender: nil)*/
        
        let presentVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountOptionsViewController") as! CreateAccountOptionsViewController
        
        // Will allow to go back
        self.navigationController?.pushViewController(presentVC, animated: true)
    }
    
    @IBAction func didTapLoginFacebook(_ sender: Any)
    {
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile, .email , .userFriends], viewController: self)
        { loginResult in
            switch loginResult
            {
            case .failed(let error):
                print(error.localizedDescription)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                // print("Granted permissions -", grantedPermissions)
                
                // print("Declined permissions -", declinedPermissions)
                print("Access token - ", accessToken)
                
                if AccessToken.current != nil
                {
                    print("Got access token")
                    self.callFacebookLoginAPI(fbToken: "\(accessToken.authenticationToken)")
                }
            }
        }
    }
    
    @IBAction func didTapGoogleLogIn(_ sender: Any)
    {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func didTapLogInTwitter(_ sender: Any)
    {
        Twitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil)
            {
                print("signed in as ", (session?.authToken)!, (session?.authTokenSecret)!);
                
                self.callTwitterLoginAPI(authToken: (session?.authToken)!, authSecret: (session?.authTokenSecret)!)
                
                /*
                 print("Requesting email")
                let client = TWTRAPIClient.withCurrentUser()
                
                client.requestEmail
                    { email, error in
                    if (email != nil)
                    {
                        print("signed in as ", (session?.userName)!);
                    }
                    else
                    {
                        print("error: ", error?.localizedDescription ?? "");
                    }
                }*/
                
            }
            else
            {
                print("error: ", error?.localizedDescription ?? "");
            }
        })
    }
    
    @IBAction func didTapWeChatLogin(_ sender: Any) {
    }
    
    @IBAction func didTapLoginWithAppCredentials(_ sender: Any)
    {
        performSegue(withIdentifier: "segueLoginToLoginWIthAppCredentials", sender: nil)
    }
    
    // MARK: - Google sign in delegates

    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!)
    {
        // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!)
    {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if (error == nil)
        {
            // Perform any operations on signed in user here.
            let idToken = user.authentication.idToken // Safe to send to the server
            
            callGoogleLoginAPI(glToken: idToken!)
        }
        else
        {
            print("\(error.localizedDescription)")
        }
    }

    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!)
    {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
   
    // MARK: - TextView Delegates
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        if (URL.absoluteString == termsAndConditionsURL)
        {
            self.showDocument()//(title: "Go to Terms", msg: "")
        }
        else if (URL.absoluteString == paymentsURL)
        {
            self.showDocument() //(title: "Go to Payment", msg: "")
        }
        else if (URL.absoluteString == privacyURL)
        {
            self.showDocument()//(title: "Go to Privacy", msg: "")
        }
        else if (URL.absoluteString == nonDiscriminationURL)
        {
            self.showDocument()//(title: "Go to Nondiscrimination", msg: "")
        }
        
         return false
    }

    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    // MARK: - Custom Methods
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in

        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDocument()
    {
        if let url = Bundle.main.url(forResource: "GAYANDYOU_terms_of_use_August_2017", withExtension: "pdf", subdirectory: nil, localization: nil)
        {
            let documentInteractionController = UIDocumentInteractionController(url: url)
            documentInteractionController.delegate = self
            documentInteractionController.presentPreview(animated: false)
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueLogInToRegistration"
        {
            let registerVC : Registration = segue.destination as! Registration
            registerVC.userPara = userObjToPass
            registerVC.isRegisterFb = isGoingToRegisterFb
            registerVC.isRegisterGoogle = isGoingToRegisterGoogle
            registerVC.isRegisterTwitter = isGoingToRegisterTwitter
            
            registerVC.paraSocial = regiPara
            
            if registerVC.userPara.userPictureBase64 != ""
            {
                if let decodedData = Data(base64Encoded: registerVC.userPara.userPictureBase64, options: .ignoreUnknownCharacters) {
                    registerVC.userPara.userPictureImage = UIImage(data: decodedData)!
                }
            }
        }
    }
}
