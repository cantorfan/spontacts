//
//  ShowFeedbackViewController.swift
//  Spontacts
//
//  Created by maximess142 on 16/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import AlamofireImage

class ShowFeedbackViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblLikeCount: UILabel!
    
    @IBOutlet weak var mySwitch: UISwitch!
    
    let ARR_FEEDBACK_CAT = ["(None)", "Activity Create", "Notifications", "Representation in the app", "Recommendations/Invitations", "Filter function and Sorting", "Friends", "Criticism of GAY AND YOU?", "Confirmer Manage", "News", "Profile", "Others", "Past Activities"]
    var feedbackDict = [String : Any]()
    var commentsArray = [[String : Any]]()
    var likesCount = Int()
    
    // MARK: - View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        
        likesCount = feedbackDict["suscribers"] as! Int
        lblLikeCount.text = "\(likesCount) person ❤️ this"
        
        if feedbackDict["suscribed"] as! Int == 1
        {
            mySwitch.setOn(true, animated: false)
        }
        else
        {
            mySwitch.setOn(false, animated: false)
        }
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.retrieveCommentsArray()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    @IBAction func btnAddCommentAction(_ sender: Any)
    {
        let addCommentVc : AddFeedbackCommentViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddFeedbackCommentViewController") as! AddFeedbackCommentViewController
        
        addCommentVc.feedbackId = feedbackDict["id"] as! Int
        self.navigationController?.pushViewController(addCommentVc, animated: true)
    }
    
    @IBAction func btnSwitchChanged(_ sender: Any)
    {
        mySwitch.isSelected = !mySwitch.isSelected
        
        print(mySwitch.isSelected)
        if mySwitch.isSelected == true
        {
            self.callSusbcribeFeedbackAPI()
        }
        else
        {
            self.callUnsusbcribeFeedbackAPI()
        }
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2
        {
            return commentsArray.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0
        {
            return UITableViewAutomaticDimension
        }
        else if indexPath.section == 1
        {
            return 45
        }
        else
        {
            return UITableViewAutomaticDimension
        }
        
        //return 200
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackDetails", for: indexPath) as! ShowFeedbakTableViewCell
            
            cell.lblCategory.text = self.ARR_FEEDBACK_CAT[feedbackDict["category"] as! Int]
            cell.lblTitle.text = feedbackDict["idea"] as? String
            cell.lblDescription.text = feedbackDict["idea_description"] as? String
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddButton", for: indexPath) as! ShowFeedbakTableViewCell
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackComment", for: indexPath) as! ShowFeedbakTableViewCell
            
            let JSON = commentsArray[indexPath.row] 
            
            cell.lblUserName.text = JSON["fname"] as? String
            cell.lblCommentText.text = JSON["comment"] as? String
            
            let pictureURL : String = JSON["thumb"] as! String
            if pictureURL != ""
            {
                let url = URL(string: APP_URL.BASE_URL + pictureURL )!
                let placeholderImage = #imageLiteral(resourceName: "avatar")
                let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 60.0, height: 60.0))
                
                //,filter: filter
                cell.imgUserPhoto.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: filter, imageTransition: .crossDissolve(0.2), completion:
                    { response in
                }
                )
            }
            
            return cell
        }
    }
    
    
    
    // MARK: - WEB API
    func retrieveCommentsArray()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait...")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["feedback_id" : feedbackDict["id"] as! Int]
        
        GlobalConstants().postData_Header_Parameter(subUrl: SETTINGS_URL.GET_MY_COMMENTS_ARRAY, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    if responseData["feedbacks"] != nil
                    {
                        print(responseData["feedbacks"] as Any)
                        self.commentsArray = responseData["feedbacks"] as! [[String : Any]]
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }

    func callSusbcribeFeedbackAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait...")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["feedback_id" : feedbackDict["id"] as! Int]
        
        GlobalConstants().postData_Header_Parameter(subUrl: SETTINGS_URL.SUBSCRIBE_FEEDBACK, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    self.likesCount = self.likesCount + 1
                    
                    self.lblLikeCount.text = "\(self.likesCount) person ❤️ this"
                    self.showAlert(title: "Success!", msg: responseMessage)
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func callUnsusbcribeFeedbackAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Please Wait...")
        loader.startAnimating()
        
        let headerObject = ["authtoken" : Singleton.shared.userPara.userToken]
        
        let requestObject = ["feedback_id" : feedbackDict["id"] as! Int]
        
        GlobalConstants().postData_Header_Parameter(subUrl: SETTINGS_URL.UNSUBSCRIBE_FEEDBACK, header: headerObject, parameter: requestObject, completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                
                if isSuccess
                {
                    self.likesCount = self.likesCount - 1
                    
                    self.lblLikeCount.text = "\(self.likesCount) person ❤️ this"
                    self.showAlert(title: "Success!", msg: responseMessage)
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
