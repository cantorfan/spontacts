//
//  CreateAct_MandatoryInfo.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 03/06/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit
import GooglePlaces

class CreateAct_MandatoryInfo: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, GMSAutocompleteViewControllerDelegate
{
    @IBOutlet weak var tableView: UITableView!
    
    let sharedObj = Singleton.shared
    let arrRowText = [["Name" : "All attendees", "id" : 1],
                      ["Name" : "Private (Invitation needed)", "id" : 0],
                      ["Name" : "Women Only", "id" : 2],
                      ["Name" : "Men Only", "id" : 3],
                      ["Name" : "Transgender Only", "id" : 4]]
   
    var isShowVisibleTo = Bool()
    var isShowDatePicker = Bool()
    var isTimeFlexible = Bool()
    var defaultDateFormatter = DateFormatter()
    var strDate = String()
    
    var activityPara = Para_Activity()
    var isEditMode = Bool()
    var selectedVisibleToIndex = 0 // All by default
    
    // MARK: - View Delegates
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.separatorStyle = .none
        title = "Create Activity"
        
        addLeftBarButton()
        addRightBarButton()
        defaultDateFormatter.dateFormat = "dd MMM YYYY hh:mm a"
        addLocalNotiObserver()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.setNavigationBarHidden(false, animated: false)
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    func addLeftBarButton()
    {
        let backBtn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.didTapBackButn))
        navigationItem.leftBarButtonItem = backBtn
    }
    
    func addRightBarButton()
    {
        if isEditMode == false
        {
            let createButn = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.didtTapCreateButn))
            navigationItem.rightBarButtonItem = createButn
        }
        else
        {
            let updateButn = UIBarButtonItem.init(title: "Update", style: .plain, target: self, action: #selector(self.didTapUpdateButn))
            navigationItem.rightBarButtonItem = updateButn
        }
    }
    
    func showDatePicker()
    {
        isShowDatePicker = !isShowDatePicker
        tableView.reloadData()
    }
    
    func showAlert(title : String, msg : String)
    {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in

        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(msg : String)
    {
        let alert = UIAlertController.init(title: "Enter all mandatory fields", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {
            alert -> Void in

        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkMandatoryFields() -> Bool
    {
        if activityPara.actName == ""
        {
            showAlert(msg: "Name cannot be left blank!")
            return false
        }
        else if activityPara.actDesc == ""
        {
            showAlert(msg: "Please describe your activity!")
            return false
        }
        else if activityPara.actCategoryStr == ""
        {
            showAlert(msg: "Please select a category!")
            return false
        }
        else if activityPara.actLocatnStr == ""
        {
            showAlert(msg: "Please choose a location!")
            return false
        }
        else if strDate == ""
        {
            showAlert(msg: "Please select a date!")
            return false
        }
        
        return true
    }
    
    func addLocalNotiObserver()
    {
        let notificationName = Notification.Name("categoryForActivityChosen")

        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationCategoryChosedn(noti:)), name: notificationName, object: nil)
    }
    
    func notificationCategoryChosedn(noti : Notification)
    {
        let receivedObj : [String : Any] = noti.object as! [String : Any]
        
        activityPara.actCategoryId = receivedObj["catId"] as! Int
        activityPara.actCategoryStr = receivedObj["catStr"] as! String
        
        tableView.reloadData()
    }
    
    func checkIfAnyDataIsEntered() -> Bool
    {
        if activityPara.actName != "" || activityPara.actDesc != "" || activityPara.actLocatnStr != "" || activityPara.actCategoryStr != ""
        {
            return true
        }
        
        return false
    }
    
    func showPlusPopup(msg : String)
    {
        let alert = UIAlertController.init(title: "GAY AND YOU?", message:msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "More", style: .default, handler:
            { alert -> Void in

                let upgradeVC : UpgradeViewController = storyboards.rareStoryboard.instantiateViewController(withIdentifier: "UpgradeViewController") as! UpgradeViewController
                
                self.navigationController?.pushViewController(upgradeVC, animated: true)
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
            alert -> Void in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Action Methdds
    @IBAction func didTapBackButn()
    {
        self.view.endEditing(true)
        if checkIfAnyDataIsEntered()
        {
            let alert = UIAlertController.init(title: "Are you sure you want to lose your entered data?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Discard", style: .default, handler: {
                alert -> Void in
                self.navigationController?.popViewController(animated: true)
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {
                alert -> Void in
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)

        }
    }
    
    @IBAction func didtTapCreateButn()
    {
        self.view.endEditing(true)
        
        if checkMandatoryFields()
        {
            print("We can register this activity!")
            
            activityPara.actVisibleTo = arrRowText[selectedVisibleToIndex]["id"] as! Int
            
            let loader =  startActivityIndicator(view: self.view, waitingMessage: "Creating Activity")
            loader.startAnimating()
            
            let requestObject : [String : Any] = [
                "name" : activityPara.actName,
                "description" : activityPara.actDesc,
                "subcategory_id" : 0,
                "category_id" : activityPara.actCategoryId,
                "lat" : Double(activityPara.actLocatnLatitude),
                "lng" : Double(activityPara.actLocatnLongitude),
                "time" : activityPara.actDate.timeIntervalSince1970,
                "time_flexible" : (activityPara.actIsFlexible ? 1 : 0),
                "visibility" : activityPara.actVisibleTo,
                "post_on_facebook" : (activityPara.actIsPostToFacebook ? 1 : 0),
                "location_string" : activityPara.actLocatnStr,
                "max_atteendies":activityPara.actMaxAttendees
            ]
            
            print(requestObject)
            
            let headerObject = ["authtoken" : sharedObj.userPara.userToken]
            
            GlobalConstants().postDataFor(subUrl: ACTIVITY_URL.CREATE_ACT, header: headerObject, parameter: requestObject, resultKeyName: "activity", completionHandler:
                {
                    (isSuccess,responseMessage,responseData) -> Void in
                    
                    loader.stopAnimating()
                    print(responseData)
                    
                    if isSuccess
                    {
                        print(responseData)
                        
                        self.activityPara.actId = responseData["id"] as! Int
                        if self.activityPara.actCategoryId == 8 && self.sharedObj.catImgBase64 != ""
                        {
                            self.callAddCategoryPictureAPI()
                        }
                        else
                        {
                            self.performSegue(withIdentifier: "segueCreateActivityToShowActivity", sender: nil)
                        }
                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
            })
        }
    }
    
    @IBAction func didTapUpdateButn()
    {
        self.view.endEditing(true)
        
        if checkMandatoryFields()
        {
            print("Updating this activity!")
            
            activityPara.actVisibleTo = arrRowText[selectedVisibleToIndex]["id"] as! Int

            let loader =  startActivityIndicator(view: self.view, waitingMessage: "Updating Activity")
            loader.startAnimating()
            
            let requestObject : [String : Any] = [
                "activity_id" : activityPara.actId,
                "name" : activityPara.actName,
                "description" : activityPara.actDesc,
                "subcategory_id" : 0,
                "category_id" : activityPara.actCategoryId,
                "lat" : Double(activityPara.actLocatnLatitude),
                "lng" : Double(activityPara.actLocatnLongitude),
                "time" : activityPara.actDate.timeIntervalSince1970,
                "time_flexible" : (activityPara.actIsFlexible ? 1 : 0),
                "visibility" : activityPara.actVisibleTo,
                "post_on_facebook" : 0,
                "location_string" : activityPara.actLocatnStr,
                "max_atteendies":activityPara.actMaxAttendees
            ]
            
            print(requestObject)
            
            let headerObject = ["authtoken" : sharedObj.userPara.userToken]
            
            GlobalConstants().postData_Header_Parameter(subUrl: ACTIVITY_URL.UPDATE_ACTIVITY, header: headerObject, parameter: requestObject, completionHandler:
                {
                    (isSuccess,responseMessage,responseData) -> Void in
                    
                    loader.stopAnimating()
                    print(responseData)
                    
                    if isSuccess
                    {
                        let JSON = responseData["activity"] as! [String : Any]
                        self.activityPara.actId = JSON["id"] as! Int
                        
                        if self.activityPara.actCategoryId == 8 && self.sharedObj.catImgBase64 != ""
                        {
                            self.callUpdateCategoryPictureAPI()
                        }
                        else
                        {
                            self.activityPara.actLocatnLongitude = JSON["lng"] as! Double
                            self.activityPara.actIsFlexible = (JSON["time_flexible"] != nil)
                            let timeInterval = JSON["time"] as? Int
                            self.activityPara.actDate = Date.init(timeIntervalSince1970: Double(timeInterval!))
                            self.activityPara.actId = JSON["id"] as! Int
                            self.activityPara.actVisibleTo = JSON["visibility"] as! Int
                            self.activityPara.actDesc = JSON["description"] as! String
                            self.activityPara.actLocatnLatitude = JSON["lat"] as! Double
                            self.activityPara.actName = JSON["name"] as! String
                            self.activityPara.actCreatedEpoch = JSON["created_time"] as! Double
                            self.activityPara.actCategoryId = JSON["category_id"] as! Int
                            self.activityPara.actLocatnStr = JSON["location_string"] as! String
                            self.activityPara.actImgJSONArr = JSON["images"] as! NSArray
                            
                            let notificationName = Notification.Name("activityUpdated")
                            
                            let obj = ["activity" : self.activityPara] as [String : Any]
                            // Post notification
                            NotificationCenter.default.post(name: notificationName, object: obj)
                            
                            self.navigationController?.popViewController(animated: true)
                        }

                    }
                    else
                    {
                        self.showAlert(title: "Fail!", msg: responseMessage)
                    }
            })
        }
    }
    
    @IBAction func didTapCategoryButn()
    {
        self.performSegue(withIdentifier: "segueCreateActivityToCategoriesList", sender: nil)
    }
    
    @IBAction func didChangeDate(sender : UIDatePicker)
    {
        activityPara.actDate = sender.date
        
        strDate = defaultDateFormatter.string(from: sender.date)
        tableView.reloadData()
    }
    
    @IBAction func didChangeTimeFlexibleSwitch(sender : UISwitch)
    {
        isTimeFlexible = sender.isOn
        activityPara.actIsFlexible = sender.isOn
        
        if sender.isOn
        {
            defaultDateFormatter.dateFormat = "dd MMM YYYY"
        }
        else
        {
            defaultDateFormatter.dateFormat = "dd MMM YYYY hh:mm a"
        }
        
        strDate = defaultDateFormatter.string(from: activityPara.actDate)

        tableView.reloadData()
    }
    
    @IBAction func didTapAttendees()
    {
        if sharedObj.userPara.isPlusMember == 1
        {
            getAttendeesCountAlert()
        }
        else
        {
            showPlusPopup(msg:  "Limit the maximal number of attendees. Discover GAY AND YOU? Plus.")
        }
    }
    
    func getAttendeesCountAlert()
    {
        let alertController = UIAlertController.init(title: nil, message: "Enter number of Attendees", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: configurationTextField)
        
        
        let confirmAction = UIAlertAction(title: "Done", style: .default) { (_) in
            if let field = alertController.textFields?[0]
            {
                print(field.text!)
                self.activityPara.actMaxAttendees = Int(field.text!)!
                self.tableView.reloadData()
            }
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .destructive, handler: {
            alert -> Void in
            
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }

    func configurationTextField(textField: UITextField!)
    {
        textField.placeholder = "Enter number"
        textField.keyboardType = .numberPad
    }
    
    @IBAction func didTapVisibleTo()
    {
        isShowVisibleTo = !isShowVisibleTo
        tableView.reloadData()
    }
    
    @IBAction func didChangeFBSwitch(sender : UISwitch)
    {
        activityPara.actIsPostToFacebook = sender.isOn
    }
    
    func callAddCategoryPictureAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Uploading Category Image")
        loader.startAnimating()
        
        let requestObject : [String : Any] = [
            "activity_id" : activityPara.actId,
            "image_base_64" : sharedObj.catImgBase64]
        
        print(requestObject)
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataFor(subUrl: ACTIVITY_URL.ADD_CATEGORY_IMG, header: headerObject, parameter: requestObject, resultKeyName: "activity", completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                print(responseData)
                
                if isSuccess
                {
                    print(responseData)
                    
                    self.activityPara.actCategoryImgURL = responseData["category_picture"] as! String
                    self.performSegue(withIdentifier: "segueCreateActivityToShowActivity", sender: nil)
                }
                else
                {
                    self.performSegue(withIdentifier: "segueCreateActivityToShowActivity", sender: nil)
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
        })
    }
    
    func callUpdateCategoryPictureAPI()
    {
        let loader =  startActivityIndicator(view: self.view, waitingMessage: "Uploading Category Image")
        loader.startAnimating()
        
        let requestObject : [String : Any] = [
            "activity_id" : activityPara.actId,
            "image_base_64" : sharedObj.catImgBase64]
        
        print(requestObject)
        
        let headerObject = ["authtoken" : sharedObj.userPara.userToken]
        
        GlobalConstants().postDataFor(subUrl: ACTIVITY_URL.ADD_CATEGORY_IMG, header: headerObject, parameter: requestObject, resultKeyName: "activity", completionHandler:
            {
                (isSuccess,responseMessage,responseData) -> Void in
                
                loader.stopAnimating()
                print(responseData)
                
                if isSuccess
                {
                    print(responseData)
                    
                    self.activityPara.actCategoryImgURL = responseData["category_picture"] as! String
                }
                else
                {
                    self.showAlert(title: "Fail!", msg: responseMessage)
                }
                
                let JSON = responseData["activity"] as! [String : Any]
                
                self.activityPara.actLocatnLongitude = JSON["lng"] as! Double
                self.activityPara.actIsFlexible = (JSON["time_flexible"] != nil)
                let timeInterval = JSON["time"] as? Int
                self.activityPara.actDate = Date.init(timeIntervalSince1970: Double(timeInterval!))
                self.activityPara.actId = JSON["id"] as! Int
                self.activityPara.actVisibleTo = JSON["visibility"] as! Int
                self.activityPara.actDesc = JSON["description"] as! String
                self.activityPara.actLocatnLatitude = JSON["lat"] as! Double
                self.activityPara.actName = JSON["name"] as! String
                self.activityPara.actCreatedEpoch = JSON["created_time"] as! Double
                self.activityPara.actCategoryId = JSON["category_id"] as! Int
                self.activityPara.actLocatnStr = JSON["location_string"] as! String
                self.activityPara.actImgJSONArr = JSON["images"] as! NSArray
                
                let notificationName = Notification.Name("activityUpdated")
                
                let obj = ["activity" : self.activityPara] as [String : Any]
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: obj)
                
                self.navigationController?.popViewController(animated: true)
        })
    }
    
    // MARK:- UI Methods
    func firstHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 130))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let containerView = UIView.init(frame: CGRect(x: 0, y: 5, width: view.frame.size.width, height: 125))
        containerView.backgroundColor = UIColor.white
        
        let tfName = UITextField(frame: CGRect(x: 10, y: 5, width: view.frame.size.width - 20, height: 39))
        tfName.textAlignment = .left
        tfName.font = UIFont.init(name: Font.BEBAS, size: 15)
        tfName.placeholder = "Name of Activity"
        tfName.tag = 100
        tfName.delegate = self
        tfName.backgroundColor = UIColor.white
        tfName.text = activityPara.actName
        containerView.addSubview(tfName)
        
        let sep1 = UIView.init(frame: CGRect(x: 0, y: 5 + 39, width: view.frame.size.width, height: 1))
        sep1.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        containerView.addSubview(sep1)
        
        let tfDesc = UITextView(frame: CGRect(x: 10, y: sep1.frame.origin.y + sep1.frame.size.height, width: view.frame.size.width - 20, height: 59))
        tfDesc.textAlignment = .left
        tfDesc.font = UIFont.init(name: Font.BEBAS, size: 15)
        //tfDesc.placeholder = "Activity Description"
        tfDesc.tag = 200
        tfDesc.delegate = self
        tfDesc.backgroundColor = UIColor.white
        tfDesc.text = activityPara.actDesc
        containerView.addSubview(tfDesc)
        
        let sep2 = UIView.init(frame: CGRect(x: 0, y: tfDesc.frame.origin.y + tfDesc.frame.size.height + 1, width: view.frame.size.width, height: 1))
        sep2.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        containerView.addSubview(sep2)
        
        let catButn = UIButton.init(frame: CGRect(x: 0, y: sep2.frame.origin.y + sep2.frame.size.height, width: view.frame.size.width, height: 40))
        catButn.backgroundColor = UIColor.white
        catButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        catButn.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        catButn.addTarget(self, action: #selector(self.didTapCategoryButn), for: .touchUpInside)
        
        let lblLeft = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width / 3, height: 40))
        lblLeft.textAlignment = .left
        lblLeft.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblLeft.text = "Category"
        catButn.addSubview(lblLeft)
        
        let lblRight = UILabel(frame: CGRect(x: view.frame.size.width / 3 + 10, y: 0, width: view.frame.size.width / 3 * 2 - 50
            , height: 40))
        lblRight.textAlignment = .right
        lblRight.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblRight.text = activityPara.actCategoryStr
        lblRight.textColor = sharedObj.hexStringToUIColor(hex: "2196F3")
        catButn.addSubview(lblRight)
        
        let iconArrow = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: 10, width: 20, height: 20))
        iconArrow.image = #imageLiteral(resourceName: "arrow_right")
        catButn.addSubview(iconArrow)
        
        containerView.addSubview(catButn)
        headerView.addSubview(containerView)
        return headerView
    }

    func secondHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 86))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let containerView = UIView.init(frame: CGRect(x: 0, y: 5, width: view.frame.size.width, height: 82))
        containerView.backgroundColor = UIColor.white
        
        let tfWhere = UITextField(frame: CGRect(x: 10, y: 0, width: view.frame.size.width - 50, height: 40))
        tfWhere.textAlignment = .left
        tfWhere.font = UIFont.init(name: Font.BEBAS, size: 15)
        tfWhere.placeholder = "Where"
        tfWhere.tag = 300
        tfWhere.delegate = self
        tfWhere.text = activityPara.actLocatnStr
        containerView.addSubview(tfWhere)
        
        let iconLocation = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: 10, width: 20, height: 20))
        iconLocation.image = #imageLiteral(resourceName: "location_20")
        containerView.addSubview(iconLocation)
        
        let sep1 = UIView.init(frame: CGRect(x: 0, y: 40, width: view.frame.size.width, height: 1))
        sep1.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        containerView.addSubview(sep1)
        
        let btnWhen = UIButton.init(frame: CGRect(x: 10, y: sep1.frame.origin.y + sep1.frame.size.height, width: view.frame.size.width - 50, height: 40))
        btnWhen.backgroundColor = UIColor.white
        btnWhen.titleLabel?.font = UIFont.init(name: Font.BEBAS, size: 15)

        let btnTitle = strDate == "" ? "When" : strDate
        btnWhen.setTitle(btnTitle, for: .normal)
        btnWhen.setTitleColor(UIColor.black, for: .normal)
        btnWhen.addTarget(self, action: #selector(showDatePicker), for: .touchUpInside)
        btnWhen.contentHorizontalAlignment = .left
        containerView.addSubview(btnWhen)

        let iconTime = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: btnWhen.frame.origin.y + 10, width: 20, height: 20))
        iconTime.image = #imageLiteral(resourceName: "clock_20")
        containerView.addSubview(iconTime)

        headerView.addSubview(containerView)
        return headerView
    }
    
    func thirdHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 45))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let containerView = UIView.init(frame: CGRect(x: 0, y: 5, width: view.frame.size.width, height: 40))
        containerView.backgroundColor = UIColor.white
        
        let btnAttendees = UIButton.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        btnAttendees.backgroundColor = UIColor.white
        btnAttendees.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnAttendees.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnAttendees.addTarget(self, action: #selector(didTapAttendees), for: .touchUpInside)
        
        let lblLeft = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width / 3 * 2, height: 40))
        lblLeft.textAlignment = .left
        lblLeft.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblLeft.text = "Maximal number of attendees"
        btnAttendees.addSubview(lblLeft)
        
        let lblRight = UILabel(frame: CGRect(x: view.frame.size.width / 3 * 2 - 50, y: 0, width: view.frame.size.width / 3, height: 40))
        lblRight.textAlignment = .right
        lblRight.font = UIFont.init(name: Font.BEBAS, size: 15)
        if sharedObj.userPara.isPlusMember == 1 && activityPara.actMaxAttendees != 0
        {
            lblRight.text = "\(activityPara.actMaxAttendees)"
        }
        btnAttendees.addSubview(lblRight)
        
        let iconTime = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: 10, width: 20, height: 20))
        iconTime.image = #imageLiteral(resourceName: "limit_attendees")
        btnAttendees.addSubview(iconTime)
        containerView.addSubview(btnAttendees)

        headerView.addSubview(containerView)
        return headerView
    }
    
    func fourthHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 45))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let containerView = UIView.init(frame: CGRect(x: 0, y: 5, width: view.frame.size.width, height: 40))
        containerView.backgroundColor = UIColor.white
        
        let btnVisibleTo = UIButton.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        btnVisibleTo.backgroundColor = UIColor.white
        btnVisibleTo.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .selected)
        btnVisibleTo.setBackgroundImage(UIImage().imageWithColor(color: (UIColor.gray).withAlphaComponent(0.20)), for: .highlighted)
        btnVisibleTo.addTarget(self, action: #selector(didTapVisibleTo), for: .touchUpInside)
        
        let lblLeft = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width / 3 * 2, height: 40))
        lblLeft.textAlignment = .left
        lblLeft.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblLeft.text = "Visible to"
        btnVisibleTo.addSubview(lblLeft)
        
        let lblRight = UILabel(frame: CGRect(x: view.frame.size.width / 3 * 2 - 50, y: 0, width: view.frame.size.width / 3, height: 40))
        lblRight.textAlignment = .right
        lblRight.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblRight.textColor = sharedObj.hexStringToUIColor(hex: "2196f3")
        //let arrVisibleTo = ["All", "Private"]
        lblRight.text = arrRowText[selectedVisibleToIndex]["Name"] as? String
        btnVisibleTo.addSubview(lblRight)
        
        let icon = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: 10, width: 20, height: 20))
        icon.image = #imageLiteral(resourceName: "down")
        btnVisibleTo.addSubview(icon)
        containerView.addSubview(btnVisibleTo)
        
        headerView.addSubview(containerView)
        return headerView
    }
    
    func fifthHeaderView( headerView : UIView) -> UIView
    {
        var headerView = headerView
        headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 136))
        headerView.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        
        let containerView = UIView.init(frame: CGRect(x: 0, y: 5, width: view.frame.size.width, height: 82))
        containerView.backgroundColor = UIColor.white
        
        let lblLeft = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width / 3 * 2, height: 40))
        lblLeft.textAlignment = .left
        lblLeft.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblLeft.text = "Invite Friends"
        containerView.addSubview(lblLeft)
        
        let icon = UIImageView.init(frame: CGRect(x: view.frame.size.width - 35, y: 10, width: 20, height: 20))
        icon.image = #imageLiteral(resourceName: "share")
        containerView.addSubview(icon)
        
        let sep1 = UIView.init(frame: CGRect(x: 0, y: 40, width: view.frame.size.width, height: 1))
        sep1.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        containerView.addSubview(sep1)
        
        let imgView = UIImageView.init(frame: CGRect(x: 10, y: sep1.frame.origin.y + sep1.frame.size.height + 10, width: 20, height: 20))
        imgView.image = #imageLiteral(resourceName: "facebook_friends")
        containerView.addSubview(imgView)
        
        let lblLeft2 = UILabel(frame: CGRect(x: imgView.frame.origin.x + imgView.frame.size.width + 5, y: sep1.frame.origin.y + sep1.frame.size.height, width: view.frame.size.width / 2, height: 40))
        lblLeft2.textAlignment = .left
        lblLeft2.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblLeft2.text = "Post on Facebook"
        containerView.addSubview(lblLeft2)
        
        let fbSwitch = UISwitch.init(frame: CGRect(x: view.frame.size.width - 60, y: sep1.frame.origin.y + sep1.frame.size.height + 5, width: 50, height: 40))
        fbSwitch.addTarget(self, action: #selector(didChangeFBSwitch(sender:)), for: .valueChanged)
        fbSwitch.setOn(activityPara.actIsPostToFacebook, animated: false)
        containerView.addSubview(fbSwitch)
        
        let lblTip = UILabel(frame: CGRect(x: 10, y: containerView.frame.origin.y + containerView.frame.size.height, width: view.frame.size.width - 20, height: 50))
        lblTip.textAlignment = .left
        lblTip.font = UIFont.init(name: Font.BEBAS, size: 15)
        lblTip.numberOfLines = 0
        lblTip.lineBreakMode = .byWordWrapping
        lblTip.backgroundColor = sharedObj.hexStringToUIColor(hex: "eeeeee")
        lblTip.text = "Share your activity on Facebook and motivate your friends to join in."
        headerView.addSubview(lblTip)
        
        headerView.addSubview(containerView)
        return headerView
    }
    
    // MARK: - TableView Datasource and Delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            if isShowDatePicker
            {
                return 1
            }
        }
        if section == 3
        {
            if  isShowVisibleTo
            {
                return arrRowText.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if  section == 0
        {
            return 150
        }
        else if section == 1
        {
            return 86
        }
        else if section == 2
        {
            return 45
        }
        else if section == 3
        {
            return 45
        }
        else if section == 4
        {
            return 136
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        // Datepicker view
        if indexPath.section == 1
        {
            return 200
        }
        
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()//.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
        headerView.backgroundColor = UIColor.clear
        
        if section == 0
        {
            headerView.addSubview(firstHeaderView(headerView: headerView))
        }
        else if section == 1
        {
            headerView.addSubview(secondHeaderView(headerView: headerView))
        }
        else if section == 2
        {
            headerView.addSubview(thirdHeaderView(headerView: headerView))
        }
        else if section == 3
        {
            headerView.addSubview(fourthHeaderView(headerView: headerView))
        }
        else if section == 4
        {
            headerView.addSubview(fifthHeaderView(headerView: headerView))
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        
        var titleLabel = UILabel()
        
        var flexLbl = UILabel()
        var flexSwitch = UISwitch()
        var picker = UIDatePicker()
        var sepView = UIView()
        
        if (cell == nil)
        {
            cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
            cell?.selectionStyle = UITableViewCellSelectionStyle.gray
            
            titleLabel = UILabel(frame: CGRect(x: 30, y: 0, width: view.frame.size.width - 80, height: 35))
            titleLabel.textAlignment = .left
            titleLabel.font = UIFont.init(name: Font.BEBAS, size: 15)
            titleLabel.tag = 1
            
            flexLbl = UILabel.init(frame: CGRect(x: 30, y: 5, width: view.frame.size.width / 2, height: 40))
            flexLbl.textAlignment = .left
            flexLbl.font = UIFont.init(name: Font.BEBAS, size: 15)
            flexLbl.tag = 2
            flexLbl.text = "Time is flexible."
            
            flexSwitch = UISwitch.init(frame: CGRect(x: view.frame.size.width - 70, y: 5, width: 60, height: 40))
            flexSwitch.tag = 3
            flexSwitch.addTarget(self, action: #selector(didChangeTimeFlexibleSwitch(sender:)), for: .valueChanged)
            
            sepView = UIView.init(frame: CGRect(x: 25, y: flexLbl.frame.size.height + flexLbl.frame.origin.y, width: view.frame.size.width - 50, height: 1))
            sepView.backgroundColor = UIColor.gray
            sepView.tag = 4
            
            picker = UIDatePicker.init(frame: CGRect(x: 30, y: flexLbl.frame.size.height + flexLbl.frame.origin.y + 5, width: view.frame.size.width - 60, height: 150))
            picker.addTarget(self, action: #selector(self.didChangeDate), for: .valueChanged)
            picker.tag = 5
            picker.backgroundColor = UIColor.white
            picker.minimumDate = Date()
            
            cell?.contentView.addSubview(titleLabel)
            cell?.contentView.addSubview(flexLbl)
            cell?.contentView.addSubview(flexSwitch)
            cell?.contentView.addSubview(sepView)
            cell?.contentView.addSubview(picker)
        }
        else
        {
            titleLabel = cell?.contentView.viewWithTag(1) as! UILabel
            flexLbl = cell?.contentView.viewWithTag(2) as! UILabel
            flexSwitch = cell?.contentView.viewWithTag(3) as! UISwitch
            sepView = (cell?.contentView.viewWithTag(4))!
            picker = cell?.contentView.viewWithTag(5) as! UIDatePicker
        }
        
        titleLabel.isHidden = true
        flexLbl.isHidden = true
        flexSwitch.isHidden = true
        sepView.isHidden = true
        picker.isHidden = true
        
        if indexPath.section == 1
        {
            flexLbl.isHidden = false
            flexSwitch.isHidden = false
            sepView.isHidden = false
            picker.isHidden = false
            
            if isTimeFlexible
            {
                picker.datePickerMode = UIDatePickerMode.date
            }
            else
            {
                picker.datePickerMode = UIDatePickerMode.dateAndTime
            }
            
            cell?.selectionStyle = .none
        }
        else if indexPath.section == 3
        {
            titleLabel.isHidden = false
            titleLabel.text = arrRowText[indexPath.row]["Name"] as? String
            if selectedVisibleToIndex == indexPath.row
            {
                cell?.accessoryType = .checkmark
            }
            else
            {
                cell?.accessoryType = .none
            }
            
            cell?.selectionStyle = .blue
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 3
        {
            if indexPath.row == 0 || indexPath.row == 1
            {
                selectedVisibleToIndex = indexPath.row
                tableView.reloadData()
            }
            else
            {
                if sharedObj.userPara.isPlusMember == 1
                {
                    selectedVisibleToIndex = indexPath.row
                    tableView.reloadData()
                }
                else
                {
                    showPlusPopup(msg: "Determine who can see your activity. Discover GAY AND YOU? Plus.")
                }
            }
        }
    }
    
    // MARK: - Textfield Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if  textField.tag == 300
        {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField.tag == 100
        {
            activityPara.actName = textField.text!
        }
        else if textField.tag == 200
        {
            activityPara.actDesc = textField.text!
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }

    // MARK: - GMSAutocompleteViewController Delegate
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        activityPara.actLocatnStr = place.name
        activityPara.actLocatnLatitude = place.coordinate.latitude
        activityPara.actLocatnLongitude = place.coordinate.longitude
        
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print(place.coordinate)
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
    {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if  segue.identifier == "segueCreateActivityToShowActivity"
        {
            let showActivity : ShowActivity_VC = segue.destination as! ShowActivity_VC
            showActivity.userPara = sharedObj.userPara
            showActivity.activityPara = activityPara
            showActivity.isFromCreateActivity = true
            let goingToDict = ["id": 0,
                               "user_id": sharedObj.userPara.userId,
                               "fname": sharedObj.userPara.userFirstName,
                               "lname": sharedObj.userPara.userLastName,
                               "thumb": sharedObj.userPara.userPictureThumbUrl] as [String : Any]
            showActivity.activityPara.actGoingArray.append(goingToDict)
        }
    }
}

extension CreateAct_MandatoryInfo : UITextViewDelegate
{
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.tag == 200
        {
            activityPara.actDesc = textView.text!
        }
    }
}
