//
//  ShowActivityTableViewCell.swift
//  Spontacts
//
//  Created by maximess142 on 02/09/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class ShowActivityTableViewCell: UITableViewCell {

    @IBOutlet weak var lblActName: UILabel!
    
    @IBOutlet weak var lblCreatedTime: UILabel!
    
    @IBOutlet weak var lblDesc: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
