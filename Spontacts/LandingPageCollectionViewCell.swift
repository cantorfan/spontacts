
//
//  LandingPageCollectionViewCell.swift
//  Spontacts
//
//  Created by Pratiksha Mohadare on 12/07/17.
//  Copyright © 2017 Pratiksha. All rights reserved.
//

import UIKit

class LandingPageCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgCell: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
}

class LandingPageButtonsCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var lblButton: UILabel!
    
}
